<?php
ob_start();
include './header.php';
include './productphp.php';
?>
<br/>
<div class="col-md-6 col-sm-6 col-xs-12" style="margin: 0 auto;">
    <div class="panel panel-info">
        <div class="panel-heading">
            <?php
            if (isset($cate_data['ads_name']) && $cate_data['ads_name'] != '') {
                echo 'Edit post';
            } else {
                echo 'Add post';
            }
            ?>

        </div>
        <div class="panel-body">
            <form role="form" method="post" enctype="multipart/form-data" action="productphp.php">
                <input type="hidden" name="ads_id" value="<?php echo $_GET['id']; ?>">
                <div class="form-group col-sm-12">
                    <label>Name</label>
                    <input class="form-control" type="text" style="width:100%;margin:0px;" name="ads_name" value="<?php echo (isset($cate_data['ads_name']) && $cate_data['ads_name'] != '') ? $cate_data['ads_name'] : ''; ?>"/>
                </div>
                <div class="form-group col-sm-12 col-xs-12 col-md-12">
                    <label>Text area</label>
                    <textarea class="form-control" name="ads_details" rows="3" ><?php echo (isset($cate_data['ads_details']) && $cate_data['ads_details'] != '') ? $cate_data['ads_details'] : ''; ?></textarea>
                </div>
                <div class="form-group col-sm-3">
                    <label>File input</label>
                    <input type="file" name="ads_img" class="phone form-control" style="opacity: 10;width: 150%;height: 6%;position: unset;">
                </div>
                <div style="clear:both;margin-left: 15px;"><br/>
                    <button type="submit" name="submit" value="<?php echo $abc; ?>" class="btn btn-info" style="width: 10%;">Submit</button></div>
            </form>
        </div>
    </div>
</div>
<br/>
<?php
include_once './footer.php';
?>