<?php
include 'header.php';
?>

<div class="container">
    <div class="row">
        <div class="col-sm-1">
            <div class="thumbnail">
                <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png" style="height: 55px;">
            </div><!-- /thumbnail -->
        </div><!-- /col-sm-1 -->

        <div class="col-sm-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>myusername</strong> 
                </div>
                <div class="panel-body">
                    Panel content
                </div><!-- /panel-body -->
            </div><!-- /panel panel-default -->
        </div><!-- /col-sm-5 -->
    </div><!-- /row -->
</div><!-- /container -->
<?php
include 'footer.php';
?>

