<?php

//database_connection.php

require_once 'connection.php';

function fetch_user_last_activity($user_id, $con) {
    $query = "
	SELECT * FROM login_details 
	WHERE user_id = '$user_id' 
	ORDER BY last_activity DESC 
	LIMIT 1
	";
    $statement = mysqli_query($con, $query);


    $result = mysqli_fetch_all($statement, MYSQLI_ASSOC);
    foreach ($result as $row) {
        return $row['last_activity'];
    }
}

function fetch_user_chat_history($from_user_id, $to_user_id, $con) {
    $query = "
	SELECT * FROM chat_message 
	WHERE (from_user_id = '" . $from_user_id . "' 
	AND to_user_id = '" . $to_user_id . "') 
	OR (from_user_id = '" . $to_user_id . "' 
	AND to_user_id = '" . $from_user_id . "') 
	ORDER BY timestamp DESC
	";
    $statement = mysqli_query($con, $query);


    $result = mysqli_fetch_all($statement, MYSQLI_ASSOC);
    $output = '<ul class="list-unstyled">';
    foreach ($result as $row) {
        $user_name = '';
        $dynamic_background = '';
        $chat_message = '';
        if ($row["from_user_id"] == $from_user_id) {
            if ($row["status"] == '2') {
                $chat_message = '<em>This message has been removed</em>';
                $user_name = '<b class="text-success">You</b>';
            } else {
                $chat_message = $row['chat_message'];
                $user_name = '<button type="button" class="btn btn-danger btn-xs remove_chat" id="' . $row['chat_message_id'] . '">x</button>&nbsp;<b class="text-success">You</b>';
            }


            $dynamic_background = 'background-color:#ffe6e6;';
        } else {
            if ($row["status"] == '2') {
                $chat_message = '<em>This message has been removed</em>';
            } else {
                $chat_message = $row["chat_message"];
            }
            $user_name = '<b class="text-danger">' . get_user_name($row['from_user_id'], $con) . '</b>';
            $dynamic_background = 'background-color:#ffffe6;';
        }
        $output .= '
		<li style="border-bottom:1px dotted #ccc;padding-top:8px; padding-left:8px; padding-right:8px;' . $dynamic_background . '">
			<p>' . $user_name . ' - ' . $chat_message . '
				<div align="right">
					- <small><em>' . $row['timestamp'] . '</em></small>
				</div>
			</p>
		</li>
		';
    }
    $output .= '</ul>';
    $query = "
	UPDATE chat_message 
	SET status = '0' 
	WHERE from_user_id = '" . $to_user_id . "' 
	AND to_user_id = '" . $from_user_id . "' 
	AND status = '1'
	";
    $statement = mysqli_query($con, $query);
    return $output;
}

function get_user_name($user_id, $con) {
    $query = "SELECT username FROM tbl_user WHERE user_id = '$user_id'";
    $statement = mysqli_query($con, $query);


    $result = mysqli_fetch_all($statement, MYSQLI_ASSOC);
    foreach ($result as $row) {
        return $row['username'];
    }
}

function count_unseen_message($from_user_id, $to_user_id, $con) {
    $query = "
	SELECT * FROM chat_message 
	WHERE from_user_id = '$from_user_id' 
	AND to_user_id = '$to_user_id' 
	AND status = '1'
	";
    $statement = mysqli_query($con, $query);
     $output = '';
    if ($statement) {
        $count = mysqli_num_rows($statement);
       
        if ($count > 0) {
            $output = '<span class="label label-success">' . $count . '</span>';
        }
    }
    return $output;
}

function fetch_is_type_status($user_id, $con) {
    $query = "
	SELECT is_type FROM login_details 
	WHERE user_id = '" . $user_id . "' 
	ORDER BY last_activity DESC 
	LIMIT 1
	";
    $statement = mysqli_query($con, $query);


    $result = mysqli_fetch_all($statement, MYSQLI_ASSOC);
    $output = '';
    foreach ($result as $row) {
        if ($row["is_type"] == 'yes') {
            $output = ' - <small><em><span class="text-muted">Typing...</span></em></small>';
        }
    }
    return $output;
}

function fetch_group_chat_history($con) {
    $query = "
	SELECT * FROM chat_message 
	WHERE to_user_id = '0'  
	ORDER BY timestamp DESC
	";

    $statement = mysqli_query($con, $query);


    $result = mysqli_fetch_all($statement, MYSQLI_ASSOC);

    $output = '<ul class="list-unstyled">';
    foreach ($result as $row) {
        $user_name = '';
        $dynamic_background = '';
        $chat_message = '';
        if ($row["from_user_id"] == $_SESSION["user_id"]) {
            if ($row["status"] == '2') {
                $chat_message = '<em>This message has been removed</em>';
                $user_name = '<b class="text-success">You</b>';
            } else {
                $chat_message = $row["chat_message"];
                $user_name = '<button type="button" class="btn btn-danger btn-xs remove_chat" id="' . $row['chat_message_id'] . '">x</button>&nbsp;<b class="text-success">You</b>';
            }

            $dynamic_background = 'background-color:#ffe6e6;';
        } else {
            if ($row["status"] == '2') {
                $chat_message = '<em>This message has been removed</em>';
            } else {
                $chat_message = $row["chat_message"];
            }
            $user_name = '<b class="text-danger">' . get_user_name($row['from_user_id'], $connect) . '</b>';
            $dynamic_background = 'background-color:#ffffe6;';
        }

        $output .= '

		<li style="border-bottom:1px dotted #ccc;padding-top:8px; padding-left:8px; padding-right:8px;' . $dynamic_background . '">
			<p>' . $user_name . ' - ' . $chat_message . ' 
				<div align="right">
					- <small><em>' . $row['timestamp'] . '</em></small>
				</div>
			</p>
		</li>
		';
    }
    $output .= '</ul>';
    return $output;
}

?>