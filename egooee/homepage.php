<div style="padding-top: 35px;box-sizing: border-box;background-image: linear-gradient(to bottom right, #C33764, #1D2671);min-height: 100%;padding-bottom: 20px;padding-left: 5px;padding-right: 5px;">
            <form action="javascript:void(0);" method="post" role="form" style="background: #0200008c;max-width: 870px;margin: 0 auto;color:white;" id="new_form">
                <!--Modal body--> 
                <div class="modal-body">
                    <center style="font-size:27px;">Welcome to <span style="color:red;">E</span><span style="color:yellow;">g</span><span style="color:green;">o</span><span style="color:red;">o</span><span style="color:yellow;">e</span><span style="color:green;">e</span></center>
                    <center style="font-size:27px;" class="m_v13">Social Listing Services </center>
                    <center style="font-size:27px;" class="m_v13">Coming Soon   </center>
                    <center style="font-size:27px;" class="m_v14">Register your business and get a free business ad space   </center>
                    <div class="row">
                        <div class="form-group col-md-12" style="max-width: 500px;margin: 10px auto;">
                            <input type="email" class="form-control" placeholder="Enter Email address" required="" id="email" name="email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12" style="max-width: 500px;margin: 10px auto;">
                            <input type="text" class="form-control" placeholder="Enter Business Name" required="" id="business_name" name="business_name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12" style="max-width: 500px;margin: 10px auto;">
                            <input type="password" class="form-control" placeholder="Enter Password" required="" id="password" name="password">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12" style="max-width: 500px;margin: 10px auto;">
                            <input type="password" class="form-control" placeholder="Reenter Password" required="" id="repassword" name="repassword">
                        </div>
                    </div>


                    <div class="row">

                        <div class="form-group col-md-12" style="max-width: 650px;margin: 10px auto;">

                            <input type="submit" class="btn btn-success pull-right" id="submit" name="submit" value="Submit">
                            <input type="checkbox" class="pull-right" required="" id="check1" name="check1" style="height: 25px;width: 25px;vertical-align: middle;margin-top: 8px;margin-right: 10px;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="max-width: 780px;margin: 10px auto;">
                            Please note: The business or service you’re registering must be one that is legal in your state or country. 
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12" style="max-width: 780px;margin: 10px auto;">
                            <div class="alert alert-success d-none" id="error">Register successfully done. Check your email for more information.</div>
                        </div>

                    </div>
                </div>

            </form>

        </div>
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Terms and Condition</h5>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        I agree to list a legal business on Egooee, I also agree and gives Egooee the rights to remove or delete any posting or ad that it deems inappropriate that I have posted on the site. 
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" id="cl1" class="btn btn-danger" data-dismiss="modal">Ok</button>
                    </div>

                </div>
            </div>
        </div>
        <!--====== Javascripts & Jquery ======-->
       