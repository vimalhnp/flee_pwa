<?php
include 'connect.php';
include 'ad_controller.php';
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
    if ((time() - $_SESSION['last_login_timestamp']) > 600) {
        header("location:logout.php");
        exit;
    } else {
        $_SESSION['last_login_timestamp'] = time();
    }
}
$seoq = "select * from seo";
$seor = mysqli_query($con, $seoq);
$seod = mysqli_fetch_assoc($seor);
?>

<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>EGOOEE</title>
        <meta charset="UTF-8">
        <meta name="description" content="<?php echo $seod['description'] ?>">
        <meta name="keywords" content="<?php echo $seod['keywords'] ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link href="img/favicon.ico" rel="shortcut icon"/>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

        <!-- Priceing Page -->

        <!--<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100' rel='stylesheet' type='text/css'>-->
        <!--end priceing page -->

        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"/>

        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/flaticon.css"/>
        <link rel="stylesheet" href="css/slicknav.min.css"/>
        <link rel="stylesheet" href="css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/my.css"/>


        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <style>
        .sticky {
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 9;
        }
        .modal a{
            color:black;
        }
        .main-menu li a{
            margin-right: 27px;
        }
        .owl-carousel .owl-stage-outer{
            height:410px;
        }
        .set_text{
            top:20px;
        }
        .top-letest-product-section {
            padding-top: 5px;

        }
        .product-section{
            padding: 5px;
        }

        .name{
            /*width:120px;*/
            color: #0079c1;
            height: 14.53em; 
            /*line-height: 1em;*/ 
            font-size: 20px;
            font-weight: 400;
            text-overflow: ellipsis;
            margin-bottom: 12px;
            cursor: pointer;
            word-break: break-all;
            overflow:hidden;
            /*white-space: nowrap;*/
            transition: height 0.15s ease-out;
        }
        .name:hover{
            overflow: visible; 
            white-space: normal;
            height:auto; /* just added this line */
            transition: height 0.25s ease-in;
        }


        .multiselect-container>li>a>label {
            padding: 4px 20px 3px 20px;
        }
        .btn-group{
            width:100%;
        }
        .btn-group button{
            text-align: left !important;
        }
        .paper_clip{      
            font-size: 36px;
            position: relative;
            bottom: 24px;
            right: 0px;
            color: red;

        }
        .multiselect-container>li>a{
            color:black;
        }
        #panel {
            display: none;
        }
        .multiselect{
            border: 1px solid #ced4da !important;
        }

        .multiselect-container{
            height: 250px;
            overflow-y: scroll;
        }

    </style>
    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.3"></script>

        <!-- Header section -->
        <header class="" style="font-size:14px;">
            <div class="">
                <div class="container">

                    <div class="modal" id="signin">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!--Modal Header--> 
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Sign in</h5>

                                </div>
                                <form action="javascript:void(0);" method="post" role="form" id="loginForm">
                                    <!--Modal body--> 
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label for="login_email">User Email or User Name</label>
                                            <input type="text" class="form-control" id="login_email" aria-describedby="emailHelp" placeholder="Enter User Email or User Name" name="useremail" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="login_password">Password</label>
                                            <input type="password" class="form-control" id="login_password" placeholder="Password" name="password" required="">
                                        </div>
                                        <a href="javascript:void(0);" id="forget_password" data-target="#reset_model" data-toggle="modal">Forget Password?</a>
                                        <!--<a href="javascript:void(0);" id="register_open" data-target="#register_popup" data-toggle="modal" style="margin-left:5px;">Register</a><br>-->
                                        <a href="register.php" style="margin-left:5px;">Register</a><br>
                                        <div class="alert alert-danger login_error" style="display: none;width: 100%;">Email or Password doesn't match, Login Failed!</div>
                                        <div class="alert alert-danger login_otp" style="display: none;width: 100%;">Your account is not activated yet, please check your email to activate it.</div>
                                    </div>

                                    <!--Modal footer--> 
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-primary" id="login-submit" name="loginnew" value="Submit">
                                        <label for="login-submit"></label>
                                        <button type="button" class="btn btn-danger" id="login_close" data-dismiss="modal">Cancel</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--otp model-->
                    <div class="modal" id="otp_model">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!--Modal Header--> 
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Check your email for OTP.</h5>
                                </div>
                                <form action="javascript:void(0);" method="post" role="form" id="otpform">
                                    <!--Modal body--> 
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label for="login_email">OTP</label>
                                            <input type="number" class="form-control" id="otp" placeholder="Enter OTP" name="otp" required="">
                                            <input type="hidden" class="form-control" id="email_hide" name="email_hide">
                                        </div>
                                        <div class="alert alert-danger otp_error" style="display: none;width: 100%;">OTP doesn't match!</div>
                                    </div>

                                    <!--Modal footer--> 
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-primary" id="otp_submit" name="otp_submit" value="Submit">
                                        <button type="button" class="btn btn-danger" id="otp_close" data-dismiss="modal">Cancel</button>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--success modal-->
                    <div class="modal" id="success_model">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!--Modal Header--> 
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">EGOOEE</h5>
                                </div>

                                <!--Modal body--> 
                                <div class="modal-body">
                                    <div><h5>Please check your email for confirm registration.</h5></div><br/>
                                    Note : if you haven't seen an email from us, please check your spam folder.
                                    <a href="index.php" class="btn btn-success pull-right">OK</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--forget password-->
                    <div class="modal" id="reset_model">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!--Modal Header--> 
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Reset Password</h5>
                                </div>
                                <form action="javascript:void(0);" method="post" role="form" id="resetform">
                                    <!--Modal body--> 
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label>Email ID</label>
                                            <input type="email" class="form-control" id="reset_email" placeholder="Enter Email" name="reset_email" required="">
                                        </div>
                                        <div class="alert alert-danger reset_error" style="display: none;width: 100%;">Email doesn't match, reset Failed!</div>
                                    </div>

                                    <!--Modal footer--> 
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-primary" id="reset_submit" name="reset_submit" value="Submit">
                                        <button type="button" class="btn btn-danger" id="reset_close" data-dismiss="modal">Cancel</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--confirm modal-->
                    <div class="modal" id="confirm_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!--Modal Header--> 
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Create New Password</h5>
                                </div>
                                <form action="javascript:void(0);" method="post" role="form" id="confirmform">
                                    <!--Modal body--> 
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="hidden" class="form-control" id="confirm_email" name="confirm_email">
                                            <input type="password" class="form-control" id="confirm_password" placeholder="Enter New Password" name="confirm_password" required="">
                                        </div>
                                        <!--<div class="alert alert-danger reset_error" style="display: none;width: 100%;">Email doesn't match, reset Failed!</div>-->
                                    </div>

                                    <!--Modal footer--> 
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--register business modal-->
                    <div class="modal" id="business_add">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <?php
                                if (isset($_SESSION['user_id'])) {
                                    $first_bs = "select * from tbl_company where user_id=" . $_SESSION['user_id'];
                                    $first_br = mysqli_query($con, $first_bs);
                                    $first_bd = mysqli_fetch_assoc($first_br);
                                }
                                ?>
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Register Business</h5>
                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">

                                                    <label>Choose Photo :</label>
                                                    <input type="file" class=""  name="add_logo" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                Note: Image size 250*360 required
                                            </div>

                                            <div class="form-group col-md-6">                                   
                                                <input type="hidden" class="form-control" id="user_id"  name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                                <label>Business Name :</label>
                                                <input type="text" class="form-control" id="business_name"  name="business_name" required="" placeholder="Business Name" value="<?php echo (isset($first_bd['c_name'])) ? $first_bd['c_name'] : ''; ?>">
                                            </div>
                                            <div class="form-group col-md-6">                                   

                                                <label>Title :</label>
                                                <input type="text" class="form-control" readonly="" value="<?php echo (isset($_SESSION['title'])) ? ucwords($_SESSION['title']) : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Business Type :</label>
                                                    <select class="form-control" name="category_id">
                                                        <?php
                                                        $category = "select * from tbl_category where status=1";
                                                        $category_result = mysqli_query($con, $category);
                                                        while ($cat_data = mysqli_fetch_assoc($category_result)) {
                                                            $first_bselected = '';
                                                            if (isset($first_bd['category_id']) && $first_bd['category_id'] == $cat_data['category_id']) {
                                                                $first_bselected = 'selected';
                                                            }
                                                            ?>
                                                            <option <?php echo $first_bselected ?> value="<?php echo $cat_data['category_id']; ?>"><?php echo $cat_data['name']; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Email :</label>
                                                <input type="email" class="form-control" name="business_email" required="" placeholder="Business Email" value="<?php echo (isset($_SESSION['useremail']) && $_SESSION['useremail'] != '') ? $_SESSION['useremail'] : ''; ?>">
                                                <?php echo (isset($_GET['action']) && $_GET['action'] == 'er') ? '<small class="text text-danger">Email already registered</small>' : ''; ?><small></small>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Business Address :</label>
                                                    <textarea class="form-control" rows="2" placeholder="Business Address" id="business_address" name="business_address"><?php echo (isset($first_bd['c_address'])) ? $first_bd['c_address'] : ''; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Business Description :</label>
                                                    <textarea class="form-control" rows="2" placeholder="Describe Your Business" id="describe_business" name="describe_business"><?php echo (isset($first_bd['c_description'])) ? $first_bd['c_description'] : ''; ?></textarea>
                                                </div>
                                            </div>
                                        </div>                                     
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>State :</label>
                                                    <input type="text" class="form-control" name="state" required="" placeholder="State" value="<?php echo (isset($first_bd['state'])) ? $first_bd['state'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Country :</label>
                                                    <select class="form-control" name="country">
                                                        <?php
                                                        $country_sql = "select * from countries where status=1";
                                                        $country_res = mysqli_query($con, $country_sql);
                                                        while ($country = mysqli_fetch_assoc($country_res)) {
                                                            $first_bcountryselected = '';
                                                            if (isset($first_bd['country_id']) && $country['country_id'] == $first_bd['country_id']) {
                                                                $first_bcountryselected = 'selected';
                                                            }
                                                            echo "<option " . $first_bcountryselected . " value='" . $country['country_id'] . "'>" . $country['country_name'] . "</option>";
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Zip Code :</label>
                                                    <input type="text" class="form-control" name="zipcode" required="" placeholder="Zip Code" value="<?php echo (isset($first_bd['zipcode'])) ? $first_bd['zipcode'] : ''; ?>">
                                                </div>
                                            </div>
                                        </div>                                     
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" id="business_submit" name="business_submit" value="Submit">
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--ad post modal-->
                    <div class="modal" id="post_add">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Post an Ad</h5>
                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Choose Image :</label>
                                                    <input type="file" class="" id="b_logo"  name="b_logo" required="">
                                                    <input type="hidden" id="ad_type"  name="ad_type">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                Note: Image size 700*700 required
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">                                   
                                                    <input type="hidden" class="form-control" id="userid"  name="userid"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                                    <label>Title :</label>
                                                    <input type="text" class="form-control" id="ad_name"  name="ad_name" required="" placeholder="Title">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">                                   

                                                    <label>Days Limit :</label>
                                                    <select class="form-control" id="ad_expiry" name="ad_expiry">
                                                        <option value="7">7 Days</option>
                                                        <option value="14">14 Days</option>
                                                        <option value="30">30 Days</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Phone Number :</label>
                                                    <input type="text" class="form-control" id="ad_phone"  name="ad_phone" required="" placeholder="Phone Number">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Business Name</label>
                                                    <select class="form-control" name="company_id">
                                                        <?php
                                                        if (isset($_SESSION['user_id'])) {
                                                            $company_q = "select * from tbl_company where user_id=" . $_SESSION['user_id'];
                                                            $company_r = mysqli_query($con, $company_q);
                                                            while ($company_d = mysqli_fetch_assoc($company_r)) {
                                                                ?>
                                                                <option value="<?php echo $company_d['company_id'] ?>"><?php echo ucfirst($company_d['c_name']); ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Address :</label>
                                                    <textarea class="form-control" rows="2" placeholder="Address" id="ad_address" name="ad_address"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Description :</label>
                                                    <textarea class="form-control" rows="2" placeholder="Description" id="ad_message" name="ad_message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>State/Province :</label>
                                                    <input type="text" class="form-control" placeholder="State/Province" name="state" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Zip Code :</label>
                                                    <input type="text" class="form-control" placeholder="Zip Code" name="zip_code" />
                                                </div>
                                                <input type="hidden" name="dwcountry">
                                            </div>
                                        </div>
                                        <a href="javascript:void(0);" id="re_open_event">Post an Event</a>
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" id="post_submit" name="ad_submit" value="Submit">
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                    <!--post event-->
                    <div class="modal" id="add_event">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Post an Event</h5>
                                </div>
                                <form action="ad_controller.php" method="post" role="form" id="addpostform" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Choose Image :</label>
                                                    <input type="file" class="" id="add_event_image"  name="add_event_image" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                Note: Image size 400*400 required
                                            </div>
                                        </div>
                                        <div class="form-group">                                   
                                            <input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                            <label>Event Title :</label>
                                            <input type="text" class="form-control" id="event_name"  name="event_name" required="" placeholder="Event Title">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">  
                                                    <label>State :</label>
                                                    <input type="text" class="form-control" id="event_state"  name="event_state" required="" placeholder="State">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label>Event Type :</label>
                                                    <select class="form-control" name="event_category">
                                                        <?php
                                                        $sq = "select * from tbl_ad_category where status=1";
                                                        $re = mysqli_query($con, $sq);
                                                        while($da = mysqli_fetch_assoc($re)){
                                                            echo '<option value="'.$da['ad_category_id'].'">'.$da['category_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label>Start :</label>
                                                    <input type="date" class="form-control" id="event_start"  name="event_start" required="" placeholder="Start Date">
                                                    <!--<small>Start Date</small>-->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label>End :</label>
                                                    <input type="date" class="form-control" id="event_end"  name="event_end" required="" placeholder="End Date">
                                                    <!--<small>End Date</small>-->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Description :</label>
                                            <textarea class="form-control" rows="2" placeholder="Description" id="event_description" name="event_description"></textarea>
                                        </div>
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" id="event_submit" name="event_submit" value="Submit">

                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                    <!--profile-->
                    <?php
                    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                        $user_sql = "select * from tbl_user where user_id=" . $_SESSION['user_id'];

                        $user_result = mysqli_query($con, $user_sql);

                        if (mysqli_num_rows($user_result)) {
                            $user_data = mysqli_fetch_assoc($user_result);
                        }
                    }
                    ?>
                    <div class="modal" id="profile">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Profile</h5>

                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group"> 
                                                            <label>Choose Photo :</label>
                                                            <input type="file" name="user_image" id="user_image">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group"> 
                                                            <label>Email :</label>
                                                            <input type="email" class="form-control" name="email" required="" value="<?php echo (isset($user_data['email']) && $user_data['email'] != '') ? $user_data['email'] : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <img src="<?php echo (isset($user_data['image']) && $user_data['image'] != '') ? $user_data['image'] : 'img/default-avatar.png' ?>" id="user_image_view" class="img-responsive rounded-circle pull-right" style="max-height:138px;" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                                    <input type="hidden" class="form-control" name="user_type"  value="<?php echo (isset($_SESSION['user_type']) && $_SESSION['user_type'] != '') ? $_SESSION['user_type'] : ''; ?>">
                                                    <label>First Name :</label>
                                                    <input type="text" class="form-control" name="fname" required="" value="<?php echo (isset($user_data['fname']) && $user_data['fname'] != '') ? $user_data['fname'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label>Last Name :</label>
                                                    <input type="text" class="form-control" name="lname" required="" value="<?php echo (isset($user_data['lname']) && $user_data['lname'] != '') ? $user_data['lname'] : ''; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="profile_enticements_div">
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label>Birth Date :</label>
                                                    <input type="date" class="form-control" name="dob" value="<?php echo (isset($user_data['dob']) && $user_data['dob'] != '') ? $user_data['dob'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"> 
                                                    <label>Influences </label>

                                                    <select class="form-control" name="enticements[]" id="profile_enticements" multiple="multiple">
                                                        <?php
                                                        if (isset($user_data['gender'])) {
                                                            $encmt_sql1 = "select * from tbl_reg_category where status=1 AND type='" . $user_data['gender'] . "' group by cate_name";

                                                            $encmt_res1 = mysqli_query($con, $encmt_sql1);
                                                            $user_enfu_arr = '';
                                                            if (isset($user_data['reg_category_ids'])) {
                                                                $user_enfu_arr = json_decode($user_data['reg_category_ids']);
                                                            }
                                                            while ($encmt1 = mysqli_fetch_assoc($encmt_res1)) {
                                                                $select1 = '';
                                                                if (in_array($encmt1['cate_name'], $user_enfu_arr)) {
                                                                    $select1 = 'selected';
                                                                }
                                                                echo "<option " . $select1 . " value='" . $encmt1['cate_name'] . "'>" . ucwords($encmt1['cate_name']) . "</option>";
                                                            }
                                                        }
                                                        ?>
                                                    </select>    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class='col-md-6'>
                                                <div class='form-group'>
                                                    <label>Street Address :</label>
                                                    <input type="text" placeholder="Street Address" class="form-control" name="street_address" value="<?php echo (isset($user_data['saddress']) && $user_data['saddress'] != '') ? $user_data['saddress'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class='col-md-6'>
                                                <div class='form-group'>
                                                    <label>State/Province :</label>
                                                    <input type="text" placeholder="State/Province" class="form-control" name="state" value="<?php echo (isset($user_data['state']) && $user_data['state'] != '') ? $user_data['state'] : ''; ?>">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label >Country</label>
                                                    <select class="form-control" id="country" name="country" onchange="country_zipcode()">
                                                        <?php
                                                        $country_sql = "select * from countries where status=1";
                                                        $country_res = mysqli_query($con, $country_sql);
                                                        while ($country = mysqli_fetch_assoc($country_res)) {
                                                            $select2 = '';
                                                            if (isset($user_data['country']) && $user_data['country'] == $country['country_id']) {
                                                                $select2 = "selected";
                                                            }
                                                            echo "<option " . $select2 . " value='" . $country['country_id'] . "'>" . $country['country_name'] . "</option>";
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Zip Code :</label>
                                                    <input type="text" class="form-control" name="zipcode" value="<?php echo (isset($user_data['zipcode']) && $user_data['zipcode'] != '') ? $user_data['zipcode'] : ''; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" name="update_profile" value="Update">
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--company modal-->

                    <div class="modal" id="company_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Business Details</h5>
                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group"> 
                                                            <label>Business Name :</label>
                                                            <input type="text" class="form-control" name="business_name" id="business_name_view" required="" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group"> 
                                                            <label>Choose Photo :</label>
                                                            <input type="file" name="business_image" id="business_image">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <img src="" id="business_image_view" class="img-responsive pull-right" style="max-height:138px;" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group"> 
                                            <input type="hidden" class="form-control" name="company_id" id="company_id_view" value="">
                                            <!--<input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">-->
                                            <label>Business Email :</label>
                                            <input type="email" class="form-control" name="business_email" id="business_email_view" required="" value="">
                                        </div>

                                        <div class="form-group"> 
                                            <label>Location :</label>
                                            <textarea class="form-control" rows="2" name="address" id="address_view"></textarea>                        
                                        </div>
                                        <div class="form-group"> 
                                            <label>Description :</label>
                                            <textarea class="form-control" rows="2" name="description" id="description_view"></textarea>                        
                                        </div>
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" name="update_business" value="Update">
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--change password-->
                    <div class="modal" id="change_password">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Change Password</h5>

                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="form-group"> 
                                            <input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                            <label>Email : </label>
                                            <input type="email" class="form-control" readonly value="<?php echo (isset($user_data['email']) && $user_data['email'] != '') ? $user_data['email'] : ''; ?>">
                                            <input type="hidden" class="form-control" name="email" required="" value="<?php echo (isset($user_data['email']) && $user_data['email'] != '') ? $user_data['email'] : ''; ?>">
                                        </div>
                                        <div class="form-group"> 
                                            <label>New Password :</label>
                                            <input type="password" class="form-control" name="password" required="">
                                        </div>
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" name="change_password" value="Update">

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--blog-->
                    <div class="modal" id="write_blog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="margin: 0 auto;">Write a Post</h5>
                                    </div>   
                                    <div class="modal-body">
                                        <div class="form-group">                                   
                                            <input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                            <label>Title :</label>
                                            <input type="text" class="form-control"  name="blog_title" required="" placeholder="Title">
                                        </div>
                                        <div class="form-group">                                   
                                            <label>User Name :</label>
                                            <input type="text" class="form-control"  name="user_name" required="" placeholder="User Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Description :</label>
                                            <textarea class="form-control" rows="2" placeholder="Description" maxlength="200" name="blog_description" required=""></textarea>
                                        </div>
                                        <!--                                        <div class="form-group">
                                                                                    <label>Address :</label>
                                                                                    <textarea class="form-control" rows="2" placeholder="Address" name="blog_address" required=""></textarea>
                                                                                </div>-->
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" name="blog_submit" value="Submit">
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--dish-->
                    <div class="modal" id="dish">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="margin: 0 auto;">Post your favorite dish</h5>
                                    </div>   
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">                                   
                                                    <input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                                    <label>Dish Name :</label>
                                                    <input type="text" class="form-control"  name="dish_name" required="" placeholder="Dish Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">                                   
                                                    <label>Restaurant Name :</label>
                                                    <input type="text" class="form-control"  name="restaurant_name" required="" placeholder="Restaurant Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">                                   
                                                    <label>Dish Type :</label>
                                                    <select class="form-control" name="category">
                                                        <option value="us">US</option>
                                                        <option value="caribbean">Caribbean</option>
                                                        <option value="international">International</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">                                   
                                                    <label>Phone :</label>
                                                    <input type="number" class="form-control"  name="phone" required="" placeholder="Phone Number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Message :</label>
                                                    <textarea class="form-control" rows="2" placeholder="Message" name="message" required=""></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Location :</label>
                                                    <textarea class="form-control" rows="2" placeholder="Location" name="location" required=""></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Choose Photo</label>
                                            <input type="file" name="dish_image" required="" style="display:block;" />
                                        </div>
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" name="dish_submit" value="Submit">
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--feed-->
                    <div class="modal" id="post_feed">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Post a feed</h5>
                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Choose Image :</label>
                                                    <input type="file" name="image" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                Note: Image size 700*700 required
                                            </div>
                                        </div>
                                        <div class="form-group">                                   
                                            <input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                            <label>Title :</label>
                                            <input type="text" class="form-control"  name="title" required="" placeholder="Title">
                                        </div>
                                        <div class="form-group">                                                                            
                                            <label>Description :</label>
                                            <textarea rows="2" class="form-control" name="desc" placeholder="Description"></textarea>
                                        </div>
                                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                        <input type="submit" class="btn btn-primary pull-right" name="feed_submit" value="Submit">
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>


                    <!--channel modal-->
                    <div class="modal" id="channel_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <input type="hidden" name="hd" id="hd">
                                    <h5 class="modal-title" style="margin: 0 auto;">Select Channel</h5>
                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                Choose Pin Color :
                                                <!--<input type="color" name="color" id="color">-->
                                                <select name="color" class="form-control" id="color">
                                                    <option value="#ed0909">Red</option>
                                                    <option value="#2157ed">Blue</option>
                                                    <option value="#ef10dd">Pink</option>
                                                    <option value="#1ba517">Green</option>
                                                    <option value="#f9d909">Yellow</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <br><input type="text" name="new_channel_name" id="new_channel_name" class="form-control" required="" placeholder="Channel Name">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">                                                   
                                                    <br><button type="button" name="create_channel" id="create_channel" class="form-control btn btn-primary">Create</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="create_channel_error" class="d-none"></div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <center style="margin-bottom:1rem;">-OR-</center>
                                                Choose an existing channel
                                            </div>
                                        </div>
                                        <input type="hidden" name="post_save_id" id="post_save_id">
                                        <div class="row">
                                            <div class="col-md-12" id="channel_div_ajax">


                                                <?php
                                                if (isset($_SESSION['user_id'])) {
                                                    $channels_q = "select * from channel where status=1 and user_id=" . $_SESSION['user_id'];
                                                    $channels_r = mysqli_query($con, $channels_q);
                                                    while ($channels_d = mysqli_fetch_assoc($channels_r)) {

//                                                        echo $channels_d['color_code'];
                                                        echo "<button type='button' onclick='save_pinned_post(" . $channels_d['channel_id'] . "," . $_SESSION['user_id'] . ",\"" . $channels_d['color_code'] . "\");'  class='btn btn-secondary' style='margin:5px;border-radius:20px;'>" . $channels_d['name'] . "</buton>";
                                                    }
                                                }
                                                ?>


                                            </div>

                                        </div>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>


                    <!--plan_warning-->
                    <div class="modal" id="open_plan_popup">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title" style="margin: 0 auto;">Up Grade to</h5>
                                </div>
                                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                                    <!-- Modal body -->
                                    <div class="modal-body">

                                        <div class="btn-group">

                                            <a href="pricing.php?action=upgrade" class="btn btn-primary"> Business-Pro</a>

                                            <div style="margin:8px;">OR</div>

                                            <a href="pricing.php?action=upgrade" class="btn btn-primary"> Business-EX</a>

                                        </div>
                                        <a href="javascript:void(0)" class="btn btn-outline-dark pull-right" data-dismiss="modal" style="margin: 20px;margin-right: 0;">Got IT</a>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header section end -->
