<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>
<section class="product-section">
    <div class="container-fluid">
        <?php
        require_once '10_slider.php';
        ?>
    </div>
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <!--<center><h4 style="margin-bottom: 10px;padding:20px;">Classified</h4></center>-->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="" class="btn">Help Wanted</a>
                                    <a href="" class="btn">Jobs</a>
                                    <a href="" class="btn">House for Sale</a>
                                    <a href="" class="btn">For Rent</a>
                                    <a href="" class="btn">For Sale</a>
                                    <a href="" class="btn">Automobile</a>
                                    <a href="" class="btn">Business Opportunities</a>
                                </div>
                                <?php
                                $review_query = "select * from tbl_post where status=1 AND type=1";

                                $review_result = mysqli_query($con, $review_query);
                                if (mysqli_num_rows($review_result)) {
                                    while ($reviews = mysqli_fetch_assoc($review_result)) {

                                        $d1 = strtotime($reviews['added_on']);
                                        $d2 = strtotime(date('Y-m-d'));

                                        $ddd = $d2 - $d1;
                                        $day = $ddd / (60 * 60 * 24);

                                        if ($day > $reviews['expire']) {

                                            $as = "update tbl_post set status=0 where post_id='" . $reviews['post_id'] . "'";
                                            $re = mysqli_query($con, $as);
                                            
                                            
                                        } else {
                                            ?>

                                            <div class="col-md-4 col-sm-12" style="margin-bottom: 10px;">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <!--                                                            <div class="col-md-5 col-5 col-lg-5">
                                                                                                                            <img src="<?php echo (isset($reviews['image']) && $reviews['image'] != '') ? $reviews['image'] : 'img/default-avatar.png'; ?>" class="img-responsive">
                                                            
                                                                                                                        </div>-->
                                                            <div class="col-md-7 col-lg-12 col-7">
                                                                <div><b><?php echo ucfirst($reviews['b_name']); ?></b>
                                                                    <span class="pull-right"><?php echo date('d-m-Y', strtotime($reviews['added_on'])) ?></span> 

                                                                </div>
                                                                <div style="font-size: 13px;height: 75px;overflow-y: auto;text-align: justify;overflow-x: hidden;"><?php echo ucfirst($reviews['b_message']); ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>
    </div>
</section>
<?php
require_once 'footer.php';
?>
