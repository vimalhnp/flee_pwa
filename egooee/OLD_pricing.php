<?php
include 'connection.php';
include_once './header.php';
?>
<script>
    // $("#alet").hide();
    function like(uid) {
        if (uid != 0) {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'plan_like', 'user_id': uid},
                success: function (ans) {
                    if (ans) {

                        if (ans == "already given")
                        {
                            // $("#like1").addClass("alert alert-success").text(ans).css("margin-top", "200px");
                            $("#al1").show();
                        } else
                        {
                            $("#like1").text(ans);
//                        $(".d_like_" + aid).addClass("text-danger");

                            alert(ans);
                        }
                    }
                }
            });
        }
    }


    function dislike(uid) {
        if (uid != 0) {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'plan_dislike', 'user_id': uid},
                success: function (ans) {
                    if (ans) {
                        if (ans == "already given")
                        {
                            $("#al1").show();
                        } else
                        {
                            $("#dislike1").text(ans);
//                        $(".d_like_" + aid).addClass("text-danger");

                            alert(ans);
                        }
                    }
                }
            });
        }
    }



    function star_rating(uid) {
        $("#star1").click(function () {
            // var name = 'rate_' + aid;

            var radioValue = $("input[type='radio'][name='rate1']:checked").val();
            if (radioValue) {

                $.ajax({
                    url: 'rating.php',
                    type: 'POST',
                    data: {'action': 'post1', 'user_id': uid, 'text': radioValue},
                    success: function (ans) {
                        if (ans == "already star given") {
                            //alert("You are give a - " + radioValue + " star");
                            //alert(ans);
                             $("#al2").show();
                        } else
                        {
                            alert(ans);
                        }
                    }
                });
            } else
            {
                alert("error in radiovalue");
            }
        });
        $("#star2").click(function () {
            var radioValue = $("input[type='radio'][name='rate2']:checked").val();
            //alert(radioValue);
            if (radioValue) {
                // alert("You are give a - " + radioValue +" star");
                $.ajax({
                    url: 'rating.php',
                    type: 'POST',
                    data: {'action': 'post1', 'user_id': uid, 'text': radioValue},
                    success: function (ans) {
                        if (ans == "already star given") {
                            
                             $("#al2").show();
                        } else
                        {
                            alert(ans);
                        }
                    }
                });
            }
        });
        $("#star3").click(function () {
            var radioValue = $("input[type='radio'][name='rate3']:checked").val();
            //alert(radioValue);
            if (radioValue) {
                // alert("You are give a - " + radioValue +" star");
                $.ajax({
                    url: 'rating.php',
                    type: 'POST',
                    data: {'action': 'post1', 'user_id': uid, 'text': radioValue},
                    success: function (ans) {
                        
                        if (ans == "already star given") {
                            
                             $("#al2").show();
                        } else
                        {
                            alert(ans);
                        }
                    }
                });
            }
        });
        $("#star4").click(function () {
            var radioValue = $("input[type='radio'][name='rate4']:checked").val();
            //alert(radioValue);
            if (radioValue) {
                // alert("You are give a - " + radioValue +" star");
                $.ajax({
                    url: 'rating.php',
                    type: 'POST',
                    data: {'action': 'post1', 'user_id': uid, 'text': radioValue},
                    success: function (ans) {
                       if (ans == "already star given") {
                            
                             $("#al2").show();
                        } else
                        {
                            alert(ans);
                        }
                    }
                });
            }
        });
        $("#star5").click(function () {
            var radioValue = $("input[type='radio'][name='rate5']:checked").val();
            // alert(radioValue);
            if (radioValue) {
                // alert("You are give a - " + radioValue +" star");
                $.ajax({
                    url: 'rating.php',
                    type: 'POST',
                    data: {'action': 'post1', 'user_id': uid, 'text': radioValue},
                    success: function (ans) {
                      if (ans == "already star given") {
                            
                             $("#al2").show();
                        } else
                        {
                            alert(ans);
                        }
                    }
                });
            }
        });
    }





</script>



<style>

    @import url(https://fonts.googleapis.com/css?family=Lato:400,700);

    body {
        background: #F2F2F2;
        padding: 0;
        maring: 0;
    }

    #price {
        text-align: center;
    }

    .plan {
        display: inline-block;
        margin: 10px 1%;
        font-family: 'Lato', Arial, sans-serif;
    }

    .plan-inner {
        background: #fff;
        margin: 0 auto;
        min-width: 280px;
        max-width: 100%;
        position:relative;
    }

    .entry-title {
        background: #53CFE9;
        height: 140px;
        position: relative;
        text-align: center;
        color: #fff;
        margin-bottom: 30px;
    }

    .entry-title>h3 {
        background: #20BADA;
        font-size: 20px;
        padding: 5px 0;
        text-transform: uppercase;
        font-weight: 700;
        margin: 0;
    }

    .entry-title .price {
        position: absolute;
        bottom: -25px;
        background: #20BADA;
        height: 95px;
        width: 95px;
        margin: 0 auto;
        left: 0;
        right: 0;
        overflow: hidden;
        border-radius: 50px;
        border: 5px solid #fff;
        line-height: 80px;
        font-size: 28px;
        font-weight: 700;
    }

    .price span {
        position: absolute;
        font-size: 9px;
        bottom: -10px;
        left: 30px;
        font-weight: 400;
    }

    .entry-content {
        color: #323232;
    }

    .entry-content ul {
        margin: 0;
        padding: 0;
        list-style: none;
        text-align: center;
    }

    .entry-content li {
        border-bottom: 1px solid #E5E5E5;
        padding: 10px 0;
    }

    .entry-content li:last-child {
        border: none;
    }

    .btn1 {
        padding: 3em 0;
        text-align: center;
    }

    .btn1 a {
        background: #323232;
        padding: 10px 30px;
        color: #fff;
        text-transform: uppercase;
        font-weight: 700;
        text-decoration: none
    }
    .hot {
        position: absolute;
        top: -7px;
        background: #F80;
        color: #fff;
        text-transform: uppercase;
        z-index: 2;
        padding: 2px 5px;
        font-size: 9px;
        border-radius: 2px;
        right: 10px;
        font-weight: 700;
    }
    .basic .entry-title {
        background: #75DDD9;
    }

    .basic .entry-title > h3 {
        background: #44CBC6;
    }

    .basic .price {
        background: #44CBC6;
    }

    .standard .entry-title {
        background: #4484c1;
    }

    .standard .entry-title > h3 {
        background: #3772aa;
    }

    .standard .price {
        background: #3772aa;
    }

    .ultimite .entry-title > h3 {
        background: #DD4B5E;
    }

    .ultimite .entry-title {
        background: #F75C70;
    }

    .ultimite .price {
        background: #DD4B5E;
    }
</style>

<?php //include './header.php'; ?>
<div id="price">
    <!--price tab-->



    <div class="plan">
        <div class="plan-inner">
            <div class="entry-title">
                <?php
                $sql = "SELECT * FROM pricing where plan_number=1";
                $res = mysqli_query($con, $sql);

                if (mysqli_num_rows($res) > 0) {
                    $row = mysqli_fetch_assoc($res);
                    $fprice = $row['plan_price'];
                } else {
                    echo "Somwthing wrong!!";
                }
                ?>
                <h3>Plan one</h3>
                <div class="price">$<?php echo $fprice ?>
                </div>
            </div>
            <!--      <div class="entry-content">
                    <ul>
                      <li><strong>1x</strong> option 1</li>
                      <li><strong>2x</strong> option 2</li>
                      <li><strong>3x</strong> option 3</li>
                      <li><strong>Free</strong> option 4</li>
                      <li><strong>Unlimited</strong> option 5</li>
                    </ul>
                  </div>-->
            <div class="btn1">
                <input type="checkbox" name="check1" id='c1'  style="width: 20px;height: 20px;" >&nbsp; <a href="#"    id="b1">Check Out</a>

                <div class="modal" id="check1">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h5 class="modal-title" style="margin: 0 auto;">Plan One</h5>

                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                1
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
<!--                                <input type="submit" class="btn btn-primary" value="Submit">-->
                                <button type="button" class="btn btn-danger" id="ok" >Ok</button>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of price tab-->
    <!--price tab-->
    <div class="plan basic">
        <div class="plan-inner">
            <div class="hot">hot</div>
            <div class="entry-title">

                <?php
                $sql = "SELECT * FROM pricing where plan_number=2";
                $res = mysqli_query($con, $sql);

                if (mysqli_num_rows($res) > 0) {
                    $row = mysqli_fetch_assoc($res);
                    $sprice = $row['plan_price'];
                } else {
                    echo "Somwthing wrong!!";
                }
                ?>

                <h3>Plan Two</h3>
                <div class="price">$<?php echo $sprice; ?>
                </div>
            </div>
            <!--      <div class="entry-content">
                    <ul>
                      <li><strong>1x</strong> option 1</li>
                      <li><strong>2x</strong> option 2</li>
                      <li><strong>3x</strong> option 3</li>
                      <li><strong>Free</strong> option 4</li>
                      <li><strong>Unlimited</strong> option 5</li>
                    </ul>
                  </div>-->
            <div class="btn1">
                <input type = "checkbox" name = "check2" id = 'c2' style = "width: 20px;height: 20px;" >&nbsp;
                <a href = "#" id = "b2" >Submit</a>&nbsp;
                &nbsp;
                &nbsp;
                <?php
                $uid = '0,';
                if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                    $uid = $_SESSION['user_id'] . ",";
                }
                ?>
                <a href = "javascript:void(0);" onclick = "like(<?php echo $uid ?>);" style = "color: black;background-color: white;padding: 0px;"><i class = "fa fa-thumbs-o-up"></i></a>&nbsp;
                &nbsp;
                &nbsp;
                <a href = "javascript:void(0);" onclick = "dislike(<?php echo $uid ?>);" style = "color: black;background-color: white;padding: 0px;"><i class = "fa fa-thumbs-o-down"></i></a><br>
                <?php
                $s1 = "SELECT * FROM `price_engagement` WHERE engagement_type=1";
                $r1 = mysqli_query($con, $s1);

                if (mysqli_num_rows($r1)) {
                    $like = 0;
                    while ($row = mysqli_fetch_assoc($r1)) {
                        $like++;
                    }
                } else {
                    $like = 0;
                }


                $s2 = "SELECT * FROM `price_engagement` WHERE engagement_type=0";
                $r2 = mysqli_query($con, $s2);

                if (mysqli_num_rows($r2)) {
                    $dislike = 0;
                    while ($row = mysqli_fetch_assoc($r2)) {
                        $dislike++;
                    }
                } else {
                    $dislike = 0;
                }
                ?>
                <!--<h6 id="tlike" style="margin-left: 150px;">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $like; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dislike; ?>  </h6>-->
                &nbsp;&nbsp;&nbsp;&nbsp;<span id="like1" style="margin-left: 150px;"><?php echo $like; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="dislike1"><?php echo $dislike; ?></span>
                <div style="display: none;" id="al1" class="alert alert-danger">Alreay Given</div>
                <div class = "modal" id = "check2">
                    <div class = "modal-dialog">
                        <div class = "modal-content">

                            <!--Modal Header -->
                            <div class = "modal-header">
                                <h5 class = "modal-title" style = "margin: 0 auto;">Plan Two</h5>

                            </div>

                            <!--Modal body -->
                            <div class = "modal-body">
                                2
                            </div>

                            <!--Modal footer -->
                            <div class = "modal-footer">
                            <!--<input type = "submit" class = "btn btn-primary" value = "Submit"> -->
                                <button type = "button" class = "btn btn-danger" id = "ok2" data-dismiss = "modal" >Ok</button>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of price tab-->
    <!--price tab-->
    <div class = "plan standard">
        <div class = "plan-inner">
            <div class = "entry-title">

                <?php
                $sql = "SELECT * FROM pricing where plan_number=3";
                $res = mysqli_query($con, $sql);

                if (mysqli_num_rows($res) > 0) {
                    $row = mysqli_fetch_assoc($res);
                    $tprice = $row['plan_price'];
                } else {
                    echo "Somwthing wrong!!";
                }
                ?>


                <h3>Plan Three</h3>
                <div class = "price">$<?php echo $tprice ?>
                </div>
            </div>
            <!--<div class = "entry-content">
            <ul>
            <li><strong>2x</strong> Free Entrance</li>
            <li><strong>Free</strong> Snacks</li>
            <li><strong>Custom</strong> Swags</li>
            <li><strong>2x</strong> Certificate</li>
            <li><strong>Free</strong> Wifi</li>
            </ul>
            </div> -->
            <div class = "btn1">


                <p class="rate" style="margin-left: 80px;">
                    <?php
//                    if (isset($data['post_id'])) {
//                        $query_avg = "select avg(text) as avg from user_engagement where type=2 and post_id='" . $data['post_id'] . "'";
//                        $result_avg = mysqli_query($con, $query_avg);
//                        $data_avg = mysqli_fetch_assoc($result_avg);
//                        $avg = $data_avg['avg'];
//                    }
                    ?>
                    <input type="radio" id="star5" name="rate5" value="5"/>
                    <label for="star5" onclick="star_rating(<?php echo $_SESSION['user_id'] ?>)">5 stars</label>
                    <input type="radio" id="star4" name="rate4" value="4"/>
                    <label for="star4" onclick="star_rating(<?php echo $_SESSION['user_id'] ?>)">4 stars</label>
                    <input type="radio" id="star3" name="rate3" value="3"/>
                    <label for="star3" onclick="star_rating(<?php echo $_SESSION['user_id'] ?>)">3 stars</label>
                    <input type="radio" id="star2" name="rate2" value="2"/>
                    <label for="star2" onclick="star_rating(<?php echo $_SESSION['user_id'] ?>)">2 stars</label>
                    <input type="radio" id="star1" name="rate1" value="1"/>
                    <label for="star1" onclick="star_rating(<?php echo $_SESSION['user_id'] ?>)">1 star</label>


                </p><br><br>





                <input type = "checkbox" name = "check3" id = 'c3'style = "width: 20px;height: 20px;" >&nbsp;
                <a href = "#" id = "b3">Order Now</a>
                <br><br><div style="display: none;" id="al2" class="alert alert-danger">Alreay Given</div>
                <div class = "modal" id = "check3">
                    <div class = "modal-dialog">
                        <div class = "modal-content">

                            <!--Modal Header -->
                            <div class = "modal-header">
                                <h5 class = "modal-title" style = "margin: 0 auto;">plan Three</h5>

                            </div>

                            <!--Modal body -->
                            <div class = "modal-body">
                                3
                            </div>

                            <!--Modal footer -->
                            <div class = "modal-footer">
                            <!--<input type = "submit" class = "btn btn-primary" value = "Submit"> -->
                                <button type = "button" class = "btn btn-danger" id = "ok3" data-dismiss = "modal" >Ok</button>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of price tab-->
    <!--price tab-->
    <!--<div class = "plan ultimite">
    <div class = "plan-inner">
    <div class = "entry-title">
    <h3>Unlimited Wash</h3>
    <div class = "price">$100<span>/PER CAR</span>
    </div>
    </div>
    <div class = "entry-content">
    <ul>
    <li><strong>1x</strong> option 1</li>
    <li><strong>2x</strong> option 2</li>
    <li><strong>3x</strong> option 3</li>
    <li><strong>Free</strong> option 4</li>
    <li><strong>Unlimited</strong> option 5</li>
    </ul>
    </div>
    <div class = "btn">
    <a href = "#">Order Now</a>
    </div>
    </div>
    </div> -->
    <!--end of price tab-->
</div>
<?php
include_once './footer.php';
?>
<script>

    $('#c1').change(function () {
        if ($("#c1").prop("checked")) {

            $("#check1").show();
            $("#b1").show();
        }




    });
    $('#c2').change(function () {
        if ($("#c2").prop("checked")) {

            $("#check2").show();
            $("#b2").show();
        }




    });
    $('#c3').change(function () {
        if ($("#c3").prop("checked")) {

            $("#check3").show();
            $("#b3").show();
        }




    });
    $("#ok").click(function () {

        $("#check1").hide();
    });
    $("#ok2").click(function () {

        $("#check2").hide();
    });
    $("#ok3").click(function () {

        $("#check3").hide();
    });

</script>