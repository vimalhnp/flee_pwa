<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
    
} else {
    header('Location:index.php');
}
$display = "";
if (isset($_GET['vc']) && $_GET['vc'] != '') {
    $tsql = "select * from tbl_talk where verify_code=" . $_GET['vc'];
    $tres = mysqli_query($con, $tsql);
    if (mysqli_num_rows($tres) < 1) {
        echo "<h4 style='margin:0 auto;width:500px;margin-top:100px;color:red;'>Link Expired! Please contact to Admin.</h4>";
        $display = 'd-none';
    }
}
if (isset($_POST['btnsubmit'])) {

    $d1 = "";
    $a = "";
    if ($_FILES['add_logo']['name'] != "") {
        $filename = md5(time()) . basename($_FILES['add_logo']['name']);
        $tmpname = ($_FILES['add_logo']['tmp_name']);
        $dir = "../img/";
        $d1 = "img/" . $filename;
        $filepath = $dir . $filename;
        $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
        if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
            $error = "Image format not allowed";
        } else {
            $stat = move_uploaded_file($tmpname, $d1);
            $a = ', logo="' . $d1 . '"';
        }
    }

    $date = date('Y-m-d');
    if (isset($_GET['vc']) && $_GET['vc'] != '') {
        $sql2 = "update tbl_talk set user_id='" . $_SESSION['user_id'] . "',logo='" . $d1 . "',description='" . $_POST['desc'] . "',modify_on='" . $date . "',verify_code='' where verify_code='" . $_GET['vc'] . "'";
    } else {
        $sql2 = "insert into tbl_talk (user_id,logo,description,type,verify_code,status,added_on,modify_on) values('" . $_SESSION['user_id'] . "','" . $d1 . "','" . $_POST['desc'] . "','',0,0,'" . $date . "','" . $date . "')";
    }
    $res2 = mysqli_query($con, $sql2);
    header('Location:index.php?action=success');
}
$url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
<div class="container" style="max-width: 700px;min-height: 470px;">
    <div class="<?php echo $display ?>">
        <center><br><h3>Request to Top Talk</h3><br></center>
        <form action="" method="post" role="form" enctype="multipart/form-data" style="border:1px solid #e0dfdf">

            <!--Modal body--> 
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        Choose Image : <input type = "file" class = "" required="" id = "add_logo" name = "add_logo">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        Description :<textarea class = "form-control" rows="4" required = "" name = "desc" placeholder = "Description"></textarea>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <input type="submit"  class="btn btn-info" name="btnsubmit" value="Submit" style="width:15%">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </form>
        <br/>
        <div class="row">
            <div class="col-md-10 form-group">
                Generate Link : <input type = "text" class = "form-control"  name = "top_talk_link" id="top_talk_link">
                <input type = "hidden" id="hidden_url" value="<?php echo $url ?>">
            </div>
            <div class="col-md-2 form-group">
                <label></label>
                <input type = "button" class = "form-control btn btn-info"  name = "click" value="Generate" onclick="generate_link()">
            </div>
        </div>
    </div>
</div>

<?php
include_once './footer.php';
?>
<script>
    function generate_link() {
        $.ajax({
            url: 'ad_controller.php',
            type: 'post',
            data: {'action': 'generate_link', 'type': 0},
            success: function (res) {
                if (res) {
                    $("#top_talk_link").val($("#hidden_url").val() + '?vc=' + res);
                }
            }
        });
    }
</script>
