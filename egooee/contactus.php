<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>
<div class="container">
    <center><br><h3>Request to add Feed</h3><br></center>
    <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data" style="border:1px solid #e0dfdf">
        <!--Modal body--> 
        <div class="modal-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Choose Image :</label>
                        <input type="file" class="" id="b_logo"  name="b_logo" required="">
                        <input type="hidden" id="ad_type"  name="ad_type" value="2">
                    </div>
                </div>
                <div class="col-md-4">
                    Note: Image size 500*500 required
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">                                   
                        <label>First Name :</label>
                        <input type="text" class="form-control" name="fname" value="<?php echo (isset($_SESSION['fname'])) ? ucfirst($_SESSION['fname']) : ''; ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">                                   
                        <label>Last Name :</label>
                        <input type="text" class="form-control" name="lname" value="<?php echo (isset($_SESSION['lname'])) ? ucfirst($_SESSION['lname']) : ''; ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Business Name</label>
                        <select class="form-control" name="company_id">
                            <?php
                            $company_q = "select * from tbl_company where user_id=" . $_SESSION['user_id'];
                            $company_r = mysqli_query($con, $company_q);
                            while ($company_d = mysqli_fetch_assoc($company_r)) {
                                ?>
                                <option value="<?php echo $company_d['company_id'] ?>"><?php echo ucfirst($company_d['c_name']); ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6" id="post_name_div">
                    <div class="form-group">                                   
                        <input type="hidden" class="form-control" id="userid"  name="userid"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                        <label>Company Title :</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo (isset($_SESSION['title'])) ? ucwords($_SESSION['title']) : ''; ?>">
                    </div>
                </div>
                <div class="col-md-6" id="ad_enticements">
                    <div class="form-group"> 
                        <label>Choose Ad Influences </label>

                        <select class="form-control" name="enticements[]" id="enticements1" multiple="multiple" required="">
                            <?php
                            $encmt_sql1 = "select * from tbl_reg_category where status=1 group by cate_name";

                            $encmt_res1 = mysqli_query($con, $encmt_sql1);

                            $user_enfu_arr = json_decode($user_data['reg_category_ids']);

                            while ($encmt1 = mysqli_fetch_assoc($encmt_res1)) {

                                echo "<option value='" . $encmt1['cate_name'] . "'>" . ucwords($encmt1['cate_name']) . "</option>";
                            }
                            ?>
                        </select>    
                    </div>
                </div>



                <div class="col-md-6">
                    <div class="form-group">
                        <label>Phone Number :</label>
                        <input type="text" class="form-control" id="ad_phone"  name="ad_phone" required="" placeholder="Phone Number">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Describe your Ad :</label>
                        <textarea class="form-control" rows="2" placeholder="Describe your Ad" id="ad_message" name="ad_message"></textarea>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Website Link</label>
                        <input type="text" name="websitelink"  class="form-control">
                    </div>
                </div>
            </div>
            <div><a href="javascript:void(0);" id="flip">Select Target Audience</a></div>
            <div id="panel">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Country :</label>

                            <select name="dwcountry" class="form-control">
                                <option value="">--Select Country--</option>
                                <?php
                                $viewdata = "select * from countries;";
                                $resdata = mysqli_query($con, $viewdata);

                                while ($rowdata = mysqli_fetch_assoc($resdata)) {


                                    echo "<option value='" . $rowdata['country_id'] . "'>" . $rowdata['country_name'] . "</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>State/Province :</label>
                            <input type="text" class="form-control" placeholder="State/Province" name="state" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Zip Code :</label>
                            <input type="text" class="form-control" placeholder="Zip Code" name="zip_code" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group text-center">
                        <span><input type="checkbox" id="check1" value="1" required="" style="height: 25px;width: 25px;vertical-align: middle;"> I agree with the <a href="#">terms of service</a></span>

                    </div>
                </div>
                <div class="col-md-2">

                    <a href="index.php" class="btn btn-danger pull-right" style="margin-left:5px;">Cancel</a>
                    <input type="submit" class="btn btn-primary pull-right" id="post_submit" name="ad_submit" value="Submit">
                </div>
            </div>
            <!--<a href="javascript:void(0);" id="re_open_event">Post an Event</a>-->

        </div>


        <!--     Modal footer 
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-primary" id="post_submit" name="ad_submit" value="Submit">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                        </div><br>-->
    </form>




    <!--<form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data" style="border:1px solid #e0dfdf">
                                         Modal body 
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Choose Image :</label>
                                                        <input type="file" name="image" required="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    Note: Image size 700*700 required
                                                </div>
                                            </div>
                                            <div class="form-group">                                   
                                                <input type="hidden" class="form-control" name="user_id"  value="<?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? $_SESSION['user_id'] : ''; ?>">
                                                <label>Title :</label>
                                                <input type="text" class="form-control"  name="title" required="" placeholder="Title">
                                            </div>
                                            <div class="form-group">                                                                            
                                                <label>Description :</label>
                                                <textarea rows="2" class="form-control" name="desc" placeholder="Description"></textarea>
                                            </div>
                                            <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" style="margin-left:5px;margin-bottom: 10px;">Cancel</button>
                                            <input type="submit" class="btn btn-primary pull-right" name="feed_submit" value="Submit">
                                        </div>
    
                                    </form>-->



    <br><br>





</div>




<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Terms and Condition</h4>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                I agree with the <a href="#">terms of service</a>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="cl1" class="btn btn-danger" data-dismiss="modal">Ok</button>
            </div>

        </div>
    </div>
</div>


<?php
include_once './footer.php';
?>
<script>
    $(document).ready(function () {
        $("#flip").click(function () {

            $("#panel").slideDown("slow");
        });
        $("#check1").click(function () {
            if ($("#check1").prop("checked")) {
                $('#myModal').show();
            }
        });
        $("#cl1").click(function () {
            $('#myModal').hide();
            //alert("saa");
        });
    });
</script>