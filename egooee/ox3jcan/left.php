<?php
ob_start();

include '../connection.php';
$sql = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
$res = mysqli_query($con, $sql);

$row = mysqli_fetch_assoc($res);
$type = $row['user_type'];

$activePage = basename($_SERVER['PHP_SELF'], "");
?>

<style>
    .open .sub-menu .subtitle{
        display:none;
    }
    .active_color{
        color:#03a9f3 !important;
    }
</style>

<!-- Left Panel --> 
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default"> 
        <div id="main-menu" class="main-menu collapse navbar-collapse">




            <ul class="nav navbar-nav">
                <li class="<?= ($activePage == 'dashboard.php') ? 'active' : ''; ?>">
                    <a href="dashboard.php"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="<?= ($activePage == 'setting.php') ? 'active' : ''; ?>">
                           <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                    <a href="setting.php"> <i class="menu-icon fa fa-desktop"></i>Display Setting</a>
                </li>
                <!--<li class="menu-title">General</li> /.menu-title -->
                <!--
                                <li class="">
                <?php
                if ($type == 'admin') {
                    ?>
                                                                                            
                <?php } ?>
                
                
                                </li>-->
                <!--                <li class="menu-item-has-children dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Forms</a>
                                    <ul class="sub-menu children dropdown-menu">
                                        <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                                        <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                                    </ul>
                                </li>-->
                <li class="menu-item-has-children dropdown <?= ($activePage == 'manage_user.php' || $activePage == 'View_user.php') ? 'active show' : ''; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>User Information</a>
                    <ul class="sub-menu children dropdown-menu <?= ($activePage == 'manage_user.php' || $activePage == 'View_user.php') ? 'show' : ''; ?>">
                        <li class="<?= ($activePage == 'manage_user.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a>-->
                            <i class="menu-icon fa fa-user <?= ($activePage == 'manage_user.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_user.php') ? 'active_color' : ''; ?>" href="manage_user.php"> Manage Users</a>
                        </li>
                        <li class="<?= ($activePage == 'manage_user.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-user <?= ($activePage == 'View_user.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'View_user.php') ? 'active_color' : ''; ?>" href="View_user.php"> Users Company</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown <?= ($activePage == 'manage_ads.php' || $activePage == 'manage_ad_review.php' || $activePage == 'third_party_ads.php' || $activePage == 'manage_feed.php') ? 'active show' : ''; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-adn"></i>AD's</a>
                    <ul class="sub-menu children dropdown-menu <?= ($activePage == 'manage_ads.php' || $activePage == 'manage_ad_review.php' || $activePage == 'third_party_ads.php') || $activePage == 'manage_feed.php' ? 'show' : ''; ?>">

                        <li class="<?= ($activePage == 'manage_ads.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a>-->
                            <i class="menu-icon fa fa-adn <?= ($activePage == 'manage_ads.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_ads.php') ? 'active_color' : ''; ?>" href="manage_ads.php"> Manage Ads</a> 
                        </li>
                        <li class="<?= ($activePage == 'manage_ad_review.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-recycle <?= ($activePage == 'manage_ad_review.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_ad_review.php') ? 'active_color' : ''; ?>" href="manage_ad_review.php"> Manage AD Reviews </a>
                        </li>

                        <li class="<?= ($activePage == 'third_party_ads.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-recycle <?= ($activePage == 'third_party_ads.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'third_party_ads.php') ? 'active_color' : ''; ?>" href="third_party_ads.php">Third Party Ads </a>
                        </li>
                        <li class="<?= ($activePage == 'manage_feed.php') ? 'active' : ''; ?>">
                          <!--<i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a>-->
                            <i class="menu-icon fa fa-adn <?= ($activePage == 'manage_feed.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_feed.php') ? 'active_color' : ''; ?>" href="manage_feed.php"> Manage Biz Feed</a> 
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown <?= ($activePage == 'manage_post.php' || $activePage == 'manage_review.php') ? 'active show' : ''; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-sticky-note-o"></i>Post</a>
                    <ul class="sub-menu children dropdown-menu <?= ($activePage == 'manage_post.php' || $activePage == 'manage_review.php') ? 'show' : ''; ?>">

                        <li class="<?= ($activePage == 'manage_post.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a>-->
                            <i class="menu-icon fa fa-podcast <?= ($activePage == 'manage_post.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_post.php') ? 'active_color' : ''; ?>" href="manage_post.php"> Manage Business </a>
                        </li>
                        <li class="<?= ($activePage == 'manage_review.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-recycle <?= ($activePage == 'manage_review.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_review.php') ? 'active_color' : ''; ?>" href="manage_review.php"> Manage Reviews </a>
                        </li>
                    </ul>
                </li>
                <li class="<?= ($activePage == 'manage_classified.php') ? 'active' : ''; ?>">
                    <a href="manage_classified.php"><i class="menu-icon fa fa-tags"></i>classified</a>
                </li>
                <li class="menu-item-has-children dropdown <?= ($activePage == 'manage_event.php' || $activePage == 'manage_top_rated_business.php' || $activePage == 'manage_top_talk.php' || $activePage == 'manage_top_rated.php' || $activePage == 'manage_dish.php' || $activePage == 'manage_ad_category.php' || $activePage == 'manage_blog.php') ? 'active show' : ''; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-globe"></i>Home Page Setting</a>
                    <ul class="sub-menu children dropdown-menu <?= ($activePage == 'manage_event.php' || $activePage == 'manage_top_rated_business.php' || $activePage == 'manage_top_talk.php' || $activePage == 'manage_top_rated.php' || $activePage == 'manage_dish.php' || $activePage == 'manage_ad_category.php' || $activePage == 'manage_blog.php') ? 'show' : ''; ?>">
                        <li class="<?= ($activePage == 'manage_ad_category.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-thermometer-full <?= ($activePage == 'manage_ad_category.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_ad_category.php') ? 'active_color' : ''; ?>" href="manage_ad_category.php"> Event Type</a>
                        </li>
                        <li class="<?= ($activePage == 'manage_event.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a>-->
                            <i class="menu-icon fa fa-envira <?= ($activePage == 'manage_event.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_event.php') ? 'active_color' : ''; ?>" href="manage_event.php"> Upcoming Event</a>
                        </li>
                        <li class="<?= ($activePage == 'manage_top_rated_business.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-building <?= ($activePage == 'manage_top_rated_business.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_top_rated_business.php') ? 'active_color' : ''; ?>" href="manage_top_rated_business.php"> Top Rated Business </a>
                        </li>
                        <li class="<?= ($activePage == 'manage_top_talk.php') ? 'active' : ''; ?>">
                           <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-tags <?= ($activePage == 'manage_top_talk.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_top_talk.php') ? 'active_color' : ''; ?>" href="manage_top_talk.php"> Top Talk</a>
                        </li>
                        <li class="<?= ($activePage == 'manage_top_rated.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-building <?= ($activePage == 'manage_top_rated.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_top_rated.php') ? 'active_color' : ''; ?>" href="manage_top_rated.php"> Trending </a>
                        </li>
                        <li class="<?= ($activePage == 'manage_dish.php') ? 'active' : ''; ?>">
                           <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-delicious <?= ($activePage == 'manage_dish.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_dish.php') ? 'active_color' : ''; ?>" href="manage_dish.php"> Manage Dish</a>
                        </li>
                        <li class="<?= ($activePage == 'manage_blog.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-black-tie <?= ($activePage == 'manage_blog.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_blog.php') ? 'active_color' : ''; ?>" href="manage_blog.php"> Manage Blog</a>
                        </li>

                    </ul>
                </li>
                <li class="menu-item-has-children dropdown <?= ($activePage == 'manage_plan_pricing.php' || $activePage == 'manage_business_category.php' || $activePage == 'manage_register_category.php' || $activePage == 'manage_countries.php' || $activePage == 'manage_seo.php') ? 'active show' : ''; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cog"></i>Setting</a>
                    <ul class="sub-menu children dropdown-menu <?= ($activePage == 'manage_plan_pricing.php' || $activePage == 'manage_business_category.php' || $activePage == 'manage_register_category.php' || $activePage == 'manage_countries.php' || $activePage == 'manage_seo.php') ? 'show' : ''; ?>">
                        <li class="<?= ($activePage == 'manage_plan_pricing.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a>-->
                            <i class="menu-icon fa fa-product-hunt <?= ($activePage == 'manage_plan_pricing.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_plan_pricing.php') ? 'active_color' : ''; ?>" href="manage_plan_pricing.php"> Manage plan price </a>
                        </li>

                        <li class="<?= ($activePage == 'manage_business_category.php') ? 'active' : ''; ?>">
                           <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-tachometer <?= ($activePage == 'manage_business_category.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_business_category.php') ? 'active_color' : ''; ?>" href="manage_business_category.php"> Business Type</a>
                        </li>
                        <li class="<?= ($activePage == 'manage_register_category.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-tachometer <?= ($activePage == 'manage_register_category.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_register_category.php') ? 'active_color' : ''; ?>" href="manage_register_category.php"> Influences</a>
                        </li>
                        <li class="<?= ($activePage == 'manage_countries.php') ? 'active' : ''; ?>">
                            <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-tachometer <?= ($activePage == 'manage_countries.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_countries.php') ? 'active_color' : ''; ?>" href="manage_countries.php"> Manage Countries</a>
                        </li>
<!--                        <li class="<?= ($activePage == 'setting.php') ? 'active' : ''; ?>">
                           <i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>
                            <i class="menu-icon fa fa-cog <?= ($activePage == 'setting.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'setting.php') ? 'active_color' : ''; ?>" href="setting.php"> Display Setting</a>
                        </li>-->
                        <li class="<?= ($activePage == 'seo.php') ? 'active' : ''; ?>">
                           <!--<i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a>-->
                            <i class="menu-icon fa fa-cog <?= ($activePage == 'manage_seo.php') ? 'active_color' : ''; ?>"></i><a class="<?= ($activePage == 'manage_seo.php') ? 'active_color' : ''; ?>" href="manage_seo.php"> Manage SEO</a>
                        </li>


                    </ul>
                </li>
                <li>
                    <a href="../index.php" target="_blank"><i class="menu-icon fa fa-home"></i>Go to website </a>
                </li>
                <!--                <li class="">
                
                
                <?php
                if ($type == 'admin') {
                    ?>
                                                                                            <a href="View_user.php"  ><i class="menu-icon fa fa-user"></i>Users Company</a>
                <?php } ?>
                
                
                                </li>-->

                <!--                <li>
                
                                    <a href="manage_ads.php"><i class="menu-icon fa fa-adn"></i>Manage Ads </a>
                
                                </li>-->
                <!--                <li class="">
                                    <a href="manage_post.php"><i class="menu-icon fa fa-podcast"></i>Manage Posts </a>
                                </li>-->

                <!--                <li class="">
                                    <a href="manage_review.php"><i class="menu-icon fa fa-recycle"></i>Manage Post Reviews </a>
                                </li>-->

                <!--                <li class="">
                                    <a href="manage_ad_review.php"><i class="menu-icon fa fa-recycle"></i>Manage AD Reviews </a>
                                </li>-->

                <!--                <li class="">
                                    <a href="manage_event.php"><i class="menu-icon fa fa-envira"></i>Upcoming Event</a>
                                </li>-->

                <?php
                if ($type == 'admin') {
                    ?>
                    <!--                    <li class="">
                                            <a href="manage_plan_pricing.php"><i class="menu-icon fa fa-product-hunt"></i>Manage plan price </a>
                                        </li>-->



                    <!--                    <li class="">
                                            <a href="manage_top_rated_business.php"><i class="menu-icon fa fa-building"></i>Top Rated Business </a>
                                        </li>-->

                    <!--                       <li class="">
                                               <a href="manage_restaurants.php"><i class="menu-icon fa fa-tasks"></i>Manage Business </a>
                                        </li>-->

                    <!--                    <li class="">
                                            <a href="manage_top_talk.php"><i class="menu-icon fa fa-tasks"></i>Top Talk</a>
                                        </li>-->

                    <!--                    <li class="">
                                            <a href="manage_dish.php"><i class="menu-icon fa fa-digg"></i>Manage Dish</a>
                                        </li>-->

                    <!--                    <li class="">
                                            <a href="manage_blog.php"><i class="menu-icon fa fa-black-tie"></i>Manage Blog</a>
                                        </li>-->

                    <!--                    <li class="">
                                            <a href="manage_ad_category.php"><i class="menu-icon fa fa-digg"></i>Manage AD Category</a>
                                        </li>-->

                    <!--                    <li class="">
                                            <a href="manage_business_category.php"><i class="menu-icon fa fa-digg"></i>Business Category</a>
                                        </li>-->

                    <!--                    <li class="">
                                            <a href="setting.php"><i class="menu-icon fa fa-cog"></i>Setting</a>
                                        </li>-->


                <?php } ?>

                <!--                <li class="">
                                    <a href="View_products.php"><i class="menu-icon fa fa-product-hunt"></i>Manage Products </a>
                                </li>
                
                
                
                
                                <li class="menu-title">Customers</li> /.menu-title 
                                <li>
                                    <a href="customer_req.php"> <i class="menu-icon fa fa-registered"></i>Requirements  </a>
                
                                </li>
                                <li>
                                    <a href="customer_orders_a.php"> <i class="menu-icon fa fa-first-order"></i>Customer Orders  </a>
                
                                </li>
                                <li>
                                    <a href="customize_payment_a.php"> <i class="menu-icon fa fa-first-order"></i>Customize Payments  </a>
                
                                </li>-->



                <!--               
                                <li>
                                    <a href="reports.php"> <i class="menu-icon fa fa-pie-chart"></i>Reports
                                    </a>
              





            </ul>
        </div><!-- /.navbar-collapse -->
                </nav>
                </aside><!-- /#left-panel --> 
                <!-- Left Panel -->




