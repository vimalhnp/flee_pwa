<?php
ob_start();
session_start();


include '../connection.php';
if (isset($_SESSION['uname_admin'])) {

    $sql = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $res = mysqli_query($con, $sql);

    $row = mysqli_fetch_assoc($res);
    $type = $row['user_type'];
} else {
    header('Location:index.php');
}



if ($type == "admin") {

    $all = "SELECT tbl_user.fname,tbl_user.lname,tbl_category.name,tbl_post.* FROM `tbl_post` INNER JOIN tbl_category on tbl_category.category_id=tbl_post.b_type INNER JOIN tbl_user on tbl_user.user_id=tbl_post.user_id where tbl_post.type=3";
}

$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {

        $dataview .= "<tr>";
        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['post_id'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= "<img src='" . '../' . $rowall['image'] . "' style='height:60px; width:60px;'>";
        $dataview .= "</td>";

        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
        $n = $rowall['fname'] . " " . $rowall['lname'];
        $dataview .= ucwords($n);
        $dataview .= "</td>";
        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['name']);
        $dataview .= "</td>";

        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['b_name']);
        $dataview .= "</td>";
//        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
//        $allowedlimit = 29;
//        $dataview .= (mb_strlen($rowall['b_message']) > $allowedlimit) ? mb_substr($rowall['b_message'], 0, $allowedlimit) . "...." : $rowall['b_message'];
//        //$dataview .= ucwords($rowall['b_message']);
//        $dataview .= "</td>";
        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['b_address']);
        $dataview .= "</td>";
        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['b_phone'];
        $dataview .= "</td>";
//        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
//        $dataview .= ucwords($rowall['state']);
//        $dataview .= "</td>";
//        $dataview .= "<td onclick='postmodalopen(" . $rowall['post_id'] . ");return false;' data-toggle='modal'>";
//        $dataview .= $rowall['zipcode'];
//        $dataview .= "</td>";


        $dataview .= "<td style=width:150px;>";


        if ($rowall['status'] == "pending") {
            $dataview .= "<p class='text-danger'>Pending</p>";
        } else if ($rowall['status'] == 2) {
            $dataview .= "<p class='text-danger'>Rejected</p>";
        } else if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_post.php?id=" . $rowall['post_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_post.php?id=" . $rowall['post_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['status'] == "1") {
            $dataview .= "&nbsp;<a href='update_post.php?uid=" . $rowall['post_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        if ($type == 'admin') {
            $dataview .= "&nbsp;<a href='manage_post.php?did=" . $rowall['post_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
        }

        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_post where post_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_post.php');
        }

        $dataview .= "</td>";

        $dataview .= "</tr>";
        ?>


        <?php
    }
}

//check

if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_post set status='0' where post_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_post set status='1' where post_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_post.php');
    } else {
        echo "Not";
        //exit();
    }
}

include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>

                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Manage Business</strong>
                                    </div>
                                    <div class="card-body">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                    <th>User Name</th>
                                                    <th>Business Type</th>
                                                    <th>Business Name</th>
<!--                                                    <th>Message</th>-->
                                                    <th>Address</th>
                                                    <th>Contact No</th>
<!--                                                    <th>State</th>
                                                    <th>Zip Code</th>-->
                                                    <!--<th>Add_on</th>-->
<!--                                                    <th>Modified on</th>-->
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->





                <div class="modal" id="postpopup">

                    <div class=" modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Business Information</h4>

                            </div>
                            <div class="modal-body">
                                <table class="table">
                                    <tr>

                                        <td>
                                            User name :
                                        </td>
                                        <td id="post_cname">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Business Type :
                                        </td>
                                        <td id="post_btype">

                                        </td>
                                    <tr>

                                        <td>
                                            Business Names :
                                        </td>
                                        <td id="post_bname">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Message :
                                        </td>
                                        <td id="post_message">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Address :
                                        </td>
                                        <td id="post_address">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Contact NO :
                                        </td>
                                        <td id="post_phone">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            State :
                                        </td>
                                        <td id="post_state">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Zip Code :
                                        </td>
                                        <td id="post_zipcode">

                                        </td>
                                    </tr>
                                </table>

                            </div>


                        </div>

                    </div>

                </div>




            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }



    function postmodalopen(post_id) {
        $("#postpopup").modal("show");
        //alert(post_id);
        $.ajax({
            url: 'ajax.php',
            type: 'post',
            data: {'action': 'get_post_data', 'idpost': post_id},
            success: function (res) {
                if (res) {
                    var result = $.parseJSON(res);
                    console.log(result);
                    $("#post_cname").text(result['fname']);
                    $("#post_btype").text(result['b_type']);
                    $("#post_bname").text(result['b_name']);
                    $("#post_message").text(result['b_message']);
                    $("#post_address").text(result['b_address']);
                    $("#post_state").text(result['state']);
                    $("#post_phone").text(result['b_phone']);
                    $("#post_zipcode").text(result['zipcode']);
                }
            }
        });
    }


</script>