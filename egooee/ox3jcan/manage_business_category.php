<?php
ob_start();
session_start();

include '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    
} else {
    header('Location:index.php');
}

$all = "SELECT * from tbl_category  ";
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr>";
        $dataview .= "<td>";
        $dataview .= $rowall['category_id'];
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucwords($rowall['name']);
        $dataview .= "</td>";

        $dataview .= "<td>";

        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_business_category.php?id=" . $rowall['category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_business_category.php?id=" . $rowall['category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['status'] == 1) {
            $dataview .= "&nbsp;<a href='manage_business_category.php?cnid=" . $rowall['category_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        $dataview .= "&nbsp<a href='manage_business_category.php?did=" . $rowall['category_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";

        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_category where category_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_business_category.php');
        }

        $dataview .= "</td>";

        $dataview .= "</tr>";
    }
}

if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_category set status='0' where category_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_category set status='1' where category_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_business_category.php');
    } else {
        echo "Not";
        exit();
    }
}

include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        $block = 'd-none';
        if (isset($_GET['cnid'])) {
            $s10 = "select * from tbl_category where category_id='" . $_GET['cnid'] . "'";
            $r10 = mysqli_query($con, $s10);

            $ra10 = mysqli_fetch_assoc($r10);
            $block = '';
        }
        ?>

        <div class="content pb-0"> 
            <section>
                <div class="row">
                    <div class="col-md-12 <?php echo $block ?>" id="form_div">
                        <div class="card">
                            <div class="card-header">
                                <center><strong>Add or Update </strong>Business Type</center>
                            </div>
                            <div class="card-body card-block">
                                <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal" id="fmreg">
                                    <div class="row form-group">
                                        <div class="col-12 col-md-6">
                                            Business Type :
                                            <input type="text" name="cn" id="cn" required=""  placeholder="Business Type"  class="form-control" value="<?php echo (isset($ra10['name']) && $ra10['name'] != '') ? $ra10['name'] : ''; ?>">
                                        </div>
                                        <div class="col-md-12" style="margin-top: 10px;">
                                            This type also add as Influences in male and female
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-md-12">
                                            <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                            <a class="btn btn-danger" href="manage_business_category.php" style="width:15%">Cancel</a>

                                        </div>
                                    </div>
                                    <?php
                                    if (isset($_POST['btnsubmit'])) {
                                        if (isset($_GET['cnid'])) {
                                            $sql2 = "UPDATE tbl_category set name='" . $_POST['cn'] . "',modify_on='" . date('Y-m-d') . "' where category_id='" . $_GET['cnid'] . "'";
                                        } else {
                                            $dd = date('Y-m-d');
                                            $esql1 = "insert into tbl_reg_category (cate_name,type,selected_by_default,added_on,modify_on,status) values('" . $_POST['cn'] . "','male',0,'$dd','$dd',1)";
                                            mysqli_query($con, $esql1);

                                            $esql2 = "insert into tbl_reg_category (cate_name,type,selected_by_default,added_on,modify_on,status) values('" . $_POST['cn'] . "','female',0,'$dd','$dd',1)";
                                            $eres2 = mysqli_query($con, $esql2);

                                            $sql2 = "insert into tbl_category (name,added_on,modify_on,status) values('" . $_POST['cn'] . "','$dd','$dd',1)";
                                        }
                                        $res2 = mysqli_query($con, $sql2);


                                        if ($res2) {

                                            header('Location:manage_business_category.php');
                                        } else {
                                            echo $sql2;
                                            echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                        }
                                    }
                                    ?>
                                </form>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Manage Business Type</strong><a href="javascript:void(0);" onclick="show_add_form();" class="btn btn-success" style="color: white;">Add Type</a>
                            </div>
                            <div class="card-body">   
                                <?php
                                $rr = "select * from tbl_category;";
                                $sq = mysqli_query($con, $rr);
                                ?>

                                <form action="#" method="post">
                                    <div class="row form-group">
                                        <div class="col-12 col-md-6">
                                            <strong>Which Business You Wants to Show in Home Page :</strong>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <select class="form-control" name="bcategory" style="">
                                                <option value="0">None</option>

                                                <?Php
                                                while ($re = mysqli_fetch_assoc($sq)) {

                                                    if ($re['show_status'] == 1) {
                                                        ?>

                                                        <option value="<?php echo $re['category_id'] ?>" selected=""><?php echo $re['name'] ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $re['category_id'] ?>" ><?php echo $re['name'] ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="submit"  class="btn btn-info" name="btn" value="Show" style="width:50%">
                                        </div>
                                    </div>
                                </form>
                                <?php
                                if (isset($_POST['btn'])) {

                                    $ca = $_POST['bcategory'];
                                    echo $ca;
                                    $z = "select * from tbl_category";
                                    $x = mysqli_query($con, $z);


                                    while ($ro = mysqli_fetch_assoc($x)) {

                                        if ($ro['category_id'] == $_POST['bcategory']) {
                                            $sq = "update tbl_category set show_status='1' where category_id='" . $ro['category_id'] . "'";
                                            $rq = mysqli_query($con, $sq);
                                        } else {
                                            $sq = "update tbl_category set show_status='0' where category_id='" . $ro['category_id'] . "'";
                                            $rq = mysqli_query($con, $sq);
                                        }
                                    }


                                    header('Location:manage_business_category.php');
                                    ?>

                                    <!--$sq="update tbl_category set show_status='1' where category_id='".$category."'";-->
                                <?php }
                                ?>

                                <br><br>

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Type</th>
    <!--                                        <th>Added On</th>
                                            <th>Modify On</th>-->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        echo $dataview;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">
    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }

    function show_add_form() {
        $("#cn").val('');
        $("#form_div").removeClass("d-none");
    }

</script>