<?php
ob_start();
session_start();
include '../connection.php';

if (isset($_SESSION['uname_admin'])) {


    $sql = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $res = mysqli_query($con, $sql);

    $row = mysqli_fetch_assoc($res);
    $type = $row['user_type'];
} else {
    header('Location:index.php');
}

$sql1 = "select count(user_id) as uid from tbl_user ";
$res1 = mysqli_query($con, $sql1);

$row = mysqli_fetch_assoc($res1);
$user = $row['uid'];



$ip_all = file("../ip.txt");
$ip_cur = $_SERVER['REMOTE_ADDR'] . "\n";
if (!in_array($ip_cur, $ip_all)) {
    $ip_all[] = $ip_cur;
    file_put_contents("../ip.txt", implode($ip_all));
}
include './head.php';
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <?php
                if ($type == 'admin') {
                    ?>
                    <div class="orders">
                        <div class="row" style="min-height: 500px;">
                            <div class="col-sm-12 col-lg-3">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mr-4">
                                                <small>Number Of</small>
                                                <h4 class="mb-0"><i class="fa fa-user"></i> <?php echo $user ?></h4>
                                            </div>
                                            <div class="chart ml-auto">
                                                User
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-12 col-lg-3">
                                <div class="card card-hover">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="mr-4">
                                                <small>Number Of</small>
                                                <h4 class="mb-0"><i class="fa fa-user"></i> <?php echo count($ip_all) ?></h4>
                                            </div>
                                            <div class="chart ml-auto">
                                                Views
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
<!--                            <div class="col-xl-12"> 
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="box-title" style="color: blue;">Dashboard</h4>
                                                                                    <h6 class="pull-right">Click order id for view details in order</h6>
                                    </div>
                                    <div class="card-body--">
                                        <h4 class="" style="margin-left: 20px;" >Total Number Of User : <?php echo $user ?></h4>
                                        <br>
                                        <h4 class="" style="margin-left: 20px;" >Total Number Of Views : <?php echo count($ip_all) ?></h4>
                                        <br><br>
                                    </div>
                                </div>  /.card 
                            </div>   /.col-lg-8 -->


                        </div> 
                    </div> <!-- /.order -->
                    <?php
                } else {
                    echo "User Dashboard";
                }
                ?>
            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
