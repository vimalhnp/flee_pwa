<?php
ob_start();
session_start();

include_once '../connection.php';

if (isset($_SESSION['uname_admin'])) {

    $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $r = mysqli_query($con, $s);

    $ro = mysqli_fetch_assoc($r);

    $type = $ro['user_type'];
} else {
    header('Location:index.php');
}


if ($type == "admin") {
    $all = "select tbl_user.fname,tbl_user.lname,tbl_dish.* from tbl_dish inner join tbl_user on tbl_dish.user_id=tbl_user.user_id order by tbl_dish.category,tbl_dish.dish_id";
}
$resall = mysqli_query($con, $all);

$dataview = "";
$categories_printed = array();
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $c = $rowall['category'];

        if (!in_array($c, $categories_printed)) {
            if (count($categories_printed) > 0) {
                $dataview .= '</tbody></table></div></div>';
            }
            $categories_printed[] = $c;
            if (strrchr($c, 'us')) {
                $c = strtoupper($c);
            } else {
                $c = ucfirst($c);
            }
//            $dataview.= '<center><h4 style="margin-bottom: 20px;padding:20px;">' . ucfirst($c) . ' Dishes </h4></center>';
            $dataview .= '<div class="card"><div class="card-header">
                                        <strong class="card-title" style="margin:o auto;">' . $c . ' Dish</strong><a class="btn btn-success" href="add_dish.php" style="color: white;margin-left:50px;">Add Dish</a>
                                    </div>
                                    <div class="card-body">';
            $dataview .= '<table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                
                                                    <th>Image</th>
                                                    <th>Restaurant Name</th>
                                                    <th>Dish Name</th>
                                                    <th>Dish Type</th>
                                                    <th>Location</th>
                                                    <th>Contact No</th>
                                                    <th>Action</th>                                  
                                                   </tr>
                                            </thead>
                                            <tbody>';
        }


        $dataview .= "<tr>";

        $dataview .= "<td onclick='dishmodalopen(" . $rowall['dish_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= "<img src='" . '../' . $rowall['image'] . "' style='height:60px; width:60px;'>";
        $dataview .= "</td>";

        $dataview .= "<td onclick='dishmodalopen(" . $rowall['dish_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['res_name']);
        $dataview .= "</td>";
        $dataview .= "<td onclick='dishmodalopen(" . $rowall['dish_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['dish_name']);
        $dataview .= "</td>";
        $dataview .= "<td onclick='dishmodalopen(" . $rowall['dish_id'] . ");return false;' data-toggle='modal'>";
        if (strrchr($rowall['category'], 'us')) {
            $dataview .= strtoupper($rowall['category']);
        } else {
            $dataview .= ucwords($rowall['category']);
        }

        $dataview .= "</td>";



        $dataview .= "<td onclick='dishmodalopen(" . $rowall['dish_id'] . ");return false;' data-toggle='modal'>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['location']) > $allowedlimit) ? mb_substr($rowall['location'], 0, $allowedlimit) . "...." : $rowall['location'];
        // $dataview .= ucwords($rowall['b_address']);
        $dataview .= "</td>";
        $dataview .= "<td onclick='dishmodalopen(" . $rowall['dish_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['phone'];
        $dataview .= "</td>";



        $dataview .= "<td>";


        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_dish.php?id=" . $rowall['dish_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_dish.php?id=" . $rowall['dish_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['status'] == "1") {
            $dataview .= "&nbsp;<a href='add_dish.php?uid=" . $rowall['dish_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        if ($type == 'admin') {
            $dataview .= "&nbsp<a href='manage_dish.php?did=" . $rowall['dish_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
        }



        $dataview .= "</td>";

        $dataview .= "</tr>";
        ?>



        <?php
    }
    $dataview .= '</tbody></table>';
    if (isset($_GET['did'])) {

        $up1 = "delete from tbl_dish where dish_id='" . $_GET['did'] . "'";
        $resultup1 = mysqli_query($con, $up1);
        header('location:manage_dish.php');
    }
}

//check

if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_dish set status='0' where dish_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_dish set status='1' where dish_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_dish.php');
    } else {
        echo "Not";
        //exit();
    }
}





include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <?php
                                echo $dataview;
                                ?>
                            </div>


                        </div>
                    </div><!-- .animated -->





            </section>
        </div>



        <div class="clearfix"></div>
    </div>
    <div class="modal" id="dishpopup">
        <div class="modal-dialog">


            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dish Information</h4>
                </div>
                <div class="modal-body">
                    <!--<h1>BODY HERE</h1>-->
                    <?php
                    $ed = $rowall['dish_id'];
                    $s = "select tbl_user.fname,tbl_user.lname,tbl_dish.* from tbl_dish inner join tbl_user on tbl_dish.user_id=tbl_user.user_id where tbl_dish.dish_id='$ed'";
                    $r = mysqli_query($con, $s);

                    $row1 = mysqli_fetch_assoc($r);
                    ?>
                    <table class="table">
                        <tr>
                            <td>
                                User Name :
                            </td>
                            <td id="dish_user">
                                <?php echo ($row1['fname']) . " " . ($row1['lname']); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Restaurant Name :
                            </td>
                            <td id="dish_res">
                                <?php echo ucfirst($row1['res_name']); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Dish Name :
                            </td>
                            <td id="dish_name">
                                <?php echo ucfirst($row1['dish_name']); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Dish Type :
                            </td>
                            <td id="dish_category">
                                <?php echo ucfirst($row1['category']); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Message :
                            </td>
                            <td id="dish_message">
                                <?php echo ucfirst($row1['message']); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Location :
                            </td>
                            <td id="dish_location">
                                <?php echo ucfirst($row1['location']); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Contact No :
                            </td>
                            <td id="dish_phone">
                                <?php echo ucfirst($row1['phone']); ?>
                            </td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
    </div>
    <?php
    include('script.php');
    ?>


    <script language="JavaScript" type="text/javascript">
        function checkDelete() {
            return confirm('Are you sure want to delete?');
        }

        function dishmodalopen(dish_id) {
            $("#dishpopup").modal("show");
//            $("#dishpopup").show();
            $.ajax({
                url: 'dish_ajax.php',
                type: 'post',
                data: {'action': 'get_dish_data', 'dish_id': dish_id},
                success: function (res) {
                    if (res) {
                        var result = $.parseJSON(res);
//                        console.log(result);
                        $("#dish_user").text(result['fname'] + ' ' + result['lname']);
                        $("#dish_category").text(result['category']);
                        $("#dish_message").text(result['message']);
                        $("#dish_location").text(result['location']);
                        $("#dish_name").text(result['dish_name']);
                        $("#dish_phone").text(result['phone']);
                        $("#dish_res").text(result['res_name']);
                    }
                }
            });
        }

    </script>
    <?php
    include ('footer.php');
    ?>