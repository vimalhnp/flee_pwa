<?php
ob_start();
session_start();

include '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    
} else {
    header('Location:index.php');
}


$all = "SELECT * from setting  ";
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr>";
        $dataview .= "<td>";
        $dataview .= $rowall['setting_id'];
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucwords($rowall['type']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucwords($rowall['text']);
        $dataview .= "</td>";




        $dataview .= "<td>";



        if ($rowall['status'] == 1 && $rowall['setting_id'] != 9) {
            $dataview .= "<a href='setting.php?id=" . $rowall['setting_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0 && $rowall['setting_id'] != 9) {
            $dataview .= "<a href='setting.php?id=" . $rowall['setting_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['setting_id'] == 5 || $rowall['setting_id'] == 7) {
            $dataview .= "&nbsp;&nbsp;<a href='setting.php?cnid=" . $rowall['setting_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;&nbsp;&nbsp;";
        }
        if ($rowall['type'] == 'pricing title' || $rowall['setting_id'] == 8) {
            $dataview .= "&nbsp;&nbsp;<a href='setting.php?cnid=" . $rowall['setting_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;&nbsp;&nbsp;";
        }
        if ($rowall['type'] == 'limit influence' || $rowall['setting_id'] == 9) {
            $dataview .= "&nbsp;&nbsp;<a href='setting.php?cnid=" . $rowall['setting_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;&nbsp;&nbsp;";
        }



        $dataview .= "</td>";

        $dataview .= "</tr>";
    }
}
$all1 = "SELECT * from countries where country_id='231'";
$resall1 = mysqli_query($con, $all1);

$dataview1 = "";
if (mysqli_num_rows($resall1) > 0) {
    $rowall1 = mysqli_fetch_assoc($resall1);
    $dataview1 .= "<tr>";
    $dataview1 .= "<td>";
    $dataview1 .= $rowall1['country_name'];
    $dataview1 .= "</td>";

    $dataview1 .= "<td>";

    if ($rowall1['enable_state'] == 1) {
        $dataview1 .= "<a href='setting.php?cid=" . $rowall1['country_id'] . "&st=" . $rowall1['enable_state'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
    } else if ($rowall1['enable_state'] == 0) {
        $dataview1 .= "<a href='setting.php?cid=" . $rowall1['country_id'] . "&st=" . $rowall1['enable_state'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
    }

    $dataview1 .= "</td>";

    $dataview1 .= "</tr>";
}



if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update setting set status='0' where setting_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update setting set status='1' where setting_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:setting.php');
    } else {
        echo "Not";
        exit();
    }
}
if (isset($_GET['cid'])) {

    if ($_GET['st'] == 1) {

        $up2 = "update countries set enable_state='0' where country_id='" . $_GET['cid'] . "'";
    } else {
        $up2 = "update countries set enable_state='1' where country_id='" . $_GET['cid'] . "'";
    }
    $resultup2 = mysqli_query($con, $up2);

    if ($resultup2) {
        header('Location:setting.php');
    } else {
        echo "Not";
        exit();
    }
}



include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');

        $block = 'd-none';
        if (isset($_GET['cnid'])) {
            $s10 = "select * from setting where setting_id='" . $_GET['cnid'] . "'";
            $r10 = mysqli_query($con, $s10);

            $ra10 = mysqli_fetch_assoc($r10);
            $block = '';
        }
        ?>

        <div class="content pb-0"> 
            <section>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12 <?php echo $block ?>">
                            <div class="card">
                                <div class="card-header">
                                    <center><strong>Update </strong>setting</center>
                                </div>
                                <div class="card-body card-block">

                                    <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal">

                                        <div class="row">

                                            <div class="col-12 col-md-6 form-group">
                                                <?php echo (isset($ra10['type']) && $ra10['type'] != '') ? ucwords($ra10['type']) : '';  ?> :
                                                <textarea name="cr" rows="3" class="form-control"><?php echo (isset($ra10['text']) && $ra10['text'] != '') ? $ra10['text'] : ''; ?></textarea>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col col-md-12">
                                                <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                                <a class="btn btn-danger" href="setting.php" style="width:15%">Cancel</a>

                                            </div>
                                        </div>
                                        <?php
                                        if (isset($_POST['btnsubmit'])) {


                                            if (isset($_GET['cnid'])) {
                                                $sql2 = "UPDATE setting set text='" . $_POST['cr'] . "' where setting_id='" . $_GET[cnid] . "'";
                                            }
                                            $res2 = mysqli_query($con, $sql2);


                                            if ($res2) {

                                                header('Location:setting.php');
                                            } else {
                                                //  echo $sql2;
                                                echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                            }
                                        }
                                        ?>


                                    </form>
                                </div>




                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong class="card-title">Setting</strong>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Country Name</th>
                                                <th style="min-width: 70px;">Enable State</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            echo $dataview1;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>NO</th>
                                                <th>Type</th>
                                                <th>Text</th>
                                                <th style="min-width: 100px;">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            echo $dataview;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>


                    </div>
                </div><!-- .animated -->




            </section>
        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function show_add_form() {
        $("#cn").val('');
        $("#form_div").removeClass("d-none");
    }

</script>