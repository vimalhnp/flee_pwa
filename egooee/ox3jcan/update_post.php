<?php
ob_start();
session_start();


include_once '../connection.php';
if (isset($_SESSION['uname_admin'])) {

    if (isset($_GET['uid'])) {

        $updateq = "select * from tbl_post where post_id='" . $_GET['uid'] . "'";
        $updater = mysqli_query($con, $updateq);

        $row1 = mysqli_fetch_assoc($updater);
    } else {
        header('Location:index.php');
    }
} else {
    header('Location:index.php');
}include './head.php';
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong>Update </strong>Business</center>
                        </div>
                        <div class="card-body card-block">




                            <form action="#" enctype="multipart/form-data"  method="post" class="form-horizontal" id="fmreg">


                                <div class="row form-group">

                                   
                                    <div class="col-12 col-md-6">
                                        Business Name:<input type="text" name="bname" required=""  placeholder="Business Name"  value="<?php echo ucfirst($row1['b_name']); ?>" class="form-control">
                                    </div>
                                     <div class="col-12 col-md-6">
                                        Contact No : <input class="form-control" required=""  value="<?php echo $row1['b_phone']; ?>" maxlength="20" minlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  name="bphone"></div>
                                </div>





                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                        Business Message : <textarea class="form-control" required=""    name="bmessage" placeholder="Ad Message"><?php echo ucfirst($row1['b_message']); ?></textarea></div>

                                    <div class="col-12 col-md-6">
                                        Business Address : <textarea class="form-control"  required=""  name="baddress" placeholder="Business Address"><?php echo ucfirst($row1['b_address']); ?></textarea></div>
                                </div>
                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                        State :<input type="text" name="state" required=""  placeholder="state"  value="<?php
                                        if (isset($_GET['uid'])) {
                                            echo ucfirst($row1['state']);
                                        }
                                        ?>" class="form-control">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        Zip Code : <input class="form-control" required="" placeholder="Zip Code"  value="<?php
                                        if (isset($_GET['uid'])) {
                                            echo $row1['zipcode'];
                                        }
                                        ?>"   name="zip"></div>

                                </div>
                                <div class="row form-group">

                                   
                                    <div class="col-12 col-md-6">
                                        Image : 
                                        <!--                                        <div id="logo_preview" class="text-center">
                                                                                    <img class="" src="<?php echo '../' . $row1['image'] ?>" alt="Image Not Found" onclick="logoimage();" id="blah" style="height: 70px;" class="img-responsive">
                                                                                </div>-->
                                        <br><input type="file" class="" id="add_logo"  name="add_logo"  >

                                    </div>

                                </div>




                                <center>

                                    <div class="col col-md-12">


                                        <input type="submit"  class="btn btn-success" name="btnsubmit" value="Update" style="width:15%">
                                        <a class="btn btn-danger" href="manage_post.php" style="width:15%">Cancel</a>

                                    </div>
                                    <?php
                                    if (isset($_POST['btnsubmit'])) {


                                        $a = "";
                                        if ($_FILES['add_logo']['name'] != "") {

                                            $filename = md5(time()) . basename($_FILES['add_logo']['name']);
                                            $tmpname = ($_FILES['add_logo']['tmp_name']);
                                            $dir = "../img/product/";
                                            $d1 = "img/product/" . $filename;
                                            $filepath = $dir . $filename;
                                            $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
                                            if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
                                                $error = "Image format not allowed";
                                            } else {
                                                $stat = move_uploaded_file($tmpname, $filepath);
                                                $a = ', image="' . $d1 . '"';
                                            }
                                        }
                                        $sql2 = "update tbl_post set  state='".$_POST['state']."',zipcode='".$_POST['zip']."',b_name='" . $_POST['bname'] . "'" . $a . ",b_phone='" . $_POST['bphone'] . "',b_message='" . $_POST['bmessage'] . "',b_address='" . $_POST['baddress'] . "',modify_on='" . date('Y-m-d') . "' where post_id='" . $_GET['uid'] . "'";
                                        $res2 = mysqli_query($con, $sql2);


                                        if ($res2) {
                                            header('Location:manage_post.php');
                                        } else {
                                            echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                        }
                                    }
                                    ?>
                                </center>

                            </form>
                        </div>


                        </center>

                    </div>
                </div>
            </section>


        </div>



        <div class="clearfix"></div>



    </div>
    <?php
    include('script.php');

    include ('footer.php');
    ?>


    <script>
        function logoimage() {
            $("#add_logo").click();
            $("#add_logo").change(function () {
                readURL(this);
            });
            //console.log("ok");
        }

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>