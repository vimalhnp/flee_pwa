<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>EGOOEE</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="apple-touch-icon" href="../img/favicon.ico">
<link rel="shortcut icon" href="../img/favicon.ico">

<link rel="stylesheet" href="assets/css/normalize.css">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">


<link href="assets/weather/css/weather-icons.css" rel="stylesheet" />
<link href="assets/calendar/fullcalendar.css" rel="stylesheet" />


<link href="assets/css/charts/chartist.min.css" rel="stylesheet"> 
<link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet"> 
<!--   -->




<link rel="stylesheet" href="assets/css/flag-icon.min.css">
<link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
<link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css"> 
<!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
<link rel="stylesheet" href="assets/css/style.css">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>






</head>





<!--   -->

<style>
    #weatherWidget .currentDesc {
        color: #ffffff!important;
    }
    .traffic-chart { 
        min-height: 335px; 
    }
    #flotPie1  {
        height: 150px;
    } 
    #flotPie1 td {
        padding:3px;
    }
    #flotPie1 table {
        top: 20px!important;
        right: -10px!important;
    }
    .chart-container {
        display: table;
        min-width: 270px ;
        text-align: left;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    #flotLine5  {
        height: 105px;
    } 

    #flotBarChart {
        height: 150px;
    }
    #cellPaiChart{
        height: 160px;
    }

    .card .card-header  a{
        margin-left: 50px !important;
        
    }

</style>