<?php
ob_start();
session_start();


include_once '../connection.php';
if (isset($_SESSION['uname_admin'])) {

    if (isset($_GET['uid'])) {
        // $u1=$_GET['uid'];

        $s = "select * from tbl_user where user_id='" . $_GET['uid'] . "'";
        $r = mysqli_query($con, $s);

        $ro = mysqli_fetch_assoc($r);
        $u1 = $ro['email'];
        $u2 = $ro['user_id'];
    } else {

        $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
        $r = mysqli_query($con, $s);

        $ro = mysqli_fetch_assoc($r);
        $u2 = $ro['user_id'];



        $u1 = $_SESSION['uname_admin'];
    }

    $updateq = "select tbl_user.*,tbl_user.email as u_email,tbl_company.*,tbl_company.email as com_email,tbl_category.* from tbl_user inner JOIN tbl_company on tbl_company.user_id=tbl_user.user_id INNER JOIN tbl_category on tbl_category.category_id=tbl_company.category_id where tbl_company.company_id='" . $_GET['uid'] . "'";
    $updater = mysqli_query($con, $updateq);

    $row1 = mysqli_fetch_assoc($updater);
} else {
    header('Location:index.php');
}

include('head.php');
?>

<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong>Update </strong>Profile</center>
                        </div>
                        <div class="card-body card-block">




                            <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal" id="fmreg">



                                <div class="row form-group">
                                    <div class="col-12 col-md-6">
                                        Company Name : <input class="form-control" required=""  placeholder="Company Name" value="<?php echo $row1['c_name']; ?>"  name="cname"></div>
                                        <div class="col-12 col-md-6">
                                        Web Link : <input class="form-control"   placeholder="Web Link" value="<?php echo $row1['weburl']; ?>"  name="web"></div>
                                </div>

<!--                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                        First name<input type="text" name="fname" required=""  placeholder="First Name"  value="<?php echo ucfirst($row1['fname']); ?>" class="form-control">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        Last Name : <input class="form-control" required=""  placeholder="Last Name" value="<?php echo $row1['lname']; ?>"  name="lname"></div>

                                </div>-->





                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                        User Email : <input class="form-control" required="" value="<?php echo ucfirst($row1['u_email']); ?>" disabled=""  name="email" placeholder="Enter Email"></div>

                                    <div class="col-12 col-md-6">
                                        Company Email : <input class="form-control" required="" value="<?php echo ucfirst($row1['com_email']); ?>"   name="cemail" placeholder="Company Email"></div>


                                </div>

                                <div class="row form-group">



                                    <div class="col-12 col-md-6">
                                        Company Address : <textarea class="form-control" required=""  placeholder="Company Address"   name="caddress"><?php echo $row1['c_address']; ?></textarea></div>
                                    <div class="col-12 col-md-6">
                                        Description :<textarea class="form-control" required=""  placeholder="Company Description"   name="cdesc"><?php echo $row1['c_description']; ?></textarea></div>
                                </div>

                                <div class="row form-group">



                                    <div class="col-12 col-md-6">
                                        Country : 
                                        <select name='country' class='form-control'>
                                           <?php
                                            $sql = "select * from countries";
                                            $res = mysqli_query($con, $sql);
                                            if (mysqli_num_rows($res)) {
                                                while ($data = mysqli_fetch_assoc($res)) {

                                                    if ($data['country_id'] == $row1['country_id']) {
                                                        ?>
                                                        <option value="<?php echo $data['country_id'] ?>" selected=""><?php echo $data['country_name'] ?></option>  
                                                        <?Php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $data['country_id'] ?>"><?php echo $data['country_name'] ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                            
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        zip Code :<input class="form-control"   placeholder="Zip Code"   name="zip" value="<?php echo ucfirst($row1['zipcode']); ?>" ></div>
                                </div>


                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                      Business Type : <!--Category :<input class="form-control" required=""  placeholder="category" value="<?php echo $row1['name']; ?>"  name="ctitle"></div>-->
                                        <select class="form-control" name="category">
                                            <?php
                                            $sql = "select * from tbl_category";
                                            $res = mysqli_query($con, $sql);
                                            if (mysqli_num_rows($res)) {
                                                while ($data = mysqli_fetch_assoc($res)) {

                                                    if ($data['category_id'] == $row1['category_id']) {
                                                        ?>
                                                        <option value="<?php echo $data['category_id'] ?>" selected=""><?php echo $data['name'] ?></option>  
                                                        <?Php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $data['category_id'] ?>"><?php echo $data['name'] ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>

                                        </select>

                                    </div>

                                    <div class="col-12 col-md-6">
                                        <!--Logo  : <br><input  type="file" ></div>-->
                                               <!--<img class="" src="img/no_logo.png" alt="Image Not Found" onclick="b_logoimage();" id="ad_logo" style="height: 70px;" class="img-responsive">-->
                                    <!--<img src="../img/no_logo.png" alt="Image Not Found" style="height: 70px;" onclick="file1();" id="img1"  >-->
                                        Logo : 
                                        <!--                                        <div id="logo_preview" class="text-center">
                                                                                    <img class="" src="<?php echo '../' . $row1['c_logo'] ?>" alt="Image Not Found" onclick="logoimage();" id="blah" style="height: 70px;" class="img-responsive">
                                                                                </div>-->
                                        <br><input type="file" class="" id="add_logo"  name="add_logo" >

                                    </div>






                                    <div class="col col-md-12">
                                        <center>    
                                            <br><input type="submit"  class="btn btn-success" name="btnsubmit" value="Update" style="width:15%">
                                            <a class="btn btn-danger " href="View_user.php" style="width:15%">Cancel</a>
                                        </center>
                                    </div>
                                    <?php
                                    if (isset($_POST['btnsubmit'])) {
                                        // print_r($_FILES);
                                        //  exit();


                                        $set = "";
                                        $a = "";
                                        if ($_FILES['add_logo']['name'] != "") {




                                            if ($_FILES['add_logo']['name'] != "") {
                                                $filename = md5(time()) . basename($_FILES['add_logo']['name']);
                                                $tmpname = ($_FILES['add_logo']['tmp_name']);
                                                $dir = "../img/product/";
                                                $d1 = "img/product/" . $filename;
                                                $filepath = $dir . $filename;
                                                $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
                                                if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
                                                    $error = "Image format not allowed";
                                                } else {
                                                    $stat = move_uploaded_file($tmpname, $filepath);
                                                    $a = ',c_logo="' . $d1 . '"';
                                                }
                                                $set = ",";
                                            }
                                        }


                                        $ddd= date('Y-m-d');
                                        // var_dump($stat);
                                        //  exit();
//                                        $sql2 = "update tbl_user set fname='" . $_POST['fname'] . "',lname='" . $_POST['lname'] . "',modify_on='" . $ddd . "' where email='" . $u1 . "'";
//                                        $res2 = mysqli_query($con, $sql2);

                                        $sql3 = "update tbl_company set weburl='".$_POST['web']."',country_id='".$_POST['country']."',zipcode='".$_POST['zip']."',email='" . $_POST['cemail'] . "',c_name='" . $_POST['cname'] . "',category_id='" . $_POST['category'] . "',c_description='" . $_POST['cdesc'] . "'" . $a . ",c_address='" . $_POST['caddress'] . "',modify_on='" . date('Y-m-d') . "' where company_id='" . $_GET['uid'] . "'";
                                        $res3 = mysqli_query($con, $sql3);


                                        if ($res3) 
                                        {
                                            if (isset($_GET['uid'])) {
                                               // echo $sql3;
                                                header('Location:View_user.php');
                                            }
                                        } else {
                                            echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                        }
                                    }
                                    ?>


                            </form>
                        </div>


                        </center>

                    </div>
                </div>
            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>

</html>
<script>
    function logoimage() {
        $("#add_logo").click();
        $("#add_logo").change(function () {
            readURL(this);
        });
        //console.log("ok");
    }

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    
    
    
    
</script>
