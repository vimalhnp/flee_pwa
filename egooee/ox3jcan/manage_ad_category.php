<?php
ob_start();
session_start();

include '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    
} else {
    header('Location:index.php');
}



$all = "SELECT * from tbl_ad_category  ";
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr>";
        $dataview .= "<td>";
        $dataview .= $rowall['ad_category_id'];
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucwords($rowall['category_name']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucwords($rowall['added_on']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucwords($rowall['modify_on']);
        $dataview .= "</td>";




        $dataview .= "<td>";



        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_ad_category.php?id=" . $rowall['ad_category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_ad_category.php?id=" . $rowall['ad_category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['status'] == 1) {
            $dataview .= "&nbsp;<a href='manage_ad_category.php?cnid=" . $rowall['ad_category_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }
        ?>

        <?php
        $dataview .= "&nbsp<a href='manage_ad_category.php?did=" . $rowall['ad_category_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";



        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_ad_category where ad_category_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_ad_category.php');
        }



        $dataview .= "</td>";

        $dataview .= "</tr>";
    }
}



if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_ad_category set status='0' where ad_category_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_ad_category set status='1' where ad_category_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_ad_category.php');
    } else {
        echo "Not";
        exit();
    }
}


include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        $block = 'd-none';
        if (isset($_GET['cnid'])) {
            $s10 = "select * from tbl_ad_category where ad_category_id='" . $_GET['cnid'] . "'";
            $r10 = mysqli_query($con, $s10);

            $ra10 = mysqli_fetch_assoc($r10);
            $block = '';
        }
        ?>

        <div class="content pb-0"> 
            <section>
                <div class="row">
                    <div class="col-md-12 <?php echo $block ?>" id="form_div">
                        <div class="card">
                            <div class="card-header">
                                <center><strong>Add or Update </strong>Event Type</center>
                            </div>
                            <div class="card-body card-block">
                                <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal" id="fmreg">
                                    <div class="row form-group">
                                        <div class="col-12 col-md-6">
                                            Type :
                                            <input type="text" name="cn" id="cn" required=""  placeholder="Type"  class="form-control" value="<?php echo (isset($ra10['category_name']) && $ra10['category_name'] != '') ? $ra10['category_name'] : ''; ?>">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-md-12">
                                            <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                            <a class="btn btn-danger" href="manage_ad_category.php" style="width:15%">Cancel</a>
                                        </div>
                                    </div>
                                    <?php
                                    if (isset($_POST['btnsubmit'])) {

                                        if (isset($_GET['cnid']) && $_GET['cnid'] != '') {
                                            $sql2 = "UPDATE tbl_ad_category set category_name='" . $_POST['cn'] . "',modify_on='" . date('Y-m-d') . "' where ad_category_id='" . $_GET['cnid'] . "'";
                                        } else {
                                            $dd = date('Y-m-d');
                                            //$sql2 = "insert into tbl_ad_category (category_name,added_on,modify_on,status) values('" . $_POST['cn'] . "','$dd','$dd',1)";
                                            $sql2 = "insert into tbl_ad_category (category_name,added_on,modify_on,status) values('" . $_POST['cn'] . "','" . date('Y-m-d') . "','" . date('Y-m-d') . "',1)";
                                        }
                                        $res2 = mysqli_query($con, $sql2);

                                        if ($res2) {

                                            header('Location:manage_ad_category.php');
                                        } else {
                                            //  echo $sql2;
                                            echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                        }
                                    }
                                    ?>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Manage Event Type</strong><a href="javascript:void(0);" onclick="show_add_form();" class="btn btn-success" style="color: white">Add Type</a>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Type</th>
                                            <th>Added On</th>
                                            <th>Modify On</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        echo $dataview;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>





            </section>
        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }

    function show_add_form() {
        $("#cn").val('');
        $("#form_div").removeClass("d-none");
    }

</script>