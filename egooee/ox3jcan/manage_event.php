<?php
ob_start();
session_start();


include '../connection.php';

$curdate = date('Y-m-d');
if (isset($_SESSION['uname_admin'])) {

    $sql = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $res = mysqli_query($con, $sql);

    $row = mysqli_fetch_assoc($res);
    $type = $row['user_type'];
    $uid = $row['user_id'];
} else {
    header('Location:index.php');
}

include_once '../connection.php';

if ($type == 'admin') {
    $all = "select e.*,a.category_name from tbl_event as e INNER JOIN tbl_ad_category as a on e.category=a.ad_category_id order by e.category ASC";
} else {
    $all = "select tbl_user.fname,tbl_user.lname,tbl_event.* from tbl_event INNER JOIN tbl_user on tbl_user.user_id=tbl_event.user_id where tbl_user.user_id='$uid'";
}

$resall = mysqli_query($con, $all);

$dataview = "";
$categories_printed = array();
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $c = $rowall['category_name'];
        if (!in_array($c, $categories_printed)) {
            if (count($categories_printed) > 0) {
                $dataview .= '</tbody></table></div></div>';
            }
            $categories_printed[] = $c;
            $dataview .= '<div class="card"><div class="card-header">
                                        <strong class="card-title" style="margin:o auto;">' . ucfirst($c) . '</strong><a class="btn btn-success" href="add_event.php" style="color: white;margin-left:50px;">Add Event</a>
                                    </div>
                                    <div class="card-body">';
            $dataview .= '<table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                <th>NO</th>
                                                    <th>Image</th>
                                                    <th>Event Name</th>
                                                    <th>Type</th>
                                                    <th>State</th>
                                                    <th>Start</th>
                                                    <th>End</th>
                                                    <th>Action</th>                                 
                                                   </tr>
                                            </thead>
                                            <tbody>';
        }



        if ($rowall['end_date'] < $curdate) {
            $dataview .= "<tr class='table-danger'>";
        } else {
            $dataview .= "<tr>";
        }
        $dataview .= "<td onclick='eventmodalopan(" . $rowall['event_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['event_id'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='eventmodalopan(" . $rowall['event_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= "<img src='" . '../' . $rowall['image'] . "' style='height:60px; width:60px;'>";
        $dataview .= "</td>";
//       
        $dataview .= "<td onclick='eventmodalopan(" . $rowall['event_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['name']);
        $dataview .= "</td>";
        $dataview .= "<td onclick='eventmodalopan(" . $rowall['event_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['category_name'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='eventmodalopan(" . $rowall['event_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['state'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='eventmodalopan(" . $rowall['event_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['start_date'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='eventmodalopan(" . $rowall['event_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['end_date'];
        $dataview .= "</td>";
//       
        $dataview .= "<td style=min-width:100px;>";
        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_event.php?id=" . $rowall['event_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else {
            $dataview .= "<a href='manage_event.php?id=" . $rowall['event_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }
        if ($rowall['status'] == 1) {
            $dataview .= "&nbsp;<a href='add_event.php?uid=" . $rowall['event_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        if ($type == 'admin') {
            $dataview .= "&nbsp;<a href='manage_event.php?did=" . $rowall['event_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
        }


        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_event where event_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_event.php');
        }



        $dataview .= "</td>";
        $dataview .= "</tr>";
        ?>


        <?php
    }
    $dataview .= '</tbody></table>';
}

if (isset($_GET['id'])) {

    if ($_GET['st'] == 1) {
        $up = "update tbl_event set status='0' where event_id='" . $_GET['id'] . "'";
    } else {
        $up = "update tbl_event set status='1' where event_id='" . $_GET['id'] . "'";
    }
    $resup = mysqli_query($con, $up);
    if ($resup) {
        header('Location:manage_event.php');
    } else {
        echo "not";
        exit();
    }
}







include './head.php';
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>


                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <!--                                <div class="card">
                                                                    <div class="card-header">
                                                                        <strong class="card-title">Manage Upcoming Event</strong><?php
                                if ($type == 'admin') {
                                    echo '<a style="float: right" class="btn btn-success" href="add_event.php">Add Event</a>';
                                }
                                ?>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <table class="table table-striped table-bordered">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>NO</th>
                                                                                    <th>Image</th>
                                                                                    <th>Event Name</th>
                                                                                    <th>Type</th>
                                                                                    <th>State</th>
                                                                                    <th>Start</th>
                                                                                    <th>End</th>
                                                                                    <th>Action</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                <?php
                                echo $dataview;
                                ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>-->
                                <?php
                                echo $dataview;
                                ?>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->

                <div class="modal" id="eventpopup" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Event Information</h4>
                            </div>
                            <div class="modal-body">

                                <table class="table">
                                    <tr>
                                        <td>
                                            User name :
                                        </td>
                                        <td id="eu_name">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Event name :
                                        </td>
                                        <td id="e_name">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Category :
                                        </td>
                                        <td id="e_category">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            State :
                                        </td>
                                        <td id="e_state">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Start Date :
                                        </td>
                                        <td id="s_date">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            End Date :
                                        </td>
                                        <td id="e_date">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description :
                                        </td>
                                        <td id="e_desc">

                                        </td>
                                    </tr>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>



            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }

    function eventmodalopan(event_id) {
        $("#eventpopup").modal("show");
        $.ajax({
            url: 'ajax.php',
            type: 'post',
            data: {'action': 'get_event_data', 'ev_id': event_id},
            success: function (res) {
                if (res) {
                    var result = $.parseJSON(res);
//                        console.log(result);
                    $("#eu_name").text(result['fname'] + " " + result['lname']);
                    $("#e_name").text(result['name']);
                    $("#e_category").text(result['category']);
                    $("#e_state").text(result['state']);
                    $("#s_date").text(result['end_date']);
                    $("#e_date").text(result['start_date']);
                    $("#e_desc").text(result['description']);

                }
            }
        });
    }

</script>