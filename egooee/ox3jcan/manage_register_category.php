<?php
ob_start();
session_start();

include '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    
} else {
    header('Location:index.php');
}



$all = "SELECT * from tbl_reg_category where type='male' ";
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr>";
        $dataview .= "<td>";
        $dataview .= $rowall['reg_category_id'];
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= ucwords($rowall['cate_name']);
        $dataview .= "</td>";
        $dataview .= "<td>";
        if ($rowall['selected_by_default'] == 1) {
            $dataview .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&dst=" . $rowall['selected_by_default'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['selected_by_default'] == 0) {
            $dataview .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&dst=" . $rowall['selected_by_default'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }
        $dataview .= "</td>";

        $dataview .= "<td>";

        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['status'] == 1) {
            $dataview .= "&nbsp;<a href='manage_register_category.php?cnid=" . $rowall['reg_category_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        $dataview .= "&nbsp<a href='manage_register_category.php?did=" . $rowall['reg_category_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";

        $dataview .= "</td>";

        $dataview .= "</tr>";
    }
}




$all = "SELECT * from tbl_reg_category where type='female' ";
$resall = mysqli_query($con, $all);

$dataview1 = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview1 .= "<tr>";
        $dataview1 .= "<td>";
        $dataview1 .= $rowall['reg_category_id'];
        $dataview1 .= "</td>";

        $dataview1 .= "<td>";
        $dataview1 .= ucwords($rowall['cate_name']);
        $dataview1 .= "</td>";

        $dataview1 .= "<td>";
        if ($rowall['selected_by_default'] == 1) {
            $dataview1 .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&dst=" . $rowall['selected_by_default'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['selected_by_default'] == 0) {
            $dataview1 .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&dst=" . $rowall['selected_by_default'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }
        $dataview1 .= "</td>";

        $dataview1 .= "<td>";

        if ($rowall['status'] == 1) {
            $dataview1 .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview1 .= "<a href='manage_register_category.php?id=" . $rowall['reg_category_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['status'] == 1) {
            $dataview1 .= "&nbsp;<a href='add_register_category.php?cnid=" . $rowall['reg_category_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        $dataview1 .= "&nbsp<a href='manage_register_category.php?did=" . $rowall['reg_category_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";

        $dataview1 .= "</td>";

        $dataview1 .= "</tr>";
    }
}



if (isset($_GET['did'])) {

    $up1 = "delete from tbl_reg_category where reg_category_id='" . $_GET['did'] . "'";
    $resultup1 = mysqli_query($con, $up1);
    header('location:manage_register_category.php');
}




if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_reg_category set status='0' where reg_category_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_reg_category set status='1' where reg_category_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_register_category.php');
    } else {
        echo "Not";
        exit();
    }
    if ($_GET['dst'] == 1) {

        $up2 = "update tbl_reg_category set selected_by_default='0' where reg_category_id='" . $_GET['id'] . "'";
    } else {
        $up2 = "update tbl_reg_category set selected_by_default='1' where reg_category_id='" . $_GET['id'] . "'";
    }
    $resultup2 = mysqli_query($con, $up2);

    if ($resultup2) {
        header('Location:manage_register_category.php');
    } else {
        echo "Not";
        exit();
    }
}
$block = 'd-none';
$t = "";
if (isset($_GET['m'])) {
    $t = "male";
    $block = '';
} else if (isset($_GET['f'])) {
    $t = "female";
    $block = '';
}


include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');

        if (isset($_GET['cnid'])) {
            $s10 = "select * from tbl_reg_category where reg_category_id='" . $_GET['cnid'] . "'";
            $r10 = mysqli_query($con, $s10);

            $ra10 = mysqli_fetch_assoc($r10);

            $block = '';
        }
        ?>

        <div class="content pb-0"> 

            <section>



                <div class="row">
                    <div class="col-md-12 <?php echo $block ?>" id="form_div">
                        <div class="card">
                            <div class="card-header">
                                <center><strong>Add or Update </strong><?php echo ucfirst($t) ?> influence</center>
                            </div>
                            <div class="card-body card-block">




                                <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal" id="fmreg">


                                    <div class="row form-group">

                                        <div class="col-12 col-md-6">
                                            Influence Name :<input type="text" name="cn" required=""  placeholder="category Name"  value="<?php echo (isset($ra10['cate_name']) && $ra10['cate_name'] != '') ? $ra10['cate_name'] : ''; ?>" class="form-control">
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col col-md-12">


                                            <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                            <a class="btn btn-danger" href="manage_register_category.php" style="width:15%">Cancel</a>

                                        </div>
                                    </div>
                                    <?php
                                    if (isset($_POST['btnsubmit'])) {


                                        if (isset($_GET['cnid'])) {
                                            $sql2 = "UPDATE tbl_reg_category set cate_name='" . $_POST['cn'] . "',modify_on='" . date('Y-m-d') . "' where reg_category_id='" . $_GET['cnid'] . "'";
                                        } else {
                                            $dd = date('Y-m-d');
                                            //$sql2 = "insert into tbl_ad_category (category_name,added_on,modify_on,status) values('" . $_POST['cn'] . "','$dd','$dd',1)";
                                            $sql2 = "insert into tbl_reg_category (cate_name,type,added_on,modify_on,status) values('" . $_POST['cn'] . "','" . $t . "','" . date('Y-m-d') . "','" . date('Y-m-d') . "',1)";
                                        }
                                        $res2 = mysqli_query($con, $sql2);


                                        if ($res2) {

                                            header('Location:manage_register_category.php');
                                        } else {
                                            //  echo $sql2;
                                            echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                        }
                                    }
                                    ?>


                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Manage Influences for Male</strong><a href="manage_register_category.php?m=0" class="btn btn-success" style="color: white">Add Influence</a>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Category Name</th>
                                            <th>Selected by default</th>
                                            <!--<th>Modify On</th>-->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        echo $dataview;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>









            </section>
        </div>

        <div class="content pb-0"> 

            <section>



                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Manage Influences for Female</strong><a href="manage_register_category.php?f=0" class="btn btn-success" style="color: white">Add Influence</a>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Category Name</th>
                                            <th>Selected by default</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        echo $dataview1;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>









            </section>
        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }
</script>