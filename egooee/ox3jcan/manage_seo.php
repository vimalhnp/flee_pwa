<?php
ob_start();
session_start();

include_once '../connection.php';

if (isset($_SESSION['uname_admin'])) {

    $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $r = mysqli_query($con, $s);

    $ro = mysqli_fetch_assoc($r);

    $type = $ro['user_type'];
} else {
    header('Location:index.php');
}


if ($type == "admin") {
    $all = "select * from seo";
}
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr >";
        $dataview .= "<td>";
        $dataview .= $rowall['description'];
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= $rowall['keywords'];
        $dataview .= "</td>";
        
        $dataview .= "<td>";
       
       
            $dataview .= "&nbsp;<a href='manage_seo.php?cnid=" . $rowall['seo_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
      
        

        $dataview .= "</td>";


        $dataview .= "</tr>";
    }
}

include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');

        $block = 'd-none';
        if (isset($_GET['cnid'])) {
            $s10 = "select * from seo where seo_id='" . $_GET['cnid'] . "'";
            $r10 = mysqli_query($con, $s10);

            $ra10 = mysqli_fetch_assoc($r10);
            $block = '';
        }
        ?>

        <div class="content pb-0" style="min-height:450px;"> 

            <section>
                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-md-12 <?php echo $block ?>" id="form_div">
                                <div class="card">
                                    <div class="card-header">
                                        <center><strong>Update </strong> SEO</center>
                                    </div>
                                    <div class="card-body card-block">




                                        <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal" id="fmreg">


                                            <div class="row form-group">

                                                <div class="col-12 col-md-6">
                                                    Description :<textarea  name="description" class="form-control" required=""><?php echo (isset($ra10['description']) && $ra10['description'] != '') ? $ra10['description'] : ''; ?></textarea>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    Keywords :<textarea  name="keywords" class="form-control" required=""><?php echo (isset($ra10['keywords']) && $ra10['keywords'] != '') ? $ra10['keywords'] : ''; ?></textarea>
                                                </div>
                                                

                                            </div>


                                            <div class="row">
                                                <div class="col col-md-12">
                                                    <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                                    <a class="btn btn-danger" href="manage_seo.php" style="width:15%">Cancel</a>

                                                </div>
                                            </div>
                                            <?php
                                            if (isset($_POST['btnsubmit'])) {

                                                if (isset($_GET['cnid'])) {
                                                    $sql2 = "update seo set description='" . $_POST['description'] . "',keywords='" . $_POST['keywords'] . "' where seo_id='" . $_GET['cnid'] . "'";
                                                } 
                                                $res2 = mysqli_query($con, $sql2);


                                                if ($res2) {
                                                    header('Location:manage_seo.php');
                                                } else {
                                                    echo $sql2;
                                                    echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                                }
                                            }
                                            ?>


                                        </form>
                                    </div>




                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Manage SEO</strong>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Keywords</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div>
            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>




</body>

</html>
