-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 13, 2019 at 09:41 AM
-- Server version: 5.7.13-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `egooee`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ad`
--

DROP TABLE IF EXISTS `tbl_ad`;
CREATE TABLE IF NOT EXISTS `tbl_ad` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `add_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=deactive,1=active,2=pending,3=rejected',
  PRIMARY KEY (`ad_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

DROP TABLE IF EXISTS `tbl_post`;
CREATE TABLE IF NOT EXISTS `tbl_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_type` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `b_name` varchar(250) NOT NULL,
  `b_message` text NOT NULL,
  `b_address` varchar(250) NOT NULL,
  `b_phone` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `add_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `password` varchar(250) NOT NULL,
  `otp` int(11) NOT NULL,
  `c_name` varchar(150) NOT NULL,
  `c_title` varchar(150) NOT NULL,
  `c_address` text NOT NULL,
  `c_description` text NOT NULL,
  `c_logo` varchar(255) NOT NULL,
  `add_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_engagement`
--

DROP TABLE IF EXISTS `user_engagement`;
CREATE TABLE IF NOT EXISTS `user_engagement` (
  `user_engagement_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `text` varchar(250) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=like,2=rating,3=comment',
  `add_on` date NOT NULL,
  `modify_on` date NOT NULL,
  PRIMARY KEY (`user_engagement_id`),
  KEY `ad_id` (`post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_ad`
--
ALTER TABLE `tbl_ad`
  ADD CONSTRAINT `tbl_ad_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD CONSTRAINT `tbl_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `user_engagement`
--
ALTER TABLE `user_engagement`
  ADD CONSTRAINT `user_engagement_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `tbl_post` (`post_id`),
  ADD CONSTRAINT `user_engagement_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
