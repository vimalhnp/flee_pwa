<?php
ob_start();
session_start();

include_once '../connection.php';

if (isset($_SESSION['uname_admin'])) {

    if (isset($_GET['uid'])) {
        $sew = "select * from tbl_dish where dish_id='" . $_GET['uid'] . "'";
        $red = mysqli_query($con, $sew);

        $row1 = mysqli_fetch_assoc($red);
    }


    $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $r = mysqli_query($con, $s);

    $ro = mysqli_fetch_assoc($r);
    $di=$ro['user_id'];
    
    $type = $ro['user_type'];
} else {
    header('Location:index.php');
}

include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>


                <div class="card">
                    <div class="card-header">
                        <center><strong>Add  or Update </strong> Dish</center>
                    </div>
                    <div class="card-body card-block">




                        <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal" id="fmreg">


                            <div class="row form-group">

                                <div class="col-12 col-md-6">
                                    Restaurants Name :<input type="text" name="rname" required=""  placeholder="Enter Restaurants name"  value="<?php
                                    if (isset($_GET['uid'])) {
                                        echo ucfirst($row1['res_name']);
                                    }
                                    ?>" class="form-control">
                                </div>
                                 <div class="col-12 col-md-6">
                                    Dish Name :<input type="text" name="dname" required=""  placeholder="Enter Dish name"  value="<?php
                                    if (isset($_GET['uid'])) {
                                        echo ucfirst($row1['dish_name']);
                                    }
                                    ?>" class="form-control">
                                </div>

                            </div>
                            <div class="row form-group">
                                <div class="col-12 col-md-6">
                                    Contact No : <input class="form-control" placeholder="Enter Contact No" required=""  value="<?php
                                    if (isset($_GET['uid'])) {
                                        echo $row1['message'];
                                    }
                                    ?>"   name="phone"></div>
                                 <div class="col-12 col-md-6">
                                    Dish Type :
                                    
                                    <?php
                                    
                                    if(isset($_GET['uid']))
                                    {
                                        if($row1['category']=="caribbean")
                                        { ?>
                                          
                                      <select  class="form-control" name="type">
                                          <option value="caribbean" selected="">Caribbean</option>
                                        <option value="us">US</option>
                                        <option value="international">International</option>
                                    </select>
                                    
                                     <?php   }
                                        else if($row1['category']=="us")
                                        { ?>
                                           
                                        <select  class="form-control" name="type">
                                          <option value="caribbean">Caribbean</option>
                                          <option value="us" selected="">US</option>
                                        <option value="international">International</option>
                                    </select>
                                    
                                      <?php  }
                                        else
                                        { ?>
                                        
                                         <select  class="form-control" name="type">
                                          <option value="caribbean">Caribbean</option>
                                          <option value="us" >US</option>
                                          <option value="international" selected="">International</option>
                                    </select>
                                        
                                       <?php }
                                        
                                    }
                                    else{
                                    
                                    ?>
                                    <select  class="form-control" name="type">
                                        <option value="caribbean">Caribbean</option>
                                        <option value="us">US</option>
                                        <option value="international">International</option>
                                    </select>
                                    
                                    <?php } ?>
                                 </div>

                            </div>





                            <div class="row form-group">

                                <div class="col-12 col-md-6">
                                    Message : <textarea class="form-control" required=""    name="message" placeholder="Enter Message"><?php
                                        if (isset($_GET['uid'])) {
                                            echo ucfirst($row1['message']);
                                        }
                                        ?></textarea></div>
                                <div class="col-12 col-md-6">
                                    Location : <textarea class="form-control" required=""    name="location" placeholder="Enter Message"><?php
                                        if (isset($_GET['uid'])) {
                                            echo ucfirst($row1['location']);
                                        }
                                        ?></textarea></div>


                            </div>

                            <div class="row form-group">


                                <div class="col-12 col-md-6">

                                    Image :
                                    <!--                                        <div id="logo_preview" class="text-center">
                                                                                <img class="" src="<?php
                                    if (isset($_GET['uid'])) {
                                        echo '../' . $row1['image'];
                                    } else {
                                        echo "../img/no_logo.png";
                                    }
                                    ?>" alt="Image Not Found" onclick="logoimage();" id="blah" style="height: 70px;" class="img-responsive">
                                                                            </div>-->
                                    <br><input type="file" class="" id="add_logo"  name="add_logo"  >
                                </div>


                            </div>




                            <center>

                                <div class="col col-md-12">


                                    <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                    <a class="btn btn-danger" href="manage_dish.php" style="width:15%">Cancel</a>

                                </div>
                                <?php
                                if (isset($_POST['btnsubmit'])) {

                                    $a = "";
                                    if ($_FILES['add_logo']['name'] != "") {
                                        $filename = md5(time()) . basename($_FILES['add_logo']['name']);
                                        $tmpname = ($_FILES['add_logo']['tmp_name']);
                                        $dir = "../img/product/";
                                        $d1 = "img/product/" . $filename;
                                        $filepath = $dir . $filename;
                                        $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
                                        if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
                                            $error = "Image format not allowed";
                                        } else {
                                            $stat = move_uploaded_file($tmpname, $filepath);
                                            $a = ', image="' . $d1 . '"';
                                        }
                                    }
                                    //  var_dump($stat);
                                    //    exit();

                                    if (isset($_GET['uid'])) {
                                        $sql2 = "update tbl_dish set dish_name='".$_POST['dname']."',category='".$_POST['type']."',res_name='" . $_POST['rname'] . "'" . $a . ",phone='" . $_POST['phone'] . "',message='" . $_POST['message'] . "',location='" . $_POST['location'] . "',modify_on='" . date('Y-m-d') . "' where dish_id='" . $_GET['uid'] . "'";
                                    } else {
                                        $sql2 = "insert into tbl_dish (res_name,dish_name,category,user_id,message , phone , image ,location ,added_on ,modify_on , status) values('" . $_POST['rname'] . "','" . $_POST['dname'] . "','".$_POST['type']."','$di','" . $_POST['message'] . "','" . $_POST['phone'] . "','$d1','" . $_POST['location'] . "','" . date('Y-m-d') . "','" . date('Y-m-d') . "',1)";
                                    }
                                    $res2 = mysqli_query($con, $sql2);


                                    if ($res2) {
                                        header('Location:manage_dish.php');
                                    } else {
                                       // echo $sql2;
                                        echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                    }
                                }
                                ?>
                            </center>

                        </form>
                    </div>


                    </center>

                </div>


            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>




</body>

</html>
