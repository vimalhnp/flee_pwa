<?php
ob_start();
session_start();

include '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $r = mysqli_query($con, $s);

    $ro = mysqli_fetch_assoc($r);

    $type = $ro['user_type'];
} else {
    header('Location:index.php');
}


$all = "select tbl_user.*,countries.country_name from tbl_user INNER JOIN countries on countries.country_id=tbl_user.country where user_type='user'";
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {


        $dataview .= "<tr>";
        $dataview .= "<td onclick='usermodalopen(" . $rowall['user_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['user_id'];
        $dataview .= "</td>";

        $dataview .= "<td onclick='usermodalopen(" . $rowall['user_id'] . ");return false;' data-toggle='modal'>";
        $name = $rowall['fname'] . " " . $rowall['lname'];
        $dataview .= ucwords($name);
        $dataview .= "</td>";

        $dataview .= "<td onclick='usermodalopen(" . $rowall['user_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['email'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='usermodalopen(" . $rowall['user_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['username'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='usermodalopen(" . $rowall['user_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['country_name'];
        $dataview .= "</td>";

        $dataview .= "<td onclick='usermodalopen(" . $rowall['user_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['zipcode'];
        $dataview .= "</td>";

        $dataview .= "<td style='min-width:125px;'>";
         if ($rowall['type'] == 'business_user') {
            $dataview .= "<a href='View_user.php?user_id=" . $rowall['user_id'] ."' title='Company Details'><i class='fa fa-eye' style='color:blue;font-size:30px;'></i></a>&nbsp;";
        }
        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_user.php?id=" . $rowall['user_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>&nbsp;";
        } else {
            $dataview .= "<a href='manage_user.php?id=" . $rowall['user_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>&nbsp;";
        }
        $dataview .= "<a href='update_user.php?id=" . $rowall['user_id'] ."'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";

        if ($type == 'admin') {
            $dataview .= "&nbsp;<a href='manage_user.php?did=" . $rowall['user_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";
        }

        $dataview .= "</td>";
        $dataview .= "</tr>";
        ?>



        <?php
    }
}


if (isset($_GET['did'])) {

    $up1 = "delete from tbl_user where user_id='" . $_GET['did'] . "'";
    $resultup1 = mysqli_query($con, $up1);
    header('location:manage_user.php');
}
if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_user set status='0' where user_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_user set status='1' where user_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_user.php');
    } else {
        echo "Not";
    }
}

if (isset($_GET['id'])) {

    if ($_GET['st'] == 1) {
        $up = "update tbl_user set status='0' where user_id='" . $_GET['id'] . "'";
    } else {
        $up = "update tbl_user set status='1' where user_id='" . $_GET['id'] . "'";
    }
    $resup = mysqli_query($con, $up);
    if ($resup) {
        header('Location:manage_user.php');
    } else {
        echo "not";
        exit();
    }
}



include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Manage User (BIZ)</strong>
                                    </div>
                                    <div class="card-body">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>

                                                    <th>Name</th>

                                                    <th>Email</th>
                                                    <th>Username</th>
                                                    <th>Country</th>

                                                    <th>Zip-code</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div>


                <!--                <div class="modal fade" id="userpopup">
                                    <div class="modal-dialog">
                
                                         Modal content
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">User  Information</h4>
                                            </div>
                                            <div class="modal-body">
                
                                                <table class="table">
                                                    <tr>
                                                        <td>
                                                            First name :
                                                        </td>
                                                        <td id="user_fname">
                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Last name :
                                                        </td>
                                                        <td id="user_lname">
                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Email :
                                                        </td>
                                                        <td   id="user_email">
                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Country :
                                                        </td>
                                                        <td   id="user_country">
                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Zip-code :
                                                        </td>
                                                        <td   id="user_zip">
                
                                                        </td>
                                                    </tr>
                
                
                                                </table>
                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                -->





                <div class="modal" id="userpopup">

                    <div class=" modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Post Information</h4>

                            </div>
                            <div class="modal-body">
                                <table class="table">
                                    <tr>

                                        <td>
                                            First name:
                                        </td>
                                        <td id="user_fname">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Last Name:
                                        </td>
                                        <td id="user_lname">

                                        </td>
                                    <tr>

                                        <td>
                                            E-mail id:
                                        </td>
                                        <td id="user_email">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Country:
                                        </td>
                                        <td id="user_country">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Zip code:
                                        </td>
                                        <td id="user_zip">

                                        </td>
                                    </tr>


                                </table>

                            </div>


                        </div>

                    </div>

                </div>









            </section>
        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }


    function usermodalopen(user_id) {
        $("#userpopup").modal("show");
        //alert(user_id);
        $.ajax({
            url: 'ajax.php',
            type: 'post',
            data: {'action': 'get_user_data', 'iduser': user_id},
            success: function (res) {
                if (res) {
                    var result = $.parseJSON(res);
                    console.log(result);
                    //console.log(res);
                    $("#user_fname").text(result['fname']);
                    $("#user_lname").text(result['lname']);
                    $("#user_email").text(result['email']);
                    $("#user_country").text(result['country_name']);
                    $("#user_zip").text(result['zipcode']);

                }
            }
        });
    }



</script>