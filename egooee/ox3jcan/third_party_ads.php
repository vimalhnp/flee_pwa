<?php
ob_start();
session_start();


include '../connection.php';
if (isset($_SESSION['uname_admin'])) {

    $sql = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $res = mysqli_query($con, $sql);

    $row = mysqli_fetch_assoc($res);
    $type = $row['user_type'];
} else {
    header('Location:index.php');
}


$data = "";

$select = "select * from third_party_ads";
$res = mysqli_query($con, $select);

if (mysqli_num_rows($res) > 0) {
    while ($rowdata = mysqli_fetch_assoc($res)) {
        $data .= "<tr>";
        $data .= "<td>";
        $data .= $rowdata['id'];
        $data .= "</td>";
        $data .= "<td>";
        $data .= $rowdata['text'];
        $data .= "</td>";
        $data .= "<td>";
        if ($rowdata['status'] == 1) {
            $data .= "<a href='third_party_ads.php?id=" . $rowdata['id'] . "&st=" . $rowdata['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else {
            $data .= "<a href='third_party_ads.php?id=" . $rowdata['id'] . "&st=" . $rowdata['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }
        if ($rowdata['status'] == 1) {
            $data .= "&nbsp;<a href='new_third_ads?tid=" . $rowdata['id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";
        }

        $data .= "&nbsp;<a href='third_party_ads.php?did=" . $rowdata['id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";


        $data .= "</td>";
        $data .= "</tr>";
    }
} 
if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update third_party_ads set status='0' where id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update third_party_ads set status='1' where id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:third_party_ads.php');
    } else {
        echo "Not";
        //exit();
    }
}

if (isset($_GET['did'])) {
    $delete = "delete from third_party_ads where id='" . $_GET['did'] . "'";
    $resdelete = mysqli_query($con, $delete);

    if ($resdelete) {
        header('Location:third_party_ads.php');
    }
}



include './head.php';
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Third Party Ads</strong>

                                        <a href="new_third_ads.php" class="btn btn-success">Add Third Party Ads</a>
                                    </div>
                                    <div class="card-body">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>

                                                    <th>Code</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $data;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div>
            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>

<script language="JavaScript" type="text/javascript">
function checkDelete(){
    return confirm('Are you sure want to delete?');
}
</script>
