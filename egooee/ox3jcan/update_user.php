<?php
ob_start();
session_start();


include_once '../connection.php';
if (isset($_SESSION['uname_admin'])) {

    if (isset($_GET['id'])) {

        $updateq = "select * from tbl_user where user_id='" . $_GET['id'] . "'";
        $updater = mysqli_query($con, $updateq);

        $row1 = mysqli_fetch_assoc($updater);
    } else {
        header('Location:index.php');
    }
} else {
    header('Location:index.php');
}include './head.php';
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong>Update </strong>User</center>
                        </div>
                        <div class="card-body card-block">




                            <form action="#" enctype="multipart/form-data"  method="post" class="form-horizontal" id="fmreg">


                                <div class="row form-group">

                                   
                                    <div class="col-12 col-md-6">
                                        First Name:<input type="text" name="fname" value="<?php echo ucfirst($row1['fname']); ?>" class="form-control">
                                    </div>
                                     <div class="col-12 col-md-6">
                                        Last Name : <input class="form-control" name='lname' value="<?php echo $row1['lname']; ?>" ></div>
                                </div>





                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                        Gender : 
                                        <select name="gender" class="form-control">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        Street Address : <textarea class="form-control" name="address"><?php echo ucfirst($row1['saddress']); ?></textarea></div>
                                </div>
                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                        State/Province :<input type="text" name="state" required=""  placeholder="state"  value="<?php
                                        if (isset($_GET['id'])) {
                                            echo ucfirst($row1['state']);
                                        }
                                        ?>" class="form-control">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        Zip Code : <input class="form-control" required="" placeholder="Zip Code"  value="<?php
                                        if (isset($_GET['id'])) {
                                            echo $row1['zipcode'];
                                        }
                                        ?>"   name="zipcode"></div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-12 col-md-6">
                                       Country : 
                                        <select name="country" class="form-control">
                                            <?php
                                            $sql = "select * from countries where status=1";
                                            $res = mysqli_query($con, $sql);
                                           while($data = mysqli_fetch_assoc($res)){
                                               $selected='';
                                               if($data['country_id']==$row1['country']){
                                                   $selected='selected';
                                               }
                                               echo "<option ".$selected." value=".$data['country_id'].">".$data['country_name']."</option>";
                                           }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        Date of Birth : <input class="form-control" required="" placeholder="Zip Code"  value="<?php
                                        if (isset($_GET['id'])) {
                                            echo $row1['dob'];
                                        }
                                        ?>"   name="dob"></div>

                                </div>
                                <div class="row form-group">

                                   
                                    <div class="col-12 col-md-6">
                                        Image : 
                                        <!--                                        <div id="logo_preview" class="text-center">
                                                                                    <img class="" src="<?php echo '../' . $row1['image'] ?>" alt="Image Not Found" onclick="logoimage();" id="blah" style="height: 70px;" class="img-responsive">
                                                                                </div>-->
                                        <br><input type="file" class="" id="add_logo"  name="add_logo"  >

                                    </div>

                                </div>




                                <center>

                                    <div class="col col-md-12">


                                        <input type="submit"  class="btn btn-success" name="btnsubmit" value="Update" style="width:15%">
                                        <a class="btn btn-danger" href="manage_user.php" style="width:15%">Cancel</a>

                                    </div>
                                    <?php
                                    if (isset($_POST['btnsubmit'])) {


                                        $a = "";
                                        if ($_FILES['add_logo']['name'] != "") {

                                            $filename = md5(time()) . basename($_FILES['add_logo']['name']);
                                            $tmpname = ($_FILES['add_logo']['tmp_name']);
                                            $dir = "../img/product/";
                                            $d1 = "img/product/" . $filename;
                                            $filepath = $dir . $filename;
                                            $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
                                            if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
                                                $error = "Image format not allowed";
                                            } else {
                                                $stat = move_uploaded_file($tmpname, $filepath);
                                                $a = ', image="' . $d1 . '"';
                                            }
                                        }
                                        $sql2 = "update tbl_user set  state='".$_POST['state']."',zipcode='".$_POST['zipcode']."',fname='" . $_POST['fname'] . "'" . $a . ",lname='" . $_POST['lname'] . "',gender='" . $_POST['gender'] . "',saddress='" . $_POST['address'] . "',country='".$_POST['country']."',dob='".$_POST['dob']."',modify_on='" . date('Y-m-d') . "'".$a." where user_id='" . $_GET['id'] . "'";
                                      
                                        $res2 = mysqli_query($con, $sql2);


                                        if ($res2) {
                                            header('Location:manage_user.php');
                                        } else {
                                            echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                        }
                                    }
                                    ?>
                                </center>

                            </form>
                        </div>


                        </center>

                    </div>
                </div>
            </section>


        </div>



        <div class="clearfix"></div>



    </div>
    <?php
    include('script.php');

    include ('footer.php');
    ?>


    <script>
        function logoimage() {
            $("#add_logo").click();
            $("#add_logo").change(function () {
                readURL(this);
            });
            //console.log("ok");
        }

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>