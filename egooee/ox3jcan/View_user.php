<?php
ob_start();
session_start();

include_once '../connection.php';

if (isset($_SESSION['uname_admin'])) {

    $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $r = mysqli_query($con, $s);

    $ro = mysqli_fetch_assoc($r);

    $type = $ro['user_type'];
} else {
    header('Location:index.php');
}


if (isset($_GET['user_id']) && $_GET['user_id'] != '') {
    $all = "select tbl_user.user_id,tbl_company.*,tbl_category.name from tbl_company INNER JOIN tbl_category on tbl_category.category_id=tbl_company.category_id INNER JOIN tbl_user on tbl_user.user_id=tbl_company.user_id where tbl_company.user_id='".$_GET['user_id']."' ORDER by tbl_user.user_id";
} else {
    $all = "select tbl_user.user_id,tbl_company.*,tbl_category.name from tbl_company INNER JOIN tbl_category on tbl_category.category_id=tbl_company.category_id INNER JOIN tbl_user on tbl_user.user_id=tbl_company.user_id  ORDER by tbl_user.user_id";
}
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr>";
        $dataview .= "<td onclick='userprofile(" . $rowall['company_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['user_id'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='userprofile(" . $rowall['company_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= "<img src='" . '../' . $rowall['c_logo'] . "' style='height:60px; width:60px;'>";
        $dataview .= "</td>";

        $dataview .= "<td onclick='userprofile(" . $rowall['company_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['c_name'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='userprofile(" . $rowall['company_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['email'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='userprofile(" . $rowall['company_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['name'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='userprofile(" . $rowall['company_id'] . ");return false;' data-toggle='modal'>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['weburl']) > $allowedlimit) ? mb_substr($rowall['weburl'], 0, $allowedlimit) . "...." : $rowall['weburl'];
        $dataview .= "</td>";



        $dataview .= "<td>";
        if ($rowall['status'] == 1) {
            $dataview .= "<a href='View_user.php?id=" . $rowall['company_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else {
            $dataview .= "<a href='View_user.php?id=" . $rowall['company_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }
        $dataview .= "&nbsp;<a href='manage_profile.php?uid=" . $rowall['company_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";

        if ($type == 'admin') {
            $dataview .= "<a href='View_user.php?did=" . $rowall['company_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";
        }


        if (isset($_GET['id'])) {
            if ($_GET['st'] == 1) {

                $up1 = "update tbl_company set status='0' where company_id='" . $_GET['id'] . "'";
            } else {
                $up1 = "update tbl_company set status='1' where company_id='" . $_GET['id'] . "'";
            }
            $resultup1 = mysqli_query($con, $up1);

            if ($resultup1) {
                header('Location:View_user.php');
            } else {
                echo "Not";
                //exit();
            }
        }




        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_company where company_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:View_user.php');
        }

        $dataview .= "</td>";
        $dataview .= "</tr>";
    }
}

if (isset($_GET['id'])) {

    if ($_GET['st'] == 1) {
        $up = "update tbl_user set status='0' where user_id='" . $_GET['id'] . "'";
    } else {
        $up = "update tbl_user set status='1' where user_id='" . $_GET['id'] . "'";
    }
    $resup = mysqli_query($con, $up);
    if ($resup) {
        header('Location:View_user.php');
    } else {
        echo "not";
        exit();
    }
}
include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>

                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Manage User Company</strong>
                                    </div>
                                    <div class="card-body">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Logo</th>


                                                    <th>Company Name</th>
                                                    <th>Company Email</th>
                                                    <th>Business Type</th>
                                                    <th>Web Link</th>



<!--                                                    <th>Add on</th>
                                                    <th>Modify on</th>-->
                                                    <th style="min-width:75px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->


            </section>


        </div>


        <div class="modal" id="profile" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">User & User Company Information</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table">
                            <tr>
                                <td>
                                    First name :
                                </td>
                                <td id="p_fname">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Last name :
                                </td>
                                <td id="p_lname">

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Company Name :
                                </td>
                                <td id="p_cname">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Web Link :
                                </td>
                                <td id="web">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Company Email :
                                </td>
                                <td id="p_cemail">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Category :
                                </td>
                                <td id="p_category">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Company Address :
                                </td>
                                <td id="p_caddress">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Description :
                                </td>
                                <td id="p_desc">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Zipcode :
                                </td>
                                <td id="p_zipcode">

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Country :
                                </td>
                                <td id="p_country">

                                </td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>
        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>






</body>

</html>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }

    function userprofile(company_id) {
        $("#profile").modal("show");
        //alert(user_id);
        $.ajax({
            url: 'ajax.php',
            type: 'post',
            data: {'action': 'user_profile', 'company_id': company_id},
            success: function (res) {

                if (res) {
                    var result = $.parseJSON(res);
                    //console.log(result);
                    console.log(res);
                    $("#p_fname").text(result['fname']);
                    $("#p_lname").text(result['lname']);

                    $("#p_cname").text(result['c_name']);
                    $("#p_cemail").text(result['email']);
                    $("#p_category").text(result['name']);
                    $("#p_caddress").text(result['c_address']);
                    $("#p_desc").text(result['c_description']);
                    $("#p_zipcode").text(result['zipcode']);
                    $("#p_country").text(result['country_name']);
                    $("#web").text(result['weburl']);




                }
            }
        });
    }


</script>   