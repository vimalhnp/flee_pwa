<?php
ob_start();
session_start();

include '../connection.php';

$check = 0;

if (isset($_POST['btnlogin'])) {
    $sql = "select * from tbl_user where email='" . $_POST['txtusername'] . "' && status=1 && user_type='admin' ";
    $res = mysqli_query($con, $sql);

    if (mysqli_num_rows($res)) {
        $row = mysqli_fetch_assoc($res);
        $password = base64_decode($row['password']);
        if ($password == $_POST['txtpassword']) {
            $_SESSION['uname_admin'] = $_POST['txtusername'];
            $_SESSION['password_admin'] = $_POST['txtpassword'];
            header('Location:dashboard.php');
        }
    } else {
        $check = 1;
    }
}
?>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Login</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="images/alogo.png">
    <link rel="shortcut icon" href="images/halogo.png"> 

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"/>
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body class="bg-light">


    <div class="sufee-login d-flex align-content-center flex-wrap " style="margin-top: 8%;">
        <div class="container">
            <div class="login-content col-lg-2">

                <div class="login-form">
                    <form action="#" method="post">

                        <div class="login-logo">
                            <a >
                                <img class="align-content" src="../img/logo_color.png" style="width:40%">
                            </a>
                        </div>


                        <div class="row form-group">
                            <div class="col col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input type="email" id="input1-group1" name="txtusername" class="form-control" placeholder="Username" required="required">
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                <input type="password" id="txtpassword" name="txtpassword" placeholder="Password" class="form-control" required="required">

                            </div>

                        </div>




                        <!--                        <div class="checkbox">
                        
                                                    <label class="pull-right">
                                                        <a href="forgot_admin">Forgot Password?</a>
                                                    </label>
                                                                      
                                                </div>-->


                        <button type="submit" value="Login" class="btn btn-secondary btn-flat m-b-30 m-t-30" name="btnlogin">Sign in</button>

                        <center class="text-danger"><?php
                            if ($check == 1) {
                                ?>

                                <br><b>Incorrect Username or Password!!</b>

                                <?php
                            }
                            ?>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--    <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; <?php echo date("Y") ?> 
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by 
                    </div>
                </div>
            </div>
        </footer>-->
    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>
