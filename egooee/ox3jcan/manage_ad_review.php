<?php
ob_start();
session_start();


include '../connection.php';

if (isset($_SESSION['uname_admin'])) {

    $sql = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $res = mysqli_query($con, $sql);

    $row = mysqli_fetch_assoc($res);
    $type = $row['user_type'];
} else {
    header('Location:index.php');
}


if ($type == "admin") {
//$all = "select tbl_user.fname,user_engagement.*,tbl_post.b_name from tbl_user INNER JOIN user_engagement on tbl_user.user_id=user_engagement.user_id INNER JOIN tbl_post on tbl_post.post_id=user_engagement.user_engagement_id";    
    //$all="SELECT user_engagement.*,tbl_user.fname,tbl_post.post_id,tbl_post.b_name from user_engagement INNER JOIN tbl_user on  tbl_user.user_id=user_engagement.user_id INNER JOIN tbl_post on  tbl_post.post_id=user_engagement.post_id";
    //$all = "SELECT user_engagement.*,tbl_post.post_id,tbl_post.b_name,AVG(text) as reating from user_engagement INNER JOIN tbl_post on  tbl_post.post_id=user_engagement.post_id INNER JOIN tbl_user on tbl_user.user_id=user_engagement.user_id WHERE user_engagement.type=3 GROUP BY tbl_post.post_id";

    $all = "select * from tbl_ad_review ";
}



$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr >";


        $dataview .= "<td onclick='reviewmodalopen(" . $rowall['ad_review_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['ad_review_id'];
        $dataview .= "</td>";
        $dataview .= "<td onclick='reviewmodalopen(" . $rowall['ad_review_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['username']);
        $dataview .= "</td>";

        $dataview .= "<td onclick='reviewmodalopen(" . $rowall['ad_review_id'] . ");return false;' data-toggle='modal'>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['text']) > $allowedlimit) ? mb_substr($rowall['text'], 0, $allowedlimit) . "...." : $rowall['text'];

        $dataview .= "</td>";
        $dataview .= "<td onclick='reviewmodalopen(" . $rowall['ad_review_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['added_on']);
        $dataview .= "</td>";
        $dataview .= "<td onclick='reviewmodalopen(" . $rowall['ad_review_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['modify_on']);
        $dataview .= "</td>";
        $dataview .= "<td >";

        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_ad_review.php?id=" . $rowall['ad_review_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_ad_review.php?id=" . $rowall['ad_review_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }


        if ($rowall['status'] == 1) {
            $dataview .= "&nbsp;<a href='update_ad_review.php?uid=" . $rowall['ad_review_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        if ($type == 'admin') {
            $dataview .= "&nbsp;<a href='manage_ad_review.php?did=" . $rowall['ad_review_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
        }


        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_ad_review where ad_review_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_ad_review.php');
        }



        $dataview .= "</td>";

        $dataview .= "</tr>";
        ?>



        <?php
    }



    if (isset($_GET['id'])) {

        if ($_GET['st'] == 1) {
            $up = "update tbl_ad_review set status='0' where ad_review_id='" . $_GET['id'] . "'";
        } else {
            $up = "update tbl_ad_review set status='1' where ad_review_id='" . $_GET['id'] . "'";
        }
        $resup = mysqli_query($con, $up);
        if ($resup) {
            header('Location:manage_ad_review.php');
        } else {
            echo "not";
            exit();
        }
    }
}



include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>

                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Manage AD's Review</strong>
                                    </div>
                                    <div class="card-body">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>User Name</th>
                                                    <!--<th>Business Name</th>-->
                                                    <th>Review</th>
                                                    <th>Add on</th>
                                                    <th>Modify on</th>
                                                    <th>Action</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->


                <div class="modal" id="reviewpopup" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"> AD Review Information</h4>
                            </div>
                            <div class="modal-body">

                                <table class="table">
                                    <tr>
                                        <td>
                                            User name :
                                        </td>
                                        <td id="u_name">

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Review :
                                        </td>
                                        <td id="review">

                                        </td>
                                    </tr>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>



            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>

</html>

<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }



    function reviewmodalopen(review_id) {
        $("#reviewpopup").modal("show");
        $.ajax({
            url: 'ajax.php',
            type: 'post',
            data: {'action': 'get_review_data', 'idreview': review_id},
            success: function (res) {
                if (res) {
                    var result = $.parseJSON(res);
//                        console.log(result);
                    $("#u_name").text(result['username']);
                    $("#review").text(result['text']);

                }
            }
        });
    }
</script>