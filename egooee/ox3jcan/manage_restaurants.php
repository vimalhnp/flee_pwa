<?php
ob_start();
session_start();

include '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    
} else {
    header('Location:index.php');
}


$all = "select tbl_restaurant.*,tbl_category.name as c1 from tbl_restaurant inner join tbl_category on tbl_category.category_id=tbl_restaurant.name";

$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr data-toggle='modal' data-target='#myModal_" . $rowall['restaurant_id'] . "' style='cursor: pointer;'>";
        $dataview .= "<td>";
        $dataview .= $rowall['restaurant_id'];
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= ucwords($rowall['c1']);
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= ucwords($rowall['phone']);
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= ucwords($rowall['country']);
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= ucwords($rowall['zip_code']);
        $dataview .= "</td>";


        $dataview .= "<td>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr($rowall['message'], 0, $allowedlimit) . "...." : $rowall['message'];
        //$dataview .= ucwords($rowall['b_message']);
        $dataview .= "</td>";
        $dataview .= "<td>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['location']) > $allowedlimit) ? mb_substr($rowall['location'], 0, $allowedlimit) . "...." : $rowall['location'];
        // $dataview .= ucwords($rowall['b_address']);
        $dataview .= "</td>";

        $dataview .= "<td>";
        $dataview .= $rowall['added_on'];
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= $rowall['modify_on'];
        $dataview .= "</td>";

        $dataview .= "<td>";


        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_restaurants.php?id=" . $rowall['restaurant_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:20px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_restaurants.php?id=" . $rowall['restaurant_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:20px;'></i></a>";
        }

        if ($rowall['status'] == "1") {
            $dataview .= "&nbsp;<a href='add_restaurants.php?uid=" . $rowall['restaurant_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:20px;'></i></a>";
        }


        $dataview .= "&nbsp<a href='manage_restaurants.php?did=" . $rowall['restaurant_id'] . "'><i class='fa fa-trash' style='color:blue;font-size:20px;'></i></a>";



        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_restaurant where restaurant_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_restaurants.php');
        }

        $dataview .= "</td>";

        $dataview .= "</tr>";
        ?>

        <div class="modal fade" id="myModal_<?php echo $rowall['restaurant_id'] ?>" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Business Information</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $ed = $rowall['restaurant_id'];
                        $s = "select tbl_restaurant.*,tbl_category.name as cc from tbl_restaurant inner join tbl_category on category_id=tbl_restaurant.name where tbl_restaurant.restaurant_id='$ed'";
                        $r = mysqli_query($con, $s);

                        $row1 = mysqli_fetch_assoc($r);
                        ?>
                        <table class="table">
                            <tr>
                                <td>
                                    Business Type :
                                </td>
                                <td>
                                    <?php echo ($row1['cc']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contact No :
                                </td>
                                <td>
                                    <?php echo ucfirst($row1['phone']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Country :
                                </td>
                                <td>
                                    <?php echo ucfirst($row1['country']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Zip Code :
                                </td>
                                <td>
                                    <?php echo ucfirst($row1['zip_code']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Message :
                                </td>
                                <td>
                                    <?php echo ucfirst($row1['message']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Location :
                                </td>
                                <td>
                                    <?php echo ucfirst($row1['location']); ?>
                                </td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

//check

if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_restaurant set status='0' where restaurant_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_restaurant set status='1' where restaurant_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_restaurants.php');
    } else {
        echo "Not";
        //exit();
    }
}






include('head.php');
?>
<body>
<?php
include('left.php');
?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
<?php
include('header.php');
?>

        <div class="content pb-0"> 

            <section>



                <div class="animated fadeIn">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong class="card-title">Manage Business</strong><a class="btn btn-success" href="add_restaurants.php" style="float: right;color: white">Add Business</a>
                                </div>
                                <div class="card-body">
                                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>NO</th>
                                                <th>Image</th>
                                                <th>Business Type</th>
                                                <th>Contact No</th>
                                                <th>Country</th>
                                                <th>Zip Code</th>

                                                <th>Message</th>
                                                <th>Location</th>

                                                <th>Add_on</th>
                                                <th>Modified on</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
echo $dataview;
?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!-- .animated -->



            </section>
        </div>



        <div class="clearfix"></div>

<?php
include ('footer.php');
?>

    </div>
        <?php
        include('script.php');
        ?>
    <div id="container">



    </div>

</body>
