<?php

include_once '../connection.php';
if (isset($_POST['action']) && $_POST['action'] == 'get_dish_data') {
    $ed = $_POST['dish_id'];
    $s = "select tbl_user.fname,tbl_user.lname,tbl_dish.* from tbl_dish inner join tbl_user on tbl_dish.user_id=tbl_user.user_id where tbl_dish.dish_id='$ed'";
    $r = mysqli_query($con, $s);

    $row1 = mysqli_fetch_assoc($r);

    echo json_encode($row1, TRUE);
}
if (isset($_POST['action']) && $_POST['action'] == 'get_feed_data') {
    $ed = $_POST['post_id'];
    $jsn = '';
    //echo $ed;
    //$s = "SELECT tbl_post.*,countries.country_name,tbl_category.name,tbl_company.c_name from tbl_post INNER JOIN tbl_user on tbl_user.user_id=tbl_post.user_id INNER JOIN tbl_company on tbl_company.user_id=tbl_user.user_id inner join countries on countries.country_id=tbl_post.country  INNER JOIN tbl_category on tbl_category.category_id=tbl_post.b_type where tbl_post.post_id='$ed'";
    $s = "select tbl_post.*,countries.country_name,tbl_company.c_name from tbl_post LEFT JOIN countries on countries.country_id=tbl_post.country_id INNER JOIN tbl_company on tbl_company.company_id=tbl_post.b_type where tbl_post.type=2 and tbl_post.post_id='$ed'";
    $r = mysqli_query($con, $s);

    $row1 = mysqli_fetch_assoc($r);

    $ent_decode1 = json_decode($row1['reg_category_id'], true);
    if (is_array($ent_decode1)) {
        $u_ent1 = implode(',', $ent_decode1);
    }

    $jsn .= $u_ent1;
    $h1 = ['action' => 'success', 'row1' => $row1, 'jsn' => $jsn];
   
    echo json_encode($h1, TRUE);
}

if (isset($_POST['action']) && $_POST['action'] == 'user_blog') {


    $s1 = "select tbl_blog.*,tbl_user.* from tbl_blog inner join tbl_user on tbl_user.user_id=tbl_blog.user_id where tbl_blog.blog_id='" . $_POST['blog_id'] . "' ";
    $r1 = mysqli_query($con, $s1);

    $row2 = mysqli_fetch_assoc($r1);

    echo json_encode($row2, TRUE);
}



if (isset($_POST['action']) && $_POST['action'] == 'user_treading') {


    $s1 = "select tbl_talk.*,tbl_user.username from tbl_talk inner join tbl_user on tbl_user.user_id=tbl_talk.user_id where talk_id='" . $_POST['talk_id'] . "' ";
    $r1 = mysqli_query($con, $s1);

    $row2 = mysqli_fetch_assoc($r1);

    echo json_encode($row2, TRUE);
}


if (isset($_POST['action']) && $_POST['action'] == 'classified') {


    $s1 = "select tbl_post.*,tbl_user.*,tbl_category.* from tbl_post inner join tbl_user on tbl_user.user_id=tbl_post.user_id INNER JOIN tbl_category on tbl_category.category_id=tbl_post.b_type where tbl_post.type='1' and tbl_post.post_id='" . $_POST['classified_id'] . "' ";
    $r1 = mysqli_query($con, $s1);
    $row2 = mysqli_fetch_assoc($r1);
    echo json_encode($row2, TRUE);
}