<?php
ob_start();
session_start();

include_once '../connection.php';

if (isset($_SESSION['uname_admin'])) {

    $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $r = mysqli_query($con, $s);

    $ro = mysqli_fetch_assoc($r);

    $type = $ro['user_type'];
} else {
    header('Location:index.php');
}


if ($type == "admin") {
    $all = "select * from countries";
}
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr >";
        $dataview .= "<td>";
        $dataview .= $rowall['country_id'];
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= $rowall['country_code'];
        $dataview .= "</td>";
        $dataview .= "<td>";
        $dataview .= $rowall['country_name'];
        $dataview .= "</td>";
        $dataview .= "<td><center>";

        if ($rowall['enable_zipcode'] == 1) {
            $dataview .= "<a href='manage_countries.php?id1=" . $rowall['country_id'] . "&st1=" . $rowall['enable_zipcode'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['enable_zipcode'] == 0) {
            $dataview .= "<a href='manage_countries.php?id1=" . $rowall['country_id'] . "&st1=" . $rowall['enable_zipcode'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }
        $dataview .= "</center></td>";
        $dataview .= "<td>";
        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_countries.php?id=" . $rowall['country_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_countries.php?id=" . $rowall['country_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }
        if ($rowall['status'] == "1") {
            $dataview .= "&nbsp;<a href='manage_countries.php?cnid=" . $rowall['country_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }
        if ($type == 'admin') {
            $dataview .= "&nbsp<a href='manage_countries.php?did=" . $rowall['country_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
        }
        if (isset($_GET['did'])) {

            $up1 = "delete from countries where country_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_countries.php');
        }

        $dataview .= "</td>";

//        $dataview="<td>";
//
//        if ($rowall['status'] == 1) {
//            $dataview .= "<a href='manage_dish.php?id=" . $rowall['dish_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:20px;'></i></a>";
//        } else if ($rowall['status'] == 0) {
//            $dataview .= "<a href='manage_dish.php?id=" . $rowall['dish_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:20px;'></i></a>";
//        }
//
//        if ($rowall['status'] == "1") {
//              $dataview .= "&nbsp;<a href='add_dish.php?uid=" . $rowall['dish_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:20px;'></i></a>";
//        }
//
//        if ($type == 'admin') {
//            $dataview .= "&nbsp<a href='manage_dish.php?did=" . $rowall['dish_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:blue;font-size:20px;'></i></a>";
//        }
//
//
//        if (isset($_GET['did'])) {
//
//            $up1 = "delete from tbl_dish where dish_id='" . $_GET['did'] . "'";
//            $resultup1 = mysqli_query($con, $up1);
//            header('location:manage_dish.php');
//        }
//
//        $dataview .= "</td>";

        $dataview .= "</tr>";
    }
}

//check
//
if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update countries set status='0' where country_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update countries set status='1' where country_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_countries.php');
    } else {
        echo "Not";
        //exit();
    }
}



if (isset($_GET['id1'])) {
    if ($_GET['st1'] == 1) {

        $up1 = "update countries set enable_zipcode='0' where country_id='" . $_GET['id1'] . "'";
    } else {
        $up1 = "update countries set enable_zipcode='1' where country_id='" . $_GET['id1'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_countries.php');
    } else {
        echo "Not";
        //exit();
    }
}





include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');

        $block = 'd-none';
        if (isset($_GET['cnid'])) {
            $s10 = "select * from countries where country_id='" . $_GET['cnid'] . "'";
            $r10 = mysqli_query($con, $s10);

            $ra10 = mysqli_fetch_assoc($r10);
            $block = '';
        }
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-md-12 <?php echo $block ?>" id="form_div">
                                <div class="card">
                                    <div class="card-header">
                                        <center><strong>Add  or Update </strong> Country</center>
                                    </div>
                                    <div class="card-body card-block">




                                        <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal" id="fmreg">


                                            <div class="row form-group">

                                                <div class="col-12 col-md-6">
                                                    Country Code :<input type="text" name="ccode" required=""  placeholder="Enter Country Code"  value="<?php echo (isset($ra10['country_code']) && $ra10['country_code'] != '') ? $ra10['country_code'] : ''; ?>" class="form-control">
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    Country Name :<input type="text" name="cname" required=""  placeholder="Enter Country Name" value="<?php echo (isset($ra10['country_name']) && $ra10['country_name'] != '') ? $ra10['country_name'] : ''; ?>" class="form-control">
                                                </div>

                                            </div>


                                            <div class="row">
                                                <div class="col col-md-12">
                                                    <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                                    <a class="btn btn-danger" href="manage_countries.php" style="width:15%">Cancel</a>

                                                </div>
                                            </div>
                                            <?php
                                            if (isset($_POST['btnsubmit'])) {

                                                if (isset($_GET['uid'])) {
                                                    $sql2 = "update countries set country_code='" . $_POST['ccode'] . "',country_name='" . $_POST['cname'] . "' where country_id='" . $_GET['uid'] . "'";
                                                } else {
                                                    $sql2 = "insert into countries (country_code,country_name,status) values('" . $_POST['ccode'] . "','" . $_POST['cname'] . "',1)";
                                                }
                                                $res2 = mysqli_query($con, $sql2);


                                                if ($res2) {
                                                    header('Location:manage_countries.php');
                                                } else {
                                                    echo $sql2;
                                                    echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                                }
                                            }
                                            ?>


                                        </form>
                                    </div>




                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Manage Country</strong><a class="btn btn-success" href="javascript:void(0);" onclick="show_add_form();" style="color: white">Add Country</a>
                                    </div>
                                    <div class="card-body">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Country Code</th>
                                                    <th>Country Name</th>
                                                    <th>Enable Zipcode</th>
                                                    <th style="min-width:75px;">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div>
            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>




</body>

</html>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }
    function show_add_form() {
        $("#cn").val('');
        $("#form_div").removeClass("d-none");
    }
</script>