<?php
ob_start();
session_start();

include '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    $s = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $r = mysqli_query($con, $s);

    $ro = mysqli_fetch_assoc($r);

    $type = $ro['user_type'];
} else {
    header('Location:index.php');
}




if ($type == "admin") {
    $all = "select tbl_user.fname,tbl_user.lname,tbl_blog.* from tbl_blog INNER JOIN tbl_user on tbl_user.user_id=tbl_blog.user_id";
}
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr>";
        $dataview .= "<td  onclick='userblog(" . $rowall['blog_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= $rowall['blog_id'];
        $dataview .= "</td>";
        $dataview .= "<td  onclick='userblog(" . $rowall['blog_id'] . ");return false;' data-toggle='modal'>";
//        $n = $rowall['fname'] . " " . $rowall['lname'];
        $dataview .= ucwords($rowall['username']);
        $dataview .= "</td>";
        $dataview .= "<td  onclick='userblog(" . $rowall['blog_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= ucwords($rowall['title']);
        $dataview .= "</td>";
        $dataview .= "<td  onclick='userblog(" . $rowall['blog_id'] . ");return false;' data-toggle='modal'>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
        // $dataview .= ucwords($rowall['address']);
        $dataview .= "</td>";
        $dataview .= "<td  onclick='userblog(" . $rowall['blog_id'] . ");return false;' data-toggle='modal'>";
        $dataview .= (mb_strlen($rowall['description']) > $allowedlimit) ? mb_substr($rowall['description'], 0, $allowedlimit) . "...." : $rowall['description'];
        // $dataview .= ucwords($rowall['description']);
        $dataview .= "</td>";
        $dataview .= "<td>";


        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_blog.php?id=" . $rowall['blog_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_blog.php?id=" . $rowall['blog_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        if ($rowall['status'] == "1") {
            $dataview .= "&nbsp;<a href='update_blog.php?uid=" . $rowall['blog_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        if ($type == 'admin') {
            $dataview .= "&nbsp<a href='manage_blog.php?did=" . $rowall['blog_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
        }


        if (isset($_GET['did'])) {

            $up1 = "delete from tbl_blog where blog_id='" . $_GET['did'] . "'";
            $resultup1 = mysqli_query($con, $up1);
            header('location:manage_blog.php');
        }

        $dataview .= "</td>";

        $dataview .= "</tr>";
    }
}

//check

if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_blog set status='0' where blog_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_blog set status='1' where blog_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_blog.php');
    } else {
        echo "Not";
        //exit();
    }
}



include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>



                <div class="animated fadeIn">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong class="card-title">Manage Blog</strong>
                                </div>
                                <div class="card-body">
                                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>NO</th>
                                                <th>User Name</th>
                                                <th>Title</th>
                                                <th>Address</th>
                                                <th>Description</th>
<!--                                                <th>Add_on</th>
                                                <th>Modified on</th>-->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            echo $dataview;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!-- .animated -->





            </section>
        </div>
         <div class="modal" id="blog" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Blog Information</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <tr>
                                <td>
                                    User Name :
                                </td>
                                <td id="blog_uname">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Title :
                                </td>
                                <td id="blog_title">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Address :
                                </td>
                                <td id="blog_address">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Description :
                                </td>
                                <td id="blog_desc">
                                    
                                </td>
                            </tr>


                        </table>

                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }
    
    
     function userblog(blog_id) {
        $("#blog").modal("show");
        //alert(user_id);
        $.ajax({
            url: 'dish_ajax.php',
            type: 'post',
            data: {'action': 'user_blog', 'blog_id': blog_id},
            success: function (res) {
                
                if (res) {
                    var result = $.parseJSON(res);
                    
                    
                    $("#blog_uname").text(result['fname']+" "+result['lname']);
                    $("#blog_title").text(result['title']);
                   
                    $("#blog_address").text(result['address']);
                    $("#blog_desc").text(result['description']);
                   
                }
            }
        });
    }

    
    
</script>