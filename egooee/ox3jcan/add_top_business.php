<?php
ob_start();
session_start();


include_once '../connection.php';
if (isset($_SESSION['uname_admin'])) {

    if (isset($_GET['uid'])) {
        $q = "select * from tbl_top_business where top_business_id='" . $_GET['uid'] . "'";
        $s = mysqli_query($con, $q);

        $r1 = mysqli_fetch_assoc($s);
    }
} else {
    header('Location:index.php');
}
include './head.php';
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong>Add Or Update</strong> Top Rated Business</center>
                        </div>
                        <div class="card-body card-block">




                            <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal" id="fmreg">




                                <div class="row form-group">


                                    <div class="col-12 col-md-6">
                                        Company Name : <input class="form-control" required=""  placeholder="Company Name" value="<?php
                                        if (isset($_GET['uid'])) {
                                            echo $r1['c_name'];
                                        }
                                        ?>"  name="cname"></div>

                                    <div class="col-12 col-md-6">
<!--                                        Company Title :<input class="form-control" required=""  placeholder="Company Title" value="<?php
                                        if (isset($_GET['uid'])) {
                                            echo $r1['c_title'];
                                        }
                                        ?>"  name="ctitle">-->
                                        Business Type :
                                        <?php
                                        $rr="select * from tbl_category;";
                                        $sq= mysqli_query($con, $rr);
                                        ?>
                                        <select class="form-control" name="ctitle">
                                        
                                       <?Php while($re= mysqli_fetch_assoc($sq))
                                        {  
                                            
                                            if($_GET['uid'])
                                            {
                                                
                                                if($r1['c_title']==$re['category_id'])
                                                { ?>
                                            <option value="<?php echo $re['category_id'] ?>" selected=""><?php echo $re['name'] ?></option>       
                                            
                                              <?php  }
                                                else
                                                { ?>
                                                     <option value="<?php echo $re['category_id'] ?>"><?php echo $re['name'] ?></option>
                                               <?php }
                                                
                                            }
                                            else
                                            { ?>
                                                
                                            <option value="<?php echo $re['category_id'] ?>"><?php echo $re['name'] ?></option>
                                        
                                           <?php }
                                            
                                            
                                            ?>
                                           
                                        
                                        
                                        
                                        
                                      <?php  }
                                        ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">


                                    <div class="col-12 col-md-6">
                                        Location  : <textarea class="form-control" required=""    name="caddress" placeholder="Address"><?php
                                            if (isset($_GET['uid'])) {
                                                echo $r1['c_address'];
                                            }
                                            ?></textarea></div>

                                    <div class="col-12 col-md-6">
                                        Description  : <textarea class="form-control" required=""    name="desc" placeholder="Description"><?php
                                            if (isset($_GET['uid'])) {
                                                echo $r1['c_description'];
                                            }
                                            ?></textarea></div>
                                </div>



                                <div class="row form-group">




                                    <div class="col-12 col-md-6">

                                        Image :
                                        <!--                                        <div id="logo_preview" class="text-center">
                                                                                    <img class="" src="<?php
                                        if (isset($_GET['uid'])) {
                                            echo '../' . $r1['logo'];
                                        } else {
                                            echo "../img/no_logo.png";
                                        }
                                        ?>" alt="Image Not Found" onclick="logoimage();" id="blah" style="height: 70px;" class="img-responsive">
                                                                                </div>-->
                                        <br><input type="file" class="" id="add_logo"  name="add_logo" >
                                    </div>





                                </div>





                                <center>

                                    <div class="col col-md-12">


                                        <input type="submit"  class="btn btn-success" name="btnsubmit" value="Submit" style="width:15%">
                                        <a class="btn btn-danger" href="manage_top_rated_business.php" style="width:15%">Cancel</a>

                                    </div>
                                    <?php
                                    if (isset($_POST['btnsubmit'])) {


                                        $a = "";
                                        if ($_FILES['add_logo']['name'] != "") {
                                            $filename = md5(time()) . basename($_FILES['add_logo']['name']);
                                            $tmpname = ($_FILES['add_logo']['tmp_name']);
                                            $dir = "../img/";
                                            $d1 = "img/" . $filename;
                                            $filepath = $dir . $filename;
                                            $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
                                            if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
                                                $error = "Image format not allowed";
                                            } else {
                                                $stat = move_uploaded_file($tmpname, $filepath);
                                                $a = ', logo="' . $d1 . '"';
                                            }
                                        }

                                        $date = date('Y-m-d');

                                        if (isset($_GET['uid'])) {
                                            $sql2 = "update tbl_top_business set c_name='" . $_POST['cname'] . "',c_title='" . $_POST['ctitle'] . "',c_address='" . $_POST['caddress'] . "',c_description='" . $_POST['desc'] . "'" . $a . ",modify_on='" . $date . "' where top_business_id='" . $_GET['uid'] . "'";
                                        } else {
                                            $sql2 = "insert into tbl_top_business (c_name,c_title,c_address,c_description,logo,added_on,modify_on,status) values('" . $_POST['cname'] . "','" . $_POST['ctitle'] . "','" . $_POST['caddress'] . "','" . $_POST['desc'] . "','" . $d1 . "','" . $date . "','" . $date . "',1)";
                                        }

                                        $res2 = mysqli_query($con, $sql2);


                                        if ($res2) {
                                            header('Location:manage_top_rated_business.php');
                                        } else {
                                            //echo $sql2;
                                            echo "<b class='text-danger'>Something Went Wrong!!</b>";
                                        }
                                    }
                                    ?>
                                </center>

                            </form>
                        </div>


                        </center>

                    </div>
                </div>
            </section>
        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>


<script>
    function logoimage() {
        $("#add_logo").click();
        $("#add_logo").change(function () {
            readURL(this);
        });
        //console.log("ok");
    }

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

