<?php
ob_start();
session_start();


include_once '../connection.php';
if (isset($_SESSION['uname_admin'])) {
    
} else {
    header('Location:index.php');
}
$all = "SELECT * FROM `pricing`";
$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {





        $dataview .= "<tr >";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['pricing_id'] . "' style='cursor: pointer;'>";
        $dataview .= $rowall['pricing_id'];
        $dataview .= "</td>";
//        $dataview .= "<td>";
//        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
//        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['pricing_id'] . "' style='cursor: pointer;'>";
        $dataview .= ucwords($rowall['plan_name']);
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['pricing_id'] . "' style='cursor: pointer;'>";
        $dataview .= ucwords($rowall['plan_price']);
        $dataview .= "</td>";

        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['pricing_id'] . "' style='cursor: pointer;'>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['description']) > $allowedlimit) ? mb_substr($rowall['description'], 0, $allowedlimit) . "...." : $rowall['description'];
        //$dataview .= ucwords($rowall['description']);
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['pricing_id'] . "' style='cursor: pointer;'>";
        $dataview .= ucwords($rowall['total_like']);
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['pricing_id'] . "' style='cursor: pointer;'>";
        $dataview .= ucwords($rowall['total_dislikes']);
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['pricing_id'] . "' style='cursor: pointer;'>";
        $dataview .= $rowall['total_rattings'];
        $dataview .= "</td>";
        $dataview .= "<td >";

        $dataview .= "&nbsp;&nbsp;<a href='add_plan_pricing.php?uid=" . $rowall['pricing_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";
        if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_plan_pricing.php?id=" . $rowall['pricing_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_plan_pricing.php?id=" . $rowall['pricing_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        }

        $dataview .= "</td>";

        $dataview .= "</tr>";
        ?>

        <div class="modal fade" id="myModal_<?php echo $rowall['pricing_id'] ?>" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Plan Price Information</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $ed = $rowall['pricing_id'];
                        $s = "SELECT * FROM `pricing` where pricing.pricing_id='$ed'";
                        $r = mysqli_query($con, $s);

                        $row1 = mysqli_fetch_assoc($r);
                        ?>
                        <table class="table">
                            <tr>
                                <td>
                                    Plan Name :
                                </td>
                                <td>
                                    <?php echo ($row1['plan_name']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Plan Price :
                                </td>
                                <td>
                                    <?php echo ucfirst($row1['plan_price']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Description :
                                </td>
                                <td>
                                    <?php echo ucfirst($row1['description']); ?>
                                </td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}

if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update pricing set status='0' where pricing_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update pricing set status='1' where pricing_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_plan_pricing.php');
    } else {
        echo "Not";
        //exit();
    }
}

?>




<?php
include('head.php');
?>
<style>
    .pricingTable{
        text-align: center;
        color: #263248;
        background: #f1f0ff;
        transition: all 0.4s ease 0s;
        height: 675px;
        box-sizing: border-box;
    }

    .pricingTable:hover{
        background:#a297cb;
        color:#fff;
        margin-top: -15px;
        box-shadow:0 0 20px #808080;
    }



    .pricingTable .pricingTable-header{
        padding: 20px 10px;
    }

    .pricingTable .heading{
        display: block;
        font-size: 21px;
        font-weight: 900;
        text-transform: uppercase;
        color: #38e0db;
        transition: all 0.4s ease 0s;
    }

    .pricingTable:hover .heading{
        color:#fff;
    }

    .pricingTable .heading:before{
        content: "";
        width: 30%;
        display: block;
        margin: 15px auto;
        border-top: 2px solid #85838d;
        transition: all 0.4s ease 0s;
    }

    .pricingTable:hover .heading:before{
        border-top-color: #fff;
    }

    .pricingTable .pricingContent ul{
        padding: 0;
        margin-bottom: 0;
        list-style: none;
    }

    .pricingTable .pricingContent ul li{
        padding: 25px 0;
        width: 80%;
        margin: 0 auto;
        border-bottom:1px solid #d3d3d3;
        text-transform: uppercase;
        letter-spacing: 1px;
    }

    .pricingTable .pricing-plans{
        padding: 20px;
        color: #263248;
        position: relative;
        transition: all 0.4s ease 0s;
    }

    .pricingTable:hover .pricing-plans{
        color:#fff;
    }

    .pricingTable .price-value{
        display: inline-block;
        font-size: 50px;
        font-weight: 800;
    }

    .pricingTable .price-value i{
        margin-right: 10px;
    }

    .pricingTable .price-value span{
        font-size: 25px;
        margin-left: 10px;
        position: absolute;
        top: 47px;
    }

    .pricingTable .month{
        margin-left: 10px;
        position: absolute;
        bottom: 57px;
        font-size:10px;
        text-transform: uppercase;
    }

    .pricingTable .pricingTable-sign-up{
        position: relative;
        transition: all 0.5s ease 0s;
    }

    .pricingTable:hover .pricingTable-sign-up{
        padding: 30px 0;
    }

    .pricingTable .btn-block{
        width: 60%;
        padding: 14px 0;
        border: 0 none;
        color: #fff;
        font-size: 25px;
        background: #50c9d6;
        text-transform: uppercase;
        position:absolute;
        top:-50px;
        left: 20%;
        opacity: 0;
        transition: all 0.5s ease 0s;
    }

    .pricingTable .btn-block{
        top:-13px;
        opacity: 1;
    }

    .pricingTable.active{
        background:#a297cb;
        box-shadow:0 0 20px #808080;
        color: #fff;
        margin-top: -15px;
    }

    .pricingTable.active .heading:before{
        border-top-color: #fff;
    }

    .pricingTable.active .heading,
    .pricingTable.active .price-value,
    .pricingTable.active .month{
        color: #fff;
    }

    .pricingTable.active .pricingTable-sign-up{
        padding: 30px 0;
    }

    .pricingTable.active .btn-block{
        opacity: 1;
        top:30px;
    }

    @media screen and (max-width:990px){
        .pricingTable{
            margin-bottom: 50px;
        }
    }

    @media screen and (max-width:767px){
        .pricingTable.active{
            margin-top: 0;
        }
    }

    .fg {
        display: block;
        margin-bottom: 15px;
    }

    .fg input {
        padding: 0;
        height: initial;
        width: initial;
        margin-bottom: 0;
        display: none;
        cursor: pointer;
    }

    .fg label {
        position: relative;
        cursor: pointer;
    }

    .fg label:before {
        content:'';
        -webkit-appearance: none;
        background-color: transparent;
        border: 2px solid #000;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
        padding: 10px;
        display: inline-block;
        position: relative;
        vertical-align: middle;
        cursor: pointer;
        margin-right: 5px;
    }

    .fg label.error_lbl:before {
        border: 2px solid red;
    }

    .fg input:checked + label:after {
        content: '';
        display: block;
        position: absolute;
        top: 4px;
        left: 9px;
        width: 6px;
        height: 14px;
        border: solid #000;
        border-width: 0 2px 2px 0;
        transform: rotate(45deg);
    }
</style>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        ?>

        <div class="content pb-0"> 

            <section>
                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Manage plan pricing</strong>
                                        <a  href="add_plan_pricing.php" class="btn btn-success" style="color: white;">Add Plan price</a>
                                    </div>
                                    <div class="card-body">
                                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <!--<th>Image</th>-->
                                                    <th>Plan Name</th>
                                                    <th>Plan Price</th>

                                                    <th>Description</th>
                                                    <th>Total Like</th>
                                                    <th>Total Dislikes</th>
                                                    <th>Total Rattings</th>
                                                    <th style="min-width: 75px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <center>
                            <section>
                                <br><h4>
                                    <b style=" color: black;"><?php
                                        $view = "select text from setting where type='pricing title' and status='1'";
                                        $resview = mysqli_query($con, $view);

                                        $data = mysqli_fetch_assoc($resview);
                                        echo $data['text'];
                                        ?></b></h4>
                            </section>
                        </center><br/><br/>
                        <div class="row">
                            <?php
                            $sql = "SELECT * FROM pricing WHERE status = 1 ";
                            $res = mysqli_query($con, $sql);
                            $prices = [];
                            while ($row = mysqli_fetch_assoc($res)) {
                                $prices[] = $row;
                            }
                            if (count($prices) > 0) {
                                foreach ($prices as $priceArr) {
                                    ?>




                                    <div class="col-md-4 col-sm-6">
                                        <div class="pricingTable">

                                            <div class="pricingTable-header">
                                                <span class="heading">
                                                    <?php echo $priceArr['plan_name']; ?>
                                                </span>
                                            </div>
                                            <div class="pricingContent" style="word-break: break-all;padding: 0 10px;">
                                                <div style="word-break: break-all;height:70px;">
                                                    <?php echo $priceArr['description'];
                                                    ?>
                                                </div>
                                               
                                                    <img src="../<?php echo $priceArr['image'] ?>">
                                              
                                            </div>
                                            <div class="pricing-plans">
                                                <span class="price-value"><i class="fa fa-usd"></i><?php echo $priceArr['plan_price']; ?> <!-- <span>00</span>--> </span>
                                            </div>
                                            <div class="form-group fg">
                                                <input type="checkbox" class="tearms_check" id="tearms_<?php echo $priceArr['plan_number']; ?>" data-plan_id="<?php echo $priceArr['plan_number']; ?>" >                                
                                                <label for="tearms_<?php echo $priceArr['plan_number']; ?>" id="tearms_label_<?php echo $priceArr['plan_number']; ?>"></label>

                                            </div>
                                            <?php
                                            if ($priceArr['plan_number'] == 1) {
                                                ?>
                                                <div class="pricingTable-sign-up" style="margin-top: 55px;">

                                                    <a href="javascript:void(0)" class="btn btn-block btn-default" >Checkout</a>

                                                </div>
                                            <?php } else if ($priceArr['plan_number'] == 2) { ?>
                                                <div class="pricingTable-sign-up" style="margin-top: 55px;">
                                                    <a href="javascript:void(0)" class="btn btn-block btn-default" >Checkout</a>
                                                </div>
                                            <?php } else { ?>

                                                <div class="pricingTable-sign-up" style="margin-top: 55px;">
                                                    <a href="javascript:void(0)" class="btn btn-block btn-default" >Checkout</a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div><!-- .animated -->
                </div>
            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>

</html>

<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }
</script>
