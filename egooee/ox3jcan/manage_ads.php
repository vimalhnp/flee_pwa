<?php
ob_start();
session_start();


include '../connection.php';
if (isset($_SESSION['uname_admin'])) {

    $sql = "select * from tbl_user where email='" . $_SESSION['uname_admin'] . "'";
    $res = mysqli_query($con, $sql);

    $row = mysqli_fetch_assoc($res);
    $type = $row['user_type'];
} else {
    header('Location:index.php');
}



if ($type == "admin") {
    $all = "select * from tbl_ad where ad_status=0";
} 


$resall = mysqli_query($con, $all);

$dataview = "";
if (mysqli_num_rows($resall) > 0) {
    while ($rowall = mysqli_fetch_assoc($resall)) {
        $dataview .= "<tr  >";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
        $dataview .= $rowall['ad_id'];
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
        $dataview .= ucwords($rowall['title']);
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
        $allowedlimit = 29;
        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr($rowall['message'], 0, $allowedlimit) . "...." : $rowall['message'];
        // $dataview .= ucwords($rowall['message']);
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
        $dataview .= ucwords($rowall['phone']);
        $dataview .= "</td>";
        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
        $dataview .= "</td>";
        $dataview .= "<td>";
        
        if ($rowall['status'] == 2 && $type == 'admin') {
            $dataview .= "<a href='manage_ads.php?ac=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-check' title='Accept Ad' style='color:green;font-size:20px;'></i></a>&nbsp;&nbsp;&nbsp;";
            $dataview .= "<a href='manage_ads.php?rj=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-close' title='Reject Ad' style='color:red;font-size:20px;'></i></a>";
        } else if ($rowall['status'] == 3) {
            $dataview .= "<p class='text-danger'>Rejected</p>";
        } else if ($rowall['status'] == 1) {
            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
        } else if ($rowall['status'] == 0) {
            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
        } else {
            $dataview .= "<p class='text-danger'>Pending</p>";
        }





        if (isset($_GET['ac'])) {
            $up = "update tbl_ad set status='1' where ad_id='" . $_GET['ac'] . "'";
            $resup1 = mysqli_query($con, $up);

            if (isset($resup1)) {

                header('Location:manage_ads.php');
            }
        }
        if (isset($_GET['rj'])) {
            $up = "update tbl_ad set status='3' where ad_id='" . $_GET['rj'] . "'";
            $resup2 = mysqli_query($con, $up);

            if ($resup2) {
//             
                header('Location:manage_ads.php');
            }
        }

        if ($rowall['status'] == "1") {
            $dataview .= "&nbsp;<a href='update_ad.php?uid=" . $rowall['ad_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
        }

        if ($type == 'admin') {
            $dataview .= "&nbsp;<a href='manage_ads.php?did=" . $rowall['ad_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";
        }

        $dataview .= "</td>";

        $dataview .= "</tr>";
        
    }
}



$datafeed = "";

$feedadd = "SELECT * FROM tbl_ad where ad_status='8'";
;
$resfeed = mysqli_query($con, $feedadd);

if (mysqli_num_rows($resfeed) > 0) {
    while ($rowfeed = mysqli_fetch_assoc($resfeed)) {
        $datafeed .= "<tr>";
        $datafeed .= "<td>";
        $datafeed .= $rowfeed['ad_id'];
        $datafeed .= "</td>";

        $datafeed .= "<td>";
        $datafeed .= "<img src='" . '../' . $rowfeed['logo'] . "' style='height:60px; width:60px;'>";
        $datafeed .= "</td>";

//        $datafeed .= "<td>";
//        $datafeed .= $rowfeed['title'];
//        $datafeed .= "</td>";

        $datafeed .= "<td>";
        $datafeed .= $rowfeed['message'];
        $datafeed .= "</td>";

        $datafeed .= "<td>";
        $datafeed .= $rowfeed['phone'];
        $datafeed .= "</td>";

//        $datafeed .= "<td>";
//        $datafeed .= $rowfeed['address'];
//        $datafeed .= "</td>";
//        $datafeed .= "<td>";
//        $datafeed .= $rowfeed['added_on'];
//        $datafeed .= "</td>";
//
//        $datafeed .= "<td>";
//        $datafeed .= $rowfeed['modify_on'];
//        $datafeed .= "</td>";

        $datafeed .= "<td>";
        if ($rowfeed['status'] == 1) {
            $datafeed .= "<a href='manage_ads.php?acfeed=" . $rowfeed['ad_id'] . "&stafeed=" . $rowfeed['status'] . "'><i class='fa fa-toggle-on' title='Accept Ad' style='color:green;font-size:30px;'></i></a>&nbsp;&nbsp;&nbsp;";
        } else {
            $datafeed .= "<a href='manage_ads.php?defeed=" . $rowfeed['ad_id'] . "&stdfeed=" . $rowfeed['status'] . "'><i class='fa fa-toggle-off' title='Accept Ad' style='color:red;font-size:30px;'></i></a>&nbsp;&nbsp;&nbsp;";
        }

        $datafeed .= "<a href='update_ad.php?uid=" . $rowfeed['ad_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";
        $datafeed .= "&nbsp;<a href='manage_ads.php?did=" . $rowfeed['ad_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";
        $datafeed .= "</td>";
        $datafeed .= "</tr>";
    }
}

if (isset($_GET['acfeed'])) {
    $up = "UPDATE tbl_ad set status='0' where ad_id='" . $_GET['acfeed'] . "'";
    $resup = mysqli_query($con, $up);
    if ($resup) {
        header('Location:manage_ads.php');
    }
}

if (isset($_GET['defeed'])) {
    $up1 = "UPDATE tbl_ad set status='1' where ad_id='" . $_GET['defeed'] . "'";
    $resup1 = mysqli_query($con, $up1);

    if ($resup1) {
        header('Location:manage_ads.php');
    }
}


//------------------------------------------------------------------------------------------------------------


if (isset($_GET['id'])) {
    if ($_GET['st'] == 1) {

        $up1 = "update tbl_ad set status='0' where ad_id='" . $_GET['id'] . "'";
    } else {
        $up1 = "update tbl_ad set status='1' where ad_id='" . $_GET['id'] . "'";
    }
    $resultup1 = mysqli_query($con, $up1);

    if ($resultup1) {
        header('Location:manage_ads.php');
    } else {
        echo "Not";
        exit();
    }
}


$a = "SELECT count(ad_id) as tk from tbl_ad where ad_status=0";
$s = mysqli_query($con, $a);

$re = mysqli_fetch_assoc($s);

$co = $re['tk'];

$edit = "block";
if ($co >= 3) {
    $edit = "none";
}





if (isset($_GET['did'])) {
    $up1 = "delete from tbl_ad where ad_id='" . $_GET['did'] . "'";
    $resultup1 = mysqli_query($con, $up1);
    header('location:manage_ads.php');
}
include('head.php');
?>
<body>
    <?php
    include('left.php');
    ?>

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel">
        <?php
        include('header.php');
        
        
        ?>

        <div class="content pb-0"> 

            <section>


                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">

                                        <strong class="card-title">Home Slider Left Circle Ads (L)</strong><a class="btn btn-success" href="update_ad.php?ty=0">Add new ad</a><?php if ($type == 'admin') { ?>

                                        <?php }
                                        ?>
                                    </div>
                                    <div class="card-body table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                  
                                                    <th>Ad Title</th>
                                                    <th>Website</th>
                                                    <th>Contact No</th>
                                                    <th>Address</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->
                <br><br><br>

                <?php
                if ($type == "admin") {
                    $all = "select * from tbl_ad where ad_status=1";
                } else {
                    $all = "select tbl_user.fname,tbl_user.lname,tbl_user.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id where tbl_user.user_type='" . $type . "' && ad_status=1 && tbl_user.email='" . $_SESSION['uname_admin'] . "' ";
                }






                $resall = mysqli_query($con, $all);

                $dataview = "";
                if (mysqli_num_rows($resall) > 0) {
                    while ($rowall = mysqli_fetch_assoc($resall)) {
                        $dataview .= "<tr >";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= $rowall['ad_id'];
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['c_name']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['title']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['fname']);
//                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['title']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $allowedlimit = 29;
                        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr($rowall['message'], 0, $allowedlimit) . "...." : $rowall['message'];
                        //  $dataview .= ucwords($rowall['message']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['phone']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";

                        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
                        // $dataview .= ucwords($rowall['address']);
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['state'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['zipcode'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['added_on'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['modify_on'];
//                        $dataview .= "</td>";

                        $dataview .= "<td>";


                        if ($rowall['status'] == 2 && $type = 'admin') {
                            $dataview .= "<a href='manage_ads.php?ac=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-check' title='Accept Ad' style='color:green;font-size:20px;'></i></a>&nbsp;&nbsp;&nbsp;";
                            $dataview .= "<a href='manage_ads.php?rj=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-close' title='Reject Ad' style='color:red;font-size:20px;'></i></a>";
                        } else if ($rowall['status'] == 3) {
                            $dataview .= "<p class='text-danger'>Rejected</p>";
                        } else if ($rowall['status'] == 1) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>&nbsp;";
                        } else if ($rowall['status'] == 0) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;'font-size:30px;></i></a>&nbsp;";
                        } else {
                            $dataview .= "<p class='text-danger'>Pending</p>";
                        }





                        if (isset($_GET['ac'])) {
                            $up = "update tbl_ad set status='1' where ad_id='" . $_GET['ac'] . "'";
                            $resup1 = mysqli_query($con, $up);

                            if (isset($resup1)) {

                                header('Location:manage_ads.php');
                            }
                        }
                        if (isset($_GET['rj'])) {
                            $up = "update tbl_ad set status='3' where ad_id='" . $_GET['rj'] . "'";
                            $resup2 = mysqli_query($con, $up);

                            if ($resup2) {
//             
                                header('Location:manage_ads.php');
                            }
                        }







                        if ($rowall['status'] == "1") {
                            $dataview .= "<a href='update_ad.php?uid=" . $rowall['ad_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";
                        }
                        if ($type == 'admin') {
                            $dataview .= "&nbsp;<a href='manage_ads.php?did=" . $rowall['ad_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";
                        }
                        $dataview .= "</td>";

                        $dataview .= "</tr>";
                        ?>

                        <div class="modal fade" id="myModal_<?php echo $rowall['ad_id'] ?>" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">AD (R Circle) Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $ed = $rowall['ad_id'];
                                        $s = "select * from tbl_ad where ad_status=1  and tbl_ad.ad_id='$ed'";
                                        $r = mysqli_query($con, $s);

                                        $row1 = mysqli_fetch_assoc($r);
                                        ?>
                                        <table class="table">
        <!--                                            <tr>
                                                <td>
                                                    User Name :
                                                </td>
                                                <td>
                                            <?php echo ($row1['fname']) . " " . ($row1['lname']); ?>
                                                </td>
                                            </tr>-->
        <!--                                            <tr>
                                                <td>
                                                    Company Name :
                                                </td>
                                                <td>
                                            <?php echo ucfirst($row1['c_name']); ?>
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td>
                                                    Ad Title :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['title']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Website :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['message']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Contact No :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['phone']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Address :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['address']); ?>
                                                </td>
                                            </tr>


                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }


                if (isset($_GET['id'])) {
                    if ($_GET['st'] == 1) {

                        $up1 = "update tbl_ad set status='0' where ad_id='" . $_GET['id'] . "'";
                    } else {
                        $up1 = "update tbl_ad set status='1' where ad_id='" . $_GET['id'] . "'";
                    }
                    $resultup1 = mysqli_query($con, $up1);

                    if ($resultup1) {
                        header('Location:manage_ads.php');
                    } else {
                        echo "Not";
                        exit();
                    }
                }

                $a1 = "SELECT count(ad_id) as tk from tbl_ad where ad_status=1";
                $s1 = mysqli_query($con, $a1);

                $re1 = mysqli_fetch_assoc($s1);

                $co1 = $re1['tk'];

                $edit1 = "block";
                if ($co1 >= 3) {
                    $edit1 = "none";
                }

                if (isset($_GET['did'])) {
                    $up1 = "delete from tbl_ad where ad_id='" . $_GET['did'] . "'";
                    $resultup1 = mysqli_query($con, $up1);
                }
                ?>



                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title" >Home Slider Right Circle Ads (R)</strong><?php if ($type == 'admin') { ?>
                                            <a class="btn btn-success" href="update_ad.php?ty=1">Add new ad</a>
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                    <!--<th>Company Name</th>-->
                                                    <!--<th>Name</th>-->
                                                    <th>Ad Title</th>
                                                    <th>Website</th>
                                                    <th>Contact No</th>
                                                    <th>Address</th>
<!--                                                    <th>State</th>
                                                    <th>Zip Code</th>-->
<!--                                                    <th>Add_on</th>
                                                    <th>Modified on</th>-->
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div>






                <?php
                if ($type == "admin") {
                    //  $all = "select tbl_user.fname,tbl_user.lname,tbl_company.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id inner join tbl_company on tbl_company.user_id=tbl_user.user_id where ad_status=5 ";
                    $all = "select * from tbl_ad where ad_status=5";
                } else {
                    $all = "select tbl_user.fname,tbl_user.lname,tbl_user.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id where tbl_user.user_type='" . $type . "' && ad_status=1 && tbl_user.email='" . $_SESSION['uname_admin'] . "' ";
                }






                $resall = mysqli_query($con, $all);

                $dataview = "";
                if (mysqli_num_rows($resall) > 0) {
                    while ($rowall = mysqli_fetch_assoc($resall)) {
                        $dataview .= "<tr >";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= $rowall['ad_id'];
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['c_name']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['title']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['fname']);
//                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['title']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $allowedlimit = 29;
                        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr($rowall['message'], 0, $allowedlimit) . "...." : $rowall['message'];
                        //  $dataview .= ucwords($rowall['message']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['phone']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";

                        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
                        // $dataview .= ucwords($rowall['address']);
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['state'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['zipcode'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['added_on'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['modify_on'];
//                        $dataview .= "</td>";

                        $dataview .= "<td>";


                        if ($rowall['status'] == 2 && $type = 'admin') {
                            $dataview .= "<a href='manage_ads.php?ac=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-check' title='Accept Ad' style='color:green;font-size:20px;'></i></a>&nbsp;&nbsp;&nbsp;";
                            $dataview .= "<a href='manage_ads.php?rj=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-close' title='Reject Ad' style='color:red;font-size:20px;'></i></a>";
                        } else if ($rowall['status'] == 3) {
                            $dataview .= "<p class='text-danger'>Rejected</p>";
                        } else if ($rowall['status'] == 1) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>&nbsp;";
                        } else if ($rowall['status'] == 0) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>&nbsp;";
                        } else {
                            $dataview .= "<p class='text-danger'>Pending</p>";
                        }





                        if (isset($_GET['ac'])) {
                            $up = "update tbl_ad set status='1' where ad_id='" . $_GET['ac'] . "'";
                            $resup1 = mysqli_query($con, $up);

                            if (isset($resup1)) {

                                header('Location:manage_ads.php');
                            }
                        }
                        if (isset($_GET['rj'])) {
                            $up = "update tbl_ad set status='3' where ad_id='" . $_GET['rj'] . "'";
                            $resup2 = mysqli_query($con, $up);

                            if ($resup2) {
//             
                                header('Location:manage_ads.php');
                            }
                        }







                        if ($rowall['status'] == "1") {
                            $dataview .= "<a href='update_ad.php?uid=" . $rowall['ad_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";
                        }
                        if ($type == 'admin') {
                            $dataview .= "&nbsp;<a href='manage_ads.php?did=" . $rowall['ad_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";
                        }
                        $dataview .= "</td>";

                        $dataview .= "</tr>";
                        ?>

                        <div class="modal fade" id="myModal_<?php echo $rowall['ad_id'] ?>" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Home Page AD Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $ed = $rowall['ad_id'];
                                        $s = "select * from tbl_ad where ad_status=5 and tbl_ad.ad_id='$ed'";
                                        $r = mysqli_query($con, $s);

                                        $row1 = mysqli_fetch_assoc($r);
                                        ?>
                                        <table class="table">
        <!--                                            <tr>
                                                <td>
                                                    User Name :
                                                </td>
                                                <td>
                                            <?php echo ($row1['fname']) . " " . ($row1['lname']); ?>
                                                </td>
                                            </tr>-->
        <!--                                            <tr>
                                                <td>
                                                    Company Name :
                                                </td>
                                                <td>
                                            <?php echo ucfirst($row1['c_name']); ?>
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td>
                                                    Ad Title :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['title']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Website :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['message']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Contact No :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['phone']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Address :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['address']); ?>
                                                </td>
                                            </tr>


                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }


                if (isset($_GET['id'])) {
                    if ($_GET['st'] == 1) {

                        $up1 = "update tbl_ad set status='0' where ad_id='" . $_GET['id'] . "'";
                    } else {
                        $up1 = "update tbl_ad set status='1' where ad_id='" . $_GET['id'] . "'";
                    }
                    $resultup1 = mysqli_query($con, $up1);

                    if ($resultup1) {
                        header('Location:manage_ads.php');
                    } else {
                        echo "Not";
                        exit();
                    }
                }


                $a1 = "SELECT count(ad_id) as tk from tbl_ad where ad_status=5";
                $s1 = mysqli_query($con, $a1);

                $re1 = mysqli_fetch_assoc($s1);

                $co1 = $re1['tk'];

                $edit1 = "block";
                if ($co1 >= 4) {
                    $edit1 = "none";
                }


                if (isset($_GET['did'])) {
                    $up1 = "delete from tbl_ad where ad_id='" . $_GET['did'] . "'";
                    $resultup1 = mysqli_query($con, $up1);
                }
                ?>



                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Home Page Ad's</strong><?php if ($type == 'admin') { ?>
                                            <a class="btn btn-success" href="update_ad.php?ty=5">Add new ad</a>
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                    <!--<th>Company Name</th>-->
                                                    <!--<th>Name</th>-->
                                                    <th>Ad Title</th>
                                                    <th>Website</th>
                                                    <th>Contact No</th>
                                                    <th>Address</th>
<!--                                                    <th>State</th>
                                                    <th>Zip Code</th>-->
<!--                                                    <th>Add_on</th>
                                                    <th>Modified on</th>-->
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div>





                <?php
                if ($type == "admin") {
                    $all = "select tbl_home_logo.*,tbl_category.name from tbl_home_logo INNER JOIN tbl_category on tbl_category.category_id=tbl_home_logo.business_type_id where tbl_home_logo.status=1";
                }






                $resall = mysqli_query($con, $all);

                $dataview = "";
                if (mysqli_num_rows($resall) > 0) {
                    while ($rowall = mysqli_fetch_assoc($resall)) {
                        $dataview .= "<tr >";
                        $dataview .= "<td data-toggle='modal' data-target='#my_" . $rowall['home_logo_id'] . "' style='cursor: pointer;'>";
                        $dataview .= $rowall['home_logo_id'];
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#my_" . $rowall['home_logo_id'] . "' style='cursor: pointer;'>";
                        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['company_name']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['title']);
//                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#my_" . $rowall['home_logo_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['url']);
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['title']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $allowedlimit = 29;
//                        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr($rowall['message'], 0, $allowedlimit) . "...." : $rowall['message'];
//                        //  $dataview .= ucwords($rowall['message']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['phone']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
//                        // $dataview .= ucwords($rowall['address']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['state'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['zipcode'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#my_" . $rowall['home_logo_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['added_on'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#my_" . $rowall['home_logo_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['modify_on'];
//                        $dataview .= "</td>";

                        $dataview .= "<td>";


                        if ($rowall['status'] == 1) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['home_logo_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>&nbsp;";
                        } else if ($rowall['status'] == 0) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['home_logo_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>&nbsp;";
                        }





                        if (isset($_GET['ac'])) {
                            $up = "update tbl_home_logo set status='1' where home_logo_id='" . $_GET['ac'] . "'";
                            $resup1 = mysqli_query($con, $up);

                            if (isset($resup1)) {

                                header('Location:manage_ads.php');
                            }
                        }
                        if (isset($_GET['rj'])) {
                            $up = "update tbl_home_logo set status='3' where home_logo_id='" . $_GET['rj'] . "'";
                            $resup2 = mysqli_query($con, $up);

                            if ($resup2) {
//             
                                header('Location:manage_ads.php');
                            }
                        }







                        if ($rowall['status'] == "1") {
                            $dataview .= "<a href='update_home_page_logo.php?uid=" . $rowall['home_logo_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";
                        }
                        if ($type == 'admin') {
                            $dataview .= "&nbsp;<a href='manage_ads.php?did=" . $rowall['home_logo_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;";
                        }
                        $dataview .= "</td>";

                        $dataview .= "</tr>";
                        ?>

                        <div class="modal fade" id="my_<?php echo $rowall['home_logo_id'] ?>" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Home Page Center AD Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $ed = $rowall['home_logo_id'];
                                        $s = "select tbl_home_logo.*,tbl_category.name from tbl_home_logo INNER JOIN tbl_category on tbl_category.category_id=tbl_home_logo.business_type_id  where tbl_home_logo.home_logo_id='$ed'";
                                        $r = mysqli_query($con, $s);

                                        $row1 = mysqli_fetch_assoc($r);
                                        ?>
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    Business Type :
                                                </td>
                                                <td>
                                                    <?php echo ($row1['name']); ?>
                                                </td>
                                            </tr>
        <!--                                            <tr>
                                                <td>
                                                    Company Name :
                                                </td>
                                                <td>
                                            <?php echo ucfirst($row1['c_name']); ?>
                                                </td>
                                            </tr>-->


                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }


                if (isset($_GET['id'])) {
                    if ($_GET['st'] == 1) {

                        $up1 = "update tbl_home_logo set status='0' where home_logo_id='" . $_GET['id'] . "'";
                    } else {
                        $up1 = "update tbl_home_logo set status='1' where home_logo_id='" . $_GET['id'] . "'";
                    }
                    $resultup1 = mysqli_query($con, $up1);

                    if ($resultup1) {
                        header('Location:manage_ads.php');
                    } else {
                        echo "Not";
                        exit();
                    }
                }


                if (isset($_GET['did'])) {
                    $up1 = "delete from tbl_home_logo where home_logo_id='" . $_GET['did'] . "'";
                    $resultup1 = mysqli_query($con, $up1);
                }
                ?>



                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Home Page Center Ad's</strong><?php
                if ($type == 'admin') {
                    echo '<a class="btn btn-success" href="update_home_page_logo.php?ty=5">Add new ad</a>';
                }
                ?>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                    <!--<th>Company Name</th>-->
                                                    <th>Website</th>

<!--                                                    <th>Add_on</th>
                                                    <th>Modified on</th>-->
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div>








                <?php
                if ($type == "admin") {
                    //$all = "select tbl_user.fname,tbl_user.lname,tbl_company.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id inner join tbl_company on tbl_company.user_id=tbl_user.user_id where ad_status=2 ";
                    $all = "select * from tbl_ad where ad_status=2";
                } else {
                    $all = "select tbl_user.fname,tbl_user.lname,tbl_user.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id where tbl_user.user_type='" . $type . "' && ad_status=2 && tbl_user.email='" . $_SESSION['uname_admin'] . "' ";
                }






                $resall = mysqli_query($con, $all);

                $dataview = "";
                if (mysqli_num_rows($resall) > 0) {
                    while ($rowall = mysqli_fetch_assoc($resall)) {
                        $dataview .= "<tr >";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= $rowall['ad_id'];
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['c_name']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['title']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['fname']);
//                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['title']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $allowedlimit = 10;
                        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr(ucwords($rowall['message']), 0, $allowedlimit) . "...." : ucwords($rowall['message']);
                        //$dataview .= ucwords($rowall['message']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['phone']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";

                        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
                        // $dataview .= ucwords($rowall['address']);
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['state'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['zipcode'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['added_on'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['modify_on'];
//                        $dataview .= "</td>";

                        $dataview .= "<td>";


                        if ($rowall['status'] == 2 && $type = 'admin') {
                            $dataview .= "<a href='manage_ads.php?ac=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-check' title='Accept Ad' style='color:green;font-size:20px;'></i></a>&nbsp;&nbsp;&nbsp;";
                            $dataview .= "<a href='manage_ads.php?rj=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-close' title='Reject Ad' style='color:red;font-size:20px;'></i></a>";
                        } else if ($rowall['status'] == 3) {
                            $dataview .= "<p class='text-danger'>Rejected</p>";
                        } else if ($rowall['status'] == 1) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>&nbsp;";
                        } else if ($rowall['status'] == 0) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>&nbsp;";
                        } else {
                            $dataview .= "<p class='text-danger'>Pending</p>";
                        }





                        if (isset($_GET['ac'])) {
                            $up = "update tbl_ad set status='1' where ad_id='" . $_GET['ac'] . "'";
                            $resup1 = mysqli_query($con, $up);

                            if (isset($resup1)) {

                                header('Location:manage_ads.php');
                            }
                        }
                        if (isset($_GET['rj'])) {
                            $up = "update tbl_ad set status='3' where ad_id='" . $_GET['rj'] . "'";
                            $resup2 = mysqli_query($con, $up);

                            if ($resup2) {
//             
                                header('Location:manage_ads.php');
                            }
                        }







                        if ($rowall['status'] == "1") {
                            $dataview .= "<a href='update_ad.php?uid=" . $rowall['ad_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
                        }

                        if ($type == 'admin') {
                            $dataview .= "&nbsp;<a href='manage_ads.php?did=" . $rowall['ad_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
                        }
                        $dataview .= "</td>";

                        $dataview .= "</tr>";
                        ?>
                        <div class="modal fade" id="myModal_<?php echo $rowall['ad_id'] ?>" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Right Side-bar AD Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $ed = $rowall['ad_id'];
                                        $s = "select * from tbl_ad where ad_status=2 and  tbl_ad.ad_id='$ed'";
                                        $r = mysqli_query($con, $s);

                                        $row1 = mysqli_fetch_assoc($r);
                                        ?>
                                        <table class="table">
        <!--                                            <tr>
                                                <td>
                                                    User Name :
                                                </td>
                                                <td>
                                            <?php echo ($row1['fname']) . " " . ($row1['lname']); ?>
                                                </td>
                                            </tr>-->
        <!--                                            <tr>
                                                <td>
                                                    Company Name :
                                                </td>
                                                <td>
                                            <?php echo ucfirst($row1['c_name']); ?>
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td>
                                                    Ad Title :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['title']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Website :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['message']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Contact No :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['phone']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Address :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['address']); ?>
                                                </td>
                                            </tr>


                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>        


                        <?php
                    }
                }

                if (isset($_GET['did'])) {
                    $up1 = "delete from tbl_ad where ad_id='" . $_GET['did'] . "'";
                    $resultup1 = mysqli_query($con, $up1);
                }


                $a1 = "SELECT count(ad_id) as tk from tbl_ad where ad_status=2";
                $s1 = mysqli_query($con, $a1);

                $re1 = mysqli_fetch_assoc($s1);

                $co1 = $re1['tk'];

                $edit1 = "block";
                if ($co1 >= 3) {
                    $edit1 = "none";
                }



                if (isset($_GET['id'])) {
                    if ($_GET['st'] == 1) {

                        $up1 = "update tbl_ad set status='0' where ad_id='" . $_GET['id'] . "'";
                    } else {
                        $up1 = "update tbl_ad set status='1' where ad_id='" . $_GET['id'] . "'";
                    }
                    $resultup1 = mysqli_query($con, $up1);

                    if ($resultup1) {
                        header('Location:manage_ads.php');
                    } else {
                        echo "Not";
                        exit();
                    }
                }
                ?>



                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Right Side-bar Ads</strong><?php if ($type == 'admin') { ?>
                                            <a class="btn btn-success" href="update_ad.php?ty=2">Add new ad</a>
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                    <!--<th>Company Name</th>-->
                                                    <!--<th>Name</th>-->
                                                    <th>Ad Title</th>
                                                    <th>Website</th>
                                                    <th>Contact No</th>
                                                    <th>Address</th>
<!--                                                    <th>State</th>
                                                    <th>Zip Code</th>-->
<!--                                                    <th>Add_on</th>
                                                    <th>Modified on</th>-->
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->
                <br><br><br>
                <?php
                if ($type == "admin") {
                    //$all = "select tbl_user.fname,tbl_user.lname,tbl_company.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id inner join tbl_company on tbl_company.user_id=tbl_user.user_id where ad_status=3 ";
                    $all = "select * from tbl_ad where ad_status=3";
                } else {
                    $all = "select tbl_user.fname,tbl_user.lname,tbl_user.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id where tbl_user.user_type='" . $type . "' && ad_status=3 && tbl_user.email='" . $_SESSION['uname_admin'] . "' ";
                }






                $resall = mysqli_query($con, $all);

                $dataview = "";
                if (mysqli_num_rows($resall) > 0) {
                    while ($rowall = mysqli_fetch_assoc($resall)) {
                        $dataview .= "<tr >";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= $rowall['ad_id'];
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['c_name']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['title']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['fname']);
//                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['title']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $allowedlimit = 29;
                        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr($rowall['message'], 0, $allowedlimit) . "...." : $rowall['message'];
                        // $dataview .= ucwords($rowall['message']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['phone']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";

                        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
                        // $dataview .= ucwords($rowall['address']);
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['state'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['zipcode'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['added_on'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['modify_on'];
//                        $dataview .= "</td>";

                        $dataview .= "<td>";


                        if ($rowall['status'] == 2 && $type == 'admin') {
                            $dataview .= "<a href='manage_ads.php?ac=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-check' title='Accept Ad' style='color:green;font-size:20px;'></i></a>&nbsp;&nbsp;&nbsp;";
                            $dataview .= "<a href='manage_ads.php?rj=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-close' title='Reject Ad' style='color:red;font-size:20px;'></i></a>";
                        } else if ($rowall['status'] == 3) {
                            $dataview .= "<p class='text-danger'>Rejected</p>";
                        } else if ($rowall['status'] == 1) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>";
                        } else if ($rowall['status'] == 0) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>";
                        } else {
                            $dataview .= "<p class='text-danger'>Pending</p>";
                        }





                        if (isset($_GET['ac'])) {
                            $up = "update tbl_ad set status='1' where ad_id='" . $_GET['ac'] . "'";
                            $resup1 = mysqli_query($con, $up);

                            if (isset($resup1)) {

                                header('Location:manage_ads.php');
                            }
                        }
                        if (isset($_GET['rj'])) {
                            $up = "update tbl_ad set status='3' where ad_id='" . $_GET['rj'] . "'";
                            $resup2 = mysqli_query($con, $up);

                            if ($resup2) {
//             
                                header('Location:manage_ads.php');
                            }
                        }







                        if ($rowall['status'] == "1") {
                            $dataview .= "&nbsp;&nbsp;<a href='update_ad.php?uid=" . $rowall['ad_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>&nbsp;";
                        }

                        if ($type == 'admin') {
                            $dataview .= "&nbsp;<a href='manage_ads.php?did=" . $rowall['ad_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>&nbsp;&nbsp;";
                        }
                        $dataview .= "</td>";

                        $dataview .= "</tr>";
                        ?>
                        <div class="modal fade" id="myModal_<?php echo $rowall['ad_id'] ?>" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Footer AD Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $ed = $rowall['ad_id'];
                                        $s = "select * from tbl_ad where ad_status=3 and tbl_ad.ad_id='$ed'";
                                        $r = mysqli_query($con, $s);

                                        $row1 = mysqli_fetch_assoc($r);
                                        ?>
                                        <table class="table">
        <!--                                            <tr>
                                                <td>
                                                    User Name :
                                                </td>
                                                <td>
                                            <?php echo ($row1['fname']) . " " . ($row1['lname']); ?>
                                                </td>
                                            </tr>-->
        <!--                                            <tr>
                                                <td>
                                                    Company Name :
                                                </td>
                                                <td>
                                            <?php echo ucfirst($row1['c_name']); ?>
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td>
                                                    Ad Title :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['title']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Website :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['message']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Contact No :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['phone']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Address :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['address']); ?>
                                                </td>
                                            </tr>


                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }

                if (isset($_GET['did'])) {
                    $up1 = "delete from tbl_ad where ad_id='" . $_GET['did'] . "'";
                    $resultup1 = mysqli_query($con, $up1);
                }

                $a1 = "SELECT count(ad_id) as tk from tbl_ad where ad_status=3";
                $s1 = mysqli_query($con, $a1);

                $re1 = mysqli_fetch_assoc($s1);

                $co1 = $re1['tk'];

                $edit1 = "block";
                if ($co1 >= 4) {
                    $edit1 = "none";
                }

                if (isset($_GET['id'])) {
                    if ($_GET['st'] == 1) {

                        $up1 = "update tbl_ad set status='0' where ad_id='" . $_GET['id'] . "'";
                    } else {
                        $up1 = "update tbl_ad set status='1' where ad_id='" . $_GET['id'] . "'";
                    }
                    $resultup1 = mysqli_query($con, $up1);

                    if ($resultup1) {
                        header('Location:manage_ads.php');
                    } else {
                        echo "Not";
                        exit();
                    }
                }
                ?>


                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Footer Ads</strong><?php if ($type == 'admin') { ?>
                                            <a class="btn btn-success" href="update_ad.php?ty=3">Add new ad</a>
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                    <!--<th>Company Name</th>-->
                                                    <!--<th>Name</th>-->
                                                    <th>Ad Title</th>
                                                    <th>Website</th>
                                                    <th>Contact No</th>
                                                    <th>Address</th>
<!--                                                    <th>State</th>
                                                    <th>Zip Code</th>-->
<!--                                                    <th>Add_on</th>
                                                    <th>Modified on</th>-->
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->

                <br><br><br>
                <?php
                if ($type == "admin") {
                    //$all = "select tbl_user.fname,tbl_user.lname,tbl_company.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id inner join tbl_company on tbl_company.user_id=tbl_user.user_id where ad_status=4 ";
                    $all = "select * from tbl_ad where ad_status=4 ";
                } else {
                    $all = "select tbl_user.fname,tbl_user.lname,tbl_user.c_name,tbl_ad.* from tbl_ad INNER JOIN tbl_user on tbl_user.user_id=tbl_ad.user_id where tbl_user.user_type='" . $type . "'&& ad_status=4 && tbl_user.email='" . $_SESSION['uname_admin'] . "' ";
                }






                $resall = mysqli_query($con, $all);

                $dataview = "";
                if (mysqli_num_rows($resall) > 0) {
                    while ($rowall = mysqli_fetch_assoc($resall)) {
                        $dataview .= "<tr >";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= $rowall['ad_id'];
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= "<img src='" . '../' . $rowall['logo'] . "' style='height:60px; width:60px;'>";
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['c_name']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['title']);
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= ucwords($rowall['fname']);
//                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['title']);
                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $allowedlimit = 29;
//                        $dataview .= (mb_strlen($rowall['message']) > $allowedlimit) ? mb_substr($rowall['message'], 0, $allowedlimit) . "...." : $rowall['message'];
//                        //$dataview .= ucwords($rowall['message']);
//                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
                        $dataview .= ucwords($rowall['phone']);
                        $dataview .= "</td>";
                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";

                        $dataview .= (mb_strlen($rowall['address']) > $allowedlimit) ? mb_substr($rowall['address'], 0, $allowedlimit) . "...." : $rowall['address'];
                        // $dataview .= ucwords($rowall['address']);
                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['state'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td>";
//                        $dataview .= $rowall['zipcode'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['added_on'];
//                        $dataview .= "</td>";
//                        $dataview .= "<td data-toggle='modal' data-target='#myModal_" . $rowall['ad_id'] . "' style='cursor: pointer;'>";
//                        $dataview .= $rowall['modify_on'];
//                        $dataview .= "</td>";

                        $dataview .= "<td>";


                        if ($rowall['status'] == 2 && $type == 'admini') {
                            $dataview .= "<a href='manage_ads.php?ac=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-check' title='Accept Ad' style='color:green;font-size:20px;'></i></a>&nbsp;";
                            $dataview .= "<a href='manage_ads.php?rj=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-close' title='Reject Ad' style='color:red;font-size:20px;'></i></a>";
                        } else if ($rowall['status'] == 3) {
                            $dataview .= "<p class='text-danger'>Rejected</p>";
                        } else if ($rowall['status'] == 1) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-on' style='color:green;font-size:30px;'></i></a>&nbsp;";
                        } else if ($rowall['status'] == 0) {
                            $dataview .= "<a href='manage_ads.php?id=" . $rowall['ad_id'] . "&st=" . $rowall['status'] . "'><i class='fa fa-toggle-off' style='color:red;font-size:30px;'></i></a>&nbsp;";
                        } else {
                            $dataview .= "<p class='text-danger'>Pending</p>";
                        }





                        if (isset($_GET['ac'])) {
                            $up = "update tbl_ad set status='1' where ad_id='" . $_GET['ac'] . "'";
                            $resup1 = mysqli_query($con, $up);

                            if (isset($resup1)) {

                                header('Location:manage_ads.php');
                            }
                        }
                        if (isset($_GET['rj'])) {
                            $up = "update tbl_ad set status='3' where ad_id='" . $_GET['rj'] . "'";
                            $resup2 = mysqli_query($con, $up);

                            if ($resup2) {
//             
                                header('Location:manage_ads.php');
                            }
                        }







                        if ($rowall['status'] == "1") {
                            $dataview .= "&nbsp;<a href='update_ad.php?uid=" . $rowall['ad_id'] . "'><i class='fa fa-edit' style='color:blue;font-size:30px;'></i></a>";
                        }
                        if ($type == 'admin') {
                            $dataview .= "&nbsp;<a href='manage_ads.php?did=" . $rowall['ad_id'] . "'><i class='fa fa-trash' onclick='return checkDelete()' style='color:red;font-size:20px;'></i></a>";
                        }
                        $dataview .= "</td>";

                        $dataview .= "</tr>";
                        ?>

                        <div class="modal fade" id="myModal_<?php echo $rowall['ad_id'] ?>" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">10 Slide AD Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        $ed = $rowall['ad_id'];
                                        $s = "select * from tbl_ad where ad_status=4  and  tbl_ad.ad_id='$ed'";
                                        $r = mysqli_query($con, $s);

                                        $row1 = mysqli_fetch_assoc($r);
                                        ?>
                                        <table class="table">
        <!--                                            <tr>
                                                <td>
                                                    User Name :
                                                </td>
                                                <td>
                                            <?php echo ($row1['fname']) . " " . ($row1['lname']); ?>
                                                </td>
                                            </tr>-->
        <!--                                            <tr>
                                                <td>
                                                    Company Name :
                                                </td>
                                                <td>
                                            <?php echo ucfirst($row1['c_name']); ?>
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td>
                                                    Ad Title :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['title']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Website :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['message']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Contact No :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['phone']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Address :
                                                </td>
                                                <td>
                                                    <?php echo ucfirst($row1['address']); ?>
                                                </td>
                                            </tr>


                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }

                $a1 = "SELECT count(ad_id) as tk from tbl_ad where ad_status=4";
                $s1 = mysqli_query($con, $a1);

                $re1 = mysqli_fetch_assoc($s1);

                $co1 = $re1['tk'];

                $edit1 = "block";
                if ($co1 >= 10) {
                    $edit1 = "none";
                }


                if (isset($_GET['did'])) {
                    $up1 = "delete from tbl_ad where ad_id='" . $_GET['did'] . "'";
                    $resultup1 = mysqli_query($con, $up1);
                }


                if (isset($_GET['id'])) {
                    if ($_GET['st'] == 1) {

                        $up1 = "update tbl_ad set status='0' where ad_id='" . $_GET['id'] . "'";
                    } else {
                        $up1 = "update tbl_ad set status='1' where ad_id='" . $_GET['id'] . "'";
                    }
                    $resultup1 = mysqli_query($con, $up1);

                    if ($resultup1) {
                        header('Location:manage_ads.php');
                    } else {
                        echo "Not";
                        exit();
                    }
                }
                ?>

                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">10 Slider  Ads</strong><?php if ($type == 'admin') { ?>
                                            <a class="btn btn-success"  href="update_ad.php?ty=4" >Add new ad</a>
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Image</th>
                                                    <!--<th>Company Name</th>-->
                                                    <!--<th>Name</th>-->
                                                    <th>Ad Title</th>
                                                    <!--<th>Website</th>-->
                                                    <th>Contact No</th>
                                                    <th>Address</th>
<!--                                                    <th>State</th>
                                                    <th>Zip Code</th>-->
<!--                                                    <th>Add_on</th>
                                                    <th>Modified on</th>-->
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $dataview;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div><!-- .animated -->
                </div><!-- .content -->


                <div class="content">
                    <div class="animated fadeIn">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">10 Business Under Ads</strong><?php if ($type == 'admin') { ?>
                                            <a class="btn btn-success" href="update_ad.php?ty=8">Add new ad</a>
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Image</th>                                                  
                                                    <th>Website</th>
                                                    <th>Contact No</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo $datafeed;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div> 
                </div> 



            </section>


        </div>



        <div class="clearfix"></div>

        <?php
        include ('footer.php');
        ?>

    </div>
    <?php
    include('script.php');
    ?>
    <div id="container">



    </div>

</body>
<?php

function send_smtp_email($host, $username, $password, $port, $from, $from_name, $to, $subject, $message) {
    require_once ('vendor/autoload.php');
    $mail = new PHPMailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $host;  // Specify main and backup SMTP servers //get_Email_account ();
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $username;                 // SMTP username
    $mail->Password = $password;                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $port;                                    // TCP port to connect to

    $mail->From = $from;
    $mail->FromName = $from_name;
    $mail->addAddress($to);               // Name is optional
    $mail->WordWrap = 50;                                 // Set word wrap to 50 characters

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $message;

    if (!$mail->send()) {
        echo '<br/>Message could not be sent.';
        echo '<br/>Mailer Error: ' . $mail->ErrorInfo;
        ob_flush();
        return "0";
    } else {
//            echo '<br/>Message has been sent';
//            ob_flush();
        return '1';
    }
}
?>



<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are you sure want to delete?');
    }
</script>