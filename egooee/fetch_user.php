<?php

//fetch_user.php

include('database_connection.php');

session_start();

$query = "
SELECT * FROM friend_list as f
INNER JOIN tbl_user as u
ON (f.to_id=u.user_id)
WHERE f.from_id = '" . $_SESSION['user_id'] . "' AND friend_status=1
";

$statement = mysqli_query($con, $query);


$result = mysqli_fetch_all($statement, MYSQLI_ASSOC);

$output = '
<table class="table table-bordered table-striped">
	<tr>
		<th width="20%">Image</td>
		<th width="40%">Name</td>
		<th width="20%">Status</td>
		<th width="20%">Action</td>
	</tr>
';

foreach ($result as $row) {
    $status = '';
    $current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 10 second');
    $current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
    $user_last_activity = fetch_user_last_activity($row['user_id'], $con);
    if ($user_last_activity > $current_timestamp) {
        $status = '<span class="label label-success">Online</span>';
    } else {
        $status = '<span class="label label-danger">Offline</span>';
    }
    $image=$row['image'];
    if($row['image']==''){
        $image = "img/default-avatar.png";
    }
    $output .= '
	<tr>
                <td><img src="' . $image . '" height="40"></td>
		<td><div class="notification">' . $row['username'] . '</div><span class="badge badge-primary pull-right">' . count_unseen_message($row['user_id'], $_SESSION['user_id'], $con) . '</span> ' . fetch_is_type_status($row['user_id'], $con) . '</td>
		<td>' . $status . '</td>
		<td><button type="button" class="btn btn-info btn-xs start_chat" data-touserid="' . $row['user_id'] . '" data-tousername="' . $row['username'] . '">Chat</button></td>
	</tr>
	';
}

$output .= '</table>';

echo $output;
?>