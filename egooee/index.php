<?php
include 'connection.php';
include_once './header.php';

$soon_query = "select * from setting where setting_id=10 AND status=1";
$soon_res = mysqli_query($con, $soon_query);
if (mysqli_num_rows($soon_res) > 0) {
    require 'homepage.php';
} else {
    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
        $check_plan_query = "select * from tbl_buy_plan where user_id='" . $_SESSION['user_id'] . "' AND plan_number=1";
        $check_plan_result = mysqli_query($con, $check_plan_query);
        if (mysqli_num_rows($check_plan_result) > 0) {
            echo "<input type='hidden' id='open_plan_pop' value='yes_open'/>";
        }
    }
    ?>
    <!-- Hero section -->
    <section class="hero-section">
        <div class="hero-slider owl-carousel <?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? '' : 'mysetting'; ?>">
            <?php
            $sql = "select tbl_category.name,tbl_home_logo.* from tbl_home_logo INNER JOIN tbl_category on tbl_category.category_id=tbl_home_logo.business_type_id";
            $res = mysqli_query($con, $sql);


            if (mysqli_num_rows($res) > 0) {
                while ($row = mysqli_fetch_assoc($res)) {
                    ?>

                    <div class="hs-item set-bg" data-setbg="img/001.jpg">
                        <div class="row">
                            <div class="col-lg-2 text-center text-lg-left" style="margin-top: 10px;">
                                <!-- logo -->
                                <a href="./index.php" class="site-logo">
                                    <img src="img/logo_color.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="container m_v1" style="padding-top:100px;">
                            <div class="row">

                                <div class="col-xl-4 col-lg-4 text-white m_v10">

                                    <?php
                                    $adql = "select * from tbl_ad where ad_status=0 and status=1 LIMIT 3";
                                    $adsl = mysqli_query($con, $adql);

                                    $adarayl = [];
                                    while ($adrowl = mysqli_fetch_assoc($adsl)) {
                                        $adarayl[] = [$adrowl['logo'], $adrowl['message']];
                                    }
                                    $adqr = "select * from tbl_ad where ad_status=1 and status=1 LIMIT 3";
                                    $adsr = mysqli_query($con, $adqr);

                                    $adarayr = [];
                                    while ($adrowr = mysqli_fetch_assoc($adsr)) {
                                        $adarayr[] = [$adrowr['logo'], $adrowr['message']];
                                    }


                                    // print_r($adaray) ;
                                    ?>


                                    <a href="<?php echo isset($adarayl[0][1]) ? $adarayl[0][1] : ''; ?>" target="_blank"><div class="thirdparty_ads text-white" style="background-image: url('<?php echo (isset($adarayl[0][0]) && $adarayl[0][0] != "") ? $adarayl[0][0] : "img/no_logo.png"; ?>') ; background-size: cover;background-position:center;background-repeat:no-repeat;">
            <!--                                    <span>AD 1</span>-->
                                        </div></a>
                                    <a href="<?php echo isset($adarayl[1][1]) ? $adarayl[1][1] : ''; ?>" target="_blank"><div class="thirdparty_ads text-white" style="background-image: url('<?php echo (isset($adarayl[1][0]) && $adarayl[1][0] != "") ? $adarayl[1][0] : "img/no_logo.png"; ?>') ; background-size: cover;background-position:center;background-repeat:no-repeat;">
                                        <!--<span>AD 2</span>-->
                                        </div></a>
                                    <a href="<?php echo isset($adarayl[2][1]) ? $adarayl[2][1] : ''; ?>" target="_blank"><div class="thirdparty_ads text-white" style="background-image: url('<?php echo (isset($adarayl[2][0]) && $adarayl[2][0] != "") ? $adarayl[2][0] : "img/no_logo.png"; ?>') ; background-size: cover;background-position:center;background-repeat:no-repeat;">
                                        <!--<span>AD 3</span>-->
                                        </div></a>
                                </div>
                                <div class="col-xl-4 col-lg-4 text-white <?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? '' : 'mhide'; ?>">
                                    <!--<span>New Arrivals</span>-->
                                    <h2><a href="<?php echo (isset($row['url']))?$row['url']:''; ?>" target="_blank"><img src="<?php echo $row['logo'] ?>" style="height:210px;margin-top: -110px;" class="m_v11"></a></h2>

                                </div>
                                <div class="col-xl-4 col-lg-4 text-white m_v10 <?php echo (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') ? '' : 'mhide'; ?>">
                                    <a href="<?php echo isset($adarayr[0][1]) ? $adarayr[0][1] : ''; ?>" target="_blank"><div class="thirdparty_ads text-white" style="background-image: url('<?php echo (isset($adarayr[0][0]) && $adarayr[0][0] != "") ? $adarayr[0][0] : "img/no_logo.png"; ?>') ; background-size: cover;background-position:center;background-repeat:no-repeat;">
                                        <!--<span>AD 4</span>-->
                                        </div></a>
                                    <a href="<?php echo isset($adarayr[1][1]) ? $adarayr[1][1] : ''; ?>" target="_blank"><div class="thirdparty_ads text-white" style="background-image: url('<?php echo (isset($adarayr[1][0]) && $adarayr[1][0] != "") ? $adarayr[1][0] : "img/no_logo.png"; ?>') ; background-size: cover;background-position:center;background-repeat:no-repeat;" >
                                        <!--<span>AD 5</span>-->
                                        </div></a>
                                    <a href="<?php echo isset($adarayr[2][1]) ? $adarayr[2][1] : ''; ?>" target="_blank"><div class="thirdparty_ads text-white" style="background-image: url('<?php echo (isset($adarayr[2][0]) && $adarayr[2][0] != "") ? $adarayr[2][0] : "img/no_logo.png"; ?>') ; background-size: cover;background-position:center;background-repeat:no-repeat;" >
                                        <!--<span>AD 6</span>-->
                                        </div></a>
                                </div>
                            </div>
                            <div class="row" style='text-align: justify;'>
                                <div class="col-md-12" >
                                    <div class="text-center text-white set_text" >
                                        <!--At Egooee:  List your Business or Post an Ad: Get Reviews and Feedbacks: Write a post: Blog or Comment-->   
            <!--                                    <p style="line-height:20px;">  <strong>Company Name : </strong><?php echo $row['company_name'] ?>&nbsp;&nbsp;
                                            <strong>Business : </strong><?php echo $row['name'] ?>&nbsp;&nbsp;
                                        </p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>


                <div class="hs-item set-bg" data-setbg="img/001.jpg">
                    <div class="row">
                        <div class="col-lg-2 text-center text-lg-left" style="margin-top: 10px;">
                            <!--logo--> 
                            <a href="./index.php" class="site-logo">
                                <img src="img/logo_color.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="container" style="padding-top:100px;">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 text-white">
                                <div class="thirdparty_ads text-white">
                                    <span>AD 1</span>
                                </div>
                                <div class="thirdparty_ads text-white">
                                    <span>AD 2</span>
                                </div>
                                <div class="thirdparty_ads text-white">
                                    <span>AD 3</span>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 text-white">
                                <!--<span>New Arrivals</span>-->
                                <h2><img src="img/001.jpg"></h2>

                            </div>
                            <div class="col-xl-4 col-lg-4 text-white">
                                <div class="thirdparty_ads text-white">
                                    <span>AD 4</span>
                                </div>
                                <div class="thirdparty_ads text-white">
                                    <span>AD 5</span>
                                </div>
                                <div class="thirdparty_ads text-white">
                                    <span>AD 6</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style='text-align: justify;'>
                            <div class="col-md-12" >
                                <div class="text-center text-white set_text" >
                                    <!--At Egooee:  List your Business or Post an Ad: Get Reviews and Feedbacks: Write a post: Blog or Comment-->   

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php }
            ?>
        </div>
        <div class="container">
            <div class="slide-num-holder" >

                                    <!--<a  href="logout.php"><input style="color: white;" type="button" class="btn head_btn3" value="Log Out"></a>-->

                <div class="up-item m_v15" style="font-size:13px;">

                    <?php
                    if (isset($_SESSION['useremail']) && $_SESSION['useremail'] != '') {
                        ?>
                        <span id="user_setting" class="dropdown-toggle sub-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="flaticon-profile" style="font-size:22px;"></i>&nbsp;<?php echo ucwords($_SESSION['username']); ?></span>
                        <ul class="dropdown-menu m_v5" id="user_setting_dd">
                            <li><i class="fa fa-user" style="color:#007bff;"></i><a href="javascript:void(0);" onclick="profile_popup('<?php echo $_SESSION['user_type'] ?>');"> Profile</a></li>
                            <?php
                            if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'business_user') {
                                ?>
                                <li><i class="fa fa-building" style="color:#007bff;"></i><a href="company.php"> Company</a></li>
                                <?php
                            }
                            ?>
                            <!--<li><a href="#">Settings</a></li>-->
                            <li><i class="fa fa-chain" style="color:#007bff;"></i><a href="javascript:void(0);" data-toggle="modal" data-target="#change_password"> Change Password</a></li>
                            <li><i class="fa fa-sign-out" style="color:#007bff;"></i><a href="logout.php"> Log out</a></li>
                        </ul>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--<a href="logout.php"><input style="color:white" type="button" class="btn head_btn3" value="Log Out"></a>-->
                        <?php
                    } else {
                        ?>
                        <span id="user_setting" class="dropdown-toggle sub-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="flaticon-profile" style="font-size:22px;"></i>&nbsp;Sign In</span>
                        <ul class="dropdown-menu" id="user_setting_dd">
                            <li class="fa fa-sign-in" style="color:#007bff;"><a href="#" data-toggle="modal" data-target="#signin"> Sign In</a></li>
                            <li class="fa fa-user" style="color:#007bff;"><a href="register.php"> Create Account</a></li>
                        </ul>
                        <!--<a href="#" data-toggle="modal" data-target="#signin" style="color:white" >Sign In</a> or <a href="#" data-toggle="modal" data-target="#register_popup" style="color: white" >Create Account</a>-->

                        <?php
                    }
                    ?>

                </div>

            </div>
        </div>
    </section>
    <!-- Hero section end -->
    <?php
    require_once 'navbar.php';
    ?>

    <!-- Product filter section -->
    <section class="top-letest-product-section product-filter-section">
        <div class="container-fluid">

            <?php
            require_once 'searchbar.php';
//        require_once '10_slider.php';
            ?>

            <div class="row" style="margin-top:10px;">
                <div class="col-lg-9 ad_view_4">
                    <div class="row">
                        <div class="col-1"></div>
                        <?php
                        $sql = "select * from tbl_ad where status=1 AND ad_status=5 ORDER BY ad_id DESC limit 3";
                        $res = mysqli_query($con, $sql);
                        if ($res) {
                            while ($data = mysqli_fetch_assoc($res)) {
                                ?>
                                <div class="col-md col m_v24">
                                    <div class="">

                                        <div class="">
                                            <a href="<?php echo $data['message'] ?>" target="_blank">
                                                <center> <img src="<?php echo $data['logo'] ?>" style="height:180px;width:200px;" class="m_v20" alt=""></center>
                                            </a>

                                        </div>

<!--                                        <div class="pi-text text-center" style="padding:0 15px;height:100%;">

                                            <div style="clear:both;"></div>
                                            <p><b><?php echo ucwords($data['title']) ?></b> </p>
                                            <p><?php echo $data['address'] ?></p>
                                        </div>-->
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <!--                    <div class="col-lg-3 col-sm-6 col-6" style="padding-right:0;margin-bottom: 10px;">
                                                
                                            </div>-->
                        <div class="col-1"></div>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-sm-12" id="top_talk">
                    <!--top talk-->
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <center><h4 style="padding:20px;">Top Talk</h4></center>
                            <div class="row">
                                <?php
                                $sql1 = "select * from tbl_talk where status=1 AND type=0 ORDER BY tbl_talk.talk_id DESC limit 2";
                                $res1 = mysqli_query($con, $sql1);
                                if ($res1) {
                                    while ($data1 = mysqli_fetch_assoc($res1)) {
                                        ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12" style="margin-bottom: 10px;">
                                            <div class="product-item" style="border-left:1px solid #dfe1e5;">

                                                <div class="pi-text text-center" style="padding:0 15px;height:100%;display: inline-block;">

                                                    <p style="text-align:justify;font-size: 13px;word-break: break-word;" class="name"><img src="<?php echo $data1['logo'] ?>" style="height:160px;width:160px;padding-bottom: 5px;padding-right: 10px;" class="m_v19" alt="" align='left'><?php echo ucfirst($data1['description']) ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php if (isset($_SESSION['user_id'])) { ?>
                                                                                        <!--<small data-toggle="modal" data-target="#info" style="float: right;cursor: pointer;color: blue" >Request For Top Talk</small>-->
                                <small style="float: right;cursor: pointer;color: blue" ><a href="top_talk.php">Request For Top Talk</a></small>
                            <?php } ?>
                        </div>
                    </div>
                    <!--top rated business-->
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-12">
                            <center><h4 style="padding:20px;">Top Rated Business</h4></center>
                            <div class="row">
                                <?php
                                $sql2 = "select tb.*,c.name from tbl_top_business as tb inner join tbl_category as c on (tb.c_title=c.category_id) where tb.status=1 ORDER BY top_business_id DESC limit 4";
                                $res2 = mysqli_query($con, $sql2);
                                if ($res2) {
                                    while ($data2 = mysqli_fetch_assoc($res2)) {
                                        ?>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-3 m_v24" style="margin-bottom: 10px;">
                                            <div class="">

                                                <div class="">   
                                                    <center> <a href="<?php echo $data2['c_description'] ?>" target="_blank"><img src="<?php echo $data2['logo'] ?>" style="height:200px;width:200px;" class="m_v20" alt=""></a></center>
                                                </div>

<!--                                                <div class="pi-text text-center" style="padding:0 15px; height: 100%;">
                                                    <p><b><?php echo ucwords($data2['c_name']) ?></b> </p>
                                                    <p><?php echo $data2['name'] ?></p>

                                                </div>-->
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                    <!--trendings-->
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <center><h4 style="padding:20px;">Trendings</h4></center>
                            <div class="row">
                                <?php
                                $sql1 = "select * from tbl_talk where status=1 AND type=1 ORDER BY tbl_talk.talk_id DESC limit 2";
                                $res1 = mysqli_query($con, $sql1);
                                if ($res1) {
                                    while ($data1 = mysqli_fetch_assoc($res1)) {
                                        ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12" style="margin-bottom: 10px;">
                                            <div class="product-item" style="border-left:1px solid #dfe1e5;">

                                                <div class="pi-text text-center" style="padding:0 15px;height:100%;display: inline-block;">

                                                    <p style="text-align:justify;font-size: 13px;word-break: break-word;" class="name"><img src="<?php echo $data1['logo'] ?>" style="height:160px;width:160px;padding-bottom: 5px;padding-left: 10px;" class="m_v19" alt="" align='right'><?php echo ucfirst($data1['description']) ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php if (isset($_SESSION['user_id'])) { ?>
                                                                                                <!--<small data-toggle="modal" data-target="#info1" style="float: right;cursor: pointer;color: blue" >Request For Trending</small>-->
                                <small style="float: right;cursor: pointer;color: blue" ><a href="trending.php">Request For Trending</a></small>
                            <?php } ?>
                        </div>
                    </div>
                    <!--restaurants-->
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">

                            <?php
                            $temp_q = "select * from tbl_category where show_status=1";
                            $temp_r = mysqli_query($con, $temp_q);
                            if (mysqli_num_rows($temp_r)) {
                                $temp_d = mysqli_fetch_assoc($temp_r);
                            }
                            ?>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 cal-sm-12">
                                    <center><h4 style="padding:20px;"><?php echo (isset($temp_d)) ? $temp_d['name'] : ''; ?></h4></center>
                                    <div class="row">
                                        <?php
                                        $rest_sql = "select c.*,co.* from tbl_category as c inner join tbl_company as co on (co.category_id=c.category_id) where c.show_status=1 limit 10";
                                        $rest_res = mysqli_query($con, $rest_sql);
                                        $ri = 1;
                                        if (mysqli_num_rows($rest_res)) {
                                            while ($rest_data = mysqli_fetch_assoc($rest_res)) {
                                                ?>
                                                <div class="col-lg-12 col-sm-12 col-12 m_v4" style="margin-bottom: 10px;">

                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-3 col-3 col-lg-3">
                                                                    <img src="<?php echo $rest_data['c_logo']; ?>" class="img-responsive">
                                                                </div>
                                                                <div class="col-md-9 col-lg-9 col-9">
                                                                    <div><b><?php echo ucfirst($rest_data['c_name']); ?></b></div>
                                                                    <div style="font-size: 13px;text-align: justify;"><?php echo ucfirst($rest_data['c_description']); ?></div>
                                                                    <div style="font-size: 13px;text-align: justify;"><?php echo ucfirst($rest_data['c_address']); ?></div>
                                                                    <!--<div style="font-size: 13px;text-align: justify;"><?php echo ucfirst($rest_data['phone']); ?></div>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                if ($ri == 5) {

                                                    $sql_ad = "select * from tbl_ad where status=1 AND ad_status=8 ORDER BY ad_id DESC limit 2";
                                                    $res_ad = mysqli_query($con, $sql_ad);
                                                    if ($res_ad) {
                                                        while ($data_ad = mysqli_fetch_assoc($res_ad)) {
                                                            ?>
                                                            <div class="col-md-6 col-6" style="margin-bottom: 10px;">                                   
                                                                <a href="<?php echo $data_ad['message'] ?>" target="_blank">
                                                                    <center><img src="<?php echo $data_ad['logo'] ?>" style="height:180px;width:auto;" class="m_v25" alt=""></center>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                $ri++;
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>



                                <div class="col-lg-6 col-md-6  cal-sm-12 m_v4">

                                    <?php
                                    $sq = "SELECT * FROM `tbl_dish` WHERE `status`=1 ORDER BY category,dish_id";
                                    $rs = mysqli_query($con, $sq);
                                    $categories_printed = array();
                                    $index = 0;
                                    while ($row = mysqli_fetch_assoc($rs)) {
                                        $c = $row['category'];
                                        if (!in_array($c, $categories_printed)) {
                                            if (count($categories_printed) > 0) {
                                                echo '</ul>';
                                            }
                                            $categories_printed[] = $c;
                                            if(strrchr($c,'us')){
                                                $c = strtoupper($c);
                                            }else{
                                                $c= ucfirst($c);
                                            }
                                            echo '<center><h4 style="padding:20px;">' . $c . ' Dishes </h4></center>';
                                            echo '<button type="button" class="btn text-white pull-right" style="background:black;border-radius:0;" onclick="slick_move_right(\'' . $row['dish_id'] . '\')"><i class="fa fa-chevron-right"></i></button>';
                                            echo '<button type="button" class="btn text-white pull-right" style="background:black;border-radius:0;" onclick="slick_move_left(\'' . $row['dish_id'] . '\')"><i class="fa fa-chevron-left"></i></button>';
                                            echo '<div style="clear:both;"></div>';
                                            echo '<ul class="dish_slider" id="dish_slider_' . $row['dish_id'] . '">';
                                            $index = 0;
                                        }
                                        if ($index == 0) {
                                            echo '<li>';
                                        }
                                        ?>
                                        <div class="row one-time">
                                            <div class="col-lg-12  col-sm-12 col-12" style="margin-bottom: 10px;">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-3 col-3 col-lg-3">
                                                                <img src="<?php echo $row['image']; ?>" class="img-responsive">
                                                            </div>
                                                            <div class="col-md-9 col-lg-9 col-9">
                                                                <div><b><?php echo ucfirst($row['dish_name']); ?></b></div>
                                                                <div style="font-size: 13px;text-align: justify;"><?php echo ucfirst($row['message']); ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $index++;
                                        if ($index == 3) {
                                            echo '</li>';
                                            $index = 0;
                                        }
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12">
                    <?php
                    require_once 'right_side_bar_ad.php';
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Product filter section end -->

    <!-- Banner section -->
    <section class="banner-section mhide">
        <div class="container-fluid">
            <div class="row">
                <?php
                $s8 = "select * from tbl_ad where status=1 and ad_status=3  limit 4";
                $r8 = mysqli_query($con, $s8);

                if (mysqli_num_rows($r8) > 0) {
                    while ($row8 = mysqli_fetch_assoc($r8)) {
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-3 m_v24">
                            <div class="">
                                <div class="">
                                    <a href="<?php echo $row8['message'] ?>" target="_blank"><img src="<?php echo $row8['logo'] ?>" alt="" class="m_v20" style="height:250px;width:250px;"></a>

                                </div>
                                <br/>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    echo "";
                }
                ?>

            </div>
        </div>
    </section>
    <!-- Banner section end  -->
    <?php
}
include_once './footer.php';
?>

<!--<div id="info" class="modal fade" role="dialog">
    <div class="modal-dialog">

         Modal content
        <div class="modal-content">
            <form action = "#" method = "post" enctype = "multipart/form-data" class = "form-horizontal">
                <div class="modal-header">

                    <h4 class="modal-title">Request For Top Talk</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">



                    <div class="row">
                        <div class="col-md-12">
                            Choose Image : <input type = "file" class = "" required="" id = "add_logo" name = "add_logo">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            Description :<textarea class = "form-control" required = "" name = "desc" placeholder = "Description"></textarea>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <input type="submit"  class="btn btn-info" name="btnsubmit" value="Submit" style="width:15%">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>-->


<!--<div id="info1" class="modal fade" role="dialog">
    <div class="modal-dialog">

         Modal content
        <div class="modal-content">
            <form action = "#" method = "post" enctype = "multipart/form-data" class = "form-horizontal">
                <div class="modal-header">

                    <h4 class="modal-title">Request For Trending</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">



                    <div class="row">
                        <div class="col-md-12">
                            Choose Image : <input type = "file" class = "" required="" id = "add_logo" name = "add_logo">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            Description :<textarea class = "form-control" required = "" name = "desc" placeholder = "Description"></textarea>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <input type="submit"  class="btn btn-info" name="btnsub" value="Submit" style="width:15%">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>-->
<script type="text/javascript">
    $(document).ready(function () {
        $('.talk_slider').slick({
            infinite: true,
            autoplay: true,
            arrows: false,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });


    $("[id^='dish_slider_']").slick({
//    $('.dish_slider').slick({
//        nextArrow: '<i class="fa fa-arrow-circle-right"></i>',
//        prevArrow: '<i class="fa fa-arrow-circle-left"></i>',
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true

    });

    function slick_move_left(id) {
        $("#dish_slider_" + id).slick('slickPrev');
    }
    function slick_move_right(id) {
        $("#dish_slider_" + id).slick('slickNext');
    }

</script>

