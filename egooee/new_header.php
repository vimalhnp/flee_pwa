<div class="row m_v23" style="width:100%;">
    <div class="col-lg-2 text-center text-lg-left m_v7" style="margin-top: 10px;margin-bottom: 20px;">
        <!-- logo -->
        <a href="./index.php" class="site-logo">
            <img src="img/logo_color.png" alt="">
        </a>
    </div>
     <?php
            $activePage = basename($_SERVER['PHP_SELF'], "");
            ?>
    <div class="col-lg-1 col-sm-12"></div>
    <div class="col-lg-6" style="padding-top: 20px;">
        <form method="post" action="business_search.php" class="<?= ($activePage == 'feed.php') ? 'd-none' : ''; ?>">
            <div class="row" style="max-width: 632px;margin: 0 auto;">
                <div class="col-lg-8 col-md-8 col-sm-8 m_v4" style="padding-right:0;">
                    <input type="text" class="form-control" name="search1" placeholder="Search Egooee near you">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 m_v6" style="padding-left:5px;">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search2" placeholder="State/Zip Code">
                        <div class="input-group-append">
                            <button class="btn btn-default" style="border:1px solid #ced4da;" type="submit" name="search"><i class="fa fa-search"></i></button> 
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form method="post" action="feed.php" class="<?= ($activePage == 'feed.php') ?'':'d-none'; ?>" style="max-width: 500px;">
            <div class="input-group">
                <input type="search" class="form-control" name="friend_search" id="friend_search" placeholder="Search Users">
                <input type="hidden" class="form-control" name="login_user_id" value="<?php echo $_SESSION['user_id'] ?>">
                <div class="input-group-append">
                    <button class="btn btn-default" style="border:1px solid #ced4da;" type="submit" name="search_user"><i class="fa fa-search"></i></button> 
                </div>
            </div>
        </form>
    </div>
    <?php
    if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'community_user') {
        echo '<div class="col-lg-1 col-sm-12" style="padding-top: 25px;text-align:center;"><a href="feed.php" title="Go to feed page" class="mhide"><i class="fa fa-bell" style="font-size: 24px;"></i></a></div>';
    } else {
        echo '<div class="col-lg-1 col-sm-12"></div>';
    }
    ?>

    <div class="col-lg-2 text-center text-lg-left m_v8 m_v15" style="padding-top: 20px;">
        <div class="up-item m_v8" style="font-size:13px;">

            <?php
            if (isset($_SESSION['useremail']) && $_SESSION['useremail'] != '') {
                ?>
                <span id="user_setting" class="dropdown-toggle sub-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="flaticon-profile" style="font-size:22px;"></i>&nbsp;<?php echo ucwords($_SESSION['username']); ?></span>
                <ul class="dropdown-menu m_v9" id="user_setting_dd">
                    <li><i class="fa fa-user" style="color:#007bff;"></i><a href="javascript:void(0);" data-toggle="modal" data-target="#profile"> Profile</a></li>
                    <!--<li><a href="#">Settings</a></li>-->
                    <li><i class="fa fa-chain" style="color:#007bff;;"></i><a href="javascript:void(0);" data-toggle="modal" data-target="#change_password"> Change Password</a></li>
                    <li><i class="fa fa-sign-out" style="color:#007bff;;"></i><a href="logout.php"> Log out</a></li>
                </ul>

                                                                                                                                                                <!--<a href="logout.php"><input style="color:white" type="button" class="btn head_btn3" value="Log Out"></a>-->
                <?php
            } else {
                ?>
                <span id="user_setting" class="dropdown-toggle sub-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="flaticon-profile" style="font-size:22px;"></i>&nbsp;Sign In</span>
                <ul class="dropdown-menu m_v9" id="user_setting_dd">
                    <li><i class="fa fa-sign-in" style="color:#007bff;" ></i><a href="#" data-toggle="modal" data-target="#signin"> Sign In</a></li>
                    <li><i class="fa fa-user" style="color:#007bff;" ></i><a href="register.php"> Create Account</a></li>
                </ul>
                <!--<a href="#" data-toggle="modal" data-target="#signin" style="color:white" >Sign In</a> or <a href="#" data-toggle="modal" data-target="#register_popup" style="color: white" >Create Account</a>-->

                <?php
            }
            ?>

        </div>
    </div>
</div>
