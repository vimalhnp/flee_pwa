<?php
include 'connection.php';
include_once './header.php';
require_once 'new_header.php';
?>

<?php
require_once 'navbar.php';
?>

<!-- Product filter section -->
<section class="top-letest-product-section product-filter-section">
    <div class="container-fluid">
        <!--        <div class="section-title">
                    <h2>Upcoming Events</h2>
                </div>-->
        <?php
        require_once '10_slider.php';
        ?>
        
        <div class="row">
            <div class="col-lg-9 col-sm-12">
                <center><h4 style="margin-bottom: 20px;padding:20px;">Upcoming Events</h4></center>
                <div class="row">
                    <?php
                    $sql = "select * from tbl_event where status=1 ORDER BY event_id DESC";

                    $res = mysqli_query($con, $sql);

                    if (mysqli_num_rows($res)) {
                        while ($data = mysqli_fetch_assoc($res)) {
                            ?>
                            <div class="col-lg-6 col-sm-12 col-12" style="padding-right:0;margin-bottom: 10px;">
                                <div class="product-item" style="border-left:1px solid #dfe1e5;">

                                    <div class="pi-pic">

                                        <center> <img src="<?php echo $data['image'] ?>" style="height:250px;width:auto;" alt=""></center>


                                    </div>
                                    <div style="padding:0 20px;">
                                        <div>Name : <b><?php echo ucwords($data['name']) ?></b> </div>
                                        <div>State : <?php echo $data['state'] ?></div>
                                        <div>Start : <?php echo $data['start_date'] ?></div>
                                        <div>End : <?php echo $data['end_date'] ?> </div>
                                        <div>Description : <?php echo $data['description'] ?> </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo "<div class='col-md-12'><center><h2>NO UPCOMING EVENTS RIGHT NOW!</h2></center></div>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <div class="row">

                    <?php
                    require_once 'right_side_bar_ad.php';
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product filter section end -->
<?php
include_once './footer.php';
?>
