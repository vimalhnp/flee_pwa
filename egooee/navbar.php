<header class="header-section" id="myheader">
    <nav class="main-navbar">
        <div class="container">

            <?php
            $activePage = basename($_SERVER['PHP_SELF'], "");
            ?>


            <!-- menu -->
            <ul class="main-menu">
                <?php
                $ip_all = file("ip.txt");
                $ip_cur = $_SERVER['REMOTE_ADDR'] . "\n";
                if (!in_array($ip_cur, $ip_all)) {
                    $ip_all[] = $ip_cur;
                    file_put_contents("ip.txt", implode($ip_all));
                }

//echo "visitors: " . count($ip_all);


                $data_target = 'signin';
                $add_target = 'signin';
                $add_event = 'signin';
                $register_business = 'signin';
                $dish = 'signin';
                $add_blog = 'signin';

                if (isset($_SESSION['user_id']) && isset($_SESSION['useremail']) && $_SESSION['useremail'] != '') {
                    $q = "select company_id from tbl_company where user_id='" . $_SESSION['user_id'] . "'";
                    $r = mysqli_query($con, $q);
                    $budata = mysqli_fetch_assoc($r);

                    if (isset($budata['company_id']) && $budata['company_id'] != '') {

                        $data_target = 'post_add';
                    } else {
                        $data_target = 'business_add';
                    }

                    $add_target = 'req_add';
                    $add_blog = 'write_blog';
                    $add_event = 'add_event';
                    $register_business = 'business_add';
                    $dish = 'dish';
                }


                $sql10 = "select * from setting where status=1 and type='event'";
                $res10 = mysqli_query($con, $sql10);

                $v10 = "";
                if (mysqli_num_rows($res10) > 0) {
                    $v10 = '<a href="javascript:void(0);">Coming Events</a>
                <ul class="sub-menu text-left">
                    <li><a href="coming_event.php">View Coming Events</a></li>
                    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#' . $add_event . '">Post an Event</a></li>         
                </ul>';
                }
                ?>
                <?php
                if (isset($_SESSION['useremail']) && $_SESSION['useremail'] != '') {
                    ?>
                    <li style="display:none;" class="m_v16"><a href=""><i class="flaticon-profile" style="font-size:18px;"></i>&nbsp;<?php echo ucwords($_SESSION['username']); ?></a>
                        <ul class="sub-menu text-left">
                            <li><a href="javascript:void(0);" onclick="profile_popup('<?php echo $_SESSION['user_type'] ?>');"> Profile</a></li>
                            <?php
                            if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'business_user') {
                                ?>
                                <li><a href="company.php"> Company</a></li>
                                <?php
                            }
                            ?>
                            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#change_password"> Change Password</a></li>                      
                            <li><a href="logout.php"> Log out</a></li>                      
                        </ul>
                    </li>
                    <?php
                } else {
                    ?>
                    <li style="display:none;" class="m_v16"><a href=""><i class="flaticon-profile" style="font-size:18px;"></i>&nbsp;Sign In</a>
                        <ul class="sub-menu text-left">
                            <li><a href="#" data-toggle="modal" data-target="#signin"> Sign In</a></li>
                            <li><a href="register.php"> Create Account</a></li>                      
                        </ul>
                    </li>
                    <?php
                }
                ?>
                    <li class="d-none m_v16"><a href="product.php">My Feed</a></li>
                <li><a href="product.php">Now on Sale</a></li>
                <li><a href="javascript:void(0);" id="top_talk_btn">Top Talk</a></li>

                <li><a href="business_listing.php?id=24">Popping Bizs</a></li>
                <!--<li><a href="javascript:void(0);" data-toggle="modal" data-target="#<?php echo $add_blog; ?>">Write a Post</a></li>-->
                <li><?php echo $v10 ?>
                <li><a href="classified.php">Classified</a></li>
                <li class="<?= (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'community_user') ? 'd-none' : ''; ?>"><a href="javascript:void(0);">Post an Ad</a>
                    <ul class="sub-menu text-left">
                        <li><a href="javascript:void(0);" onclick="choose_ad_popup('<?php echo $data_target ?>', 1);">In Classified</a></li>

                        <?php
                        if ($data_target == 'post_add') {
                            echo '<li><a href="contactus.php">As a Feed</a></li>';
                        } else {
                            echo '<li><a href="javascript:void(0);" data-toggle="modal" data-target="#' . $data_target . '">As a Feed</a></li>';
                        }
                        ?>

                        <li><a href="javascript:void(0);" onclick="choose_ad_popup('<?php echo $data_target ?>', 3);">For Business</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" >Business Listing</a>
                    <ul class="sub-menu text-left">
                        <li><a href="business_listing.php">Go to Business Page</a></li>
                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#<?php echo $register_business; ?>">List Your Business</a></li>                       
                    </ul>

                </li>
                <?php
                $nav_set = 145;
                if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'community_user') {
//                    $nav_set = 112;
                    ?>
                    <!--                    <li><a href="feed.php">Feed</a>
                                                                    <ul class="sub-menu text-left">
                                                                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#<?php echo $feed; ?>">Write a feed</a></li>
                                                                        <li><a href="feed.php">Go to feed</a></li>
                                                                    </ul>
                                        </li>-->
                    <?php
                }
                ?>
                <li><a href="">Home Services</a>
                    <ul class="sub-menu text-left" style="height:250px;overflow-y: scroll;">
                        <li><a href="">Air Conditioning & Heating</a></li>
                        <li><a href="">Contracters</a></li>
                        <li><a href="">Electricians</a></li>
                        <li><a href="">Home Cleaners</a></li>
                        <li><a href="">Landscrapers</a></li>
                        <li><a href="">Locksmiths</a></li>
                        <li><a href="">Movers</a></li>
                        <li><a href="">Painters</a></li>
                        <li><a href="">Plumbers</a></li>
                    </ul>
                </li>
<!--                <li class="<?= ($activePage == 'community.php') ? '' : 'd-none'; ?>"><a href="javascript:void(0);" style="margin-right: 5px;cursor: default;">Recent:</a></li>
                <li class="<?= ($activePage == 'community.php') ? '' : 'd-none'; ?>"><a href="community.php?d=24" style="margin-right: 5px;">24Hrs</a></li>
                <li class="<?= ($activePage == 'community.php') ? '' : 'd-none'; ?>"><a href="community.php?d=1" style="margin-right: 5px;">1Days</a></li>
                <li class="<?= ($activePage == 'community.php') ? '' : 'd-none'; ?>"><a href="community.php?d=7" style="margin-right: 5px;">7Days</a></li>
                <li class="<?= ($activePage == 'community.php') ? '' : 'd-none'; ?>"><a href="community.php?d=30">30Days</a></li>-->
                <li class="<?= ($activePage == 'community.php') ? '' : 'd-none'; ?>" style="display:block;text-align: left;transform: translateX(<?php echo $nav_set ?>px);">
                    <a href="javascript:void(0);" style="margin-right: 5px;cursor: default;">Recent:</a>
                    <a href="community.php?d=24" style="margin-right: 5px;">24Hrs</a>
                    <a href="community.php?d=1" style="margin-right: 5px;">1Days</a>
                    <a href="community.php?d=7" style="margin-right: 5px;">7Days</a>
                    <a href="community.php?d=30">30Days</a>
                </li>

            </ul>
        </div>
    </nav>
</header>
