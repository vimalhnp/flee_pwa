<!-- Footer section -->
<?php
if (isset($soon_res) && mysqli_num_rows($soon_res) > 0) {
    
} else {
    ?>
    <section class="footer-section">
        <div class="container">
            <div class="footer-logo text-center">
                <a href="index.php"><img src="./img/logo_color.png" alt=""></a>
            </div>

        </div>
        <div class="social-links-warp">
            <div class="container">
                <div class="social-links">
                    <a href="" class="instagram"><i class="fa fa-home"></i><span>Home</span></a>
                    <a href="" class="instagram"><i class="fa fa-industry"></i><span>Services</span></a>
                    <a href="" class="instagram"><i class="fa fa-graduation-cap"></i><span>Careers </span></a>
                    <a href="" class="instagram"><i class="fa fa-address-book"></i><span>Contact </span></a>
                    <a href="" class="instagram"><i class="fa fa-info-circle"></i><span>About</span></a>
                    <a href="" class="instagram"><i class="fa fa-question-circle"></i><span>Terms of Service </span></a>
                    <a href="" class="instagram"><i class="fa fa-lock"></i><span>Privacy</span></a>
                </div>

                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> 
                <p class="text-white text-center mt-5">
                    <!--&copy;<script>document.write(new Date().getFullYear());</script>  EGOOEE All Rights Reserved.-->
                    <?php
                    $copy_q = "select * from setting where setting_id=5 and status='1'";
                    $copy_r = mysqli_query($con, $copy_q);
                    if (mysqli_num_rows($copy_r)) {
                        $copy_data = mysqli_fetch_assoc($copy_r);
                        echo $copy_data['text'];
                    }
                    ?>
                </p>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

            </div>
        </div>
    </section>
    <!-- Footer section end -->
    <?php
}
?>

<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script>
    $("#re_open_event").click(function () {
        // $("#req_add").modal("hide");
        $("#post_add").modal("hide");
        $("#add_event").modal("show");
    });

    setTimeout(function () {
        $('.alert').fadeOut('slow');
    }, 2000); // <-- time in milliseconds
//user image priview
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#user_image_view').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#user_image").change(function () {
        readURL(this);
    });
//company image priview
    function readURL1(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#business_image_view').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#business_image").change(function () {
        readURL1(this);
    });

    $("#search_click").click(function () {
        $("#search_click_div").fadeOut();
        $("#search_bar").removeClass("d-none");
    });

    function user_reg() {
        if ($("#user_type").val() == 'community_user') {
            $("#dob_div").removeClass("d-none");
            $("#register_div").removeClass("d-none");
            $("#btype1").addClass("d-none");
            $("#gender_g").removeClass("d-none");
            $("#register_form_title").html("Personal Registration");
            $("#rb_title").addClass("d-none");
            $("#rstrtaddress").addClass("d-none");
            $("#gender_g1").addClass("d-none");
            $("#b_title").addClass("d-none");
            $("#b_dob_div").addClass("d-none");
            $("#blank").removeClass("d-none");
            $("#username_div").addClass("col-md-4");
            $("#pass_div").addClass("col-md-4");
            $("#cnfpass_div").addClass("col-md-4");
            $("#blank1").removeClass("d-none");
            $("#blank2").removeClass("d-none");
//            $("#blank3").removeClass("d-none");
            $("#not_block").addClass("d-none");
            $("#preview").addClass("d-none");

            $.ajax({
                url: 'connect.php',
                type: 'post',
                data: {'action': 'enticements', 'gender': $("#gender").val()},
                success: function (res) {
                    var reg_cat = $.parseJSON(res);
                    console.log(reg_cat['title']);

                    $("#enticements").html(reg_cat['op']);
                    $("ul.multiselect-container").html(reg_cat['list']);
                    $("button.multiselect").attr('title', reg_cat['title']);
                    $("span.multiselect-selected-text").text(reg_cat['total'] + ' selected');
                    $("#encmt_div").removeClass("d-none");
                }
            });

        } else if ($("#user_type").val() == 'business_user') {

            window.location.href = "pricing.php";

        } else {
            $("#register_div").addClass("d-none");
        }
    }
    if ($("#business_type_user").val() == 'business_user') {
        business_demo();
    }
    function business_demo() {

        $("#register_div").removeClass("d-none");
        $("#register_form_title").html("Business Registration");
        $("#rb_title").removeClass("d-none");
        $("#b_title").removeClass("d-none");
        $("#gender_g1").removeClass("d-none");
        $("#rstrtaddress").removeClass("d-none");
        $("#encmt_div").addClass("d-none");
        $("#dob_div").addClass("d-none");
        $("#gender_g").addClass("d-none");
        $("#b_dob_div").removeClass("d-none");
        $("#btype1").removeClass("d-none");
        $("#blank").addClass("d-none");
        $("#blank1").addClass("d-none");
        $("#blank2").addClass("d-none");
//            $("#blank3").addClass("d-none");
        $("#username_div").removeClass("col-md-6");
        $("#cnfpass_div").removeClass("col-md-6");
        $("#pass_div").removeClass("col-md-6");
        $("#not_block").removeClass("d-none");
        $("#state_div").removeClass("d-none");
        $("#preview").addClass("d-none");
    }
//    10slider
    $(document).ready(function () {
        $('.your_slider').slick({
            infinite: true,
            autoplay: true,
            arrows: false,
            slidesToShow: 10,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
    $(function () {

        $('#enticements').multiselect({

            includeSelectAllOption: true

        });
        $('#profile_enticements').multiselect({

            includeSelectAllOption: true

        });
        $('#enticements1').multiselect({

            includeSelectAllOption: true

        });

        $('#btnget').click(function () {

            alert($('#enticements').val());

        })

    });

    function feed_com(feed_id) {
        $("#feed_id").val(feed_id);
        $("#feed_comment").modal('show');
    }

    function follow(user_id, follower_id, feed_id) {

        if (follower_id != '') {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'follow', 'user_id': user_id, 'follower_id': follower_id},
                success: function (ans) {
                    // alert(ans);
                    if (ans) {

                        if (ans == 0)
                        {
                            $('#pin_msg_' + feed_id).html('Already Followed').addClass('alert alert-danger').removeClass('d-none');
                        } else if (ans = "own")
                        {
                            $('#pin_msg_' + feed_id).html('Your Comment').addClass('alert alert-danger').removeClass('d-none');
                        } else
                        {
                            $('#pin_msg_' + feed_id).html('Follow successfully').addClass('alert alert-success').removeClass('d-none');
                            $("#total_followers").text(ans);

                        }
                    }
                }
            });
        }

    }
    function pinned(post_id, user_id, type) {

        if (user_id != '') {
            $('#create_channel_error').html('').addClass('d-none');
            $("#new_channel_name").val('');
            $("#channel_modal").modal("show");
            $("#post_save_id").val(post_id);
            $("#hd").val(type);

            $("#create_channel").click(function () {
                var color = $("#color").val();
                //alert(color);
                $.ajax({
                    url: 'rating.php',
                    type: 'POST',
                    data: {'action': 'create_channel', 'color': color, 'user_id': user_id, 'channel_name': $("#new_channel_name").val()},
                    success: function (ans) {
                        var res = $.parseJSON(ans);
                        console.log(res);
                        if (res['action'] == 'success') {
                            $('#create_channel_error').html('Channel create successfully.').removeClass('alert-danger d-none').addClass("alert alert-success");
                            $('#channel_div_ajax').html(res['channels']);

                            $('#channel_list_div').html(res['channel_list']);
                            $('#ajax_pin').html(res['pin']);

                        } else if (res['action'] == 'failed') {
                            $('#create_channel_error').html('Failed').removeClass('alert-success d-none').addClass("alert alert-danger");
                        } else if (res['action'] == 'already') {
                            $('#create_channel_error').html('Channel already created').removeClass('alert-success d-none').addClass("alert alert-danger");
                        }
//                        setTimeout(function () {
//                            $('.alert').fadeOut('slow');
//                        }, 3000);
                    }
                });
            });

        }
    }
    function save_pinned_post(channel_id, user_id, color_code) {
        //alert(color_code);
        var post_id = $("#post_save_id").val();
        var hd = $("#hd").val();

        $.ajax({
            url: 'rating.php',
            type: 'POST',
            data: {'action': 'pin', 'user_id': user_id, 'post_id': post_id, 'channel_id': channel_id, 'hd': hd},
            success: function (ans) {

                var res = $.parseJSON(ans);
                console.log(res);
                if (res['action'] == 'success') {
                    $('#pin_msg_' + post_id).html('Paper-clipped successfully').removeClass('d-none alert-danger').addClass("alert alert-success");
                    $('.paper_clip_color_' + post_id).css("color", color_code);
//                            $("#total_pinned").text(res['total']);
                } else if (res['action'] == 'failed') {
                    $('#pin_msg_' + post_id).html('Paper-clipped failed').removeClass('d-none alert-success').addClass("alert alert-danger");
                } else if (res['action'] == 'already') {
                    $('.paper_clip_color_' + post_id).css("color", 'yellow');
                    $('#pin_msg_' + post_id).html('Paper-clip already pin').removeClass('d-none alert-success').addClass("alert alert-danger");
                }
                $("#channel_modal").modal("hide");
//                        setTimeout(function () {
//                            $('.alert').fadeOut('slow');
//                       }, 2000);
            }
        });
    }
    function country_zipcode() {

        $.ajax({
            url: 'connect.php',
            type: 'POST',
            data: {'action': 'zipcode', 'country_id': $("#country1").val()},
            success: function (ans) {
//                console.log(ans);
                var an = $.parseJSON(ans);
                console.log(an);
                if (an['enable_zipcode'] == "yes") {
//                    if ($("#user_type").val() == 'business_user') {
                    $('#zip_code').removeClass('d-none');
                    $("#zipcode").prop('required', true);
//                    }
                } else if (an['enable_zipcode'] == "no") {
                    $('#zip_code').addClass('d-none');
                    $("#zipcode").removeAttr("required");
                }
                if (an['enable_state'] == "yes") {
                    $('#state_div').removeClass('d-none');
                } else if (an['enable_state'] == "no") {
                    if ($("#user_type").val() == 'community_user') {
                        $('#state_div').addClass('d-none');
                    }
                }
            }
        });
    }
    function choose_ad_popup(name, type) {
        if (name == 'post_add') {
            $("#ad_type").val(type);
            if (type == 2) {
                $("#ad_enticements").removeClass("d-none");
                $("#post_name_div").removeClass("col-md-12").addClass("col-md-6");
            } else {
                $("#ad_enticements").addClass("d-none");
                $("#post_name_div").removeClass("col-md-6").addClass("col-md-12");
            }
            $("#post_add").modal('show');
        } else if (name == 'business_add') {
            $("#business_add").modal('show');
        } else {
            $("#signin").modal('show');
        }
    }
    function profile_popup(type) {
        if (type == 'community_user') {
            $("#profile_enticements_div").removeClass("d-none");
        } else if (type == 'business_user') {
            $("#profile_enticements_div").addClass("d-none");
        }
        $("#profile").modal("show");
    }

    function company_popup(company_id) {
        if (company_id) {
            $.ajax({
                url: 'ad_controller.php',
                type: 'POST',
                data: {'action': 'company_detail', 'company_id': company_id},
                success: function (ans) {

                    var res = $.parseJSON(ans);
                    //console.log(res)
                    $("#business_name_view").val(res['c_name']);
                    $("#company_id_view").val(res['company_id']);
                    $("#business_email_view").val(res['email']);
                    $("#address_view").text(res['c_address']);
                    $("#description_view").text(res['c_description']);
                    $("#business_image_view").attr('src', res['c_logo']);
                    $("#company_modal").modal("show");
                }
            });
        }
    }
    $("#check1").click(function () {
        if ($("#check1").prop("checked")) {
            $('#myModal').show();
        }
    });
    $("#cl1").click(function () {
        $('#myModal').hide();
        //alert("saa");
    });

    $("#new_form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            },
            repassword: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            email: {
                required: "Please Enter Your Email Id",
                email: "Email Id is Invalid"
            },
            password: {
                required: "Enter your Password"
            },
            repassword: {
                required: "Your Password Not Match"
            }
        },
        submitHandler: function (form) {

            var email = $(form).find('#email').val();
            var pass = $(form).find('#password').val();
            var business = $(form).find('#business_name').val();
            $("#submit").prop('disabled', true);
            $.ajax({
                url: 'connect.php',
                type: 'POST',
                data: {'action': 'new_register', 'email': email, 'password': pass,'business_name':business},
                success: function (ans) {
                    console.log(ans);
                    if (ans == 'success') {
                        $('#error').removeClass('d-none').show('slow');
                        $('#email').val('');
                        $('#password').val('');
                        $('#repassword').val('');
                        $('#business_name').val('');
                    }
                }
            });
            return false;
        }
    });
    window.onscroll = function () {
        myFunction()
    };

    var header = document.getElementById("myheader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
    if ($("#open_plan_pop").val() == 'yes_open') {
        $("#open_plan_popup").modal("show");
    }
</script>
</body>
</html>
