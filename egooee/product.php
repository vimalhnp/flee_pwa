<?php
include 'connection.php';
ob_start();
include_once './header.php';
include_once './new_header.php';
require_once 'navbar.php';
?>
<script>
    function like_dislike(uid, aid) {
        if (uid != 0) {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'post_like', 'user_id': uid, 'post_id': aid},
                success: function (ans) {
                    if (ans) {
                        $("#total_like_" + aid).text(ans);
                        $(".d_like_" + aid).css("color", "#e03d3d");
                        $(".d_like_" + aid).removeClass("fa-heart-o");
                        $(".d_like_" + aid).addClass("fa-heart");
//                        $(".dis_" + aid).css("color", "#e03d3d");
                    }
                }
            });
        }
    }
    function star_rating(uid, aid) {
//        alert("ok");
        var radioValue = $("input[type='radio'][name='rate_" + aid + "']:checked").val();
//         alert(radioValue);
        if (radioValue) {
            // alert("You are give a - " + radioValue +" star");
            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'post_rating', 'user_id': uid, 'post_id': aid, 'text': radioValue},
                success: function (ans) {
                    if (ans == "success") {
                        $('#ratting_msg_' + aid).html('Successfully Given ' + radioValue + ' stars').removeClass('d-none');
                    } else if (ans == "already") {
                        $('#ratting_msg_' + aid).html('Sorry, You can not give ratting again!').removeClass('d-none');
                    } else if (ans == "own") {
                        $('#ratting_msg_' + aid).html('Sorry, You can not give ratting to your post!').removeClass('d-none');
                    } else {
                        $('#ratting_msg_' + aid).html('OOPS, Something went wrong!').removeClass('d-none');

                    }
                }
            });
        }
    }

//    function get_last_data(day) {
//        $.ajax({
//            url: 'ad_controller.php',
//            type: 'POST',
//            data: {'action': 'get_all_post', day: day},
//            success: function (ans) {
//                console.log(ans);
//            }
//        });
//    }
//    function view_ads() {
//        $(".ad_view_4").removeClass("d-none");
//    }
</script>
<!-- Product filter section -->
<section class="top-letest-product-section product-filter-section">
    <div class="container-fluid">
        <?php
//        require_once 'searchbar.php';
        require_once '10_slider.php';
        ?>
        <div class="row">
            <div class="col-lg-9 col-sm-12">
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <center><h4 style="margin-bottom: 20px;padding:20px;">Now on Sale</h4></center>
                        <div class="row">
                            <?php
                            $sql = "select * from tbl_post where status=1 AND type=3";
                            $res = mysqli_query($con, $sql);
                            if (mysqli_num_rows($res)) {
                                while ($data = mysqli_fetch_assoc($res)) {

                                    $d1 = strtotime($data['added_on']);
                                    $d2 = strtotime(date('Y-m-d'));
                                    $d3 = $d2 - $d1;

                                    $day = $d3 / (60 * 60 * 24);

                                    if ($day > $data['expire']) {
                                        $update = "update tbl_post set status='0' where post_id='" . $data['post_id'] . "'";
                                        mysqli_query($con, $update);
                                    }
                                    ?>
                                    <div class="col-lg-4 col-sm-6 col-6" style="padding-right:0;margin-bottom: 10px;">
                                        <div class="product-item" style="border-left:1px solid #dfe1e5;">

                                            <div class="pi-pic">
                                                <a href="view_ad.php?id=<?php echo intval($data['post_id']) ?>">
                                                    <center> <img src="<?php echo $data['image'] ?>" style="height:250px;width:auto;" alt=""></center>
                                                </a>
                                                <div class="pi-links">
                                                    <!--<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>-->
                                                    <?php
                                                    $uid = '0,';
                                                    $style = '';
                                                    $style1 = '';
                                                    $class = 'fa-heart-o';
                                                    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                                        $uid = $_SESSION['user_id'] . ",";

//                                                check already like
                                                        $check_like_q = "select * from user_engagement where type=1 and post_id='" . $data['post_id'] . "' and user_id='" . $_SESSION['user_id'] . "'";
                                                        $check_like_r = mysqli_query($con, $check_like_q);
                                                        $check_like_r = mysqli_num_rows($check_like_r);

                                                        if ($check_like_r > 0) {
//                                                    $style = "style='background:red;'";
                                                            $style1 = "style='color:#e03d3d;'";
                                                            $class = "fa-heart";
                                                        }
                                                    }
                                                    ?>
                                                    <!--<a href="javascript:void(0);" <?php echo $style ?> class="wishlist-btn dis_<?php echo $data['post_id'] ?>" onclick="like_dislike(<?php echo $uid . $data['post_id'] ?>);"><i class="flaticon-heart d_like_<?php echo $data['post_id'] ?>" <?php echo $style1 ?>></i></a>-->
                                                </div>
                                            </div>

                                            <div class="pi-text" style="padding:0 15px;">
                                                <?php
                                                if (isset($data['post_id'])) {
                                                    $query = "select * from user_engagement where type=1 and post_id='" . $data['post_id'] . "'";
                                                    $result = mysqli_query($con, $query);
                                                    $count = mysqli_num_rows($result);
                                                }
                                                ?>
                                                <!--<h6><span id="total_like_<?php echo $data['post_id'] ?>"><?php echo $count; ?></span> <a href="javascript:void(0);" style="text-decoration:none;color:black;" onclick="like_dislike(<?php echo $uid . $data['post_id'] ?>);"><i class="fa <?php echo $class; ?> d_like_<?php echo $data['post_id'] ?>" <?php echo $style1 ?>></i></a></h6>-->
                                                <div class="rate">
                                                    <?php
                                                    if (isset($data['post_id'])) {
                                                        $query_avg = "select avg(text) as avg from user_engagement where type=2 and post_id='" . $data['post_id'] . "'";
                                                        $result_avg = mysqli_query($con, $query_avg);
                                                        $data_avg = mysqli_fetch_assoc($result_avg);
                                                        $avg = $data_avg['avg'];
                                                        $disable = '';
//                                                check user own post
                                                        if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                                            $user_post_q = "select * from tbl_post where post_id='" . $data['post_id'] . "' AND user_id='" . $_SESSION['user_id'] . "'";
                                                            $user_post_r = mysqli_query($con, $user_post_q);
                                                            $user_post_d = mysqli_num_rows($user_post_r);

                                                            if ($user_post_d > 0) {
                                                                $disable = "disabled";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <form class="user-rating-form">
                                                        <span class="user-rating">
                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['post_id'] ?>" value="5" <?php echo (isset($avg) && $avg != '' && $avg > 4) ? 'checked' : ''; ?> onclick="star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['post_id'] ?>)"><span class="star"></span>

                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['post_id'] ?>" value="4" <?php echo (isset($avg) && $avg != '' && $avg <= 4) ? 'checked' : ''; ?> onclick="star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['post_id'] ?>)"><span class="star"></span>

                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['post_id'] ?>" value="3" <?php echo (isset($avg) && $avg != '' && $avg <= 3) ? 'checked' : ''; ?> onclick="star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['post_id'] ?>)"><span class="star"></span>

                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['post_id'] ?>" value="2" <?php echo (isset($avg) && $avg != '' && $avg <= 2) ? 'checked' : ''; ?> onclick="star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['post_id'] ?>)"><span class="star"></span>

                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['post_id'] ?>" value="1" <?php echo (isset($avg) && $avg != '' && $avg <= 1) ? 'checked' : ''; ?> onclick="star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['post_id'] ?>)"><span class="star"></span>
                                                        </span>
                                                        <div class="pull-right" style="padding-top:5px;"><span id="total_like_<?php echo $data['post_id'] ?>"><?php echo $count; ?></span> <a href="javascript:void(0);" style="text-decoration:none;color:black;" onclick="like_dislike(<?php echo $uid . $data['post_id'] ?>);"><i class="fa <?php echo $class; ?> d_like_<?php echo $data['post_id'] ?>" <?php echo $style1 ?>></i></a></div>
                                                    </form>

                                                </div>
                                                <p id="ratting_msg_<?php echo $data['post_id'] ?>" style="padding: 6px;margin: 0px 0 0 0;" class="clearfix alert alert-info d-none text-left"></p>

                                                <div style="clear:both;"></div>
                                                <p><b><?php echo ucwords($data['b_name']) ?></b> </p>
                                                <p><?php echo $data['b_message'] ?></p>
                                                <p><?php echo $data['b_address'] ?></p>
                                                <p>Ph: <?php echo $data['b_phone'] ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                echo "<p style='margin:0 auto;'>No Data</p>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Product filter section end -->

<?php
include_once './footer.php';
?>
<!--<script type="text/javascript">
    $(document).ready(function () {
        $('.your_slider').slick({
            infinite: true,
            autoplay: true,
            arrows: false,
            slidesToShow: 10,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
</script>-->
