<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';

if (isset($_SESSION['user_id']) && isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'community_user') {
    
} else {
    header("Location:index.php");
}


if (isset($_POST['search_user'])) {

    $queery = "SELECT * FROM tbl_user WHERE (username LIKE '%" . $_POST['friend_search'] . "%' OR fname LIKE '%" . $_POST['friend_search'] . "%' OR lname LIKE '%" . $_POST['friend_search'] . "%') AND type='community_user' AND user_id != '" . $_POST['login_user_id'] . "'";

    $resa = mysqli_query($con, $queery);
    $htmla = '';
    if (mysqli_num_rows($resa)) {

        while ($rowa = mysqli_fetch_assoc($resa)) {
            $type = "Add Friend";
            $qa = "select * from friend_list where from_id='" . $_POST['login_user_id'] . "' AND to_id='" . $rowa['user_id'] . "'";
            $ra = mysqli_query($con, $qa);
            if (mysqli_num_rows($ra)) {
                $cd = mysqli_fetch_assoc($ra);
                if ($cd['friend_status'] == 1) {
                    $type = "Friend";
                } else {
                    $type = "Request Pending";
                }
            }
            $qa1 = "select * from friend_list where to_id='" . $_POST['login_user_id'] . "' AND from_id='" . $rowa['user_id'] . "'";
            $ra1 = mysqli_query($con, $qa1);
            if (mysqli_num_rows($ra1)) {
                $cd1 = mysqli_fetch_assoc($ra1);
                if ($cd1['friend_status'] == 1) {
                    $type = "Friend";
                } else {
                    $type = "Request Pending";
                }
            }
            $img = 'img/default-avatar.png';
            if ($rowa['image'] != '') {
                $img = $rowa['image'];
            }
            $htmla .= '<div class="col-md-6" style="margin-bottom:10px;"><div class="card"><div class="card-body"><div style="min-height:40px;" id="member_list_' . $rowa['user_id'] . '"><img src="' . $img . '" height="30" width="30" style="margin-right:10px;" align="left"/>' . ucwords($rowa['username']) . '<a href="javacsript:void(0)" class="btn btn-primary btn-sm" onclick="send_friend_request(' . $rowa['user_id'] . ')" style="float:right;">' . $type . '</i></a></div></div></div></div>';
//            $htmla .= "<li class='list-group-item d-flex justify-content-between align-items-center' id='member_list_" . $rowa['user_id'] . "'>" . $rowa['username'] . "<a href='javacsript:void(0)' class='badge badge-primary' onclick='send_friend_request(" . $rowa['user_id'] . ")'><i class='fa " . $type . "'></i></a></li>";
        }
    } else {
        $htmla .= '<div class="col-md-12"><div class="card"><div class="card-body"><center>No Record</center></div></div></div>';
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'f') {
    $q3 = "select u.* from friend_list as f inner join tbl_user as u on (f.to_id=u.user_id) where f.from_id='" . $_SESSION['user_id'] . "' and f.friend_status=1";

    $r3 = mysqli_query($con, $q3);
    $ht = '<h4 style="margin-bottom:20px;">My Friends</h4>';
    $htmla = '';

    if (mysqli_num_rows($r3)) {

        while ($rowa = mysqli_fetch_assoc($r3)) {
            $img = 'img/default-avatar.png';
            if ($rowa['image'] != '') {
                $img = $rowa['image'];
            }
            $htmla .= '<div id="user_details" class="col-md-12"></div>';
        }
    } else {
        $htmla .= '<div class="col-md-12"><div class="card"><div class="card-body"><center>No Friend</center></div></div></div>';
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'fr') {
    $q3 = "select u.* from friend_list as f inner join tbl_user as u on (f.from_id=u.user_id) where f.to_id='" . $_SESSION['user_id'] . "' and f.friend_status=0";

    $r3 = mysqli_query($con, $q3);
    $ht = '<h4 style="margin-bottom:20px;">Friend Request</h4>';
    $htmla = '';
    if (mysqli_num_rows($r3)) {

        while ($rowa = mysqli_fetch_assoc($r3)) {
            $img = 'img/default-avatar.png';
            if ($rowa['image'] != '') {
                $img = $rowa['image'];
            }
            $htmla .= '<div class="col-md-6"><div class="card"><div class="card-body"><div style="min-height:40px;display:inline-block;"><img src="' . $img . '" height="40" width="40" style="margin-right:20px;" align="left"/>' . ucwords($rowa['username']) . '</div><div class="pull-right"><button class="btn btn-success btn-sm" onclick="accept_request(' . $rowa['user_id'] . ')">Accept</button> <button class="btn btn-danger btn-sm" onclick="reject_request(' . $rowa['user_id'] . ')">Reject</button></div></div></div></div>';
        }
    } else {
        $htmla .= '<div class="col-md-12"><div class="card"><div class="card-body"><center>No Pending Request</center></div></div></div>';
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'rs') {
    $q3 = "select u.* from friend_list as f inner join tbl_user as u on (f.to_id=u.user_id) where f.from_id='" . $_SESSION['user_id'] . "' and f.friend_status=0";

    $r3 = mysqli_query($con, $q3);
    $ht = '<h4 style="margin-bottom:20px;">Request Sent</h4>';
    $htmla = '';

    if (mysqli_num_rows($r3)) {

        while ($rowa = mysqli_fetch_assoc($r3)) {
            $img = 'img/default-avatar.png';
            if ($rowa['image'] != '') {
                $img = $rowa['image'];
            }
            $htmla .= '<div class="col-md-6"><div class="card"><div class="card-body"><div style="min-height:40px;display:inline-block;"><img src="' . $img . '" height="40" width="40" style="margin-right:20px;" align="left"/>' . ucwords($rowa['username']) . '</div><div class="pull-right"><button class="btn btn-danger btn-sm" onclick="cancel_request(' . $rowa['user_id'] . ')">Cancel</button></div></div></div></div>';
        }
    } else {
        $htmla .= '<div class="col-md-12"><div class="card"><div class="card-body"><center>No Pending Request</center></div></div></div>';
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'mg') {
    $q3 = "select * from group_list where creater_id='" . $_SESSION['user_id'] . "' and status=1";
    $r3 = mysqli_query($con, $q3);
    $ht = '<h4 style="margin-bottom:20px;">My Groups</h4>';
    $htmla = '';

    if (mysqli_num_rows($r3)) {

        while ($rowa = mysqli_fetch_assoc($r3)) {

            $htmla .= '<div class="col-md-6"><div class="card"><div class="card-body"><div style="min-height:40px;display:inline-block;">' . ucwords($rowa['group_name']) . '</div></div></div></div>';
        }
    } else {
        $htmla .= '<div class="col-md-12"><div class="card"><div class="card-body"><center>No Group</center></div></div></div>';
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'fg') {
    $q3 = "select * from group_list where users_id in (" . $_SESSION['user_id'] . ") and creater_id !='" . $_SESSION['user_id'] . "' and status=1";
    $r3 = mysqli_query($con, $q3);
    $ht = '<h4 style="margin-bottom:20px;">Friends Group</h4>';
    $htmla = '';

    if (mysqli_num_rows($r3)) {

        while ($rowa = mysqli_fetch_assoc($r3)) {

            $htmla .= '<div class="col-md-6"><div class="card"><div class="card-body"><div style="min-height:40px;display:inline-block;">' . ucwords($rowa['group_name']) . '</div></div></div></div>';
        }
    } else {
        $htmla .= '<div class="col-md-12"><div class="card"><div class="card-body"><center>No Group</center></div></div></div>';
    }
}



if (isset($_GET['channel']) && $_GET['channel'] != '') {

    $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

$user_ent_q = "select * from tbl_user where user_id=" . $_SESSION['user_id'];
$user_ent_r = mysqli_query($con, $user_ent_q);
$user_ent_d = mysqli_fetch_assoc($user_ent_r);
$u_ent_arr = json_decode($user_ent_d['reg_category_ids'], TRUE);
$user_zipcode = $user_ent_d['zipcode'];
$user_state = $user_ent_d['state'];

//$sql = "select p.*,u.fname,u.lname from tbl_post as p inner join tbl_user as u on (u.user_id=p.user_id) where p.status=1 AND p.type=2";
$sql = "select p.*,p.zipcode as postzip,u.username,c.c_name from tbl_post as p inner join tbl_user as u on (u.user_id=p.user_id)
        inner join tbl_company as c on (c.company_id=p.b_type)
        where p.status=1 AND p.type=2 AND (p.zipcode LIKE '%" . $user_zipcode . "%' AND p.state LIKE'%" . $user_state . "%')";

if (isset($_GET['channel']) && $_GET['channel'] != '') {
    $sql = "select p.*,u.username,pn.user_id from tbl_post as p inner join tbl_user as u on (p.user_id=u.user_id)
            inner join tbl_company as c on (c.company_id=p.b_type)
           inner join pinned as pn on (pn.post_id=p.post_id) where p.type=2 AND pn.type=1 AND  pn.channel_id=" . $_GET['channel'];
}

$res = mysqli_query($con, $sql);
if ($res) {
    if (mysqli_num_rows($res)) {

        $cf = [];
        while ($data = mysqli_fetch_assoc($res)) {
//echo "<pre>";
//print_r($data);
//exit;
            $ent_decode = json_decode($data['reg_category_id'], true);
            if (is_array($ent_decode)) {
                $u_ent = implode(',', $ent_decode);
                if (array_intersect($ent_decode, $u_ent_arr)) {


                    $pi1 = 'grey';
                    // $check_feed_q = "select * from pinned where post_id='" . $data['post_id'] . "' AND user_id='" . $_SESSION['user_id'] . "' ";
                    $check_feed_q = "select pinned.*,channel.color_code from pinned INNER JOIN channel on channel.channel_id=pinned.channel_id where pinned.post_id='" . $data['post_id'] . "' AND pinned.user_id='" . $_SESSION['user_id'] . "'";
                    $check_feed_r = mysqli_query($con, $check_feed_q);
                    if (mysqli_num_rows($check_feed_r)) {
                        while ($r4 = mysqli_fetch_assoc($check_feed_r)) {
                            $pi1 = $r4['color_code'];
                        }
                    }
                    $cf[] = $data;
                }
            }
        }
    }
}

$data = [];
$j = 0;
if (isset($cf)) {
    $i = 0;
    foreach ($cf as $cfd) {

        if ($i % 4 == 0) {
            $j++;
        }
        $data[$j][] = $cfd;
        $i++;
    }

//    $i = 0;
}
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<style>
    .box{
        position: relative;
        display: inline-block; /* Make the width of box same as image */
    }
    .box .text{
        position: absolute;
        /*z-index: 999;*/
        margin: 0 auto;
        left: 0;
        right: 0;
        top: 40%; /* Adjust this value to move the positioned div up and down */
        text-align: center;
        width: 60%; /* Set the width of the positioned div */
    }
    .custome_bar a{
        font-size: 14px;
        margin: 8px 6px;
    }
    .custome_bar button{

        margin: 8px 6px;
    }
    .notification {
        text-decoration: none;
        position: relative;
        display: inline-block;
    }
    .notification .badge{
        position: absolute;
        top: -10px;
        right: -10px;
        padding: 5px 10px;
        border-radius: 50%;
        background-color: red;
        color: white;
    }
</style>
<style>

    .chat_message_area
    {
        position: relative;
        width: 100%;
        height: auto;
        background-color: #FFF;
        border: 1px solid #CCC;
        border-radius: 3px;
    }

    #group_chat_message
    {
        width: 100%;
        height: auto;
        min-height: 80px;
        overflow: auto;
        padding:6px 24px 6px 12px;
    }

    .image_upload
    {
        position: absolute;
        top:3px;
        right:3px;
    }
    .image_upload > form > input
    {
        display: none;
    }

    .image_upload img
    {
        width: 24px;
        cursor: pointer;
    }

</style>  
<!-- Product filter section -->

<section class="top-letest-product-section product-filter-section">
    <div class="container-fluid">
        <?php
//        require_once 'searchbar.php';
        if (isset($_SESSION['user_id'])) {
            $follwers_q = "select * from friends where user_id=" . $_SESSION['user_id'];
            $follwers_r = mysqli_query($con, $follwers_q);
            $follwers_count = mysqli_num_rows($follwers_r);

            $follwing_q = "select * from friends where follower_id=" . $_SESSION['user_id'];
            $follwing_r = mysqli_query($con, $follwing_q);
            $follwing_count = mysqli_num_rows($follwing_r);

            $pinned_q = "select * from pinned where user_id=" . $_SESSION['user_id'];
            $pinned_r = mysqli_query($con, $pinned_q);
            $pinned_count = mysqli_num_rows($pinned_r);
        }
        ?>


        <div class="row">
            <div class="col-lg-9 col-sm-12">
                <div class="row" style="">
                    <div class="col-lg-12 custome_bar">
                        <!--                        <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div>Following : <span id="total_followers"><?php echo (isset($follwing_count)) ? $follwing_count : ''; ?></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <center><h4 style="margin-bottom: 10px;padding:20px;padding-top: 10px;padding-bottom: 0;">Feeds</h4></center>
                                                        <div style="text-align: center;" id="ajax_pin">
                        <?php
                        $s = "select * from channel where user_id='" . $_SESSION['user_id'] . "'";
                        $res = mysqli_query($con, $s);

                        if (mysqli_num_rows($res) > 0) {
                            while ($r1 = mysqli_fetch_assoc($res)) {
                                $co = $r1['color_code'];
                                ?>
                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <a href='feed.php?channel=<?php echo $r1['channel_id'] ?>'  title="add to channel"><i class="fa fa-paperclip" style="font-size:30px;color: <?php echo $co ?>"></i></a>
                                                                                                                                                                                                                
                                <?php
                            }
                        }
                        ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div>Followers : <span id="total_followers"><?php echo (isset($follwers_count)) ? $follwers_count : ''; ?></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <a href="" class="btn btn-outline-dark notification">Following <span class="badge"><?php echo $follwing_count ?></span></a>
                        <a href="" class="btn btn-outline-dark notification">Followers <span class="badge"><?php echo $follwers_count ?></span></a>
                        <div class="dropdown" style="display:inline-block;">
                            <?php
                            $q4 = "select * from friend_list where to_id='" . $_SESSION['user_id'] . "' and friend_status=0";

                            $r4 = mysqli_query($con, $q4);
                            $count_friend = mysqli_num_rows($r4);
                            ?>

                            <button class="btn btn-outline-dark dropdown-toggle notification" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Friend Request">
                                Friends <span class="badge badge-primary"><?php echo $count_friend; ?></span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="feed.php?action=f">My Friends</a>
                                <a class="dropdown-item" href="feed.php?action=fr">Friend Request</a>
                                <a class="dropdown-item" href="feed.php?action=rs">Request Sent</a>
                                <a class="dropdown-item" href="feed.php?action=mg">My Group</a>
                                <a class="dropdown-item" href="feed.php?action=fg">Friend Group</a>
                                <a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#group_modal">Create Group</a>
                            </div>
                        </div>
                        <!--                        <div class="dropdown" style="display:inline-block;">
                                                    <button type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown">
                                                        Groups
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="feed.php?action=mg">My Group</a>
                                                        <a class="dropdown-item" href="feed.php?action=fg">Friend Group</a>
                                                        <a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#group_modal">Create Group</a>
                                                    </div>
                                                </div>-->

                        <a href="" class="btn btn-outline-dark notification">Message <span class="badge badge-primary">5</span></a>

                        <a href="" class="btn btn-outline-dark">Total Clips </a>
                        <a href="" class="btn btn-outline-dark">Clips Sent </a>
                        <!--<a href="" class="btn btn-outline-dark" title="Your paper stock"><i class="fa fa-clipboard"></i> <span class="badge badge-primary">5</span></a>-->
                        <a href="javascript:void(0);" class="btn btn-outline-dark" data-toggle="modal" data-target="#write_blog">Post to Community</a>
                        <a href="javascript:void(0);" class="btn btn-outline-dark" title="Clipped Feed"><i class="fa fa-paperclip"></i></a>
                        <a href="javascript:void(0);" class="btn btn-outline-dark" title="My Feed" onclick="show_channel();"><img src="img/icons/pb.png" class="img-responsive" style="width:23px"></a>

                    </div>

                    <!--                    <div class="col-md-12 row" style="margin-top:10px;">
                    
                                            <div class="col-md-4">
                                                <center><form method="post" action="feed.php">
                                                    <div class="input-group mb-3">
                                                        <input type="search" class="form-control" name="friend_search" id="friend_search" placeholder="Search Users">
                                                        <input type="hidden" class="form-control" name="login_user_id" value="<?php echo $_SESSION['user_id'] ?>">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-default" style="border:1px solid #ced4da;" type="submit" name="search_user"><i class="fa fa-search"></i></button> 
                                                        </div>
                                                    </div>
                                                </form>
                                                </center>
                                            </div>
                                        </div>-->

                    <div class="col-md-12">
                        <div id="friend_result">
                            <div class="row">
                                <div class="col-md-12"><center><?php echo (isset($ht)) ? $ht : ''; ?></center></div>
                                <?php
                                if (isset($htmla)) {
                                    echo $htmla;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="friend_request_send_msg"></div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6 form-group">

                        <?php
                        if (isset($url) && $url != '') {
                            $user_link_q = "select * from channel where channel_id='" . $_GET['channel'] . "' AND user_id='" . $_SESSION['user_id'] . "'";
                            $user_link_r = mysqli_query($con, $user_link_q);
                            if (mysqli_num_rows($user_link_r) > 0) {
                                echo '<input type="text" class="form-control" value="' . $url . '">';
                            }
                        }
                        ?>
                    </div>
                </div>

                <?php
                if (isset($_GET['channel']) && $_GET['channel'] != '') {

                    include './pinned_view.php';
                    echo '<div class="row">';
                    include './feed_ads.php';
                    echo '</div>';
                } else {
                    if (!isset($htmla)) {
                        ?>

                        <div class="row">
                            <?php
                            $u = 0;
                            $p = 1;
                            $p1 = 0;
                            $review_query = "select e.*,u.image,u.user_id from user_engagement as e inner join tbl_user as u on (e.user_id=u.user_id) where e.type=3";

                            $review_result = mysqli_query($con, $review_query);
                            if (mysqli_num_rows($review_result)) {

                                while ($reviews = mysqli_fetch_assoc($review_result)) {
                                    $u++;

                                    $pin_color = 'grey';
                                    $check_feed_q = "select pinned.*,channel.color_code from pinned INNER JOIN channel on channel.channel_id=pinned.channel_id where pinned.post_id='" . $reviews['user_engagement_id'] . "' AND pinned.user_id='" . $_SESSION['user_id'] . "' ";

                                    $check_feed_r = mysqli_query($con, $check_feed_q);

                                    if (mysqli_num_rows($check_feed_r)) {

                                        while ($ro = mysqli_fetch_assoc($check_feed_r)) {
                                            $pin_color = $ro['color_code'];
                                        }
                                    }
                                    ?>

                                    <div class="col-md-6 col-sm-12" style="margin-bottom: 10px;">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">

                                                    <div class="col-md-3 col-3 col-lg-3">
                                                        <a href="view_ad.php?id=<?php echo intval($reviews['post_id']) ?>">
                                                            <img src="<?php echo (isset($reviews['image']) && $reviews['image'] != '') ? $reviews['image'] : 'img/default-avatar.png'; ?>" class="img-responsive" style="width:50px;"></a>
                                                        <div><a href="javascript:void(0)" class="pull-right paper_clip" title="Add to Channel" onclick="pinned(<?php echo $reviews['user_engagement_id'] ?>,<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>, 2)"><i class="fa fa-paperclip paper_clip_color_<?php echo ($reviews['user_engagement_id']) ?>" style="color:<?php echo $pin_color ?>"></i></a></div>
                                                    </div>
                                                    <div class="col-md-9 col-lg-9 col-9">
                                                        <div><?php echo ucfirst($reviews['username']); ?>

                                                            <div style="font-size: 13px;height: 75px;overflow-y: auto;text-align: justify;overflow-x: hidden;">
                                                                <a href="view_ad.php?id=<?php echo intval($reviews['post_id']) ?>" style="color: black">
                                                                    <?php echo ucfirst($reviews['text']); ?></a>
                                                                <br>
                                                                <a href="javascript:void(0);" onclick="follow(<?php echo $reviews['user_id'] ?>,<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>,<?php echo $reviews['user_engagement_id'] ?>)">Follow</a>
                                                                <!--<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>-->
                                                            </div>
                                                            <p id="pin_msg_<?php echo $reviews['user_engagement_id'] ?>" style="padding: 6px 20px;margin: 0px 0 0 0;" class="clearfix d-none text-left"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    //echo $u."assass";
                                    if ($u % 6 == 0) {
                                        echo '</div>';
                                        echo '<div class="row">';
                                        if (isset($data[$p]) && is_array($data[$p])) {
                                            foreach ($data[$p] as $datas) {

                                                $ent_decode1 = json_decode($datas['reg_category_id'], true);
                                                if (is_array($ent_decode1)) {
                                                    $u_ent1 = implode(',', $ent_decode1);
                                                }
                                                ?>

                                                <div class="col-lg-3 col-md-3 col-sm-12 col-12" style="padding-right:0;margin-bottom: 10px;">

                                                    <div class="product-item" style="border-left:1px solid #dfe1e5;">

                                                        <div class="row">
                                                            <div class="col-lg-12 m_v18">

                                                                <a href="view_ad.php?id=<?php echo $datas['post_id'] ?>">
                                                                    <img src="<?php echo $datas['image'] ?>" alt="" class="img-responsive">
                                                                </a>
                                                                <div><a href="javascript:void(0)" class="pull-right paper_clip" onclick="pinned(<?php echo $datas['post_id'] ?>,<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>, 1)"><i class="fa fa-paperclip paper_clip_color_<?php echo ($datas['post_id']) ?>" style="color:<?php echo $pi1 ?>"></i></a></div>


                                                                <!--<div class="col-lg-9">-->
                                                                <div class="pi-text" style="padding:0 1px;word-break: break-word;height: auto;margin-top: 10px;">

                                                                    <p>Name : <b><?php echo ucwords($datas['c_name']) ?></b> </p>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<p>Description : <?php echo $u_ent1 ?></p>-->
                                                                    <a href="javascript:void(0);" onclick="follow(<?php echo $datas['user_id'] ?>,<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>,<?php echo $datas['post_id'] ?>)">Follow</a>
                                                                    <a href="javascript:void(0);" onclick="feed_com(<?php echo $datas['post_id'] ?>);" class="pull-right">Comment</a>
                                                                </div>
                                                                <!--</div>-->
                                                            </div>
                                                        </div>
                                                        <p id="pin_msg_<?php echo $datas['post_id'] ?>" style="padding: 6px 20px;margin: 0px 0 0 0;" class="clearfix d-none text-left"></p>
                                                    </div>
                                                </div>

                                                <?PHP
                                                $p1++;
                                            }
                                        }

//                                    include './feed_ads.php';
                                        echo '</div>';
                                        echo '<div class="row">';
                                    }

                                    if ($p1 == 4) {
                                        $p++;
                                    }
                                    $p1 = 0;
                                }
                            }

                            //echo $u."asdsdsd";
                            if ($u < 6) {
                                //  echo "inside feed";
                                echo '</div>';
                                echo '<div class="row">';

                                $sa = "select * from third_party_ads";
                                $rs = mysqli_query($con, $sa);

                                if (mysqli_num_rows($rs) > 0) {

                                    while ($re = mysqli_fetch_assoc($rs)) {

                                        echo $re['text'];
                                    }
                                } else {
                                    // include './feed_ads.php';

                                    echo '</div>';
                                    echo '<div class="row">';
                                    if (isset($data[$p]) && is_array($data[$p])) {
                                        foreach ($data[$p] as $datas) {

                                            $ent_decode1 = json_decode($datas['reg_category_id'], true);
                                            if (is_array($ent_decode1)) {
                                                $u_ent1 = implode(',', $ent_decode1);
                                            }
                                            ?>

                                            <div class="col-lg-3 col-md-3 col-sm-12 col-12" style="padding-right:0;margin-bottom: 10px;">

                                                <div class="product-item" style="border-left:1px solid #dfe1e5;">

                                                    <div class="row">
                                                        <div class="col-lg-12">




                                                            <div class="pi-pic" style="padding:0 0px;">
                                                                <a href="view_ad.php?id=<?php echo $datas['post_id'] ?>">
                                                                    <img src="<?php echo $datas['image'] ?>" alt="" class="img-responsive">
                                                                </a>
                                                                <div><a href="javascript:void(0)" class="pull-right paper_clip" onclick="pinned(<?php echo $datas['post_id'] ?>,<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>, 1)"><i class="fa fa-paperclip paper_clip_color_<?php echo ($datas['post_id']) ?>" style="color:<?php echo $pi1 ?>"></i></a></div>
                                                            </div>

                                                            <!--<div class="col-lg-9">-->
                                                            <div class="pi-text" style="padding:0 20px;word-break: break-word;height: auto;margin-top: 10px;">

                                                                <p>Name : <b><?php echo ucwords($datas['username']) ?></b> </p>

                                                                <p>Description : <?php echo $u_ent1 ?></p>
                                                                <a href="javascript:void(0);" onclick="follow(<?php echo $datas['user_id'] ?>,<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>,<?php echo $datas['post_id'] ?>)">Follow</a>
                                                                <a href="javascript:void(0);" onclick="feed_com(<?php echo $datas['post_id'] ?>);" class="pull-right">Comment</a>
                                                            </div>
                                                            <!--</div>-->
                                                        </div>
                                                    </div>
                                                    <p id="pin_msg_<?php echo $datas['post_id'] ?>" style="padding: 6px 20px;margin: 0px 0 0 0;" class="clearfix d-none text-left"></p>
                                                </div>
                                            </div>

                                            <?PHP
                                            $p1++;
                                        }
                                    }

//                                    include './feed_ads.php';
                                    echo '</div>';
                                    echo '<div class="row">';
                                }


                                echo '</div>';
                                echo '<div class="row">';
                            }
                            ?>
                        </div>
                        <?php
                    }
                }
                ?>


                <!--                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="accordion" class="accordion-area">
                                            <div class="panel">
                                                <div class="panel-header" id="headingOne">
                                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Reviews</button>
                                                </div>
                                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                    <div class="panel-body">
                                                        <div class="row">
                <?php
                $review_query = "select e.*,u.image,u.user_id from feed_engagement as e inner join tbl_user as u on (e.user_id=u.user_id) where e.type=3";

                $review_result = mysqli_query($con, $review_query);
                if (mysqli_num_rows($review_result)) {

                    while ($reviews = mysqli_fetch_assoc($review_result)) {
                        ?>
                                                                
                                                                                                                    <div class="col-md-6 col-sm-12" style="margin-bottom: 10px;">
                                                                                                                        <div class="card">
                                                                                                                            <div class="card-body">
                                                                                                                                <div class="row">
                                                                
                                                                                                                                    <div class="col-md-3 col-3 col-lg-3">
                                                                                                                                        <img src="<?php echo (isset($reviews['image']) && $reviews['image'] != '') ? $reviews['image'] : 'img/default-avatar.png'; ?>" class="img-responsive" style="width:50px;">
                                                                
                                                                                                                                    </div>
                                                                                                                                    <div class="col-md-9 col-lg-9 col-9">
                                                                                                                                        <div><?php echo ucfirst($reviews['username']); ?>
                                                                                                                                            <div style="font-size: 13px;height: 75px;overflow-y: auto;text-align: justify;overflow-x: hidden;"><?php echo ucfirst($reviews['text']); ?></div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                        <?php
                    }
                }
                ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
            </div>  
            <div class="col-lg-3 col-sm-12">

                <div class="row d-none" style="margin-bottom:5px;" id="channel_div">

                    <div class="col-lg-12" id="channel_list_div">

                        <?php
                        $channels_qu = "select * from channel where status=1 and user_id=" . $_SESSION['user_id'];
                        $channels_re = mysqli_query($con, $channels_qu);

                        if (mysqli_num_rows($channels_re) > 0) {
                            ?>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <?php
                                        while ($channels_da = mysqli_fetch_assoc($channels_re)) {
                                            ?>
                                            <div class="col-md-4 box">
                                                <div style='height:100px;width:84px;'>
                                                    <img src="img/icons/pb.png" style="background-repeat: no-repeat" >

                                                    <div class="text">
                                                        <a class='' href='feed.php?channel=<?php echo $channels_da['channel_id'] ?>' class='btn btn-secondary' style='margin-bottom:5px;border-radius:20px;'><?php echo $channels_da['name'] ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>




        <div class="modal" id="feed_comment">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h5 class="modal-title" style="margin: 0 auto;">Write a comment</h5>

                    </div>
                    <form action="rating.php" method="post">
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email :</label>
                                        <input type="text" class="form-control" readonly="" placeholder="Email" value="<?php echo isset($_SESSION['useremail']) ? $_SESSION['useremail'] : ''; ?>">
                                        <input type="hidden" class="form-control" name="user_id" id="cuser_id" value="<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>">
                                        <input type="hidden" class="form-control" id="feed_id" name="feed_id" value="<?php echo isset($data['feed_id']) ? $data['feed_id'] : ''; ?>">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>User Name :</label>
                                        <input type="text" class="form-control" placeholder="Name" name="username" value="<?php echo isset($_SESSION['username']) ? $_SESSION['username'] : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Comment :</label>
                                        <textarea class="form-control" rows="2" placeholder="Comment" name="text"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="submit" class="btn custom_color" name="feed_review" value="Submit">
                            <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="group_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h5 class="modal-title" style="margin: 0 auto;">Create Group</h5>
                </div>
                <form action="ad_controller.php" method="post" role="form" enctype="multipart/form-data">
                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <br><input type="text" name="new_group_name" id="new_group_name" class="form-control" required="" placeholder="Group Name">
                                    <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">                                                   
                                    <br><button type="submit" name="create_group" id="create_group" class="form-control btn btn-primary">Create</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="container">
<!--        <div class="row">
            <div class="col-md-8 col-sm-6">
                <h4>Online User</h4>
            </div>
            <div class="col-md-2 col-sm-3">
                <input type="hidden" id="is_active_group_chat_window" value="no" />
                <button type="button" name="group_chat" id="group_chat" class="btn btn-warning btn-xs">Group Chat</button>
            </div>
            <div class="col-md-2 col-sm-3">
                <p align="right">Hi - <?php echo $_SESSION['username']; ?> - <a href="logout.php">Logout</a></p>
            </div>
        </div>-->
        <div class="table-responsive">

            <!--<div id="user_details"></div>-->
            <div id="user_model_details"></div>
        </div>
        <br />
        <br />

    </div>
    <div id="group_chat_dialog" title="Group Chat Window">
        <div id="group_chat_history" style="height:400px; border:1px solid #ccc; overflow-y: scroll; margin-bottom:24px; padding:16px;">

        </div>
        <div class="form-group">
                <!--<textarea name="group_chat_message" id="group_chat_message" class="form-control"></textarea>!-->
            <div class="chat_message_area">
                <div id="group_chat_message" contenteditable class="form-control">

                </div>
                <div class="image_upload">
                    <form id="uploadImage" method="post" action="upload.php">
                        <label for="uploadFile"><img src="upload.png" /></label>
                        <input type="file" name="uploadFile" id="uploadFile" accept=".jpg, .png" />
                    </form>
                </div>
            </div>
        </div>
        <div class="form-group" align="right">
            <button type="button" name="send_group_chat" id="send_group_chat" class="btn btn-info">Send</button>
        </div>
    </div>
</section>
<!-- Product filter section end -->
<?php
include_once './footer.php';
?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script>
//    $("#friend_search").on('keyup', function () {
//        $.ajax({
//            url: 'ad_controller.php',
//            type: 'post',
//            data: {'action': 'friend_search', 'data': $("#friend_search").val(), 'user_id': $("#cuser_id").val()},
//            success: function (res) {
//                if (res != 'blank') {
//                    var result = $.parseJSON(res);
//                    $("#friend_result").html(result);
//                } else {
//                    $("#friend_result").html('');
//                }
//            }
//        });
//    });
    function send_friend_request(request_id) {
        var user_id = $("#cuser_id").val();
        $.ajax({
            url: 'ad_controller.php',
            type: 'post',
            data: {'action': 'send_friend_request', 'from_id': user_id, 'to_id': request_id},
            success: function (res) {
                // alert(res);
                if (res == "success") {
                    $("#member_list_" + request_id + " a").text("Request Sent");
//                    $("#friend_request_send_msg").addClass("alert alert-success").html("Request sent successfully.");
                } else if (res == "failed") {
                    $("#friend_request_send_msg").addClass("alert alert-danger").html("Oops, Something wrong.");
                }
                setTimeout(function () {
                    $('.alert').fadeOut('slow');
                }, 3000);
            }
        });
    }
    function accept_request(from_id) {
        var to_id = $("#cuser_id").val();
        $.ajax({
            url: 'ad_controller.php',
            type: 'post',
            data: {'action': 'accept_request', 'from_id': from_id, 'to_id': to_id},
            success: function (res) {
                if (res == "success") {
                    $("#request_item_" + from_id).hide();
                    window.location.href = "feed.php";
                }
            }
        });
    }
    function reject_request(from_id) {
        var to_id = $("#cuser_id").val();
        $.ajax({
            url: 'ad_controller.php',
            type: 'post',
            data: {'action': 'reject_request', 'from_id': from_id, 'to_id': to_id},
            success: function (res) {
                if (res == "success") {
                    $("#request_item_" + from_id).hide();
                    window.location.href = "feed.php";
                }
            }
        });
    }
    function cancel_request(to_id) {
        var from_id = $("#cuser_id").val();
        $.ajax({
            url: 'ad_controller.php',
            type: 'post',
            data: {'action': 'cancel_request', 'from_id': from_id, 'to_id': to_id},
            success: function (res) {
                if (res == "success") {
                    window.location.href = "feed.php?action=fr";
                }
            }
        });
    }
    function remove_friend(to_id) {
        var from_id = $("#cuser_id").val();
        $.ajax({
            url: 'ad_controller.php',
            type: 'post',
            data: {'action': 'remove_friend', 'from_id': from_id, 'to_id': to_id},
            success: function (res) {
                if (res == "success") {
                    window.location.href = "feed.php?action=f";
                }
            }
        });
    }
    function show_channel() {
        $("#channel_div").removeClass("d-none");
    }


    $(document).ready(function () {

        fetch_user();

        setInterval(function () {
            update_last_activity();
            fetch_user();
            update_chat_history_data();
            fetch_group_chat_history();
        }, 5000);

        function fetch_user()
        {
            $.ajax({
                url: "fetch_user.php",
                method: "POST",
                success: function (data) {
                    $('#user_details').html(data);
                }
            })
        }

        function update_last_activity()
        {
            $.ajax({
                url: "update_last_activity.php",
                success: function ()
                {

                }
            })
        }

        function make_chat_dialog_box(to_user_id, to_user_name)
        {
            var modal_content = '<div id="user_dialog_' + to_user_id + '" class="user_dialog" title="You have chat with ' + to_user_name + '">';
            modal_content += '<div style="height:270px; border:1px solid #ccc; overflow-y: scroll; margin-bottom:24px; padding:6px;" class="chat_history" data-touserid="' + to_user_id + '" id="chat_history_' + to_user_id + '">';
            modal_content += fetch_user_chat_history(to_user_id);
            modal_content += '</div>';
            modal_content += '<div class="form-group">';
            modal_content += '<textarea name="chat_message_' + to_user_id + '" id="chat_message_' + to_user_id + '" class="form-control chat_message"></textarea>';
            modal_content += '</div><div class="form-group" align="right">';
            modal_content += '<button type="button" name="send_chat" id="' + to_user_id + '" class="btn btn-info send_chat">Send</button></div></div>';
            $('#user_model_details').html(modal_content);
        }

        $(document).on('click', '.start_chat', function () {
            var to_user_id = $(this).data('touserid');
            var to_user_name = $(this).data('tousername');
            make_chat_dialog_box(to_user_id, to_user_name);
            $("#user_dialog_" + to_user_id).dialog({
                autoOpen: false,
                width: 400
            });
            $('#user_dialog_' + to_user_id).dialog('open');
            $('#chat_message_' + to_user_id).emojioneArea({
                pickerPosition: "top",
                toneStyle: "bullet"
            });
        });

        $(document).on('click', '.send_chat', function () {
            var to_user_id = $(this).attr('id');
            var chat_message = $.trim($('#chat_message_' + to_user_id).val());
            if (chat_message != '')
            {
                $.ajax({
                    url: "insert_chat.php",
                    method: "POST",
                    data: {to_user_id: to_user_id, chat_message: chat_message},
                    success: function (data)
                    {
                        //$('#chat_message_'+to_user_id).val('');
                        var element = $('#chat_message_' + to_user_id).emojioneArea();
                        element[0].emojioneArea.setText('');
                        $('#chat_history_' + to_user_id).html(data);
                    }
                })
            } else
            {
                alert('Type something');
            }
        });

        function fetch_user_chat_history(to_user_id)
        {
            $.ajax({
                url: "fetch_user_chat_history.php",
                method: "POST",
                data: {to_user_id: to_user_id},
                success: function (data) {
                    $('#chat_history_' + to_user_id).html(data);
                }
            })
        }

        function update_chat_history_data()
        {
            $('.chat_history').each(function () {
                var to_user_id = $(this).data('touserid');
                fetch_user_chat_history(to_user_id);
            });
        }

        $(document).on('click', '.ui-button-icon', function () {
            $('.user_dialog').dialog('destroy').remove();
            $('#is_active_group_chat_window').val('no');
        });

        $(document).on('focus', '.chat_message', function () {
            var is_type = 'yes';
            $.ajax({
                url: "update_is_type_status.php",
                method: "POST",
                data: {is_type: is_type},
                success: function ()
                {

                }
            })
        });

        $(document).on('blur', '.chat_message', function () {
            var is_type = 'no';
            $.ajax({
                url: "update_is_type_status.php",
                method: "POST",
                data: {is_type: is_type},
                success: function ()
                {

                }
            })
        });

        $('#group_chat_dialog').dialog({
            autoOpen: false,
            width: 400
        });

        $('#group_chat').click(function () {
            $('#group_chat_dialog').dialog('open');
            $('#is_active_group_chat_window').val('yes');
            fetch_group_chat_history();
        });

        $('#send_group_chat').click(function () {
            var chat_message = $.trim($('#group_chat_message').html());
            var action = 'insert_data';
            if (chat_message != '')
            {
                $.ajax({
                    url: "group_chat.php",
                    method: "POST",
                    data: {chat_message: chat_message, action: action},
                    success: function (data) {
                        $('#group_chat_message').html('');
                        $('#group_chat_history').html(data);
                    }
                })
            } else
            {
                alert('Type something');
            }
        });

        function fetch_group_chat_history()
        {
            var group_chat_dialog_active = $('#is_active_group_chat_window').val();
            var action = "fetch_data";
            if (group_chat_dialog_active == 'yes')
            {
                $.ajax({
                    url: "group_chat.php",
                    method: "POST",
                    data: {action: action},
                    success: function (data)
                    {
                        $('#group_chat_history').html(data);
                    }
                })
            }
        }

        $('#uploadFile').on('change', function () {
            $('#uploadImage').ajaxSubmit({
                target: "#group_chat_message",
                resetForm: true
            });
        });

        $(document).on('click', '.remove_chat', function () {
            var chat_message_id = $(this).attr('id');
            if (confirm("Are you sure you want to remove this chat?"))
            {
                $.ajax({
                    url: "remove_chat.php",
                    method: "POST",
                    data: {chat_message_id: chat_message_id},
                    success: function (data)
                    {
                        update_chat_history_data();
                    }
                })
            }
        });

    });
</script>
