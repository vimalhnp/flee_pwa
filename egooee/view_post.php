<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>
<section class="product-section">
    <div class="container-fluid">
        <?php
//        require_once 'searchbar.php';
        ?>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-9">
                                <a href="" class="btn" style="border-radius:8px;margin: 8px 0">Community Posting</a>
                            </div>

                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <?php
                                if (isset($_GET['id']) && $_GET['id'] != '') {

                                    $review_query = "select b.*,u.image from tbl_blog as b inner join tbl_user as u on (b.user_id=u.user_id) where b.blog_id=" . $_GET['id'];
                                }
                                $review_result = mysqli_query($con, $review_query);
                                if (mysqli_num_rows($review_result)) {
                                    while ($reviews = mysqli_fetch_assoc($review_result)) {

                                        if (isset($reviews['blog_id'])) {
                                            $query = "select * from blog_engagement where blog_id='" . $reviews['blog_id'] . "' AND type=1";
                                            $result = mysqli_query($con, $query);

                                            $count = mysqli_num_rows($result);
                                        }


                                        $uid = '0,';

                                        $style1 = '';
                                        $class = 'fa-heart-o';
                                        if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                            $uid = $_SESSION['user_id'] . ",";

//                                                check already like
                                            $check_like_q = "select * from blog_engagement where blog_id='" . $reviews['blog_id'] . "' and user_id='" . $_SESSION['user_id'] . "' AND type=1";
                                            $check_like_r = mysqli_query($con, $check_like_q);
                                            $check_like_r = mysqli_num_rows($check_like_r);

                                            if ($check_like_r > 0) {
//                                                    $style = "style='background:red;'";
                                                $style1 = "style='color:#e03d3d;'";
                                                $class = "fa-heart";
                                            }
                                        }
                                        ?>

                                        <div class="col-md-12 col-sm-12" style="margin-bottom: 10px;">
                                            <div class="card">

                                                <div class="card-body">

                                                    <div class="row">
                                                        <div class="col-md-2 col-3 col-lg-2">
                                                            <img src="<?php echo (isset($reviews['image']) && $reviews['image'] != '') ? $reviews['image'] : 'img/default-avatar.png'; ?>" class="img-responsive" style="width:100px;">

                                                        </div>
                                                        <div class="col-md-10 col-lg-10 col-9">
                                                            <div><b><?php echo ucfirst($reviews['username']); ?></b>
                                                                <div class="pull-right" style="padding-top:5px;"><span id="total_like_<?php echo $reviews['blog_id'] ?>"><?php echo $count; ?></span> <a href="javascript:void(0);" style="text-decoration:none;color:black;" onclick="blog_like(<?php echo $uid . $reviews['blog_id'] ?>);"><i class="fa <?php echo $class; ?> d_like_<?php echo $reviews['blog_id'] ?>" <?php echo $style1 ?>></i></a></div>
                                                            </div>
                                                            <div style="font-size: 13px;">Date : <?php echo ucfirst($reviews['added_on']); ?></div>

                                                        </div>
                                                    </div>


                                                    <div style="font-size: 14px;margin-top: 10px;"><?php
                                                        echo ucfirst($reviews['description']);
                                                        ?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="accordion" class="accordion-area">
                            <div class="panel">
                                <div class="panel-header" id="headingOne">
                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Reviews</button>
                                </div>
                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body">
                                        <div class="row">


                                            <?php
                                            $review_query1 = "select blog_engagement.*,tbl_user.user_id,tbl_user.image,tbl_user.username from blog_engagement INNER JOIN tbl_user on tbl_user.user_id=blog_engagement.user_id where blog_engagement.blog_id='" . $_GET['id'] . "' and blog_engagement.type=3";

                                            $review_result1 = mysqli_query($con, $review_query1);
                                            if (mysqli_num_rows($review_result1) > 0) {
                                                while ($reviews1 = mysqli_fetch_assoc($review_result1)) {
                                                    ?>

                                                    <div class="col-md-6 col-sm-12" style="margin-bottom: 10px;">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">

                                                                    <div class="col-md-3 col-3 col-lg-3">
                                                                        <img src="<?php echo $reviews1['image']; ?>" class="img-responsive" style="width:50px;">
                                                                    </div>
                                                                    <div class="col-md-9 col-lg-9 col-9">
                                                                        <div><?php echo ucwords($reviews1['username']); ?></div>
                                                                        <div style="font-size: 13px;height: 75px;overflow-y: auto;text-align: justify;overflow-x: hidden;"><?php echo ucfirst($reviews1['text']); ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>

        <div class="modal" id="blog_comment">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h5 class="modal-title" style="margin: 0 auto;">Write a comment</h5>

                    </div>
                    <form action="rating.php" method="post">
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email :</label>
                                        <input type="text" class="form-control" readonly="" placeholder="Email" value="<?php echo isset($_SESSION['useremail']) ? $_SESSION['useremail'] : ''; ?>">
                                        <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>">
                                        <input type="hidden" class="form-control" id="blog_id" name="blog_id" value="<?php echo isset($data['blog_id']) ? $data['blog_id'] : ''; ?>">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>User Name :</label>
                                        <input type="text" class="form-control" placeholder="Name" name="username" value="<?php echo isset($_SESSION['username']) ? $_SESSION['username'] : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Comment :</label>
                                        <textarea class="form-control" rows="2" placeholder="Comment" name="text"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="submit" class="btn custom_color" name="blog_review" value="Submit">
                            <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal">
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
<?php
require_once 'footer.php';
?>
<script>
    function blog_like(uid, bid) {
        // alert(bid);
        if (uid != 0) {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'blog_like', 'user_id': uid, 'blog_id': bid},
                success: function (ans) {
                    console.log(ans);
                    if (ans) {
                        $("#total_like_" + bid).text(ans);
                        $(".d_like_" + bid).css("color", "#e03d3d");
                        $(".d_like_" + bid).removeClass("fa-heart-o");
                        $(".d_like_" + bid).addClass("fa-heart");
//                        $(".dis_" + aid).css("color", "#e03d3d");
                    }
                }
            });
        }
    }
    function blog_com(blog_id) {
        $("#blog_id").val(blog_id);
        $("#blog_comment").modal('show');
    }
</script>