<?php

ob_start();
include './connection.php';

$query = "SELECT * FROM ads WHERE status=1";
$connection = mysqli_query($con, $query);
$count = mysqli_num_rows($connection);
$product = '';
$user_id = '';
if (isset($_SESSION['user_id'])) {
    $user_id = $_SESSION['user_id'];
}
while ($data = mysqli_fetch_assoc($connection)) {
    $product .= ' <div class="col-lg-3 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="' . $data['ads_img'] . '" alt="">
                                <div class="pi-links">
                                    <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                                </div>
                            </div>
                            <div class="pi-text">
                                <h6>' . $data['ads_name'] . '</h6>
                                <p>' . $data['ads_details'] . '</p>
                            </div>';
    $product .= "<label class = 'full' onclick='review(" . $data['ads_id'] . ");' for='review" . $data['ads_id'] . "'></label>";
    $product .= "<textarea name='comment' id='review" . $data['ads_id'] . "' placeholder='comment'>";
    $product .= "</textarea>";
    $product .= "<input type='hidden' name='user_id' class='user_id' value='" . $user_id . "'>";
    $product .= "<button value='save'onclick='review(" . $data['ads_id'] . ");' name='submit'>save</button>"
            . "</div></div>";
}



$abc = 'add';
if (isset($_POST['submit']) && $_POST['submit'] == 'add') {
    $filename = basename($_FILES['ads_img']['name']);
    $tmpname = ($_FILES['ads_img']['tmp_name']);
    $dir = "img/product/";
    $filepath = $dir . $filename;
    $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
    $set = "ads_img='" . $filepath . "'";
    if ($_FILES['ads_img']['size'] > 5000000) {
        echo 'sorry size is too big';
        exit();
    } else if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
        echo 'not allowed';
        exit();
    } else {
        $stat = move_uploaded_file($tmpname, $filepath);
    }

//    var_dump($stat);
//    exit;
    $queery = "INSERT INTO ads SET user_id='" . $_POST['user_id'] . "', ads_name='" . $_POST['ads_name'] . "',ads_details='" . $_POST['ads_details'] . "',$set,added_on='" . date("y/m/d h:i:sa") . "',modified_on='" . date("y/m/d h:i:sa") . "',status=0";
    mysqli_query($con, $queery);
//    print_r($queery);
    header("location:product.php");
} else if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {
    $set = '';
    if (isset($_FILES['ads_img']['name']) && $_FILES['ads_img']['name'] != '') {
        $filename = basename($_FILES['image']['name']);
        $tmpname = ($_FILES['ads_img']['tmp_name']);
        $dir = "img/product/";
        $filepath = $dir . $filename;
        $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
        if ($_FILES['ads_img']['size'] > 5000000) {
            echo 'sorry size is too big';
            exit();
        } else if ($ext == 'jpg' && $ext == 'jpeg' && $ext == 'png' && $ext == 'gif' && $ext == 'svg') {
            echo 'not allowed';
            exit();
        } else {
            $stat = move_uploaded_file($tmpname, $filepath);
        }
        $set = ",image='" . $filepath . "'";
    }
    $jugal = "UPDATE ads SET  ads_name='" . $_POST['ads_name'] . "',ads_details='" . $_POST['ads_details'] . "',$set,modified_on='" . date("y/m/d h:i:sa") . "',status=1 WHERE ads_id='" . $_POST['ads_id'] . "'";
    mysqli_query($con, $jugal);

    header("location:product.php");
}
if (isset($_GET['id']) && $_GET['id'] != '') {
    $sql = "SELECT * FROM ads WHERE status=1 and ads_id='" . $_GET['id'] . "'";
    $result = mysqli_query($con, $sql);
    $cate_data = mysqli_fetch_assoc($result);
    $abc = "edit";
}
?>
