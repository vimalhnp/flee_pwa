-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 15, 2019 at 06:24 AM
-- Server version: 5.7.13-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;f
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `egooee`
--

-- --------------------------------------------------------

--
-- Table structure for table `price_engagement`
--

DROP TABLE IF EXISTS `price_engagement`;
CREATE TABLE IF NOT EXISTS `price_engagement` (
  `price_engagement_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `engagement_type` int(11) NOT NULL COMMENT '0=dislike,1=like,2=reatinf',
  `rating` int(11) DEFAULT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  PRIMARY KEY (`price_engagement_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

DROP TABLE IF EXISTS `pricing`;
CREATE TABLE IF NOT EXISTS `pricing` (
  `pricing_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_number` int(11) NOT NULL,
  `plan_name` varchar(100) DEFAULT NULL,
  `plan_price` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `total_like` int(11) NOT NULL,
  `total_dislikes` int(11) NOT NULL,
  `total_rattings` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  PRIMARY KEY (`pricing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ad`
--

DROP TABLE IF EXISTS `tbl_ad`;
CREATE TABLE IF NOT EXISTS `tbl_ad` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=deactive,1=active,2=pending,3=rejected',
  PRIMARY KEY (`ad_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

DROP TABLE IF EXISTS `tbl_post`;
CREATE TABLE IF NOT EXISTS `tbl_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_type` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `b_name` varchar(250) NOT NULL,
  `b_message` text NOT NULL,
  `b_address` varchar(250) NOT NULL,
  `b_phone` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `password` varchar(250) NOT NULL,
  `otp` int(11) NOT NULL,
  `c_name` varchar(150) NOT NULL,
  `c_title` varchar(150) NOT NULL,
  `c_address` text NOT NULL,
  `c_description` text NOT NULL,
  `c_logo` varchar(255) NOT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_engagement`
--

DROP TABLE IF EXISTS `user_engagement`;
CREATE TABLE IF NOT EXISTS `user_engagement` (
  `user_engagement_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `text` varchar(250) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=like,2=rating,3=comment',
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  PRIMARY KEY (`user_engagement_id`),
  KEY `ad_id` (`post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `price_engagement`
--
ALTER TABLE `price_engagement`
  ADD CONSTRAINT `price_engagement_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_ad`
--
ALTER TABLE `tbl_ad`
  ADD CONSTRAINT `tbl_ad_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD CONSTRAINT `tbl_post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);

--
-- Constraints for table `user_engagement`
--
ALTER TABLE `user_engagement`
  ADD CONSTRAINT `user_engagement_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `tbl_post` (`post_id`),
  ADD CONSTRAINT `user_engagement_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `tbl_ad` ADD `address` VARCHAR(250) NOT NULL AFTER `phone`;



-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_talk`
--

DROP TABLE IF EXISTS `tbl_talk`;
CREATE TABLE IF NOT EXISTS `tbl_talk` (
  `talk_id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  PRIMARY KEY (`talk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_top_business`
--

DROP TABLE IF EXISTS `tbl_top_business`;
CREATE TABLE IF NOT EXISTS `tbl_top_business` (
  `top_business_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(100) NOT NULL,
  `c_title` varchar(100) NOT NULL,
  `c_address` varchar(250) NOT NULL,
  `c_description` text NOT NULL,
  `logo` varchar(250) NOT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`top_business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS `tbl_event`;
CREATE TABLE IF NOT EXISTS `tbl_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `state` varchar(50) NOT NULL,
  `image` varchar(250) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `description` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `added_on` date NOT NULL,
  `modify_on` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_event` ADD `user_id` INT NOT NULL AFTER `category`;

INSERT INTO `tbl_category` (`category_id`, `name`) VALUES (NULL, 'Top Talk'), (NULL, 'Baber Shop');

INSERT INTO `tbl_category` (`category_id`, `name`) VALUES (NULL, 'Beauty salon'), (NULL, 'Beauty Store'), (NULL, 'Bakery'), (NULL, 'Classified'), (NULL, 'Delivery'), (NULL, 'Hardware & Locksmith'), (NULL, 'Home Services'), (NULL, 'Juice Bar'), (NULL, 'Night Life'), (NULL, 'Phone Repair'), (NULL, 'Restaurants'), (NULL, 'Supermarket'), (NULL, 'Other');


ALTER TABLE `tbl_user` CHANGE `c_title` `category_id` INT NOT NULL;



UPDATE `tbl_category` SET `name` = 'Post an Ad' WHERE `tbl_category`.`category_id` = 1;

DELETE FROM `tbl_category` WHERE `tbl_category`.`category_id` = 1;









