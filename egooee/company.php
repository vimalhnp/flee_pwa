<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';

$user_cq = "select * from tbl_company where user_id='" . $_SESSION['user_id']."' order by c_name ASC";
$user_cr = mysqli_query($con, $user_cq);
$ud = '';
while ($user_cd = mysqli_fetch_assoc($user_cr)) {
    $ud .= '<tr>';
    $ud .= '<td>';
    $ud .= ucfirst($user_cd['c_name']);
    $ud .= '</td>';
    $ud .= '<td>';
    $ud .= $user_cd['email'];
    $ud .= '</td>';
    $ud .= '<td>';
    $ud .= '<img src="' . $user_cd['c_logo'] . '" height="50" width="50"/>';
    $ud .= '</td>';
    $ud .= '<td>';
    $ud .= $user_cd['c_address'];
    $ud .= '</td>';
    $ud .= '<td>';
    $ud .= $user_cd['phone_number'];
    $ud .= '</td>';
    $ud .= '<td>';
    $ud .= '<a href="javascript:void(0);" onclick="company_popup('.$user_cd['company_id'].');">Edit</a>';
    $ud .= '</td>';
    $ud .= '</tr>';
}
?>
<section class="top-letest-product-section product-filter-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-9 col-sm-12">
                <center><h4 style="margin-bottom: 10px;padding:20px;">Company Details</h4></center>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Logo</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            <?php  
                            if(isset($ud)){
                                echo $ud;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>  
            <div class="col-lg-3 col-sm-12">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Product filter section end -->
<?php
include_once './footer.php';
?>
