/* =================================
 ------------------------------------
 Divisima | eCommerce Template
 Version: 1.0
 ------------------------------------
 ====================================*/


'use strict';


$(window).on('load', function () {
    /*------------------
     Preloder
     --------------------*/
    $(".loader").fadeOut();
    $("#preloder").delay(400).fadeOut("slow");

});

(function ($) {
    /*------------------
     Navigation
     --------------------*/
    $('.main-menu').slicknav({
        prependTo: '.main-navbar .container',
        closedSymbol: '<i class="flaticon-right-arrow"></i>',
        openedSymbol: '<i class="flaticon-down-arrow"></i>'
    });


    /*------------------
     ScrollBar
     --------------------*/
    $(".cart-table-warp, .product-thumbs").niceScroll({
        cursorborder: "",
        cursorcolor: "#afafaf",
        boxzoom: false
    });


    /*------------------
     Category menu
     --------------------*/
    $('.category-menu > li').hover(function (e) {
        $(this).addClass('active');
        e.preventDefault();
    });
    $('.category-menu').mouseleave(function (e) {
        $('.category-menu li').removeClass('active');
        e.preventDefault();
    });


    /*------------------
     Background Set
     --------------------*/
    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });



    /*------------------
     Hero Slider
     --------------------*/
    var hero_s = $(".hero-slider");
    hero_s.owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        dots: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        onInitialized: function () {
            var a = this.items().length;
            $("#snh-1").html("<span>1</span><span>" + a + "</span>");
        }
    }).on("changed.owl.carousel", function (a) {
        var b = --a.item.index, a = a.item.count;
        $("#snh-1").html("<span> " + (1 > b ? b + a : b > a ? b - a : b) + "</span><span>" + a + "</span>");

    });

    hero_s.append('<div class="slider-nav-warp"><div class="slider-nav"></div></div>');
    $(".hero-slider .owl-nav, .hero-slider .owl-dots").appendTo('.slider-nav');



    /*------------------
     Brands Slider
     --------------------*/
    $('.product-slider').owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        margin: 30,
        autoplay: true,
        navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 2,
            },
            768: {
                items: 3,
            },
            1200: {
                items: 4,
            }
        }
    });


    /*------------------
     Popular Services
     --------------------*/
    $('.popular-services-slider').owlCarousel({
        loop: true,
        dots: false,
        margin: 40,
        autoplay: true,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            991: {
                items: 3
            }
        }
    });


    /*------------------
     Accordions
     --------------------*/
    $('.panel-link').on('click', function (e) {
        $('.panel-link').removeClass('active');
        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.addClass('active');
        }
        e.preventDefault();
    });


    /*-------------------
     Range Slider
     --------------------- */
    var rangeSlider = $(".price-range"),
            minamount = $("#minamount"),
            maxamount = $("#maxamount"),
            minPrice = rangeSlider.data('min'),
            maxPrice = rangeSlider.data('max');
    rangeSlider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            minamount.val('$' + ui.values[0]);
            maxamount.val('$' + ui.values[1]);
        }
    });
    minamount.val('$' + rangeSlider.slider("values", 0));
    maxamount.val('$' + rangeSlider.slider("values", 1));


    /*-------------------
     Quantity change
     --------------------- */
    var proQty = $('.pro-qty');
    proQty.prepend('<span class="dec qtybtn">-</span>');
    proQty.append('<span class="inc qtybtn">+</span>');
    proQty.on('click', '.qtybtn', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find('input').val(newVal);
    });



    /*------------------
     Single Product
     --------------------*/
    $('.product-thumbs-track > .pt').on('click', function () {
        $('.product-thumbs-track .pt').removeClass('active');
        $(this).addClass('active');
        var imgurl = $(this).data('imgbigurl');
        var bigImg = $('.product-big-img').attr('src');
        if (imgurl != bigImg) {
            $('.product-big-img').attr({src: imgurl});
            $('.zoomImg').attr({src: imgurl});
        }
    });


    $('.product-pic-zoom').zoom();

    /*CUSTOM JS*/
    $("#register_open").click(function () {
        $('#login_close').click();

    });
    $("#login_open").click(function () {
        $('#register_popup').click();

    });

    $("#loginForm").validate({
        rules: {
            useremail: {
                required: true,
            },
            password: {
                required: true
            }
        },
        messages: {
            useremail: {
                required: "Please Enter Your Email Id OR User Name",
            },
            password: {
                required: "Enter your Password"
            }
        },
        submitHandler: function (form) {
            var email = $(form).find('#login_email').val();
            var pass = $(form).find('#login_password').val();
            $.ajax({
                url: 'connect.php',
                type: 'POST',
                data: {'action': 'do_login', 'useremail': email, 'password': pass},
                success: function (ans) {
                    console.log(ans);
                    if (ans == 'not') {
                        $('.login_otp').removeClass('d-none').show('slow');
                    } else if (ans == 'fail') {
                        $('.login_error').removeClass('d-none').show('slow');
                    } else if (ans == 'success') {
                        window.location.href = 'index.php';
                    } else if (ans == 'success1') {
                        window.location.href = 'feed.php';
                    }
                }
            });
            return false;
        }
    });
    $("#registerForm").validate({
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            useremail: {
                required: true,
                email: true
            },
            username: {
                required: true,
            },
            password: {
                required: true
            },
            cnf_password: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            firstname: {
                required: "Please Provide Firstname."
            },
            lastname: {
                required: "Please Provide Lastname."
            },
            useremail: {
                required: "Please Enter Your Email Id",
                email: "Email Id is Invalid"
            },
            username: {
                required: "Please Enter Your User Name",
            },
            password: {
                required: "Enter your Password"
            },
            cnf_password: {
                required: "Your Password Not Match"
            }
        },
        submitHandler: function (form) {
            var fname = $(form).find('#firstname').val();
            var lname = $(form).find('#lastname').val();
            var email = $(form).find('#useremail').val();
            var pass = $(form).find('#password').val();
            var cnf_pass = $(form).find('#cnf_password').val();
            var country = $(form).find('#country1').val();
            var zip_code = $(form).find('#zipcode').val();
            var user_type = $(form).find('#user_type').val();
            var buser_type = $(form).find('#business_type_user').val();
            var dob = $(form).find('#dob').val();
            var gender = '';
            if (buser_type == 'business_user') {
                gender = $(form).find('#bgender').val();
            } else if (user_type == 'community_user') {
                gender = $(form).find('#gender').val();
            }
//            alert(gender);
//            var gender = $(form).find('#gender').val();
            var username = $(form).find('#username').val();
            var title = $(form).find('#title').val();
            var state = $(form).find('#state').val();
            var street_address = $(form).find('#street_address').val();
            var encmt = $(form).find('#enticements').val();
            var b_type = $(form).find('#btype').val();
            var bs_name = $(form).find('#bs_name').val();
            var user_id = $(form).find('#new_user_id').val();
            var company_id = $(form).find('#new_company_id').val();
            var phone = $(form).find('#phone_number').val();
            $("#register_submit").text("Loading...").prop('disabled', true);
            $.ajax({
                url: 'connect.php',
                type: 'POST',
                data: {'action': 'do_register', 'useremail': email, 'password': pass, 'cnf_pass': cnf_pass, 'firstname': fname, 'lastname': lname, 'saddress': street_address, 'country': country, 'zip_code': zip_code, 'user_type': user_type, 'dob': dob, 'gender': gender, 'encmt': encmt, 'username': username, 'state': state, title: title, 'b_type': b_type, 'bs_name': bs_name, 'user_id': user_id, 'company_id': company_id, 'phone': phone},
                success: function (ans) {
                    console.log(ans);
                    if (ans == 'registered') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.register_already').removeClass('d-none').show('slow');
                    } else if (ans == 'fail') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.register_error').removeClass('d-none').show('slow');
                    } else if (ans == 'age_low_13') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.age_error_13').removeClass('d-none').show('slow');
                    } else if (ans == 'username registered') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.age_error_18').removeClass('d-none').show('slow');
                    } else if (ans == 'less_7') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.error_encmt').removeClass('d-none').show('slow');
                    } else if (ans == 'required_encmt') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.error_encmt').removeClass('d-none').show('slow');
                    } else if (ans == 'success') {
                        $("#email_hide").val(email);
                        $('#register_popup').modal('hide');
                        $('#success_model').modal('show');
                    } else if (ans == 'error_address') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.error_address').removeClass('d-none').show('slow');
                    } else if (ans == 'error_state') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.error_state').removeClass('d-none').show('slow');
                    } else if (ans == 'error_phone') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.error_phone').removeClass('d-none').show('slow');
                    } else if (ans == 'error_business_type') {
                        $("#register_submit").text("Submit").prop('disabled', false);
                        $('.error_business_type').removeClass('d-none').show('slow');
                    } else {
                        $("#register_submit").text("Submit").prop('disabled', false);
                    }
                    setTimeout(function () {
                        $('.alert').fadeOut('slow');
                    }, 5000);
                }
            });
            return false;
        }
    });

//otp model
    $("#otpform").validate({
        rules: {
            otp: {
                required: true
            }
        },
        messages: {
            otp: {
                required: "Please Provide OTP."
            }
        },
        submitHandler: function (form) {
            var otp = $(form).find('#otp').val();
            var email = $(form).find('#email_hide').val();
            $.ajax({
                url: 'connect.php',
                type: 'POST',
                data: {'action': 'check_otp', 'otp': otp, 'useremail': email},
                success: function (ans) {
                    console.log(ans);
                    if (ans == 'success') {
                        window.location.href = 'index.php';
                    } else if (ans == 'c_pass') {

                        $('#otp_model').modal('hide');
                        $('#confirm_modal').modal('show');
                        $('#confirm_email').val(email);
                    } else {
                        $('.otp_error').removeClass('d-none').show('slow');
                    }
                }
            });
            return false;
        }
    });
//    forget password
    $("#forget_password").click(function () {
        $('#login_close').click();
        $('#reset_modal').modal('show');
    });
    $("#resetform").validate({
        rules: {
            reset_email: {
                required: true,
                email: true
            }
        },
        messages: {
            reset_email: {
                required: "Please Enter Your Email Id",
                email: "Email Id is Invalid"
            }
        },
        submitHandler: function (form) {
            var email = $(form).find('#reset_email').val();
            $("#reset_submit").val("Loading...").prop('disabled', true);
            $.ajax({
                url: 'connect.php',
                type: 'POST',
                data: {'action': 'reset_pass', 'useremail': email},
                success: function (ans) {
                    console.log(ans);
                    if (ans == 'success') {
                        $("#email_hide").val(email);
                        $('#reset_model').modal('hide');
                        $('#otp_model').modal('show');
                    } else {
                        $("#register_submit").val("Submit").prop('disabled', false);
                        $('.reset_error').removeClass('d-none').show('slow');
                    }
                }
            });
            return false;
        }
    });
//    confirm password
    $("#confirmform").validate({
        rules: {
            confirm_password: {
                required: true
            }
        },
        messages: {
            confirm_password: {
                required: "Please Enter New Password"
            }
        },
        submitHandler: function (form) {
            var password = $(form).find('#confirm_password').val();
            var email = $(form).find('#confirm_email').val();
            $.ajax({
                url: 'connect.php',
                type: 'POST',
                data: {'action': 'confirm_pass', 'password': password, 'useremail': email},
                success: function (ans) {
                    if (ans == 'success') {
                        $('#confirm_modal').modal('hide');
                        $('#signin').modal('show');
                    } else {
                        $('.confirm_error').removeClass('d-none').show('slow');
                    }
                }
            });
            return false;
        }
    });
    //star rating
    $(document).ready(function () {
//        $("#star1").click(function () {
//            var radioValue = $("input[type='radio'][name='rate']:checked").val();
//            if (radioValue) {
//                alert("Your are a - " + radioValue);
//            }
//        });
//        $("#star2").click(function () {
//            var radioValue = $("input[type='radio'][name='rate']:checked").val();
//            if (radioValue) {
//                alert("Your are a - " + radioValue);
//            }
//        });
//        $("#star3").click(function () {
//            var radioValue = $("input[type='radio'][name='rate']:checked").val();
//            if (radioValue) {
//                alert("Your are a - " + radioValue);
//            }
//        });
//        $("#star4").click(function () {
//            var radioValue = $("input[type='radio'][name='rate']:checked").val();
//            if (radioValue) {
//                alert("Your are a - " + radioValue);
//            }
//        });
//        $("#star5").click(function () {
//            var radioValue = $("input[type='radio'][name='rate']:checked").val();
//            if (radioValue) {
//                alert("Your are a - " + radioValue);
//            }
//        });
    });


    $('#user_setting').click(function (e) {
        e.stopPropagation();
        $('#user_setting_dd').stop(true, true).slideToggle(400);
    });

    $("#top_talk_btn").click(function () {
        var top_location = ($("#top_talk").offset().top - 70);
        $('html, body').animate({
            scrollTop: top_location
        }, 2000);
    });


})(jQuery);
