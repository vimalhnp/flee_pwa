<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>
<section class="product-section">
    <div class="container-fluid">
        <?php
//        require_once 'searchbar.php';
        ?>
    </div>
    <div class="container">
        <?php
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        $no_of_records_per_page = 10;
        $offset = ($pageno - 1) * $no_of_records_per_page;
        $total_pages_sql = "SELECT COUNT(*) FROM tbl_blog where status=1";
        $result = mysqli_query($con, $total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);
//$sql = "SELECT * FROM tbl_blog LIMIT $offset, $no_of_records_per_page";
        ?> 
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-9">
                                <a href="" class="btn" style="border-radius:8px;margin: 8px 0">Community Posting</a>
                            </div>
                            <div class="col-md-3">
                                <a href="javascript:void(0);" class="btn btn-outline-dark" data-toggle="modal" data-target="#write_blog">Post to Community</a>
                            </div>
                        </div>
                        <!--                        <a href="#" class="btn btn-outline-dark" style="border-radius:8px;margin-left: 0px;">Friends</a>
                        <a href="#" class="btn btn-outline-dark" style="border-radius:8px;margin-left: 0px;">Groups</a>-->
                        <div class="panel-body">
                            <div class="row">
                                <?php
                                if (isset($_GET['d']) && $_GET['d'] != '') {
                                    if ($_GET['d'] == 24) {
                                        $review_query = "select b.*,u.image from tbl_blog as b inner join tbl_user as u on (b.user_id=u.user_id) where DATE(b.added_on) >=  NOW() - INTERVAL 1 DAY AND b.status = 1 LIMIT $offset, $no_of_records_per_page";
                                    } elseif ($_GET['d'] == 1) {
                                        $review_query = "select b.*,u.image from tbl_blog as b inner join tbl_user as u on (b.user_id=u.user_id) where DATE(b.added_on) >= DATE(NOW() - INTERVAL 1 DAY) AND b.status = 1 LIMIT $offset, $no_of_records_per_page";
                                    } elseif ($_GET['d'] == 7) {
                                        $review_query = "select b.*,u.image from tbl_blog as b inner join tbl_user as u on (b.user_id=u.user_id) where DATE(b.added_on) >= NOW() + INTERVAL -7 DAY AND DATE(b.added_on) <  NOW() + INTERVAL  0 DAY AND b.status = 1 LIMIT $offset, $no_of_records_per_page";
                                    } elseif ($_GET['d'] == 30) {
                                        $review_query = "select b.*,u.image from tbl_blog as b inner join tbl_user as u on (b.user_id=u.user_id) where DATE(b.added_on) >= (CURDATE() - INTERVAL 1 MONTH) AND b.status = 1 LIMIT $offset, $no_of_records_per_page";
                                    }
                                } else {
                                    $review_query = "select b.*,u.image from tbl_blog as b inner join tbl_user as u on (b.user_id=u.user_id) where b.status = 1 LIMIT $offset, $no_of_records_per_page";
                                }
                                $review_result = mysqli_query($con, $review_query);
                                if (mysqli_num_rows($review_result)) {
                                    while ($reviews = mysqli_fetch_assoc($review_result)) {

                                        if (isset($reviews['blog_id'])) {
                                            $query = "select * from blog_engagement where blog_id='" . $reviews['blog_id'] . "' AND type=1";
                                            $result = mysqli_query($con, $query);

                                            $count = mysqli_num_rows($result);
                                        }


                                        $uid = '0,';

                                        $style1 = '';
                                        $class = 'fa-heart-o';
                                        if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                            $uid = $_SESSION['user_id'] . ",";

//                                                check already like
                                            $check_like_q = "select * from blog_engagement where blog_id='" . $reviews['blog_id'] . "' and user_id='" . $_SESSION['user_id'] . "' AND type=1";
                                            $check_like_r = mysqli_query($con, $check_like_q);
                                            $check_like_r = mysqli_num_rows($check_like_r);

                                            if ($check_like_r > 0) {
//                                                    $style = "style='background:red;'";
                                                $style1 = "style='color:#e03d3d;'";
                                                $class = "fa-heart";
                                            }
                                        }
                                        ?>

                                        <div class="col-md-6 col-sm-12" style="margin-bottom: 10px;">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-3 col-3 col-lg-3">
                                                            <a href="view_post.php?id=<?php echo $reviews['blog_id'] ?>">
                                                                <img src="<?php echo (isset($reviews['image']) && $reviews['image'] != '') ? $reviews['image'] : 'img/default-avatar.png'; ?>" class="img-responsive" style="width:50px;">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-9 col-lg-9 col-9">
                                                            <div><?php echo ucfirst($reviews['username']); ?>
                                                                <div class="pull-right" style="padding-top:5px;"><span id="total_like_<?php echo $reviews['blog_id'] ?>"><?php echo $count; ?></span> <a href="javascript:void(0);" style="text-decoration:none;color:black;" onclick="blog_like(<?php echo $uid . $reviews['blog_id'] ?>);"><i class="fa <?php echo $class; ?> d_like_<?php echo $reviews['blog_id'] ?>" <?php echo $style1 ?>></i></a></div>
                                                            </div>
                                                            <div style="font-size: 13px;height: 42px;text-align: justify;"><?php
                                                                $allowedlimit = 67;
                                                                echo (mb_strlen($reviews['description']) > $allowedlimit) ? mb_substr($reviews['description'], 0, $allowedlimit) . "...." : $reviews['description'];
                                                                ?>
                                                            </div>
                                                            <div style="font-size: 13px;">Date : <?php echo ucfirst($reviews['added_on']); ?></div>
                                                            <a href="javascript:void(0);" onclick="blog_com(<?php echo $reviews['blog_id'] ?>);" class="pull-right">Write a Comment</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>


                <ul class="pagination">
                    <?php
                   
                    $pagLink = "";
                    if ($pageno <= 1) {
                        $pagLink .= "<li class='disabled'>";
                       
                    }
                     if ($pageno <= 1) {
                            $p= '';
                        } else {
                            $p= "?pageno=" . ($pageno - 1);
                        }
                        
                        $pagLink .= "<a href='community.php".$p."' class='page-link'>Prev</a>";
                        $pagLink .= "</li>";
                    for ($i = 1; $i <= $total_pages; $i++) {
                        if ($i == $pageno) {
                            $pagLink .= "<li class='page-item active'><a class='page-link' href='community.php?pageno="
                                    . $i . "'>" . $i . "</a></li>";
                        } else {
                            $pagLink .= "<li><a href='community.php?pageno=" . $i . "' class='page-link'> 
                                                " . $i . "</a></li>";
                        }
                    };
                    if ($pageno <= $total_pages) {
                        $pagLink .= "<li class='disabled'>";
                        if ($pageno >= $total_pages) {
                            $p= '';
                        } else {
                            $p= "?pageno=" . ($pageno + 1);
                        }
                        
                        $pagLink .= "<a href='community.php".$p."' class='page-link'>Next</a>";
                        $pagLink .= "</li>";
                    }
                    echo $pagLink;
                    ?>
                </ul>

            </div>
            <div class="col-lg-3">
                <?php
                if (isset($_GET['action']) && $_GET['action'] == 'success') {
                    ?>
                    <div class="alert alert-success">Successfully done.</div>
                    <?php
                }
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>

        <div class="modal" id="blog_comment">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h5 class="modal-title" style="margin: 0 auto;">Write a comment</h5>

                    </div>
                    <form action="rating.php" method="post">
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email :</label>
                                        <input type="text" class="form-control" readonly="" placeholder="Email" value="<?php echo isset($_SESSION['useremail']) ? $_SESSION['useremail'] : ''; ?>">
                                        <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; ?>">
                                        <input type="hidden" class="form-control" id="blog_id" name="blog_id" value="<?php echo isset($data['blog_id']) ? $data['blog_id'] : ''; ?>">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>User Name :</label>
                                        <input type="text" class="form-control" placeholder="Name" name="username" value="<?php echo isset($_SESSION['username']) ? $_SESSION['username'] : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Comment :</label>
                                        <textarea class="form-control" rows="2" placeholder="Comment" name="text"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="submit" class="btn custom_color" name="blog_review" value="Submit">
                            <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal">
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
<?php
require_once 'footer.php';
?>
<script>
    function blog_like(uid, bid) {
        // alert(bid);
        if (uid != 0) {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'blog_like', 'user_id': uid, 'blog_id': bid},
                success: function (ans) {
                    console.log(ans);
                    if (ans) {
                        $("#total_like_" + bid).text(ans);
                        $(".d_like_" + bid).css("color", "#e03d3d");
                        $(".d_like_" + bid).removeClass("fa-heart-o");
                        $(".d_like_" + bid).addClass("fa-heart");
//                        $(".dis_" + aid).css("color", "#e03d3d");
                    }
                }
            });
        }
    }
    function blog_com(blog_id) {
        $("#blog_id").val(blog_id);
        $("#blog_comment").modal('show');
    }
</script>