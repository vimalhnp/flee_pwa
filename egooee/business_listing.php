<?php
include 'connection.php';
include_once './header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>

<!-- Product filter section -->
<section class="top-letest-product-section product-filter-section">
    <div class="container-fluid">
        <?php
        require_once '10_slider.php';
        ?>
        <div class="row">
            <div class="col-lg-9 col-sm-12">
                <center><h4 style="margin-bottom: 20px;padding:20px;">Business</h4></center>
                <div class="row">
                    <?php
                    if (isset($_POST['search1']) && $_POST['search1'] != '' || isset($_POST['search2']) && $_POST['search2'] != '') {
                        $sql = "select c.name,co.*,cu.country_name,bp.plan_number from tbl_category as c inner join tbl_company as co on (c.category_id=co.category_id) inner join countries as cu on (cu.country_id=co.country_id) inner join tbl_buy_plan as bp on(bp.company_id=co.company_id) where c.name like '%" . $_POST['search1'] . "%' AND co.status=1 AND (co.zipcode like '%" . $_POST['search2'] . "%' OR co.state like '%" . $_POST['search2'] . "%')";
                    } elseif (isset($_GET['id']) && $_GET['id'] == 24) {
                        $sql = "select c.name,co.*,cu.country_name,bp.plan_number from tbl_category as c inner join tbl_company as co on (c.category_id=co.category_id) inner join countries as cu on (cu.country_id=co.country_id) inner join tbl_buy_plan as bp on(bp.company_id=co.company_id) where co.status=1 AND DATE(co.added_on) >=  NOW() - INTERVAL 1 DAY";
                    } else {
                        $sql = "select c.name,co.*,cu.country_name,bp.plan_number from tbl_category as c inner join tbl_company as co on (c.category_id=co.category_id) inner join countries as cu on (cu.country_id=co.country_id) inner join tbl_buy_plan as bp on(bp.company_id=co.company_id) where co.status=1";
                    }
                    $res = mysqli_query($con, $sql);
                    if ($res) {
                        if (mysqli_num_rows($res)) {
                            while ($data = mysqli_fetch_assoc($res)) {
                                if ($data['plan_number'] != 1) {
                                    ?>
                                    <?php
                                    $uid = '0,';
                                    $style = '';
                                    $style1 = '';
                                    $class = 'fa-heart-o';
                                    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                        $uid = $_SESSION['user_id'] . ",";

//                                                check already like
                                        $check_like_q = "select * from company_engagement where type=1 and company_id='" . $data['company_id'] . "' and user_id='" . $_SESSION['user_id'] . "'";
                                        $check_like_r = mysqli_query($con, $check_like_q);
                                        $check_like_r = mysqli_num_rows($check_like_r);

                                        if ($check_like_r > 0) {
                                            $style1 = "style='color:#e03d3d;'";
                                            $class = "fa-heart";
                                        }
                                    }
                                    ?>
                                    <?php
                                    if (isset($data['company_id'])) {
                                        $query = "select * from company_engagement where type=1 and company_id='" . $data['company_id'] . "'";
                                        $result = mysqli_query($con, $query);
                                        $count = mysqli_num_rows($result);
                                        $query_count_like = "select * from company_engagement where type=1";
                                        $result_count_like = mysqli_query($con, $query_count_like);
                                        $total_count_like = mysqli_num_rows($result_count_like);
                                        $query_count_star = "select * from company_engagement where type=2";
                                        $result_count_star = mysqli_query($con, $query_count_star);
                                        $total_count_star = mysqli_num_rows($result_count_star);
                                        $query_count_review = "select * from company_engagement where type=3 AND company_id='".$data['company_id']."'";
                                        $result_count_review = mysqli_query($con, $query_count_review);
                                        $total_count_review = mysqli_num_rows($result_count_review);
                                    }
                                    ?>
                                    <?php
                                    if (isset($data['company_id'])) {
                                        $query_avg = "select avg(text) as avg from company_engagement where type=2 and company_id='" . $data['company_id'] . "'";
                                        $result_avg = mysqli_query($con, $query_avg);
                                        $data_avg = mysqli_fetch_assoc($result_avg);
                                        $avg = $data_avg['avg'];
                                        $disable = '';
//                                                check user own post
                                        if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                            $user_post_q = "select * from tbl_company where company_id='" . $data['company_id'] . "' AND user_id='" . $_SESSION['user_id'] . "'";
                                            $user_post_r = mysqli_query($con, $user_post_q);
                                            $user_post_d = mysqli_num_rows($user_post_r);

                                            if ($user_post_d > 0) {
                                                $disable = "disabled";
                                            }
                                        }
                                    }
                                    ?>
                                    <!--                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12" style="padding-right:0;margin-bottom: 10px;">
                                                                            <div class="product-item" style="border-left:1px solid #dfe1e5;">
                                    
                                                                                <div class="pi-pic">
                                                                                    <a href="<?php echo $data['weburl'] ?>" target="_blank" >
                                                                                        <center> <img src="<?php echo (isset($data['c_logo']) && $data['c_logo'] != '') ? $data['c_logo'] : 'img/product/free_business.png'; ?>" style="height:210px;width:auto;" alt=""></center>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="pi-text" style="padding:0 15px;">
                                                                                    <div class="rate">
                                    
                                                                                        <form class="user-rating-form">
                                    <?php
                                    if (isset($data['plan_number']) && $data['plan_number'] == 3) {
                                        ?>
                                                                                                                        <span class="user-rating">
                                                                                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="5" <?php echo (isset($avg) && $avg != '' && $avg > 4) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>
                                                            
                                                                                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="4" <?php echo (isset($avg) && $avg != '' && $avg <= 4) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>
                                                            
                                                                                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="3" <?php echo (isset($avg) && $avg != '' && $avg <= 3) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>
                                                            
                                                                                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="2" <?php echo (isset($avg) && $avg != '' && $avg <= 2) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>
                                                            
                                                                                                                            <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="1" <?php echo (isset($avg) && $avg != '' && $avg <= 1) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>
                                                                                                                        </span>
                                        <?php
                                    }

                                    if (isset($data['plan_number']) && $data['plan_number'] == 2) {
                                        ?>
                                                                                                                        <div class="pull-right" style="padding-top:5px;"><span id="total_like_<?php echo $data['company_id'] ?>"><?php echo $count; ?></span> <a href="javascript:void(0);" style="text-decoration:none;color:black;" onclick="company_like_dislike(<?php echo $uid . $data['company_id'] ?>);"><i class="fa <?php echo $class; ?> d_like_<?php echo $data['company_id'] ?>" <?php echo $style1 ?>></i></a></div>
                                        <?php
                                    } else {
                                        ?>
                                                                                                                        <div class="pull-right" style="padding-top:5px;visibility: hidden;"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></div>
                                        <?php
                                    }
                                    ?>
                                                                                        </form>
                                    
                                                                                    </div>
                                                                                    <p id="ratting_msg_<?php echo $data['company_id'] ?>" style="padding: 6px;margin: 0px 0 0 0;" class="clearfix alert alert-info d-none text-left"></p>
                                    
                                                                                    <div style="clear:both;"></div>
                                                                                    <p>Business Name : <b><?php echo ucwords($data['c_name']) ?></b> </p>
                                                                                    <p style="word-break:break-all;">Message : <?php echo $data['c_description'] ?></p>
                                                                                    <p style="word-break:break-all;">Location : <?php echo $data['c_address'] ?></p>
                                                                                    <p>Ph : <?php echo $data['phone_number'] ?></p>
                                    <?php
                                    if (isset($data['plan_number']) && $data['plan_number'] == 2 || $data['plan_number'] == 3) {
                                        ?>
                                                                                                                <div class="p-review">
                                        <?php
                                        if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                            ?>
                                                                                                                                                <a href="javascript:void(0)" onclick="write_review_on_company(<?php echo $_SESSION['user_id'] ?>,<?php echo $data['company_id'] ?>);">Write a Review</a>
                                            <?php
                                        } else {
                                            ?>
                                                                                                                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#signin">Write a Review</a>
                                            <?php
                                        }
                                        ?>
                                                                                                                </div>
                                        <?php
                                    }
                                    ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12" style="margin-bottom: 10px;">

                                        <div class="card" style="min-height:240px;">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4 col-4 col-lg-4">
                                                        <a href="<?php echo $data['weburl'] ?>" target="_blank" >
                                                            <img src="<?php echo (isset($data['c_logo']) && $data['c_logo'] != '') ? $data['c_logo'] : 'img/product/free_business.png'; ?>" class="img-responsive" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 col-lg-8 col-8">
                                                        <div><b><?php echo ucwords($data['c_name']) ?></b> </div>


                                                        <form class="user-rating-form" style="display:inline-block;">
                                                            <?php
                                                            if (isset($data['plan_number']) && $data['plan_number'] == 3) {
                                                                ?>
                                                                <span class="user-rating">
                                                                    <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="5" <?php echo (isset($avg) && $avg != '' && $avg > 4) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>

                                                                    <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="4" <?php echo (isset($avg) && $avg != '' && $avg <= 4) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>

                                                                    <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="3" <?php echo (isset($avg) && $avg != '' && $avg <= 3) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>

                                                                    <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="2" <?php echo (isset($avg) && $avg != '' && $avg <= 2) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>

                                                                    <input type="radio" <?php echo $disable ?> name="rate_<?php echo $data['company_id'] ?>" value="1" <?php echo (isset($avg) && $avg != '' && $avg <= 1) ? 'checked' : ''; ?> onclick="company_star_rating(<?php echo $_SESSION['user_id'] ?>, <?php echo $data['company_id'] ?>)"><span class="star"></span>
                                                                </span>
                                                                <?php
                                                            }

                                                            if (isset($data['plan_number']) && $data['plan_number'] == 2) {
                                                                ?>
                                                                <div style="padding-top:5px;"><span id="total_like_<?php echo $data['company_id'] ?>"><?php echo $count; ?></span> <a href="javascript:void(0);" style="text-decoration:none;color:black;" onclick="company_like_dislike(<?php echo $uid . $data['company_id'] ?>);"><i class="fa <?php echo $class; ?> d_like_<?php echo $data['company_id'] ?>" <?php echo $style1 ?>></i></a></div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </form>
                                                        <?php
                                                        if (isset($data['plan_number']) && $data['plan_number'] == 2) {
                                                            echo '<span class="pull-right">Out of : '.$total_count_like.'</span>';
                                                        }else if (isset($data['plan_number']) && $data['plan_number'] == 3) {
                                                            echo '<span class="pull-right">Out of : '.$total_count_star.'</span>';
                                                        }
                                                        ?>
                         
                                                        <div id="ratting_msg_<?php echo $data['company_id'] ?>" style="padding: 6px;margin: 0px 0 0 0;" class="clearfix alert alert-info d-none text-left"></div>

                                                        <div style="text-align: justify;">Message : <?php echo $data['c_description'] ?></div>
                                                        <div style="text-align: justify;">Location : <?php echo $data['c_address'] ?></div>
                                                        <div>Ph : <?php echo $data['phone_number'] ?></div>
                                                        <?php
                                                        if (isset($data['plan_number']) && $data['plan_number'] == 2 || $data['plan_number'] == 3) {
                                                            ?>
                                                            <div class="p-review pull-right">
                                                                <?php
                                                                if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                                                    ?>
                                                                    <a href="javascript:void(0)" onclick="write_review_on_company(<?php echo $_SESSION['user_id'] ?>,<?php echo $data['company_id'] ?>);">Write a Review</a>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#signin">Write a Review</a>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <span> : <?php echo $total_count_review ?></span>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div id="accordion" class="accordion-area" style="margin-top:15px;">
                                                            <div class="panel">
                                                                <div class="panel-header" id="headingOne">
                                                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Reviews</button>
                                                                </div>
                                                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <?php
                                                                            if (isset($data['company_id'])) {
                                                                                $review_query = "select ce.*,u.image,u.user_id from company_engagement as ce inner join tbl_user as u on (ce.user_id=u.user_id) where ce.type=3 and ce.company_id='" . $data['company_id'] . "' order by ce.company_engagement_id desc";

                                                                                $review_result = mysqli_query($con, $review_query);
                                                                                if (mysqli_num_rows($review_result)) {
                                                                                    while ($reviews = mysqli_fetch_assoc($review_result)) {
                                                                                        ?>

                                                                                        <div class="col-md-12 col-sm-12">
                                                                                            <div class="card" style="border-radius:0;border-bottom:0;border-left:none;border-right:none;">
                                                                                                <div class="card-body">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-3 col-3 col-lg-3">
                                                                                                            <img src="<?php echo (isset($reviews['image']) && $reviews['image'] != '') ? $reviews['image'] : 'img/default-avatar.png'; ?>" class="img-responsive" style="width:50px;">
                                                                                                        </div>
                                                                                                        <div class="col-md-9 col-lg-9 col-9">
                                                                                                            <div><?php echo ucfirst($reviews['username']); ?><span class="pull-right"><?php echo date('d-m-Y', strtotime($reviews['added_on'])); ?></span></div>
                                                                                                            <div style="font-size: 13px;height: 75px;overflow-y: auto;text-align: justify;overflow-x: hidden;"><?php echo ucfirst($reviews['text']); ?></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        } else {
                            echo "<p style='margin:0 auto;'>No Data</p>";
                        }
                    } else {
                        echo "<p style='margin:0 auto;'>No Data</p>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Product filter section end -->
<div class="modal" id="review">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title" style="margin: 0 auto;">Write a Review</h5>

            </div>

            <?php
//                                    user data

            if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                $user_q = "select * from tbl_user where user_id=" . $_SESSION['user_id'];

                $user_r = mysqli_query($con, $user_q);
                $user_d = mysqli_fetch_assoc($user_r);
            }
            ?>
            <form action="rating.php" method="post" id="review">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email :</label>
                                <input type="text" class="form-control" readonly="" placeholder="Email" value="<?php echo isset($user_d['email']) ? $user_d['email'] : ''; ?>">
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($user_d['user_id']) ? $user_d['user_id'] : ''; ?>">
                                <input type="hidden" class="form-control" name="company_id" id="company_id">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name :</label>
                                <input type="text" class="form-control" placeholder="Name" name="rname" id="rname" value="<?php echo isset($user_d['fname']) ? $user_d['fname'] . ' ' . $user_d['lname'] : ''; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Review :</label>
                                <textarea class="form-control" rows="2" placeholder="Review" name="text" id="text"></textarea>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn custom_color" id="submit_company_review" name="submit_company_review" value="Submit Review">
                    <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal">
                </div>
            </form>

        </div>
    </div>
</div>
<?php
include_once './footer.php';
?>
<script>
    function company_like_dislike(uid, cid) {
        if (uid != 0) {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'company_like', 'user_id': uid, 'company_id': cid},
                success: function (ans) {
                    if (ans) {
                        $("#total_like_" + cid).text(ans);
                        $(".d_like_" + cid).css("color", "#e03d3d");
                        $(".d_like_" + cid).removeClass("fa-heart-o");
                        $(".d_like_" + cid).addClass("fa-heart");
                    }
                }
            });
        }
    }
    function company_star_rating(uid, cid) {
//        alert("ok");
        var radioValue = $("input[type='radio'][name='rate_" + cid + "']:checked").val();
//         alert(radioValue);
        if (radioValue) {
            // alert("You are give a - " + radioValue +" star");
            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'company_rating', 'user_id': uid, 'company_id': cid, 'text': radioValue},
                success: function (ans) {
                    if (ans == "success") {
                        $('#ratting_msg_' + cid).html('Successfully Given ' + radioValue + ' stars').removeClass('d-none');
                    } else if (ans == "already") {
                        $('#ratting_msg_' + cid).html('Sorry, You can not give ratting again!').removeClass('d-none');
                    } else if (ans == "own") {
                        $('#ratting_msg_' + cid).html('Sorry, You can not give ratting to your business!').removeClass('d-none');
                    } else {
                        $('#ratting_msg_' + cid).html('OOPS, Something went wrong!').removeClass('d-none');

                    }
                }
            });
        }
    }
    function write_review_on_company(uid, cid) {
        if (uid != '' && cid != '') {
            $("#review").modal("show");
            $("#company_id").val(cid);
        }
    }
</script>