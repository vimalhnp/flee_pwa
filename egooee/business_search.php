<?php
include 'connection.php';
include_once './header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>

<!-- Product filter section -->
<section class="top-letest-product-section product-filter-section">
    <div class="container-fluid">
        <?php
        require_once '10_slider.php';
        ?>
        <div class="row">
            <div class="col-lg-9 col-sm-12">
                <center><h4 style="margin-bottom: 20px;padding:20px;">Business</h4></center>
                <div class="row">
                    <?php
                    if (isset($_POST['search1']) && $_POST['search1'] != '' || isset($_POST['search2']) && $_POST['search2'] != '') {
                        $sql = "select c.name,co.*,cu.country_name from tbl_category as c inner join tbl_company as co on (c.category_id=co.category_id) inner join countries as cu on (cu.country_id=co.country_id) where c.name like '%" . $_POST['search1'] . "%' AND co.status=1 AND (co.zipcode like '%" . $_POST['search2'] . "%' OR co.state like '%" . $_POST['search2'] . "%')";
                    }else{
                        header("Location:index.php");
                    }
                    $res = mysqli_query($con, $sql);
                    if ($res) {
                        if (mysqli_num_rows($res)) {
                            while ($data = mysqli_fetch_assoc($res)) {
                           
                                    ?>
                                   
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12" style="margin-bottom: 10px;">

                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    
                                                    <div class="col-md-8 col-lg-8 col-8">
                                                        <div>Business Name : <b><?php echo ucwords($data['c_name']) ?></b> </div>
                                                        <div>Website : <a href="<?php echo $data['weburl'] ?>" target="_blank" ><?php echo $data['weburl'] ?></a> </div>
                                                        <div style="text-align: justify;">Location : <?php echo $data['c_address'] ?></div>
                                                        <div>Ph : <?php echo $data['phone_number'] ?></div>
                                                        
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                               
                            }
                        } else {
                            echo "<p style='margin:0 auto;'>No Data</p>";
                        }
                    } else {
                        echo "<p style='margin:0 auto;'>No Data</p>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>
    </div>
</section>
<!-- Product filter section end -->
<div class="modal" id="review">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title" style="margin: 0 auto;">Write a Review</h5>

            </div>

            <?php
//                                    user data

            if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                $user_q = "select * from tbl_user where user_id=" . $_SESSION['user_id'];

                $user_r = mysqli_query($con, $user_q);
                $user_d = mysqli_fetch_assoc($user_r);
            }
            ?>
            <form action="rating.php" method="post" id="review">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email :</label>
                                <input type="text" class="form-control" readonly="" placeholder="Email" value="<?php echo isset($user_d['email']) ? $user_d['email'] : ''; ?>">
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($user_d['user_id']) ? $user_d['user_id'] : ''; ?>">
                                <input type="hidden" class="form-control" name="company_id" id="company_id">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name :</label>
                                <input type="text" class="form-control" placeholder="Name" name="rname" id="rname" value="<?php echo isset($user_d['fname']) ? $user_d['fname'] . ' ' . $user_d['lname'] : ''; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Review :</label>
                                <textarea class="form-control" rows="2" placeholder="Review" name="text" id="text"></textarea>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn custom_color" id="submit_company_review" name="submit_company_review" value="Submit Review">
                    <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal">
                </div>
            </form>

        </div>
    </div>
</div>
<?php
include_once './footer.php';
?>
<script>
    function company_like_dislike(uid, cid) {
        if (uid != 0) {

            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'company_like', 'user_id': uid, 'company_id': cid},
                success: function (ans) {
                    if (ans) {
                        $("#total_like_" + cid).text(ans);
                        $(".d_like_" + cid).css("color", "#e03d3d");
                        $(".d_like_" + cid).removeClass("fa-heart-o");
                        $(".d_like_" + cid).addClass("fa-heart");
                    }
                }
            });
        }
    }
    function company_star_rating(uid, cid) {
//        alert("ok");
        var radioValue = $("input[type='radio'][name='rate_" + cid + "']:checked").val();
//         alert(radioValue);
        if (radioValue) {
            // alert("You are give a - " + radioValue +" star");
            $.ajax({
                url: 'rating.php',
                type: 'POST',
                data: {'action': 'company_rating', 'user_id': uid, 'company_id': cid, 'text': radioValue},
                success: function (ans) {
                    if (ans == "success") {
                        $('#ratting_msg_' + cid).html('Successfully Given ' + radioValue + ' stars').removeClass('d-none');
                    } else if (ans == "already") {
                        $('#ratting_msg_' + cid).html('Sorry, You can not give ratting again!').removeClass('d-none');
                    } else if (ans == "own") {
                        $('#ratting_msg_' + cid).html('Sorry, You can not give ratting to your business!').removeClass('d-none');
                    } else {
                        $('#ratting_msg_' + cid).html('OOPS, Something went wrong!').removeClass('d-none');

                    }
                }
            });
        }
    }
    function write_review_on_company(uid, cid) {
        if (uid != '' && cid != '') {
            $("#review").modal("show");
            $("#company_id").val(cid);
        }
    }
</script>