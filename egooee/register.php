<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>
<div class="container" style="min-height: 300px;">
    <form action="javascript:void(0)" method="post" role="form" enctype="multipart/form-data" id="registerForm">
        <div class="modal-body" style="border:1px solid #e0dfdf;margin-top: 10px;">
            <center><h3 class="m_v17">Register</h3></center>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>User Type</label>
                        <?php
                        $user_selected = '';
                        if (isset($_GET['uid']) && $_GET['uid'] != '' && isset($_GET['cid']) && $_GET['cid'] != '') {
                            ?>
                            <select class="form-control" name="business_type_user" id="business_type_user">
                                <option value="business_user">Business User</option>
                            </select>    
                            <?php
                        } else {
                            ?>
                            <select class="form-control" name="user_type" id="user_type" onclick="user_reg();">
                                <option value="">-- Select User Type --</option>
                                <option value="business_user">Business User</option>
                                <option value="community_user">Individual(Community) User</option>
                            </select>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="d-none" id="register_div">
                <center><br><h3 id="register_form_title">Business Registration</h3> <span id="not_block" style="float:right;">(Note :  <label> Must be at least Age 18 </label>)</span><br></center>      
                <div class="row" style="clear:both;">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php
                            if (isset($_GET['uid']) && $_GET['uid'] != '' && isset($_GET['cid']) && $_GET['cid'] != '') {
                                ?>
                                <input type="hidden" class="form-control" id="new_user_id" name="business_user_id" value="<?php echo $_GET['uid'] ?>">
                                <input type="hidden" class="form-control" id="new_company_id" name="new_company_id" value="<?php echo $_GET['cid'] ?>">
                                <?php
                            } else {
                                ?>
                                <input type="hidden" class="form-control" id="new_user_id" name="business_user_id" value="">
                                <input type="hidden" class="form-control" id="new_company_id" name="new_company_id" value=""> 
                                <?php
                            }
                            ?>
                            <label for="firstname">First Name*</label>
                            <input type="text" class="form-control" aria-describedby="emailHelp" id="firstname" name="firstname" placeholder="Enter First Name" required="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="lastname">Last Name*</label>
                            <input type="text" class="form-control" aria-describedby="emailHelp" id="lastname" name="lastname" placeholder="Enter Last Name" required="">
                        </div>
                    </div>

                    <div class="col-md-4 d-none" id="blank">

                    </div>

                    <div class="col-md-1 d-none" id="gender_g1">
                        <div class="form-group" style="width: 101px;">
                            <label style="margin-bottom: 24px;"> </label>
                            <select class="form-control" name="gender" id="bgender">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>    
                        </div>
                    </div>


                    <div class="col-md-2 d-none" style="margin-left: 10px;" id="btype1">
                        <div class="form-group" style="width: 241px;">
                            <label style="margin-bottom: 24px;"> </label>
                            Business Type*
                            <select class="form-control" name="btype" id="btype">
                                <option value="0">--Select Type--</option>
                                <?php
                                $sql = "select * from tbl_category;";
                                $restype = mysqli_query($con, $sql);

                                if (mysqli_num_rows($restype) > 0) {
                                    while ($rowtype = mysqli_fetch_assoc($restype)) {
                                        ?>
                                        <option value="<?php echo $rowtype['category_id'] ?>"><?php echo $rowtype['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>

                            </select>    
                        </div>
                    </div>
                    <div class="col-md-4" id="reg_email_div">
                        <div class="form-group">
                            <label for="useremail">Email Address*</label>
                            <input type="email" class="form-control" aria-describedby="emailHelp" id="useremail" name="useremail" placeholder="Enter Email Address" required="">
                        </div>
                    </div>

                    <div class="col-md-4 d-none" id="gender_g">
                        <div class="form-group" style="width: 101px;">
                            <label style="margin-bottom: 24px;"> </label>
                            <select class="form-control" name="gender" id="gender" onclick="user_reg();">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>    
                        </div>
                    </div>

                    <div class="col-md-4" id="rb_title">
                        <div class="form-group">
                            <label for="useremail">Business Name*</label>
                            <input type="text" class="form-control" id="bs_name" name="bs_name" placeholder="Enter Business_name">
                        </div>
                    </div>
                    <div class="col-md-4 d-none" id="b_title">
                        <div class="form-group">
                            <label for="useremail">Title*</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
                        </div>
                    </div>
                    <?php
                    $eentq = "select * from setting where setting_id=9";
                    $eentr = mysqli_query($con, $eentq);
                    $eentd = mysqli_fetch_assoc($eentr);
                    ?>
                    <div class="col-md-4 d-none" id="encmt_div">
                        <div class="form-group">
                            <label>Influences* <span style="font-size:12px;" class="m_v22">(Please Select At Least <?php echo $eentd['text'] ?> Influences)</span></label>

                            <select class="form-control" name="enticements" id="enticements" multiple="multiple">

                            </select>    
                        </div>
                    </div>
                    <div class="col-md-4 d-none" id="dob_div">
                        <div class="form-group" id="c_dob">
                            <label for="dob">Date of Birth* <span style="font-size:12px;" class="m_v22">(Must be at least Age 13)</span></label>
                            <input type="date" class="form-control" aria-describedby="emailHelp" id="dob" name="dob" placeholder="Enter Date of Birth">
                        </div>

                    </div>
                    <div class="col-md-4" id="username_div">
                        <div class="form-group">
                            <label for="password">Username*</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter User Name" required="">
                        </div>
                    </div>
                    <div class="col-md-4 d-none" id="blank1">
                        <div class="form-group">
                        </div>
                    </div>

                    <div class="col-md-4" id="pass_div">
                        <div class="form-group">
                            <label for="password">Password*</label>
                            <input type="password" class="form-control" aria-describedby="emailHelp" id="password" name="password" placeholder="Enter Password" required="">
                        </div>
                    </div>
                    <div class="col-md-4" id="cnfpass_div">
                        <div class="form-group">
                            <label for="password">Confirm Password*</label>
                            <input type="password" class="form-control" aria-describedby="emailHelp" id="cnf_password" name="cnf_password" placeholder="Confirm Password" required="">
                        </div>
                    </div>
                    <div class="col-md-4 d-none" id="blank2">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class='col-md-4' id="bphone_div">
                        <div class='form-group'>
                            <label>Phone Number* :</label>
                            <input type="number" placeholder="Phone number" id="phone_number" class="form-control" name="phone_number" >
                        </div>
                    </div>
                    <div class='col-md-4' id="rstrtaddress">
                        <div class='form-group'>
                            <label>Street Address* :</label>
                            <input type="text" placeholder="Street Address" id="street_address" class="form-control" name="street_address" >
                        </div>
                    </div>
                    <?php
                    $state_sql = "select * from countries where country_id=231";
                    $state_res = mysqli_query($con, $state_sql);
                    $state_res = mysqli_fetch_assoc($state_res);
                    $state_enable = 'd-none';
                    $zipcode_enable = 'd-none';
                    $required_zipcode = 'required';
                    if ($state_res['enable_state'] == 1) {
                        $state_enable = "";
                    }
                    if ($state_res['enable_zipcode'] == 1) {
                        $zipcode_enable = "";
                    } else if ($state_res['enable_zipcode'] == 0) {
                        $required_zipcode = "";
                    }
                    ?>
                    <div class="col-md-4 <?php echo $state_enable ?>" id="state_div">
                        <div class="form-group">
                            <label for="state">State/Province</label>
                            <input type="text" class="form-control" id="state" name="state" placeholder="State/Province">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Country</label>
                            <select class="form-control" id="country1" name="country" onchange="country_zipcode()">
                                <?php
                                $country_sql = "select * from countries where status=1";
                                $country_res = mysqli_query($con, $country_sql);
                                while ($country = mysqli_fetch_assoc($country_res)) {
                                    $select = '';
                                    if ($country['country_id'] == 231) {
                                        $select = "selected";
                                    }
                                    echo "<option value='" . $country['country_id'] . "' " . $select . ">" . $country['country_name'] . "</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 <?php echo $zipcode_enable ?>" id="zip_code">
                        <div class="form-group">
                            <label for="zipcode">Zip Code*</label>
                            <input type="text" class="form-control" id="zipcode" name="zipcode" required="" placeholder="Zip Code">
                        </div>
                    </div>
                </div>
                <a href="javascript:void(0);" id="login_open" data-target="#signin" data-toggle="modal">Already have an account?</a><br/>
                <div class="alert alert-danger register_error" style="display: none;width: 100%;">Registration Failed! Please try again.</div>
                <div class="alert alert-danger register_already" style="display: none;width: 100%;">User already registered.</div>
                <div class="alert alert-danger age_error_13" style="display: none;width: 100%;">age must be at least 13 and above required!</div>
                <div class="alert alert-danger age_error_18" style="display: none;width: 100%;">User name already registered</div>

                <div class="alert alert-danger error_encmt d-none" style="width: 100%;">Please select minimum <?php echo $eentd['text'] ?> Influences!</div>
                <div class="alert alert-danger error_address d-none" style="width: 100%;">Street address required!</div>
                <div class="alert alert-danger error_state d-none" style="width: 100%;">State required!</div>
                <div class="alert alert-danger error_phone d-none" style="width: 100%;">Phone number required!</div>
                <div class="alert alert-danger error_business_type d-none" style="width: 100%;">Business type required!</div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="index.php" class="btn btn-danger pull-right" style="margin-left: 5px;">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right" id="register_submit" name="click" style="margin-left: 5px;">Submit</button>
                        <button type="button" class="btn btn-light pull-right" id="preview">Preview</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <br><br>
</div>
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <center>Preview</center>
                <div class="product-item" style="border:1px solid #dfe1e5;width: 325px;margin:0 auto;">

                    <div class="pi-pic" style="margin-top:5px;">
                        <?php
                        if (isset($_GET['cid']) && $_GET['cid'] != '') {
                            $sqla = "select * from tbl_company where company_id=" . $_GET['cid'];
                            $resa = mysqli_query($con, $sqla);
                            $dataa = mysqli_fetch_assoc($resa);
                        }
                        ?>
                        <center> <img src="<?php echo $dataa['c_logo'] ?>" style="height:210px;width:auto;" alt="" id=""></center>


                    </div>
                    <div class="pi-text" style="padding:0 15px;height: auto;">

                        <div class="rate">

                            <form class="user-rating-form">
                                <?php
                                if (isset($_GET['pid']) && $_GET['pid'] == 3) {
                                    ?>
                                    <span class="user-rating">
                                        <input type="radio" value="5"><span class="star"></span>
                                        <input type="radio" value="4"><span class="star"></span>
                                        <input type="radio" value="3"><span class="star"></span>
                                        <input type="radio" value="2"><span class="star"></span>
                                        <input type="radio" value="1"><span class="star"></span>

                                    </span>
                                    <?php
                                }

                                if (isset($_GET['pid']) && $_GET['pid'] == 2) {
                                    ?>
                                    <div class="pull-right" style="padding-top:5px;"><span>1000</span> <a href="javascript:void(0);" style="text-decoration:none;color:black;"><i class="fa fa-heart-o"></i></a></div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="pull-right" style="padding-top:5px;visibility: hidden;"><a href="javascript:void(0);"><i class="fa fa-heart"></i></a></div>
                                    <?php
                                }
                                ?>
                            </form>

                        </div>
                        <div style="clear:both;"></div>
                        <div id="demo_preview">

                        </div>
                        <?php
                        if (isset($_GET['pid']) && $_GET['pid'] == 2 || $_GET['pid'] == 3) {
                            ?>
                            <div class="p-review">

                                <a href="javascript:void(0)" style="color:#007bff;">Write a Review</a>

                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="cl1" class="btn btn-success" data-dismiss="modal">Ok</button>
            </div>

        </div>
    </div>
</div>
<?php
include_once './footer.php';
?>
<script>
    $("#preview").click(function () {
        $('#myModal').show();
        $("#demo_preview").html("<p>Business Name : <b>" + $('#bs_name').val() + "</b> </p><p style='word-break:break-word;'>Location : " + $('#street_address').val() + "</p><p>Ph : " + $('#phone_number').val() + "</p>");
    });
    $("#cl1").click(function () {
        $('#myModal').hide();
        //alert("saa");
    });
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#user_image_view').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#user_image").change(function () {
        readURL(this);
    });
</script>