<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>
<section class="product-section">

    <div class="container-fluid">

    </div>
    <div class="container-fluid">
        <?php
        require_once '10_slider.php';
//        require_once 'searchbar.php';
        ?>

    </div>
    <div class="container">

        <!--        <div class="back-link">
                    <a href="index.php"> &lt;&lt; Back to Home</a>
                </div>-->

        <?php
        $sql = "select * from tbl_ad where ad_id=" . $_GET['id'];
        $res = mysqli_query($con, $sql);
        if ($res) {
            $data = mysqli_fetch_assoc($res);
        }
        ?>
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="product-pic-zoom">
                            <img class="product-big-img" src="<?php echo $data['logo'] ?>" alt="">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 product-details">
                        <h2 class="p-title"><?php echo $data['title'] ?></h2>
                        <p>Message : <?php echo $data['message'] ?></p>
                        <p>Location : <?php echo $data['address'] ?></p>
                        <p>Ph : <?php echo $data['phone'] ?></p>

                        <div class="p-review">
                            <?php
                            $review_button = 'signin';
                            if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                $review_button = 'review1';
                            }


                            $tem_q = "SELECT COUNT(ad_review_id) AS cn FROM tbl_ad_review where ad_id='" . $_GET['id'] . "'";
                            $tem_r = mysqli_query($con, $tem_q);

                            $temp_ro = mysqli_fetch_assoc($tem_r);

                            $count1 = $temp_ro['cn'];
                            ?>

                            <a href="javascript:void(0);"><?php echo $count1; ?> reviews</a>|<a href="#" data-toggle="modal" data-target="#<?php echo $review_button ?>">Write a review</a>

                            <div class="modal" id="review1">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h5 class="modal-title" style="margin: 0 auto;">Write a review</h5>

                                        </div>

                                        <?php
//                                    user data

                                        if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                            $user_q = "select * from tbl_user where user_id=" . $_SESSION['user_id'];

                                            $user_r = mysqli_query($con, $user_q);
                                            $user_d = mysqli_fetch_assoc($user_r);
                                        }
                                        ?>
                                        <form action="#" method="post" id="review">
                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Email :</label>
                                                            <input type="text" class="form-control" readonly="" placeholder="Email" value="<?php echo isset($user_d['email']) ? $user_d['email'] : ''; ?>">
                                                            <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($user_d['user_id']) ? $user_d['user_id'] : ''; ?>">
                                                            <input type="hidden" class="form-control" name="post_id" value="<?php echo isset($data['post_id']) ? $data['post_id'] : ''; ?>">
                                                            <input type="hidden" class="form-control" name="eg_id" id="eg_id" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Name :</label>
                                                            <input type="text" class="form-control" placeholder="Name" name="rname" id="rname" value="<?php echo isset($user_d['fname']) ? $user_d['fname'] . ' ' . $user_d['lname'] : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Review :</label>
                                                            <textarea class="form-control" rows="2" placeholder="Review" name="text" id="text"></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <input type="submit" class="btn custom_color" id="submit_review" name="ad_review" value="Submit Review">
                                                <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal">
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="accordion" class="accordion-area">
                            <div class="panel">
                                <div class="panel-header" id="headingOne">
                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Reviews</button>
                                </div>
                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body">
                                        <div class="row">


                                            <?php
                                            $review_query = "select tbl_ad_review.*,tbl_user.fname,tbl_user.user_id,tbl_user.image,tbl_user.lname from tbl_ad_review INNER JOIN tbl_user on tbl_user.user_id=tbl_ad_review.user_id where tbl_ad_review.ad_id='" . $_GET['id'] . "'";
                                            $review_result = mysqli_query($con, $review_query);
                                            if (mysqli_num_rows($review_result)) {
                                                while ($reviews = mysqli_fetch_assoc($review_result)) {
                                                    ?>

                                                    <div class="col-md-6 col-sm-12" style="margin-bottom: 10px;">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">

                                                                    <?php
                                                                    $edit = 'none';
                                                                    if (isset($_SESSION['user_id']) && $_SESSION['user_id']==$reviews['user_id']) {

                                                                        $onetime_q = "SELECT * FROM tbl_ad_review where user_id='" . $_SESSION['user_id'] . "' AND ad_id='" . $_GET['id'] . "' AND already_update=0";


                                                                        $onetime_r = mysqli_query($con, $onetime_q);
                                                                        if (mysqli_num_rows($onetime_r)) {

//                                                                            $r1 = mysqli_fetch_assoc($onetime_r);

                                                                            $edit = 'block';
                                                                        }
                                                                    }
                                                                    //  echo $edit.$_SESSION['user_id'];
                                                                    ?>


                                                                    <div class="col-md-3 col-3 col-lg-3">
                                                                        <img src="<?php echo $reviews['image']; ?>" class="img-responsive" style="width:50px;">
                                                                    </div>
                                                                    <div class="col-md-9 col-lg-9 col-9">
                                                                        <div><?php echo ucfirst($reviews['fname']) . ' ' . ucfirst($reviews['lname']); ?><a href="javascript:void(0);"  class="pull-right" title="You can only one time edit your review." style="display: <?php echo $edit ?>;" onclick="edit_review(<?php echo $reviews['ad_review_id'] ?>, '<?php echo $reviews['text'] ?>', '<?php echo $reviews['username'] ?>');">Edit</a></div>
                                                                        <div style="font-size: 13px;height: 75px;overflow-y: auto;text-align: justify;overflow-x: hidden;"><?php echo ucfirst($reviews['text']); ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>
    </div>
</section>
<?php
require_once 'footer.php';
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.your_slider').slick({
            infinite: true,
            autoplay: true,
            arrows: false,
            slidesToShow: 10,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
</script>


<?php
if (isset($_POST['ad_review'])) {

    // echo "Bhargav";

    $q = "select * from tbl_ad_review where ad_id='" . $_GET['id'] . "' and user_id='" . $_SESSION['user_id'] . "'";
    $r = mysqli_query($con, $q);

    if (mysqli_num_rows($r) < 1) {

//            $q = "select * from user_engagement where post_id='" . $_POST['post_id'] . "' and user_id='" . $_POST['user_id'] . "' and type=3";
//            $r = mysqli_query($con, $q);
//
//            if (mysqli_num_rows($r) < 1) {

        $sql = "INSERT INTO tbl_ad_review SET already_update='0', ad_id='" . $_GET['id'] . "',user_id='" . $_SESSION['user_id'] . "',text='" . $_POST['text'] . "',username='" . $_POST['rname'] . "', added_on='" . date('Y-m-d') . "',modify_on='" . date('Y-m-d') . "',status=1";

        $res = mysqli_query($con, $sql);


        if ($res) {
            echo 'success';
        } else {
            echo "Error";
        }
    } else {



        $sql = "UPDATE tbl_ad_review SET text='" . $_POST['text'] . "',username='" . $_POST['rname'] . "',already_update=1, modify_on='" . date('Y-m-d') . "' where ad_review_id='" . $_POST['eg_id'] . "'";
        $res = mysqli_query($con, $sql);


        echo $sql;
    }

    header("Location:admin_ad.php?id=" . $_GET['id']);
}
?>
<script type="text/javascript">

    function edit_review(rid, rtext, rname) {

        $("#eg_id").val(rid);
        $("#text").val(rtext);
        $("#rname").val(rname);
        $("#review1").modal("show");
    }
</script>