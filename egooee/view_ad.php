<?php
require_once 'header.php';
require_once 'new_header.php';
require_once 'navbar.php';
?>
<section class="product-section">
    <div class="container-fluid">
        <?php
        require_once '10_slider.php';
        ?>
    </div>
    <div class="container">
        <?php
        $sql = "select * from tbl_post where post_id=" . $_GET['id'];
        $res = mysqli_query($con, $sql);
        if ($res) {
            $data = mysqli_fetch_assoc($res);
        }
        ?>
        <div class="row">
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="product-pic-zoom">
                                    <img class="product-big-img" src="<?php echo $data['image'] ?>" alt="">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 product-details">
                                <h2 class="p-title"><?php echo $data['b_name'] ?></h2>
                                <p>Description : <?php echo $data['b_message'] ?></p>
                                <p>Address : <?php echo $data['b_address'] ?></p>
                                <p>Ph : <?php echo $data['b_phone'] ?></p>
                                <p>State : <?php echo $data['state'] ?></p>
                                <p>Zipcode : <?php echo $data['zipcode'] ?></p>
                                <?php
                                $sql1 = "select * from user_engagement where type=3 and post_id=" . $_GET['id'];
                                $res1 = mysqli_query($con, $sql1);
                                if ($total_review = mysqli_num_rows($res1)) {
                                    
                                }
                                ?>
                                <div class="p-review">
                                    <?php
                                    $review_button = 'signin';
                                    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                                        $review_button = 'review';
                                    }
                                    ?>

                                    <a href="javascript:void(0);"><?php echo $total_review; ?> reviews</a>|<a href="#" data-toggle="modal" data-target="#<?php echo $review_button; ?>">Write a review</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="accordion" class="accordion-area">
                            <div class="panel">
                                <div class="panel-header" id="headingOne">
                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Reviews</button>
                                </div>
                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body">
                                        <div class="row">
                                            <?php
                                            if (isset($data['post_id'])) {
                                                $review_query = "select e.*,u.image,u.user_id from user_engagement as e inner join tbl_user as u on (e.user_id=u.user_id) where e.type=3 and e.post_id='" . $data['post_id'] . "'";

                                                $review_result = mysqli_query($con, $review_query);
                                                if (mysqli_num_rows($review_result)) {
                                                    while ($reviews = mysqli_fetch_assoc($review_result)) {
                                                        ?>

                                                        <div class="col-md-6 col-sm-12" style="margin-bottom: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">

                                                                        <?php
                                                                        $edit = 'none';
                                                                        if (isset($_SESSION['user_id'])) {

                                                                            $onetime_q = "select * from user_engagement where user_id='" . $_SESSION['user_id'] . "' AND user_engagement_id='" . $reviews['user_engagement_id'] . "' AND already_updated=0";

                                                                            $onetime_r = mysqli_query($con, $onetime_q);
                                                                            if (mysqli_num_rows($onetime_r)) {
                                                                                $edit = 'block';
                                                                            }
                                                                        }
                                                                        ?>



                                                                        <div class="col-md-3 col-3 col-lg-3">
                                                                            <img src="<?php echo (isset($reviews['image']) && $reviews['image'] != '') ? $reviews['image'] : 'img/default-avatar.png'; ?>" class="img-responsive" style="width:50px;">
                                                                        </div>
                                                                        <div class="col-md-9 col-lg-9 col-9">
                                                                            <div><?php echo ucfirst($reviews['username']); ?><a href="javascript:void(0);"  class="pull-right" title="You can only one time edit your review." style="display: <?php echo $edit ?>;" onclick="edit_review(<?php echo $reviews['user_engagement_id'] ?>, '<?php echo $reviews['text'] ?>', '<?php echo $reviews['username'] ?>');">Edit</a></div>
                                                                            <div style="font-size: 13px;height: 75px;overflow-y: auto;text-align: justify;overflow-x: hidden;"><?php echo ucfirst($reviews['text']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <?php
                require_once 'right_side_bar_ad.php';
                ?>
            </div>
        </div>

        <div class="modal" id="review">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h5 class="modal-title" style="margin: 0 auto;">Write a review</h5>

                    </div>

                    <?php
//                                    user data

                    if (isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') {
                        $user_q = "select * from tbl_user where user_id=" . $_SESSION['user_id'];

                        $user_r = mysqli_query($con, $user_q);
                        $user_d = mysqli_fetch_assoc($user_r);
                    }
                    ?>
                    <form action="rating.php" method="post" id="review">
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email :</label>
                                        <input type="text" class="form-control" readonly="" placeholder="Email" value="<?php echo isset($user_d['email']) ? $user_d['email'] : ''; ?>">
                                        <input type="hidden" class="form-control" name="user_id" value="<?php echo isset($user_d['user_id']) ? $user_d['user_id'] : ''; ?>">
                                        <input type="hidden" class="form-control" name="post_id" value="<?php echo isset($data['post_id']) ? $data['post_id'] : ''; ?>">
                                        <input type="hidden" class="form-control" name="eg_id" id="eg_id" value="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name :</label>
                                        <input type="text" class="form-control" placeholder="Name" name="rname" id="rname" value="<?php echo isset($user_d['fname']) ? $user_d['fname'] . ' ' . $user_d['lname'] : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Review :</label>
                                        <textarea class="form-control" rows="2" placeholder="Review" name="text" id="text"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="submit" class="btn custom_color" id="submit_review" name="submit_review" value="Submit Review">
                            <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal">
                        </div>
                    </form>

                </div>
            </div>
        </div>


    </div>
</section>
<?php
require_once 'footer.php';
?>
<script type="text/javascript">

    function edit_review(rid, rtext, rname) {

        $("#eg_id").val(rid);
        $("#text").val(rtext);
        $("#rname").val(rname);
        $("#review").modal("show");
    }
</script>