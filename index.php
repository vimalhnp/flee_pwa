<?php
include_once './header.php';
?>
<style>
    /*jssor slider loading skin spin css*/
    .jssorl-009-spin img {
        animation-name: jssorl-009-spin;
        animation-duration: 1.6s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    @keyframes jssorl-009-spin {
        from { transform: rotate(0deg); }
        to { transform: rotate(360deg); }
    }

    /*jssor slider bullet skin 032 css*/
    .jssorb032 {position:absolute;}
    .jssorb032 .i {position:absolute;cursor:pointer;}
    .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
    .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
    .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
    .jssorb032 .i.idn {opacity:.3;}

    /*jssor slider arrow skin 051 css*/
    .jssora051 {display:block;position:absolute;cursor:pointer;}
    .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
    .jssora051:hover {opacity:.8;}
    .jssora051.jssora051dn {opacity:.5;}
    .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
</style>
<div class = "containt">
    <div class = "header_btn">
        <a href = "#"><input type = "button" class = "btnn form-control col-md-2 btn head_btn2 termsbutton" value = "about"></a>
        <?php
        if (isset($_SESSION['useremail']) && $_SESSION['useremail'] != '') {
            ?>
            <a href="logout.php"><input type="button" class="btnn form-control col-md-2 btn head_btn3" value="Log Out"></a>
            <?php
        } else {
            ?>
            <button type="button" class="btn btn-primary signin" data-toggle="modal" data-target="#signin">
                SIGN IN
            </button>

            <!-- The Modal -->
            <div class="modal" id="signin">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h5 class="modal-title" style="margin: 0 auto;">Sign in</h5>

                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <form action="index.php" method="post" role="form">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">User Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email Address" id="useremail" name="useremail">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" id="password" name="password">
                                </div>
                                <a href="#">Forget Password?</a><br>
                                <a href="javascript:void(0);" id="register_open" data-target="#abc" data-toggle="modal">Register</a>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" id="login-submit" name="loginnew" value="Submit">
                            <button type="button" class="btn btn-danger" id="login_close" data-dismiss="modal">Cancel</button>
                        </div>
                        </form>

                    </div>
                </div>
            </div>

            <?php
        }
        ?>
        </a>
    </div>
    <br>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
            <div>
                <img data-u="image" src="assets/image/1300x500/003.jpg" />
                <div style="width: 100%;background-color: rgba(255, 255, 255, 0.5);height: 100%;" class="pt-5">

                    <div class="row justify-content-center">
                        <div class="third_party_ads">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="site_name d-block" style="width: 210px;"><h1>Company 1</h1></div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                    </div>
                    <div class="row justify-content-center mt-5">
                        <input type="search" aria-label="search" class="bttnn form-control col-lg-6 col-lg-offset-3 search_field_btn1" placeholder="Search field ">
                    </div>
                    <div class="row justify-content-center mt-5">
                        <a href="#"><input type="button" class="btn btn-primary btn-border" value="btn"></a>&nbsp;
                        <a href="#"><input type="button" class="btn btn-primary btn-border" value="btn"></a>
                    </div>

                </div>

            </div>
            <div>

                <img data-u="image" src="assets/image/1300x500/001.jpg" />
                <div style="width: 100%;background-color: rgba(255, 255, 255, 0.5);height: 100%;" class="pt-5">

                    <div class="row justify-content-center">
                        <div class="third_party_ads">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="site_name d-block" style="width: 210px;"><h1>Company 2</h1></div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                    </div>
                    <div class="row justify-content-center mt-5">
                        <input type="search" aria-label="search" class="bttnn form-control col-lg-6 col-lg-offset-3 search_field_btn1" placeholder="Search field ">
                    </div>
                    <div class="row justify-content-center mt-5">
                        <a href="#"><input type="button" class="btn btn-primary btn-border" value="btn"></a>&nbsp;
                        <a href="#"><input type="button" class="btn btn-primary btn-border" value="btn"></a>
                    </div>

                </div>
            </div>
            <div>

                <img data-u="image" src="assets/image/1300x500/002.jpg" />
                <div style="width: 100%;background-color: rgba(255, 255, 255, 0.5);height: 100%;" class="pt-5">

                    <div class="row justify-content-center">
                        <div class="third_party_ads">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="site_name d-block" style="width: 210px;"><h1>Company 3</h1></div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                        <div class="third_party_ads ">ADS</div>
                    </div>
                    <div class="row justify-content-center mt-5">
                        <input type="search" aria-label="search" class="bttnn form-control col-lg-6 col-lg-offset-3 search_field_btn1" placeholder="Search field ">
                    </div>
                    <div class="row justify-content-center mt-5">
                        <a href="#"><input type="button" class="btn btn-primary btn-border" value="btn"></a>&nbsp;
                        <a href="#"><input type="button" class="btn btn-primary btn-border" value="btn"></a>
                    </div>

                </div>
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    <center>
        <div class="row col-md-12" style="left:140px;top: 150px;">
            <div class="third_party_ads col-md-1">ADS</div>
            <div class="third_party_ads col-md-1">ADS</div>
            <div class="third_party_ads col-md-1">ADS</div>
            <div class="site_name col-md-3"><h1>SITE NAME</h1></div>
            <div class="third_party_ads col-md-1">ADS</div>
            <div class="third_party_ads col-md-1">ADS</div>
            <div class="third_party_ads col-md-1">ADS</div>
        </div>
    </center>
    <div class="search_field">
        <center>
            <input type="search" aria-label="search" class="bttnn form-control col-lg-8 search_field_btn1" placeholder="Search field ">
        </center>
        <a href="#"><input type="button" class="btnnn form-control col-md-2 btn search_field_btn2 termsbutton" value="btn"></a>
        <a href="#"><input type="button" class="btnnn form-control col-md-2 btn search_field_btn3 termsbutton" value="btn"></a>

    </div>
    <div class="header_btn">
        <a href="#"><input type="button" class="form-control col-lg-2 col-md-3 btn head_btncopy termsbutton" value="Terms"></a>
        <a href="#"><input type="button" class="form-control col-lg-2 col-md-3 btn head_btncopy termsbutton" value="Privacy"></a>
        <a href="product.php"><input type="button" class="form-control col-lg-2 col-md-3 btn head_btn3 termsbutton" value="Post an Ad"></a>
        <a href="sitexx.php"><input type="button" class="form-control col-lg-2 col-md-3 btn head_btn3copy termsbutton" value="siteXX"></a>
    </div>

</div>
<div class="modal" id="abc">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title" style="margin: 0 auto;">Sign in</h5>

            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="index.php" method="post" role="form">
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" id="firstname" name="firstname" placeholder="Enter First Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" id="lastname" name="lastname" placeholder="Enter Last Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" id="useremail" name="useremail" placeholder="Enter Email Address" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" id="password" name="password" placeholder="Enter Password" required>
                    </div>

                    <a href="javascript:void(0);" id="login_open" data-target="#signin" data-toggle="modal">Already Account?</a>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="register-submit" name="click">Submit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form> 

        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var jssor_1_options = {
            $AutoPlay: 0,
            $SlideDuration: 800,
            $SlideEasing: $Jease$.$OutQuint,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/

        var MAX_WIDTH = 3000;

        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        /*#endregion responsive code end*/
    });
</script>
<?php
include_once './footer.php';
?>