<?php
include './header.php';
include './productphp.php';
?>
<div class="product">
    <div class="backgrounds">
        <div class="heading">
            <div class="head_row">
            </div>
            <section class="top-letest-product-section product-filter-section">
                <div class="container-fluid">
                    <div class="section-title">
                        <h2>BROWSE TOP POSTS</h2>
                    </div>
                    <ul class="product-filter-menu">
                        <li><a href="#">Top Talks</a></li>
                        <li><a href="#">Home Services</a></li>
                        <li><a href="#">Night life</a></li>
                        <li><a href="#">Restaurants</a></li>
                        <li><a href="#">Bakery</a></li>
                        <li><a href="#">Coming Events</a></li>
                        <li><a href="#">Classified</a></li>
                    </ul>
                    <div>
                        <a href="#"><button class="success-badge form-control btn col-md-3 termsbutton">Restaurants</button></a>
                        <a href="#"><button class="success-badge form-control btn col-md-3 termsbutton">What’s Popping </button></a>
                        <a href="addpost.php"><button class="success-badge form-control btn col-md-3 termsbutton">Post your Business or Ad</button></a>
                        <a href="#"><button class="success-badge form-control btn col-md-2 termsbutton">What’s Popping </button></a>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-lg-10 col-sm-12">
                            <div class="row">

                                <?php echo $product; ?>

                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-12">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <div class="product-item">
                                        <div class="pi-pic">
                                            <img src="./img/product/2.jpg" alt="">
                                            <div class="pi-links">
                                                <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                                                <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="pi-text">
                                            <h6>$35,00</h6>
                                            <p>Flamboyant Pink Top </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-sm-12">
                                    <div class="product-item">
                                        <div class="pi-pic">
                                            <img src="./img/product/3.jpg" alt="">
                                            <div class="pi-links">
                                                <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                                                <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="pi-text">
                                            <h6>$35,00</h6>
                                            <p>Flamboyant Pink Top </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-sm-12">
                                    <div class="product-item">
                                        <div class="pi-pic">
                                            <img src="./img/product/4.jpg" alt="">
                                            <div class="pi-links">
                                                <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                                                <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="pi-text">
                                            <h6>$35,00</h6>
                                            <p>Flamboyant Pink Top </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="text-center pt-5">
                        <button class="site-btn sb-line sb-dark">LOAD MORE</button>
                    </div>
                </div>
            </section>
            <!-- Banner section -->
            <section class="banner-section">
                <div class="container">
                    <div class="banner set-bg" data-setbg="img/banner-bg.jpg">
                        <div class="tag-new">NEW</div>
                        <span>New Arrivals</span>
                        <h2>STRIPED SHIRTS</h2>
                        <a href="#" class="site-btn">SHOP NOW</a>
                    </div>
                </div>
            </section>
            <!-- Banner section end  -->

        </div>
        <div style="height:100px;clear: both;"></div>
        <?php
        include_once './footer.php';
        ?>