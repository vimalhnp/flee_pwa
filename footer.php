
<footer>
</footer>
<script src="slick/slick.js" type="text/javascript"></script>
<script src="slick/slick.min.js" type="text/javascript"></script>
<script>
    $("#register_open").click(function () {
        $('#login_close').click();

    });
    $("#login_open").click(function () {
        $('#abc').click();

    });
    var slide_wrp = ".side-menu-wrapper"; //Menu Wrapper
    var open_button = ".menu-open"; //Menu Open Button
    var close_button = ".menu-close"; //Menu Close Button
    var overlay = ".menu-overlay"; //Overlay

//Initial menu position
    $(slide_wrp).hide().css({"right": -$(slide_wrp).outerWidth() + 'px'}).delay(50).queue(function () {
        $(slide_wrp).show()
    });

    $(open_button).click(function (e) { //On menu open button click
        e.preventDefault();
        $(slide_wrp).css({"left": "0px"}); //move menu right position to 0
        setTimeout(function () {
            $(slide_wrp).addClass('active'); //add active class
        }, 50);
        $(overlay).css({"opacity": "1", "width": "100%"});
    });

    $(close_button).click(function (e) { //on menu close button click
        e.preventDefault();
        $(slide_wrp).css({"left": -$(slide_wrp).outerWidth() + 'px'}); //hide menu by setting right position 
        setTimeout(function () {
            $(slide_wrp).removeClass('active'); // remove active class
        }, 50);
        $(overlay).css({"opacity": "0", "width": "0"});
    });

    $(document).on('click', function (e) { //Hide menu when clicked outside menu area
        if (!e.target.closest(slide_wrp) && $(slide_wrp).hasClass("active")) { // check menu condition
            $(slide_wrp).css({"left": -$(slide_wrp).outerWidth() + 'px'}).removeClass('active');
            $(overlay).css({"opacity": "0", "width": "0"});
        }
    });
    $('.responsive').slick({
        autoplay: true,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 10,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 8,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
</script>
</body>
</main>
</html>