<?php
include_once 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>EGOOEE</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="assets/js/popper.min.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/jssor.slider.min.js" type="text/javascript"></script>
        <link href="admin/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/pwa.css" rel="stylesheet" type="text/css"/>
        <link href="slick/slick-theme.css" rel="stylesheet" type="text/css"/>
        <link href="slick/slick.css" rel="stylesheet" type="text/css"/>
    </head>
    <main>
        <body style="overflow-x: hidden;">