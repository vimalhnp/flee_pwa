-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2019 at 07:37 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `ads_id` int(11) NOT NULL,
  `ads_name` varchar(255) NOT NULL,
  `ads_details` varchar(255) NOT NULL,
  `ads_img` text NOT NULL,
  `status` varchar(2) NOT NULL,
  `added_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`ads_id`, `ads_name`, `ads_details`, `ads_img`, `status`, `added_on`, `modified_on`) VALUES
(1, 'xyz', 'details details details details details details details details details details details details details details details details details details details details details details \r\n\r\n', 'image/shoes.jpg', '1', '2019-05-03 10:57:25', '2019-05-04 12:01:07'),
(2, 'xyz', 'details details details details details details details details details details details details details details details details details details details details details details \r\n\r\ndetails details details details details details details details details det', 'image/cycles.jpg', '01', '2019-05-03 11:02:42', '2019-05-03 02:48:29'),
(3, 'abc', '123abc', 'image/apple-na-thin-and-light-laptop-original-imaf9ph3tusztfbc.jpeg', '01', '2019-05-03 11:02:42', '2019-05-03 02:51:04'),
(4, '123', '123abc\r\n', 'image/bluetooth.jpeg', '01', '2019-05-03 11:02:42', '2019-05-03 02:51:15'),
(5, 'fenil', 'demo', 'image/shoes.jpg', '01', '2019-05-03 11:07:24', '2019-05-03 11:07:24'),
(6, 'fenil', 'demo', 'image/shoes.jpg', '01', '2019-05-03 11:12:23', '2019-05-03 11:12:23'),
(7, 'fenil', 'dzfdsfadf', 'image/79795579-creative-fashion-logo-design-vector-sign-with-lettering-and-hanger-symbol-logotype-calligraphy.jpg', '01', '2019-05-03 11:13:37', '2019-05-03 11:13:37'),
(8, 'syscon', 'dfddsnmf,sdbnfm,dsb,m ', 'image/cycles.jpg', '01', '2019-05-03 12:29:16', '2019-05-03 12:29:16'),
(9, 'sdsada', 'bdfbdsdadd', 'image/apple-na-thin-and-light-laptop-original-imaf9ph3tusztfbc.jpeg', '1', '2019-05-03 12:30:34', '2019-05-03 04:08:06'),
(10, 'syscon', 'sma,dn,mans', 'image/', '1', '2019-05-03 02:20:05', '2019-05-03 02:20:05'),
(11, 'abc', 'abc', 'image/71077313-mobile-store-logo-template-with-orange-phone-on-white-background-can-used-for-mobile-shop-phone-serv.jpg', '01', '2019-05-03 02:47:57', '2019-05-03 02:47:57'),
(12, 'abc', '123vc c', 'image/cycles.jpg', '01', '2019-05-03 02:50:52', '2019-05-03 04:05:45'),
(13, 'abc', 'sdsfdsf', 'image/bluetooth.jpeg', '1', '2019-05-03 03:04:29', '2019-05-03 03:04:29'),
(14, 'iphone', 'jhvash', 'image/149044.jpg', '1', '2019-05-03 04:06:00', '2019-05-03 04:06:00'),
(15, 'iphone', 'dasda', 'image/149044.jpg', '01', '2019-05-03 04:08:21', '2019-05-03 04:08:21'),
(16, 'abc', 'dadada', 'image/bluetooth.jpeg', '01', '2019-05-03 04:09:29', '2019-05-03 04:09:29'),
(17, 'abc', 'demo', 'image/71077313-mobile-store-logo-template-with-orange-phone-on-white-background-can-used-for-mobile-shop-phone-serv.jpg', '1', '2019-05-04 06:59:44', '2019-05-04 06:59:44'),
(18, 'dasd', 'sad', 'image/1db8cb83-ffd8-413e-9e03-2b126962570a.jpg', '1', '2019-05-07 02:53:49', '2019-05-07 02:53:49'),
(19, 'dasd', '', 'image/1db8cb83-ffd8-413e-9e03-2b126962570a.jpg', '1', '2019-05-07 02:55:01', '2019-05-07 02:55:01'),
(20, 'aaa', 'aaa', 'image/modern-sports-logo-template-with-flat-design_23-2147946074.jpg', '1', '2019-05-07 03:07:02', '2019-05-07 03:07:02'),
(21, 'i phone x', 'asdad', 'image/1db8cb83-ffd8-413e-9e03-2b126962570a.jpg', '1', '2019-05-07 06:49:48', '2019-05-07 06:49:48'),
(22, 'i phone x', 'dasda', 'image/shoes.jpg', '1', '2019-05-07 06:51:02', '2019-05-07 06:51:02'),
(23, 'sjda', '', 'image/1db8cb83-ffd8-413e-9e03-2b126962570a.jpg', '1', '2019-05-08 06:11:52', '2019-05-08 06:11:52'),
(24, 'sanjay', 'chavda', 'image/maroon-cotton-silk-hand-pleated-kurta-2662-700x700.jpg', '1', '2019-05-08 06:16:20', '2019-05-08 06:16:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_Id` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `useremail` varchar(100) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(16) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_Id`, `firstname`, `lastname`, `useremail`, `name`, `password`, `status`) VALUES
(1, 'fenil', 'patel', 'fenil.patel.6886@gmail.com', 'fenil patel', 'fenil', 1),
(2, 'chaitali', 'patel', 'fenil@gmail.com', 'chaitali patel', 'cppatel', 1),
(3, 'jugal', 'faswala', 'jugalfaswala@gmail.com', 'Mrcrazyboy', '7177', 1),
(4, 'abcd', 'efgh', 'fenilfenil@gmail.com', 'fenil pateladf', 'fenilasdsadasdas', 1),
(5, 'abcd', 'efgh', 'fenil.patel.6886@gmail.com', 'fenil pateldfsdfsdsdfds', 'fenilsdfsd', 1),
(6, 'abcd', 'efgh', 'fenil.patel.6886@gmail.com', 'fenil pateldfsdfsdsdfds', 'fenilsdfsd', 1),
(7, 'abcd', 'efgh', 'fenil.patel.6886@gmail.com', 'fenil pateldfsdfsdsdfds', 'fenilsdfsd', 1),
(8, 'abcd', 'efgh', 'fenil.patel.6886@gmail.com', 'fenil pateldfsdfsdsdfds', 'fenilsdfsd', 1),
(9, 'abcd', 'efgh', 'fenilfenil@gmail.com', 'fenil patelsdsd', 'fenil', 1),
(10, 'abcd', 'efgh', 'fenilfenil@gmail.com', 'fenil patelsdsd', 'fenil', 1),
(11, 'abcd', 'efgh', 'fenilfenil@gmail.com', 'fenil patelsdsd', 'fenil', 1),
(12, 'abcd', 'efgh', 'fenilfenil@gmail.com', 'fenil patelsdsd', 'fenil', 1),
(13, 'abcd', 'efgh', 'fenilfenil@gmail.com', 'fenil patelsdsd', 'fenil', 1),
(14, 'abcd', 'sdzfdsf', 'fenilfenisdfdsfsdfdl@gmail.com', 'fenil pateldsfdsfd', 'fenildsfdsf', 1),
(15, 'abcd', 'sdzfdsf', 'fenilfenisdfdsfsdfdl@gmail.com', 'fenil pateldsfdsfd', 'fenildsfdsf', 1),
(16, 'abcd', 'sdzfdsf', 'fenilfenisdfdsfsdfdl@gmail.com', 'fenil pateldsfdsfd', 'fenildsfdsf', 1),
(17, 'abcd', 'sdzfdsf', 'fenilfenisdfdsfsdfdl@gmail.com', 'fenil pateldsfdsfd', 'fenildsfdsf', 1),
(18, 'abcd', 'sdzfdsf', 'fenilfenisdfdsfsdfdl@gmail.com', 'fenil pateldsfdsfd', 'fenildsfdsf', 1),
(19, 'abcd', 'sdzfdsf', 'fenil.patel.6886@gmail.com', 'fenil patel', 'fenil', 1),
(20, 'abcd', 'sdzfdsf', 'fenil.patel.6886@gmail.com', 'fenil patel', 'fenil', 1),
(21, 'abcd', 'sdzfdsf', 'fenil.patel.6886@gmail.com', 'fenil patel', 'fenil', 1),
(22, 'abcd', 'efgh', 'safsdnfjbdsfk@gmail.czxvb', 'fenil patel', 'fenil', 1),
(23, 'fenil', 'efgh', 'fenilfenil@gmail.com', 'jugal6', 'fenil', 1),
(24, 'abcd', 'efgh', 'fenilfenil@gmail.com', '123', 'fenil', 1),
(25, 'jugal', 'faswala', 'jugal@gmail.com', 'jugalfaswala', '123456', 1),
(26, 'abcd', 'efgh', 'jugal@gmail.com', 'a', '123', 1),
(27, 'abcd', 'efgh', 'sada123@gmail.com', 'asdas', '123', 1),
(28, '123', '56', 'abc@gmail.com', '123', '123', 1),
(29, '123', '456', '1234@gmail.com', '1234', '123456', 1),
(30, '123', '456', '1234@gmail.com', '1234', '123456', 1),
(31, '123', '56', '1234@gmail.com', '123', '123', 1),
(32, 'alan', 'walker', 'alanwalker@gmail.com', 'alanwalker', 'alanwalker', 1),
(33, '', '', '', '', '', 1),
(34, '', '', '', '', '', 1),
(35, '123', '456', '123456@gmail.com', '123456', '123456', 1),
(36, 'john', 'Patel', 'johnpatel@gmail.com', '', 'johnpatel', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`ads_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `ads_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
